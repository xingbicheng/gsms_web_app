package com.myqm.service.xxb.meeting.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.meeting.GoodsDao;
import com.myqm.pojo.xxb.meeting.Goods;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import com.myqm.service.xxb.meeting.GoodsService;


@Service
public class GoodsServiceImpl implements GoodsService{

	@Autowired
	public GoodsDao goodsDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if (goodsDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(Goods record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (goodsDao.save(record) > 0) 
		{			 
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public Goods getById(String id) {
		return goodsDao.getById(id);
	}


	@Override
	public String editById(Goods record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (goodsDao.editById(record) > 0)
			return record.getId();
		else
			return null; 
	}

	@Override
	public PageTableData<Goods> list(int pagenum, int pagesize,MeetingQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Goods> datalist = goodsDao.list(param);
		PageTableData<Goods> resultPage = new PageTableData<Goods>(datalist);
		PageInfo<Goods> p = new PageInfo<Goods>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<Goods> listAll(MeetingQuery param) {
		List<Goods> datalist = goodsDao.list(param);
		return datalist;
	}

}
