package com.myqm.service.xxb.meeting;

import com.myqm.pojo.xxb.meeting.Goods;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface GoodsService {
   /**
    *根据id删除Goods
    */
	String delById(String id);

   /**
    *根据新增Goods，id自增
    */
    String save(Goods record);

   /**
    *根据id查询Goods
    */
    Goods getById(String id);
    
   /**
    *根据id更新Goods
    */ 
    String editById(Goods record);

   /**
    *分页查询Goods
    */ 
    PageTableData<Goods> list(int pagenum, int pagesize,MeetingQuery param);

	/**
    *查询所有Goods
    */ 
    List<Goods> listAll(MeetingQuery param);
}