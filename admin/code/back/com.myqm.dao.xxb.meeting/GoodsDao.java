package com.myqm.dao.xxb.meeting;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.meeting.GoodsQuery;
import com.myqm.pojo.xxb.meeting.Goods;

@Mapper
public interface GoodsDao {
	
	/**
    *根据新增Goods,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(Goods record);
    
    
   /**
	*根据id删除Goods
   */
 	//@Delete("delete from goods where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询Goods
     */
    Goods getById(String id);

    /**
     *根据id更新Goods
     */ 
    int editById(Goods record);

    /**
     *分页查询所有Goods
     */    
    List<Goods> list(GoodsQuery param); 
        
}