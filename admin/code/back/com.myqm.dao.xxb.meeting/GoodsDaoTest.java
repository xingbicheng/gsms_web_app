package com.myqm.dao.xxb.meeting;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.xxb.meeting.GoodsQuery;
import com.myqm.pojo.xxb.meeting.Goods;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.xxb.meeting.GoodsDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class GoodsDaoTest {
 	@Autowired
   	GoodsDao testDao;
   
	@Test
	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		GoodsQuery param = new GoodsQuery();
		List<Goods> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(Goods  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		Goods testObj=new Goods();
		testDao.save(testObj);
	}
	 
	@Test
	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		Goods testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		Goods testObj = testDao.getById("4");
		testDao.editById(testObj);
	} 
}