package com.myqm.controller.xxb.meeting;


import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord; 
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.xxb.meeting.Goods;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import com.myqm.service.xxb.meeting.GoodsService;


@Api(tags = "Goods")
@RestController
@Validated
@RequestMapping("/api/goods")
public class GoodsController extends BaseController {

	@Autowired
	public GoodsService goodsService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
    	@ApiParam(required = true, value = "添加Goods")
    	@Valid @RequestBody Goods record) {
        return super.dbOperateResult(goodsService.save(record));
    }
    
    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
    	@ApiParam(required=true,value="修改Goods")
		@Valid @RequestBody Goods record) { 
        return super.dbOperateResult(goodsService.editById(record));
 
    } 
    
	@ApiOperation(value="删除记录")
	@GetMapping(value="/del")
	public ResultResponse<String> delById(
		@ApiParam(required=true,value="查询编号")
        @RequestParam("id") @NotNull(message = "id不能为空") String id) { 
        return super.dbOperateResult(goodsService.delById(id));
    } 

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<Goods> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        Goods record=goodsService.getById(id);
        return new ResultResponse<Goods>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<Goods>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid MeetingQuery param) {
        return new ResultResponse<PageTableData<Goods>>(
            goodsService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<Goods>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  MeetingQuery param) {
        return new ResultResponse<List<Goods>>(
            goodsService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid MeetingQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <Goods> listData= goodsService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "Goods查询数据", 
        	"Goods查询数据"), Goods.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=Goods查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid MeetingQuery param, HttpServletResponse response) 
    	throws Exception {
        List<Goods> listData= goodsService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "Goods查询数据", "Goods查询数据"), Goods.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=Goods查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
