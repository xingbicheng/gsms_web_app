import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import {
  Form,
  Input,
  Row,
  Col,
  DatePicker,
  Button,
  Card,
  InputNumber,
} from 'antd';
import styles from './style.less';
import AdvancedSelect from '@/components/AdvancedSelect';
import { BLANK_PRICE_DATA, GOODS_DETAIL_STATUS} from '../../utils/Enum'

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

@connect(({ goodsModal, basicdata, loading }) => ({
goodsModal,
  basicdata,
  submitting : loading.effects['goodsModal/edit'],
}))
@Form.create()
class GoodsDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentId : '',
    };
  }

  componentDidMount() {
    const { dispatch, selectItem, goodsDetailStatus } = this.props;
    switch (goodsDetailStatus) {
      case GOODS_DETAIL_STATUS.EDIT:
        dispatch({
          type : 'goodsDetail/get',
          payload : { id : selectItem },
        });
        break;
      case GOODS_DETAIL_STATUS.ADD:
        //  do something
        break;
      default:
        break;
    }
    this.setState({
      currentId : selectItem,
    });
  }

  handleSubmit = e => {
    const { dispatch, form, goodsDetailStatus } = this.props;
    const { currentId } = this.state;
    e.preventDefault();

    switch (goodsDetailStatus) {
      case GOODS_DETAIL_STATUS.EDIT:
        form.validateFieldsAndScroll((err, values) => {
          values={...values,goodsId:currentId};
          dispatch({
            type : 'goodsModal/edit',
            payload : values,
          });
        });
        break;
      case GOODS_DETAIL_STATUS.ADD:
        //  do something
        form.validateFieldsAndScroll((err, values) => {
          dispatch({
            type : 'goodsModal/add',
            payload : values,
          });
        });
        break;
      default:
        break;
    }


  };
  handleProviderChange = (value) => {
    console.log(value);
  };



  render() {
    const { goodsModal, basicdata ,onClose, submitting, goodsDetailStatus} = this.props;
    const {
      form : { getFieldDecorator, getFieldValue },
    } = this.props;

    const formItemLayout = {
      labelCol : {
        xs : { span : 24 },
        sm : { span : 5 },
        md : { span : 7 },
      },
      wrapperCol : {
        xs : { span : 24 },
        sm : { span : 19 },
        md : { span : 15 },
      },
    };

    const submitFormLayout = {
      wrapperCol : {
        xs : { span : 24, offset : 0 },
        sm : { span : 10, offset : 7 },
      },
    };
    let { detail } = goodsModal;
    const { providerData } = basicdata;
    const editDisable = true&&goodsDetailStatus === GOODS_DETAIL_STATUS.EDIT;
    detail = !editDisable? {}:detail;

    return (
      <Card bordered={false}>
          <Form  hideRequiredMark style={{ marginTop : 8 }}>
              <Row type="flex" justify="center">
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="商品名">
                      {getFieldDecorator('goodsName', {
                      initialValue : detail.goodsName || '',
                      rules : [
                      {
                      required : true,
                      message : formatMessage({ id : 'validation.title.required' }),
                      },
                      ],
                      })(<Input disabled={editDisable}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="商品类型">
                      {getFieldDecorator('goodsType', {
                      initialValue : detail.goodsType || '',
                      })(<Input disabled={editDisable}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="商品规格">
                      {getFieldDecorator('specifications', {
                      initialValue : detail.specifications || '',
                      })(<Input disabled={editDisable}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="单位">
                      {getFieldDecorator('unitName', {
                      initialValue : detail.unitName || '',
                      })(<Input disabled={editDisable}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem
                          {...formItemLayout}
                          label="上次供应商"
                  >
                      {getFieldDecorator('providerId', {
                      initialValue : detail.providerId || '',
                      })(<AdvancedSelect dataSource={providerData} type="PROVIDER" onChange={this.handleProviderChange}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="上次价格">
                      {getFieldDecorator('price', {
                      initialValue : detail.price || 0,
                      })(<Input disabled={editDisable}/>)}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="损耗率">
                      {getFieldDecorator('lossRate', {
                      initialValue : detail.lossRate || 0,
                      rules: [ { required: true, message: '请输入损耗率' }],
                      })(<InputNumber
                          formatter={value => `${value}%`}
                      parser={value => value.replace('%', '')}
                      style={{ width : '100%' }}
                      />,
                      )}
                  </FormItem>
                  </Col>

                  <Col span={12}>
                  <FormItem
                          {...formItemLayout}
                          label="是否允许空价"
                  >
                      {getFieldDecorator('blackPrice',{
                      initialValue : detail.blackPrice || '0',
                      })(
                      <AdvancedSelect dataSource={BLANK_PRICE_DATA} placeholder="请选择" onChange={this.handleProviderChange}/>,
                          )}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="数量预警">
                      {getFieldDecorator('warningAmount', {
                      initialValue : detail.warningAmount || 0,
                      })(<InputNumber
                          style={{ width : '100%' }}
                      />,
                      )}
                  </FormItem>
                  </Col>
                  <Col span={12}>
                  <FormItem {...formItemLayout} label="价格预警">
                      {getFieldDecorator('warningPrice', {
                      initialValue : detail.warningPrice || 0,
                      })(<InputNumber
                          style={{ width : '100%' }}
                      />,
                      )}
                  </FormItem>
                  </Col>
              </Row>
              <FormItem {...submitFormLayout} style={{ marginTop : 32 }}>
                  <Button type="primary" onClick={this.handleSubmit} loading={submitting}>
                      保存
                  </Button>
                  <Button style={{ marginLeft : 8 }} onClick={onClose}>
                      取消
                  </Button>
              </FormItem>
          </Form>

      </Card>

    )
      ;
  }
}

export default GoodsDetail;
