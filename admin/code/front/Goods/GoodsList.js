import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Drawer,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ToolBarGroup from '@/components/ToolBarGroup';
import GoodsDatail from './GoodsDetail';
import AdvancedSearchForm from '../../components/AdvancedSearchForm/index';
import styles from './GoodsList.less';
import { GOODS_DETAIL_STATUS} from '../../utils/Enum'
import { emailRegular, phoneRegular } from '../../utils/regular';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const blankPriceMap = ['default', 'processing'];
const status = ['是', '否'];


@connect(({ goodsModal, print, loading }) => ({
  goodsModal,
  print,
  loading: loading.models.goodsModal,
}))
@Form.create()
class GoodsList extends PureComponent {
  state = {
    drawerVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    goodsDetailStatus:GOODS_DETAIL_STATUS.ADD,
  };

  columns = [
    {
      title: 'id',
      dataIndex: 'id',
      sorter: false,
      // align: 'right',
      key: 'id',
    },
    {
      title: 'goodsName',
      dataIndex: 'goodsName',
      sorter: false,
      // align: 'right',
      key: 'goodsName',
    },
    {
      title: 'goodsTypeId',
      dataIndex: 'goodsTypeId',
      sorter: false,
      // align: 'right',
      key: 'goodsTypeId',
    },
    {
      title: 'unitName',
      dataIndex: 'unitName',
      sorter: false,
      // align: 'right',
      key: 'unitName',
    },
    {
      title: 'price',
      dataIndex: 'price',
      sorter: false,
      // align: 'right',
      key: 'price',
    },
    {
      title: 'pinyin',
      dataIndex: 'pinyin',
      sorter: false,
      // align: 'right',
      key: 'pinyin',
    },
    {
      title: 'code',
      dataIndex: 'code',
      sorter: false,
      // align: 'right',
      key: 'code',
    },
    {
      title: 'internal',
      dataIndex: 'internal',
      sorter: false,
      // align: 'right',
      key: 'internal',
    },
    {
      title: 'alternateName',
      dataIndex: 'alternateName',
      sorter: false,
      // align: 'right',
      key: 'alternateName',
    },
    {
      title: 'updateTime',
      dataIndex: 'updateTime',
      sorter: false,
      // align: 'right',
      key: 'updateTime',
    },
    {
      title: 'memo',
      dataIndex: 'memo',
      sorter: false,
      // align: 'right',
      key: 'memo',
    },
    {
      title: 'updaterName',
      dataIndex: 'updaterName',
      sorter: false,
      // align: 'right',
      key: 'updaterName',
    },
    {
      title: 'lossRate',
      dataIndex: 'lossRate',
      sorter: false,
      // align: 'right',
      key: 'lossRate',
    },
    {
      title: 'pickUp',
      dataIndex: 'pickUp',
      sorter: false,
      // align: 'right',
      key: 'pickUp',
    },
    {
      title: 'warningPrice',
      dataIndex: 'warningPrice',
      sorter: false,
      // align: 'right',
      key: 'warningPrice',
    },
    {
      title: 'warningAmount',
      dataIndex: 'warningAmount',
      sorter: false,
      // align: 'right',
      key: 'warningAmount',
    },
    {
      title: 'currentPrice',
      dataIndex: 'currentPrice',
      sorter: false,
      // align: 'right',
      key: 'currentPrice',
    },
    {
      title: 'blackPrice',
      dataIndex: 'blackPrice',
      sorter: false,
      // align: 'right',
      key: 'blackPrice',
    },
    {
      title: 'providerId',
      dataIndex: 'providerId',
      sorter: false,
      // align: 'right',
      key: 'providerId',
    },
    {
      title: 'specifications',
      dataIndex: 'specifications',
      sorter: false,
      // align: 'right',
      key: 'specifications',
    },
    // {
    //   title: '操作',
    //   render: (text, record) => (
    //     <Fragment>
    //       <a onClick={() => this.handleUpdateModalVisible(true, record)}>更新</a>
    //       <Divider type="vertical"/>
    //       <a href="">删除</a>
    //     </Fragment>
    //   ),
    // },
  ];

  componentDidMount() {
    this.listPage();
  }

  listPage = (params) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'goodsModal/fetch',
      payload: params || {
        currentPage: 1,
        pageSize: 10,
      },
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'goodsModal/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'goodsModal/fetch',
      payload: {},
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'goodsModal/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'goodsModal/fetch',
        payload: values,
      });
    });
  };

  handleDrawerVisible = flag => {
    this.setState({ drawerVisible: flag[0] });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleAdd = flag => {
    this.state.goodsDetailStatus = GOODS_DETAIL_STATUS.ADD;
    // this.setState({
    //   goodsDetailStatus:GOODS_DETAIL_STATUS.ADD,
    // })
    this.handleDrawerVisible(flag);
  };

  handleEdit = flag =>{
    this.state.goodsDetailStatus = GOODS_DETAIL_STATUS.EDIT;
    // this.setState({
    //   goodsDetailStatus:GOODS_DETAIL_STATUS.EDIT,
    // })
    this.handleDrawerVisible(flag);
  }

  handleRefresh = () => {
    this.forceUpdate();
  };

  handleDelete = () =>{
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if(selectedRows.length > 1){
      message.error('只能单条删除');
    } else{
      const selectedItem = selectedRows[0].goodsId;
      dispatch({
        type:'goodsModal/delete',
        payload:{
          id: selectedItem,
        }
      })
    }
  }

  handleUpdate = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'goodsModal/update',
      payload: {
        name: fields.name,
        desc: fields.desc,
        key: fields.key,
      },
    });

    message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onCloseDrawer = () => {
    this.setState({
      drawerVisible: false,
    });
  };

  handlePrint = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    dispatch({
      type:'print/saveData',
      payload:{
        formPrintData:selectedRows,
      }
    })
  }

  render() {
    const {
goodsModal: { data, pagination },
      loading,
    } = this.props;
    let selectedItem = '';
    const { selectedRows, drawerVisible, updateModalVisible, goodsDetailStatus } = this.state;
    if (selectedRows.length > 0) {
      selectedItem = selectedRows[0].goodsId;
    }

    const menu = (
      <Menu selectedKeys={[]}>
          <Menu.Item key="remove" onClick={() => this.handleDrawerVisible(true)}>删除</Menu.Item>
          <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleDrawerVisible: this.handleDrawerVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const btnList = {
      primaryBtn: [{
        func: this.handleAdd,
        param: [true],
        key: 'ADD',
      }, {
        func: this.handleRefresh,
        param: [true, '其他'],
        key: 'REFRESH',
      }],
      patchBtn: [{
        func: this.handleEdit,
        param: [true],
        key: 'EDIT',
      },{
        func: this.handleDelete,
        param: {},
        key: 'DELETE',
      },{
        func: this.handlePrint,
        param: {},
        key: 'PRINT',
      }],
      menuBtn: [{
        func: this.handleDrawerVisible,
        param: { s: 1, n: 2 },
        key: 'PATCH_DELETE',
      }],
    };
    const drawerTitle = '修改';

    const searchList = [
      {
        title: '商品名称',
        field: 'goodsName',
        required: true,
        message: '必填(测试)',
        type: 'input',
      },
      {
        title: '损耗率',
        field: 'lossRate',
        message: '损耗率输入错误',
        type: 'inputNumber',
      },
    ];

    const goodsDetailProps = {
      selectItem:selectedItem,
goodsDetailStatus,
      onClose:this.onCloseDrawer,
    }
    return (
      <PageHeaderWrapper title="查询表格">
          <AdvancedSearchForm
                  searchList={searchList}
                  doSearch={this.listPage}
                  pagination={pagination}
          />
          <Card bordered={false}>
              <div className={styles.tableList}>
                  <ToolBarGroup btnOptions={btnList} selectedRows={selectedRows} />
                  <StandardTable
                          selectedRows={selectedRows}
                          loading={loading}
                          data={data}
                          columns={this.columns}
                          onSelectRow={this.handleSelectRows}
                          onChange={this.handleStandardTableChange}
                          rowKey={record => record.id}
                  />
              </div>
          </Card>
          <Drawer
                  title={drawerTitle}
                  placement="right"
                  closable
                  onClose={this.onCloseDrawer}
                  visible={drawerVisible}
                  width={800}
                  destroyOnClose
          >
              <GoodsDatail {...goodsDetailProps} />
          </Drawer>
      </PageHeaderWrapper>
    );
  }
}

export default GoodsList;
