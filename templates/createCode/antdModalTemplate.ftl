import * as ${antdServiceName?uncap_first} from '@/services/${antdName?uncap_first}';
import { routerRedux } from 'dva/router';
import { message } from 'antd';

export default {
  namespace : '${antdModalName?uncap_first}',

  state : {
    data : [],
    list : [],
    pagination : {},
    detail : {},
  },

  effects : {
    * fetch({ payload }, { call, put }) {
      payload = payload ? payload : { currentpage : 1 };
      const response = yield call(${antdServiceName?uncap_first}.queryList, payload);
      const { data : { rows, pageNumber, pageSize, total, totalPage } } = response;
      const list = rows;
      const pagination = {
        current : pageNumber,
        pageSize : pageSize,
        total : total,
      };
      const result = {
        list,
        pagination,
      };
      yield put({
        type : 'save',
        payload : result,
      });
    },
    * get({ payload }, { call, put }) {
      const response = yield call(${antdServiceName?uncap_first}.getDetail, payload);
      yield put({
        type : 'saveDetail',
        payload : response.data,
      });
    },
    * edit({ payload }, { call, put }) {
      const { ${antdName?uncap_first}Id } = payload;
      if (!${antdName?uncap_first}Id) {
        message.error('编辑时，没有id号码');
        return;
      }
      const response = yield call(${antdServiceName?uncap_first}.editDetail, payload);
      if (response.code == 0) {
        yield put({
          type : 'fetch',
          payload : ${antdName?uncap_first}Id,
        });
        message.success('更新成功');
      }
      else{
        message.error('更新失败:'+response.msg);
      }
    },
    * add({ payload }, { call, put }) {
      const response = yield call(${antdServiceName?uncap_first}.addDetail, payload);
      if (response.code == 0) {
        yield put({
          type : 'fetch',
          payload : ${antdName?uncap_first}Id,
        });
        message.success('添加成功');
      }
      else{
        message.error('添加失败:'+response.msg);
      }
    },
    * delete({ payload }, { call, select, put }) {
      const response = yield call(${antdServiceName?uncap_first}.deleteDetail, payload);
      if (response.code == 0) {
        const {pagination:{current,pageSize}} = yield select(_=>_.${antdModalName?uncap_first});
        yield put({
          type : 'fetch',
          payload : {
            currentPage:current,
            pageSize,
          },
        });
        message.success('删除成功');
      }
    },

  },

  reducers : {
    save(state, action) {
      return {
        ...state,
        data : action.payload,
      };
    },
    saveDetail(state, action) {
      return {
        ...state,
        detail : action.payload,
      };
    },
  },
};
