import request from '@/utils/request/request';
import Config from '../../config/api';
import { createTheURL } from '../utils/utils';
import { GLOBAL_URL } from '../utils/ip';
import  {REQUEST_HEADER_LIST} from '../utils/request/requestHeader'

export async function queryList(param) {
  return request(createTheURL(Config.API.${antdName?upper_case}, 'list'), {
    method: 'GET',
    body:param,
  });
}

export async function getDetail(param) {
  return request(createTheURL(Config.API.${antdName?upper_case}, 'get'), {
    method: 'GET',
    body:param,
  });
}
export async function editDetail(param) {
  return request(createTheURL(Config.API.${antdName?upper_case}, 'edit'), {
    method: 'POST',
    body:param,
  });
}
export async function addDetail(param) {
  return request(createTheURL(Config.API.${antdName?upper_case}, 'add'), {
    method: 'POST',
    body:param,
  });
}
export async function deleteDetail(param) {
  return request(createTheURL(Config.API.${antdName?upper_case}, 'del'), {
    method: 'GET',
    body:param,
  });
}

export async function getProviderList() {
  return request();
}

