package ${servicePackageName}.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import ${daoPackageName}.${beanName}Dao;
import ${beanPackageName}.${beanName};
import ${voPackageName}.${voName};
import ${servicePackageName}.${beanName}Service;


@Service
public class ${beanName}ServiceImpl implements ${beanName}Service{

	@Autowired
	public ${beanName}Dao ${beanName?uncap_first}Dao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if (${beanName?uncap_first}Dao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(${beanName} record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (${beanName?uncap_first}Dao.save(record) > 0) 
		{			 
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public ${beanName} getById(String id) {
		return ${beanName?uncap_first}Dao.getById(id);
	}


	@Override
	public String editById(${beanName} record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (${beanName?uncap_first}Dao.editById(record) > 0)
			return record.getId();
		else
			return null; 
	}

	@Override
	public PageTableData<${beanName}> list(int pagenum, int pagesize,${voName} param) {
		PageHelper.startPage(pagenum, pagesize);
		List<${beanName}> datalist = ${beanName?uncap_first}Dao.list(param);
		PageTableData<${beanName}> resultPage = new PageTableData<${beanName}>(datalist);
		PageInfo<${beanName}> p = new PageInfo<${beanName}>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<${beanName}> listAll(${voName} param) {
		List<${beanName}> datalist = ${beanName?uncap_first}Dao.list(param);
		return datalist;
	}

}
