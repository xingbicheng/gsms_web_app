import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import moment from 'moment';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Drawer,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ToolBarGroup from '@/components/ToolBarGroup';
import GoodsDatail from './${antdDetailName}';
import AdvancedSearchForm from '../../components/AdvancedSearchForm/index';
import styles from './${antdPageName}.less';
import { ${antdName?upper_case}_DETAIL_STATUS} from '../../utils/Enum'
import { emailRegular, phoneRegular } from '../../utils/regular';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const blankPriceMap = ['default', 'processing'];
const status = ['是', '否'];


@connect(({ ${antdModalName?uncap_first}, print, loading }) => ({
  ${antdModalName?uncap_first},
  print,
  loading: loading.models.${antdModalName?uncap_first},
}))
@Form.create()
class ${antdPageName} extends PureComponent {
  state = {
    drawerVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    ${antdName?uncap_first}DetailStatus:${antdName?upper_case}_DETAIL_STATUS.ADD,
  };

  columns = [
    <#list fieldList as var >
    {
      title: '${var.name}',
      dataIndex: '${var.name}',
      sorter: false,
      // align: 'right',
      key: '${var.name}',
    },
    </#list>
    // {
    //   title: '操作',
    //   render: (text, record) => (
    //     <Fragment>
    //       <a onClick={() => this.handleUpdateModalVisible(true, record)}>更新</a>
    //       <Divider type="vertical"/>
    //       <a href="">删除</a>
    //     </Fragment>
    //   ),
    // },
  ];

  componentDidMount() {
    this.listPage();
  }

  listPage = (params) => {
    const { dispatch } = this.props;
    dispatch({
      type: '${antdModalName?uncap_first}/fetch',
      payload: params || {
        currentPage: 1,
        pageSize: 10,
      },
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${r'$'}{sorter.field}_${r'$'}{sorter.order}`;
    }

    dispatch({
      type: '${antdModalName?uncap_first}/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: '${antdModalName?uncap_first}/fetch',
      payload: {},
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: '${antdModalName?uncap_first}/remove',
          payload: {
            key: selectedRows.map(row => row.key),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: '${antdModalName?uncap_first}/fetch',
        payload: values,
      });
    });
  };

  handleDrawerVisible = flag => {
    this.setState({ drawerVisible: flag[0] });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleAdd = flag => {
    this.state.${antdDetailName?uncap_first}Status = ${antdName?upper_case}_DETAIL_STATUS.ADD;
    // this.setState({
    //   ${antdDetailName?uncap_first}Status:${antdName?upper_case}_DETAIL_STATUS.ADD,
    // })
    this.handleDrawerVisible(flag);
  };

  handleEdit = flag =>{
    this.state.${antdDetailName?uncap_first}Status = ${antdName?upper_case}_DETAIL_STATUS.EDIT;
    // this.setState({
    //   ${antdDetailName?uncap_first}Status:${antdName?upper_case}_DETAIL_STATUS.EDIT,
    // })
    this.handleDrawerVisible(flag);
  }

  handleRefresh = () => {
    this.forceUpdate();
  };

  handleDelete = () =>{
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if(selectedRows.length > 1){
      message.error('只能单条删除');
    } else{
      const selectedItem = selectedRows[0].${antdName?uncap_first}Id;
      dispatch({
        type:'${antdModalName?uncap_first}/delete',
        payload:{
          id: selectedItem,
        }
      })
    }
  }

  handleUpdate = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: '${antdModalName?uncap_first}/update',
      payload: {
        name: fields.name,
        desc: fields.desc,
        key: fields.key,
      },
    });

    message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  onCloseDrawer = () => {
    this.setState({
      drawerVisible: false,
    });
  };

  handlePrint = () => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    dispatch({
      type:'print/saveData',
      payload:{
        formPrintData:selectedRows,
      }
    })
  }

  render() {
    const {
${antdModalName?uncap_first}: { data, pagination },
      loading,
    } = this.props;
    let selectedItem = '';
    const { selectedRows, drawerVisible, updateModalVisible, ${antdDetailName?uncap_first}Status } = this.state;
    if (selectedRows.length > 0) {
      selectedItem = selectedRows[0].${antdName?uncap_first}Id;
    }

    const menu = (
      <Menu selectedKeys={[]}>
          <Menu.Item key="remove" onClick={() => this.handleDrawerVisible(true)}>删除</Menu.Item>
          <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleDrawerVisible: this.handleDrawerVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    const btnList = {
      primaryBtn: [{
        func: this.handleAdd,
        param: [true],
        key: 'ADD',
      }, {
        func: this.handleRefresh,
        param: [true, '其他'],
        key: 'REFRESH',
      }],
      patchBtn: [{
        func: this.handleEdit,
        param: [true],
        key: 'EDIT',
      },{
        func: this.handleDelete,
        param: {},
        key: 'DELETE',
      },{
        func: this.handlePrint,
        param: {},
        key: 'PRINT',
      }],
      menuBtn: [{
        func: this.handleDrawerVisible,
        param: { s: 1, n: 2 },
        key: 'PATCH_DELETE',
      }],
    };
    const drawerTitle = '修改';

    const searchList = [
      {
        title: '商品名称',
        field: 'goodsName',
        required: true,
        message: '必填(测试)',
        type: 'input',
      },
      {
        title: '损耗率',
        field: 'lossRate',
        message: '损耗率输入错误',
        type: 'inputNumber',
      },
    ];

    const ${antdDetailName?uncap_first}Props = {
      selectItem:selectedItem,
${antdDetailName?uncap_first}Status,
      onClose:this.onCloseDrawer,
    };
    return (
      <PageHeaderWrapper title="查询表格">
          <AdvancedSearchForm
                  searchList={searchList}
                  doSearch={this.listPage}
                  pagination={pagination}
          />
          <Card bordered={false}>
              <div className={styles.tableList}>
                  <ToolBarGroup btnOptions={btnList} selectedRows={selectedRows} />
                  <StandardTable
                          selectedRows={selectedRows}
                          loading={loading}
                          data={data}
                          columns={this.columns}
                          onSelectRow={this.handleSelectRows}
                          onChange={this.handleStandardTableChange}
                          rowKey={record => record.id}
                  />
              </div>
          </Card>
          <Drawer
                  title={drawerTitle}
                  placement="right"
                  closable
                  onClose={this.onCloseDrawer}
                  visible={drawerVisible}
                  width={800}
                  destroyOnClose
          >
              <GoodsDatail {...${antdDetailName?uncap_first}Props} />
          </Drawer>
      </PageHeaderWrapper>
    );
  }
}

export default ${antdPageName};
