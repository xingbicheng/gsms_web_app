package ${daoPackageName};
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import ${voPackageName}.${beanName}Query;
import ${beanPackageName}.${beanName};

@Mapper
public interface ${beanName}Dao {
	
	/**
    *根据新增${beanName},id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(${beanName} record);
    
    
   /**
	*根据id删除${beanName}
   */
 	//@Delete("delete from ${tableName} where id = ${r'#{id,jdbcType=VARCHAR}'}")
    int delById(String id);
     
    
    /**
     *根据id查询${beanName}
     */
    ${beanName} getById(String id);

    /**
     *根据id更新${beanName}
     */ 
    int editById(${beanName} record);

    /**
     *分页查询所有${beanName}
     */    
    List<${beanName}> list(${beanName}Query param); 
        
}