package ${daotestPackageName};
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import ${beanPackageName}.${beanName}Query;
import ${beanPackageName}.${beanName};
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ${daoPackageName}.${beanName}Dao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class ${beanName}DaoTest {
 	@Autowired
   	${beanName}Dao testDao;
   
	@Test
	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		${beanName}Query param = new ${beanName}Query();
		List<${beanName}> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(${beanName}  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		${beanName} testObj=new ${beanName}();
		testDao.save(testObj);
	}
	 
	@Test
	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		${beanName} testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		${beanName} testObj = testDao.getById("4");
		testDao.editById(testObj);
	} 
}