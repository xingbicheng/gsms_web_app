package ${servicePackageName};

import ${beanPackageName}.${beanName};
import ${voPackageName}.${voName};
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface ${beanName}Service {
   /**
    *根据id删除${beanName}
    */
	String delById(String id);

   /**
    *根据新增${beanName}，id自增
    */
    String save(${beanName} record);

   /**
    *根据id查询${beanName}
    */
    ${beanName} getById(String id);
    
   /**
    *根据id更新${beanName}
    */ 
    String editById(${beanName} record);

   /**
    *分页查询${beanName}
    */ 
    PageTableData<${beanName}> list(int pagenum, int pagesize,${voName} param);

	/**
    *查询所有${beanName}
    */ 
    List<${beanName}> listAll(${voName} param);
}