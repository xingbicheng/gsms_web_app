<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="Bookmark" href="/public/img/favicon.ico" >
    <link rel="Shortcut Icon" href="/public/img/favicon.ico" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../../../../lib/html5shiv.js"></script>
    <script type="text/javascript" src="../../../../lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="../../../../static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../../static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="../../../../lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="../../../../static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="../../../../static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
    <script type="text/javascript" src="../../../../lib/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--[global function start]-->
    <script type="text/javascript" src="../../../utils/utils.js"></script>
    <!--[global function end]-->
    <!--/meta 作为公共模版分离出去-->
    <title>新建${htmlPageName}</title>
    <meta name="keywords" content="后台管理系统 --> 新建${htmlPageName}">
    <meta name="description" content="本系统使用了jquery, layUI, dataTables, h-ui等框架">
</head>
<body>
<article class="page-container">
    <!--添加表单-->
    <form action="" method="post" class="form form-horizontal" id="form-${htmlPageName}-add">
        <#list fieldList as var >
               <div class="row cl">
                   <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>${var.name}：</label>
                   <div class="formControls col-xs-8 col-sm-9">
                       <input type="text" class="input-text" value="" placeholder="please input ${var.name}" id="${var.name}" name="${var.name}">
                   </div>
               </div>
        </#list>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</article>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="../../../../lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="../../../../lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="../../../../static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="../../../../static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="../../../../lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="../../../../lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="../../../../lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){
        $.fn.serializeObject = function() {
            var obj = {};
            var arr = this.serializeArray();
            $.each(arr, function() {
                if (obj[this.name] !== undefined) {
                    if (!obj[this.name].push) {
                        obj[this.name] = [obj[this.name]];
                    }
                    obj[this.name].push(this.value || '');
                } else {
                    obj[this.name] = this.value || '';
                }
            });
            return obj;
        };

        $("#form-${htmlPageName}-add").validate({
            rules:{
                <#list fieldList as var >
                      ${var.name}:{
                          required: false,
                      },
                </#list>
            },
            onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
                var formData = $(form).serializeObject()
                $.ajax({
                    type: "POST",
                    url: `${r'$'}{getSessionStorage('api').URL}/api/${htmlPageName}/add`,
                    data: JSON.stringify(formData),  //传入组装的参数
                    cache: false,
                    dataType: "json",
                    beforeSend: function(request) {
                        request.setRequestHeader('token', getSessionStorage('userData').token);
                        request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                    },
                    success: function (result) {
                        if (result.code === 0) {
                            layer.msg('添加成功!',{icon:1});
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.$('#example').DataTable().ajax.reload();
                            parent.layer.close(index);
                        } else {
                            layer.msg(result.msg,{icon:2,time:1000});
                        }
                    },
                    error: function(XmlHttpRequest, textStatus, errorThrown){
                        layer.msg('error!',{icon:2,time:1000});
                    }
                });
            }
        });
    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>