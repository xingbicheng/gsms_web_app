package com.myqm.service.sysfun;

import com.myqm.pojo.sysfun.Department;
import com.myqm.vo.sysfun.DepartmentQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface DepartmentService {
   /**
    *根据id删除Department
    */
	String delById(String id);

   /**
    *根据新增Department，id自增
    */
    String save(Department record);

   /**
    *根据id查询Department
    */
    Department getById(String id);
    
   /**
    *根据id更新Department
    */ 
    String editById(Department record);

   /**
    *分页查询Department
    */ 
    PageTableData<Department> list(int pagenum, int pagesize,DepartmentQuery param);

	/**
    *查询所有Department
    */ 
    List<Department> listAll(DepartmentQuery param);
}