package com.myqm.service.sysfun.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.DepartmentDao;
import com.myqm.pojo.sysfun.Department;
import com.myqm.vo.sysfun.DepartmentQuery;
import com.myqm.service.sysfun.DepartmentService;


@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	public DepartmentDao departmentDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if (departmentDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(Department record) {
		//请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (departmentDao.save(record) > 0) 
		{			 
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public Department getById(String id) {
		return departmentDao.getById(id);
	}


	@Override
	public String editById(Department record) {
		//请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (departmentDao.editById(record) > 0)
			return record.getId();
		else
			return null; 
	}

	@Override
	public PageTableData<Department> list(int pagenum, int pagesize,DepartmentQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Department> datalist = departmentDao.list(param);
		PageTableData<Department> resultPage = new PageTableData<Department>(datalist);
		PageInfo<Department> p = new PageInfo<Department>(datalist);
		resultPage.setCurrentPage(p.getPageNum());
		resultPage.setShowRows(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<Department> listAll(DepartmentQuery param) {
		List<Department> datalist = departmentDao.list(param);
		return datalist;
	}

}
