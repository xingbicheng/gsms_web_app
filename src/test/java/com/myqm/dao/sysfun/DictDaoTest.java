package com.myqm.dao.sysfun;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.dao.sysfun.DictDao;
import com.myqm.pojo.sysfun.Dict;
 

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest  
public class DictDaoTest {
	@Autowired
	DictDao testObject; 
	
	/**
	 * 测试保存
	 */
	@Transactional
	@Test
	public void testSave() {
		Dict t = new Dict(); 
		  
		t.setK("a");
		t.setVal("a");
		t.setType("2");
		t.setMemo("123");
		t.setCreaterId("1");
		testObject.save(t); 
		
		Dict result = testObject.getById(t.getId());
		assertNotEquals(result, null);
				
		
	}

	
//	/**
//	 * 测试通过id得到list
//	 */
//	@Test
//	public void testGetById() {
//		Long id = (long) 1;
//		Dict t = testObject.getById(id);
//		System.out.println(t.toString());
//	}
	
	
//	/**
//	 * 测试删除
//	 */
//	@Test
//	public void testDeleteById() {
//		Long id = (long) 18;
//		if(testObject.delById(id)==1) {
//			System.out.println("true");
//		}
//	}
	
	
//	/**
//	 * 测试编辑
//	 */
//	@Test
//	public void testEditById() {
//		Long id = (long) 17;
//		Dict d = testObject.getById(id);
//		d.setK("123");
//		if(testObject.editById(d)==1)
//			System.out.println("编辑成功");
//	}
	
	
}
