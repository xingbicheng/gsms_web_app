package com.myqm.dao.sysfun;

import static org.junit.Assert.*;

import org.apache.catalina.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.sysfun.SysUser;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest  
public class UserDaoTest {

	@Autowired
	private UserDao userDao;
	@Test
	public void testGetUser() {
		SysUser user=userDao.getUser("admin");
		System.out.println(user.getUsername() );
	}

}
