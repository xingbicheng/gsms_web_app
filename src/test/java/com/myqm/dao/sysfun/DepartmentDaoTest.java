package com.myqm.dao.sysfun;
import java.util.List;
import com.myqm.pojo.sysfun.Department;
import com.myqm.vo.sysfun.DepartmentQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.sysfun.DepartmentDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class DepartmentDaoTest {
 	@Autowired
   	DepartmentDao testDao;
   
	@Test
	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		DepartmentQuery param = new DepartmentQuery();
		List<Department> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(Department  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		Department testObj=new Department();
		testDao.save(testObj);
	}
	 
	@Test
	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		Department testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		Department testObj = testDao.getById("4");
		testDao.editById(testObj);
	} 
}