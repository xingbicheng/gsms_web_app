package com.myqm.dao.gs.order;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.myqm.pojo.gs.order.OrderDetailIn;
import com.myqm.vo.gs.order.OrderDetailInQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.myqm.pojo.gs.order.OrderDetail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderDetailDaoTest {
	@Autowired
    OrderDetailDao detailDao;
	@Autowired
	WebOrderDao webOrderDao;
	
	@Test
	public void testdele() {
		int delById = webOrderDao.delById("a41341399c8143c0909224ce5c30d520");
		System.out.println(delById);
	}
//	@Test
//	public void testEditStateByIds() {
//		List<OrderDetail>details=new ArrayList<>();
//		OrderDetail detail=new OrderDetail();
//		OrderDetail detail2=new OrderDetail();
//		detail.setId("100000");
//		detail2.setId("100001");
//		detail.setState("7");
//		detail2.setState("7");
//		details.add(detail);
//		details.add(detail2);
//		detailDao.editStateByIds(details);
//	}

	@Test
	public void testEditReceiveInfo() {
		List<OrderDetail>details=new ArrayList<>();
		OrderDetail detail=new OrderDetail();
		OrderDetail detail2=new OrderDetail();
		
		detail.setId("100000");
		detail.setState("2");
		detail.setReceiveAmount(10.0);
		detail.setReceivePrice(new BigDecimal(10.0));
		detail.setReceiveSum(new BigDecimal(10.0));
		
		detail2.setId("100001");
		detail2.setState("2");
		detail2.setReceiveAmount(10.0);
		detail2.setReceivePrice(new BigDecimal(10.0));
		detail2.setReceiveSum(new BigDecimal(10.0));
		details.add(detail);
		details.add(detail2);
		
		detailDao.editReceiveInfo(details);
		
		
	}

	@Autowired
    OrderDetailInDao orderDetailInDao;
	@Test
    public void testOrderDetailIn() {

        OrderDetailInQuery orderDetailInQuery = new OrderDetailInQuery();
        orderDetailInQuery.setDetailId("011d32bcce9611e8bed300163e0a39a2");
        orderDetailInQuery.setInId("027446bbd28111e89e0900163e0a39a2");
        List<OrderDetailIn> orderDetailIn = orderDetailInDao.list(orderDetailInQuery);
        for (OrderDetailIn o : orderDetailIn)
            System.out.println(o);
    }
}
