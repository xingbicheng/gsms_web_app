package com.myqm.dao.gs.basic;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.vo.gs.basic.StoreCustomerQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class StoreCustomerDaoTest {

	@Autowired
	StoreCustomerDao testDao;
	
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		StoreCustomerQuery param = new StoreCustomerQuery();
		param.setCustomerTypeId("1");
		List<StoreCustomer> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(StoreCustomer  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		StoreCustomer testObj=new StoreCustomer();
		testObj.setCreaterId("4");
		testObj.setStoreId("1");
		testObj.setCustomerTypeId("1");
		testObj.setMemo("3");
		testObj.setUpdaterId("4");
		testDao.save(testObj);
	}
	
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		StoreCustomer testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		StoreCustomer testObj =testDao.getById("4");
		testObj.setCreaterId("9");
		testDao.editById(testObj);
	}
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		StoreCustomer testObj=new StoreCustomer();
		testDao.delById("1");
	}

}
