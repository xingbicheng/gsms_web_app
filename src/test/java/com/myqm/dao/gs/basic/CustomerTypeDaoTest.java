package com.myqm.dao.gs.basic;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.vo.gs.basic.CustomerTypeQuery;
import com.myqm.vo.gs.basic.GoodsQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CustomerTypeDaoTest {
 	@Autowired
   	CustomerTypeDao testDao;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		CustomerTypeQuery param = new CustomerTypeQuery();
		param.setCustomerType("三台");
		List<CustomerType> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(CustomerType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		CustomerType testObj=new CustomerType();
		testObj.setCustomerType("1");
		testObj.setCode("asdas");
		testObj.setId("1221");
		testObj.setMemo("adsas");
		testObj.setPinyin("asdsa");
		testObj.setUpdaterName("asdsa");
		testObj.setProperty("1");
		testDao.save(testObj);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		CustomerType testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		CustomerType testObj = testDao.getById("11");
		testObj.setCustomerType("大学");
		testDao.editById(testObj);
	} 
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void deleById()
	{
		CustomerType testObj=new CustomerType();
		testDao.delById("9");
	} 
}
