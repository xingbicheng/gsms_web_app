package com.myqm.dao.gs.basic;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.vo.gs.basic.StorehouseQuery;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class StorehouseDaoTest {
 	@Autowired
   	StorehouseDao testDao;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		StorehouseQuery param = new StorehouseQuery();
		param.setStoreTypeId("1");
		List<Storehouse> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(Storehouse  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		Storehouse testObj=new Storehouse();
		testObj.setStoreName("10号仓库");
		testObj.setCode("10");
		testObj.setStoreTypeId("1");
		testObj.setUpdaterName("10");
		testDao.save(testObj);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		Storehouse testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		Storehouse testObj = testDao.getById("4");
		testObj.setCode("5");
		testDao.editById(testObj);
	} 
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		Storehouse testObj=new Storehouse();
		testDao.delById("10");
	}
}
