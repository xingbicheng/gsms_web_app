package com.myqm.dao.gs.basic;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.pojo.gs.basic.StorehouseType;
import com.myqm.vo.gs.basic.StorehouseQuery;
import com.myqm.vo.gs.basic.StorehouseTypeQuery;



@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class StorehouseTypeDaoTest {
 	@Autowired
   	StorehouseTypeDao testDao;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		StorehouseTypeQuery param = new StorehouseTypeQuery();
		param.setStoreType("成都分库");
		List<StorehouseType> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(StorehouseType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		StorehouseType testObj=new StorehouseType();
		testObj.setCode("4");
		testObj.setStoreType("绵阳分库");
		testObj.setMemo("4");
		testObj.setPinyin("myfk");
		testObj.setUpdaterName("0");
		testDao.save(testObj);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		StorehouseType testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		StorehouseType testObj = testDao.getById("6");
		testObj.setCode("5");
		testDao.editById(testObj);
	} 
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		StorehouseType testObj=new StorehouseType();
		testDao.delById("c31988c4bc2511e8b7cf00163e0a39a2");
	}
}
