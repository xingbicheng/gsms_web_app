package com.myqm.dao.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.vo.gs.order.OrderDetailQuery;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.order.OrderDetailDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class OrderDetailDaoTest {
 	@Autowired
   	OrderDetailDao testDao;
   
	@Test
	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		OrderDetailQuery param = new OrderDetailQuery();
		List<OrderDetail> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(OrderDetail  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		OrderDetail testObj=new OrderDetail();
		testDao.save(testObj);
	}
	 
	@Test
	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		OrderDetail testObj=testDao.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		OrderDetail testObj = testDao.getById("4");
		testDao.editById(testObj);
	} 
}