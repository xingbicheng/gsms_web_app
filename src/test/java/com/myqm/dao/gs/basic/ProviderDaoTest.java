package com.myqm.dao.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.gs.basic.Provider;
import com.myqm.vo.gs.basic.ProviderQuery;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.basic.ProviderDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class ProviderDaoTest {
 	@Autowired
   	ProviderDao testDao;
	//@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	/*public void testList() {
		ProviderQuery param = new ProviderQuery();
		List<Provider> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(Provider  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}*/
	@Test
	//@Transactional
	/*测试新增数据,支持回滚*/
	public void testSave()
	{
		Provider testObj=new Provider();
		testObj.setCode("11");
		testObj.setProviderName("11");
		testObj.setUpdaterName("11");
		testObj.setProviderType("a");
		testObj.setId("a");
		testObj.setProviderTypeId("11");
		testDao.save(testObj);
	}
	/* @Test
	 //删除数据
	 public void testdelete() {
		 testDao.delById("11");
	 }*/
	/*@Test
	//@Transactional
	测试根据id查询数据,支持回滚
	public void testGetById()
	{
		Provider testObj=testDao.getById("11");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}*/
	
	/*@Test
	//@Transactional
	*//**测试编辑数据,支持回滚*//*
	public void testEditById()
	{
		Provider testObj = testDao.getById("11");
		testObj.setMemo("xiugai");
		testDao.editById(testObj);
	} */
}