package com.myqm.dao.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsTypeQuery;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.basic.GoodsTypeDao;
import org.junit.runners.MethodSorters;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GoodsTypeDaoTest {
 	@Autowired
   	GoodsTypeDao testDao;
	static String id;
	
    @Test
	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testBBList() {
		GoodsTypeQuery param = new GoodsTypeQuery();
		param.setCodePy("rl");
		param.setUpdaterName("系统管理员");
		param.setMemo(null);
		param.setGoodsType("肉类");
		param.setCode("2");
		List<GoodsType> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(GoodsType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsType());
		}
	}
    
	@Test
//	@Transactional
	/**测试新增数据,支持回滚*/
	public void testAASave()
	{
		GoodsType testObj=new GoodsType();
		testObj.setCode("特别的类");
		testObj.setGoodsType("超级大大龙虾");
		testObj.setId("10241024");
		testObj.setMemo("暂无");
		testObj.setPinyin("cjddlx");
		testObj.setUpdaterName("一只小白");
		testDao.save(testObj);
		id=testObj.getId();
		System.out.println(testObj.getId());
	}
	 
	@Test
//	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testCCGetById()
	{
		GoodsType testObj=testDao.getById(id);
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
//	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testDDEditById()
	{
		GoodsType testObj = testDao.getById(id);
		testObj.setCode("不特别的类");
		testObj.setGoodsType("超级小小龙虾");
		testObj.setId("10241024");
		testObj.setMemo("暂时无");
		testObj.setPinyin("cjxxlx");
		testObj.setUpdaterName("两只小白");
		testDao.save(testObj); 
		System.out.println("GoodsType: "+testObj.getGoodsType());
		testDao.delById(id);
		testDao.editById(testObj);
	}
	
	@Test
//	@Transactional
	/**测试删除数据，支持回滚*/
	public void testEEdelete() {
		testDao.delById(id);
		
	}
}