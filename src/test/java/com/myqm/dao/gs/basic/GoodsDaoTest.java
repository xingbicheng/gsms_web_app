package com.myqm.dao.gs.basic;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBException; 
import com.myqm.pojo.gs.basic.Goods; 
import com.myqm.service.gs.basic.impl.TPlusServiceImpl;
import com.myqm.vo.gs.basic.GoodsQuery;

//import scala.annotation.meta.setter;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.basic.GoodsDao;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GoodsDaoTest {
 	@Autowired
   	GoodsDao testDao;
 	@Autowired
 	TPlusServiceImpl tPlusServiceImpl;
 	@Autowired
 	TPlusGoodsDao tPlusGoodsDao;
 	static String goodsId;
 	
	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testBBList() {
		GoodsQuery param = new GoodsQuery();
    	param.setUnitName("斤");
    	param.setCodePy("m");
    	//param.setGoodsName("墨鱼");
    	param.setUpdaterName("一只小白");
    	param.setAlternateName(null);
		List<Goods> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(Goods  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsName());
		}
	}
	@Test
//	@Transactional
	/**测试新增数据,支持回滚*/
	public void testAAsave()
	{
		Goods testObj=new Goods();
		testObj.setGoodsName("超级大大龙虾");
		testObj.setGoodsType("大大虾类");
		testObj.setGoodsTypeId("8");
		testObj.setLossRate(100.00f);
		testObj.setId("10241024");
		testObj.setMemo("没有");
		testObj.setPinyin("cjddlx");
		testObj.setWarningAmount(200.00f);
		testObj.setPrice(new BigDecimal(22.2));
		//testObj.setPickUp(true);
		testObj.setUpdaterName("一只小白");
		testObj.setUnitName("大只");
		testObj.setInternal("2");
		testObj.setCode("222");
		testObj.setWarningPrice(new BigDecimal(33.3));
		testDao.save(testObj);
		goodsId = testObj.getId();
	}
	 
	@Test
//	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testCCGetById()
	{
		Goods testObj=testDao.getById(goodsId);
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
//	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testDDEditById()
	{
		Goods testObj = testDao.getById(goodsId);
		testObj.setGoodsName("超级小小龙虾");
		testObj.setGoodsType("小小虾类");
		testObj.setGoodsTypeId("8");
		testObj.setLossRate(100.00f);
		testObj.setId("10241024");
		testObj.setMemo("暂时没有");
		testObj.setPinyin("cjxxlx");
		testObj.setWarningAmount(200.00f);
		testObj.setPrice(new BigDecimal(22.2));
		//testObj.setPickUp(true);
		testObj.setUpdaterName("两只小白");
		testObj.setUnitName("小只");
		testObj.setInternal("3");
		testObj.setCode("333");
		testObj.setWarningPrice(new BigDecimal(33.3));
		testDao.editById(testObj);
	} 
	
	@Test
//	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEEdelete() {
		testDao.delById(goodsId);
	}
	
	@Test
//	@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testReplace() throws JAXBException {
		String str = tPlusServiceImpl.updateCustomer(); 
		System.out.println(str);
		//RootGoods unXml =  (RootGoods)tPlusServiceImpl.unXml(str);
		//tPlusGoodsDao.replaceInto(unXml.getItems());
		//testDao.delById(goodsId);
	}
}
