package com.myqm.dao.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.gs.basic.ProviderType;
import com.myqm.vo.gs.basic.ProviderTypeQuery;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.basic.ProviderTypeDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class ProviderTypeDaoTest {
 	@Autowired
   	ProviderTypeDao testDao;
   
	//@Test
	//@Transactional
	/*测试查询所有数据,支持回滚*/
	/*public void testList() {
		ProviderTypeQuery param = new ProviderTypeQuery();
		param.setCodePy("测");
		List<ProviderType> list = testDao.list(param); 
    	System.out.println(list.size());
    	for(ProviderType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}*/
//	@Test
//	//@Transactional
//	/**测试新增数据,支持回滚*/
	/*public void testSave()
	{
		ProviderType testObj=new ProviderType();
		testObj.setId("3");
		testObj.setCode("3");
		testObj.setPinyin("测试");
		testObj.setProviderType("ww");
		testObj.setUpdaterName("zjw");
		testDao.save(testObj);
	}*/
//	@Test
//	//@Transactional
	 /*测试删除数据*/
//	public void testdelete() {
//    	testDao.delById("1");
//	}
/*	@Test
//	@Transactional
	//测试根据id查询数据,支持回滚
	public void testGetById()
	{
		ProviderType testObj=testDao.getById("1");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}*/
	@Test
//	//@Transactional
//	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		ProviderType testObj = testDao.getById("1");
		testObj.setPinyin("xiugai");
		testDao.editById(testObj);
	} 
}