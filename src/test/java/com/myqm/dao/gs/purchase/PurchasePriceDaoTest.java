package com.myqm.dao.gs.purchase;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.dao.gs.purchase.PurchasePriceDao;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class PurchasePriceDaoTest {

	@Autowired
	PurchasePriceDao testDao;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		PurchasePriceQuery param = new PurchasePriceQuery();
		List<PurchasePrice> list = testDao.list(param); 
		param.setGoodsId("1");
    	System.out.println(list.size());
    	for(PurchasePrice  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		PurchasePrice testObj=new PurchasePrice();
		testObj.setProviderId("1");
		testObj.setGoodsId("1");
		testObj.setPurchaseDetailId("047d5e39dcac11e8910500163e0a39a2");
		testObj.setAmount(new BigDecimal(22));
		testObj.setPrice(new BigDecimal(22.2)); 
		testObj.setState("1");
		testObj.setCreaterId("1");
		testDao.save(testObj);
	}
	
	@Test
	//@Transactional
	public void saveAll(){
		List<PurchasePrice>list=new ArrayList<>();
		PurchasePrice testObj=new PurchasePrice();
		testObj.setProviderId("1");
		testObj.setGoodsId("1");
		testObj.setPurchaseDetailId("047d5e39dcac11e8910500163e0a39a2");
		testObj.setAmount(new BigDecimal(22));
		testObj.setPrice(new BigDecimal(22.2));
	 
		testObj.setState("1");
		testObj.setCreaterId("1");
		list.add(testObj);
		testDao.saveAll(list);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		PurchasePrice testObj=testDao.getById("1");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
//	@Test
//	//@Transactional
//	/**测试编辑数据,支持回滚*/ 
//	public void testEditById()
//	{
//		PurchasePrice testObj = testDao.getById("1");
//		testObj.setUpdaterId("1");
//		testDao.editById(testObj);
//	} 
	
//	@Test
//	//@Transactional
//	 public void  testEditByIds() {
//		// TODO Auto-generated method stub
//		List<PurchasePrice>list=new ArrayList<>();
//		PurchasePrice testObj = testDao.getById("1");
//		testObj.setUpdaterId("5");
//		list.add(testObj);
//		testDao.editByIds(list);
//	}
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void delById()
    {
		testDao.delById("4");
	}
	
	@Test
    //@Transactional
	public void delFlagById() {
		testDao.delFlagById("6");
	}
}
   
