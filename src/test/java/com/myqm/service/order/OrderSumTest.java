package com.myqm.service.order;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderSum;
import com.myqm.pojo.gs.order.OrderTotal;
import com.myqm.service.gs.order.impl.OrderSumServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderSumTest {
    @Autowired
    OrderSumServiceImpl orderSumService;
    @Test
    public void Sumtest(){
        OrderTotal orderTotal=new OrderTotal();
       orderTotal.setClassificationCustomer(true);
       orderTotal.setState("2");
      List<OrderSum> orderSum = orderSumService.sum(orderTotal);
        System.out.println();
    }
}
