package com.myqm.service.sysfun;

import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.service.gs.purchase.PurchaseOrderService;
import com.myqm.service.gs.purchase.impl.PerPurchaseServiceImpl;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PerPurchaseServiceTest
{

    @Autowired
    PerPurchaseServiceImpl perPurchaseService;
    @Autowired
    PurchaseOrderService purchaseOrderService;

    @Test
    public void preListTest() throws ParseException {

        PerPurchaseQuery param=new PerPurchaseQuery();
        List<String> ids=new ArrayList<String>();
        ids.add("1");
        ids.add("2");
        ids.add("3");

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date start=new Date();//format.parse("2012-01-01");
        Date end=format.parse("2019-01-01");

        param.setCustomerTypeIds(ids);
        param.setPerStart(new java.sql.Date(start.getTime())); 
        param.setPerEnd(new java.sql.Date(end.getTime())); 
        List<PrePurchaseGoodsType> preList=perPurchaseService.getPurchaseDetail(param);
        for(PrePurchaseGoodsType p:preList)
            System.out.println(p);

    }

    @Test
    public void nextText(){
      // PurchaseOrder purchaseOrder= purchaseOrderService.next("1");
      //  System.out.println(purchaseOrder.toString());

    }
}
