package com.myqm.service.sysfun;

import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.gs.order.impl.OrderServiceImpl;
import com.myqm.vo.gs.order.OrderDetailQuery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailDao orderDetailDao;
     @Test
    public void TestNext(){
        String orderNumber="DD201804020006";
//        Order order = orderService.getNext(orderNumber);
//        System.out.println(order);
    }

    @Test
    public void TestPre(){
        String orderNumber="DD2018040200067";
//        Order order = orderService.getPre(orderNumber);
//        System.out.println(order);
    }
    
    @Test
    public void TestGetById(){
        String id="10020";
        OrderDetailQuery param = new OrderDetailQuery();
        param.setOrderId(id);
        List<OrderDetail> list = orderDetailDao.list(param);
        for (OrderDetail orderDetail : list) {
			System.out.println(orderDetail.toString());
		}
      /*  Order byId = orderService.getById(id);
        System.out.println(byId.toString());*/
//        Order order = orderService.getPre(orderNumber);
//        System.out.println(order);
    }
    
    @Test
    public void TestSave(){
    	Order record = new Order();
    	record.setCustomerId("276");
    	record.setOrderNo("asd");
    	//record.setDistributionDate();
    	record.setLumpSum(new BigDecimal(123.00));
    	record.setState("1");
    	record.setOrigin("0");
    	record.setCreaterId("1");
    	record.setUpdaterId("1");
    	record.setOuterId("1");
    	List<OrderDetail> orderDetailList =  new ArrayList<>();
    	OrderDetail  orderDetail =new OrderDetail();
    	orderDetail.setUnitPrice(new BigDecimal(100));
    	orderDetail.setAmount(1000.00);
    	orderDetail.setUnitName("asd");
    	orderDetail.setSum(new BigDecimal(10000));
    	orderDetail.setInGoodsId("1");
    	orderDetailList.add(orderDetail);
    	record.setOrderDetailList(orderDetailList);
    	orderService.save(record);
    }
    
    @Test
    public void editTest(){
    	Order byId = orderService.getById("7222e720bb0311e88a4100163e0a39a2");
    	byId.setCustomerName("asd");
    	List<OrderDetail> orderDetailList = byId.getOrderDetailList();
    	for (OrderDetail orderDetail : orderDetailList) {
    		orderDetail.setEditFlag("d");
    		orderDetail.setAmount(520.00);
		}
    	String editById = orderService.editById(byId);
    	System.out.println(editById);
    }

    @Test
    public void csvMysql() {
	    File csv = new File("d:\\order.csv");  // CSV文件路径
		   
	    BufferedReader br = null;
	    try
	    {
			try {
				br= new BufferedReader(new InputStreamReader(new FileInputStream(csv),"GBK"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } catch (FileNotFoundException e)
	    {
	        e.printStackTrace();
	    }
	    String line = "";
	    List<String>list = new ArrayList<>();
	    try {
	    		br.readLine();
	            List<String> allString = new ArrayList<>();
	            while ((line = br.readLine()) != null)  //读取到的内容给line变量
	            {
	            	Order order = new Order();
	                String[] split = line.split(",");
	                	order.setOrderNo(orderService.getNum());
	                	order.setCustomerId(split[1]);
	                	order.setCustomerName(split[2]);
	                	SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
	                	Date parse;
						try {
							parse = format.parse(split[5]);
		                	java.sql.Date date = new java.sql.Date(parse.getTime()); 
		                	order.setDistributionDate(date);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						order.setMemo(split[6]);
						order.setFlag(split[7]);
						order.setCreaterId(split[8]);
						List<OrderDetail> load = load(split[0],order);
						order.setOrderDetailList(load);
						orderService.save(order);
						
	            }
	    } catch (IOException e)
	    {
	        e.printStackTrace();
	    }

    }
    
    public   List<OrderDetail> load(String id,Order order) {
    	File csv = new File("d:\\orderdetail.csv");  // CSV文件路径
		   List<OrderDetail> list = new ArrayList<>();
	    BufferedReader br = null;
	    try
	    {
			try {
				br= new BufferedReader(new InputStreamReader(new FileInputStream(csv),"GBK"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } catch (FileNotFoundException e)
	    {
	        e.printStackTrace();
	    }
	    String line = "";
	    try {
	    		br.readLine();
	            List<String> allString = new ArrayList<>();
	            BigDecimal count =new BigDecimal(0);
	            BigDecimal FreeSum =new BigDecimal(0);
	            BigDecimal SelfSum =new BigDecimal(0);
	            while ((line = br.readLine()) != null)  //读取到的内容给line变量
	            {
	            	String[] split = line.split(",");
	            	if(split[0].equals(id)) {
	            		OrderDetail orderDetail = new OrderDetail();
	            		orderDetail.setUnitName(split[3]);
	            		orderDetail.setInGoodsId(split[4]);
	            		orderDetail.setFreeAmount(Double.parseDouble(split[5]));
	            		orderDetail.setSelfAmount(Double.parseDouble(split[6]));
	            		orderDetail.setAmount(Double.parseDouble(split[7]));
	            		orderDetail.setUnitPrice(BigDecimal.valueOf(Double.parseDouble(split[8])));
	            		orderDetail.setSum(BigDecimal.valueOf(Double.parseDouble(split[9])));
	            		orderDetail.setFreeSum(BigDecimal.valueOf(Double.parseDouble(split[8])).multiply(BigDecimal.valueOf(Double.parseDouble(split[5]))));
	            		orderDetail.setSelfSum(BigDecimal.valueOf(Double.parseDouble(split[8])).multiply(BigDecimal.valueOf(Double.parseDouble(split[6]))));
	            		count = count.add(orderDetail.getSum()) ;
	            		FreeSum = FreeSum.add(orderDetail.getFreeSum());
	            		SelfSum = SelfSum.add(orderDetail.getSelfSum());
	            		list.add(orderDetail);
	            	}
	            }
	            order.setLumpSum(count);
	            order.setFreeSum(FreeSum);
	            order.setSelfSum(SelfSum);
	            return list;
	    } catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	
    	return null;
    }
}
