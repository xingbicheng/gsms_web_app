package com.myqm.service.gs.purchase.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.service.gs.purchase.PurchasePriceService;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest

public class PurchasePriceServiceTest {

	@Autowired
	PurchasePriceService testService;
	
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		PurchasePriceQuery param = new PurchasePriceQuery();
		param.setProviderId("1");
		List<PurchasePrice> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(PurchasePrice  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		PurchasePrice testObj=new PurchasePrice();
		testObj.setProviderId("1");
		testObj.setGoodsId("1");
		testObj.setPurchaseDetailId("047d5e39dcac11e8910500163e0a39a2");
		testObj.setAmount(new BigDecimal(22));
		testObj.setPrice(new BigDecimal(22.2));
		 
		testObj.setState("1");
		testObj.setCreaterId("1");
		testService.save(testObj);
	}
	
	@Test
	//@Transactional
	public void testSaveAll() {
		List<PurchasePrice>list=new ArrayList<>();
		PurchasePrice testObj=new PurchasePrice();
		testObj.setProviderId("1");
		testObj.setGoodsId("1");
		testObj.setPurchaseDetailId("047d5e39dcac11e8910500163e0a39a2");
		testObj.setAmount(new BigDecimal(22));
		testObj.setPrice(new BigDecimal(22.2)); 
		testObj.setState("1");
		testObj.setCreaterId("1");
		list.add(testObj);
		testService.saveAll(list);
	}
	
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		PurchasePrice testObj=testService.getById("2");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		PurchasePrice testObj = testService.getById("1");
		 
		testService.editById(testObj);
	} 
	
	@Test
	//@Transactional
	 public void  testEditByIds() {
		List<PurchasePrice>list=new ArrayList<>();
		PurchasePrice testObj = testService.getById("2"); 
		list.add(testObj);
		testService.editByIds(list);
	}
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void delById()
    {
		testService.delById("10");
	}
	
	@Test
    //@Transactional
	public void delFlagById() {
		testService.delFlagById("6");
	}

}
