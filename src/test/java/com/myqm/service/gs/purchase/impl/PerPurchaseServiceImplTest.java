package com.myqm.service.gs.purchase.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.service.gs.purchase.PerPurchaseService;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest  
public class PerPurchaseServiceImplTest {
	@Autowired
	PerPurchaseService prService;
	@Test
	public void testGetPurchaseDetail() {
		PerPurchaseQuery param = new PerPurchaseQuery();
		param.setCustomerTypeIds(new ArrayList<String>());
		param.getCustomerTypeIds().add("1");
		param.getCustomerTypeIds().add("4");
		param.getCustomerTypeIds().add("5");
		param.setPerEnd(java.sql.Date.valueOf("2018-06-01"));
		param.setPerStart(java.sql.Date.valueOf("2018-04-01"));
		List<PrePurchaseGoodsType> list = prService.getPurchaseDetail(param); 
		System.out.println(list.size());
	}

}
