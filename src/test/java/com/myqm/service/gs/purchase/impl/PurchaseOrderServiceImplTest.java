package com.myqm.service.gs.purchase.impl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.dto.gs.purchase.PrePurchaseGoods;
import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.pojo.gs.purchase.PrePurchaseDetail;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.service.gs.purchase.PerPurchaseService;
import com.myqm.service.gs.purchase.PurchaseOrderService;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest  
public class PurchaseOrderServiceImplTest {  
	@Autowired
	PurchaseOrderService purchaseService;
	@Autowired
	PerPurchaseService prService;
	
	
	@Test
	public void testSave() { 
		PerPurchaseQuery param = new PerPurchaseQuery();
		param.setCustomerTypeIds(new ArrayList<String>());
		param.getCustomerTypeIds().add("1");
		param.getCustomerTypeIds().add("4");
		param.getCustomerTypeIds().add("5");
		param.setPerEnd(java.sql.Date.valueOf("2018-5-04"));  
		param.setPerStart(java.sql.Date.valueOf("2018-05-04")); 
		List<PrePurchaseGoodsType> list = prService.getPurchaseDetail(param);
		PurchaseOrder order = new PurchaseOrder(); 
		order.setOrderNo(purchaseService.getNum());
		order.setCustomerTypes("盐亭+三台+高新");
		order.setGoodsTypes("干杂类+冷冻制品");
		order.setMemo("测试新增");
		order.setPurchaseDate(java.sql.Date.valueOf("2018-10-03"));
		order.setPurchaserId("1");
		order.setSum(new BigDecimal(1000.00));
		List<PurchaseOrderDetail> detais = new ArrayList<PurchaseOrderDetail>();
		order.setOrderDetailList(detais);
		for (PrePurchaseGoodsType type:list){
			if (type.getGoodsType().equals("蔬菜类"))//||type.getGoodsType().equals("冷冻制品"))
			{
				List<PrePurchaseGoods>  goodslist = type.getGoodsGruoup();
				for (PrePurchaseGoods goods : goodslist)
				{
					PurchaseOrderDetail pd = new PurchaseOrderDetail();
					pd.setAmount(goods.getAmount());
					pd.setAveragePrice(new BigDecimal(2.10));
					pd.setStatisticAmount(goods.getStatisticAmount());
					pd.setLossAmount(goods.getLossAmount());
					pd.setGoodsId(goods.getGoodsId());
					pd.setPurchaseType("计划采购");
					List<PrePurchaseDetail> predetails = goods.getDetails();
					pd.setOrderDetailIds(new ArrayList<String>());
					for(PrePurchaseDetail d :predetails){
						pd.getOrderDetailIds().add(d.getId());
					} 
					detais.add(pd);
				} 

	}
}
		purchaseService.save(order);
	}

}
