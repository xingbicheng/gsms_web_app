package com.myqm.service.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.vo.gs.basic.CustomerQuery;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.gs.basic.CustomerDao;
@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CustomerServiceTest {
 	@Autowired
   	CustomerService testService;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		CustomerQuery param = new CustomerQuery();
		param.setCustomerTypeId("1");
		List<Customer> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(Customer  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		Customer testObj=new Customer();
		testObj.setCustomerTypeId("2");
		testObj.setCustomerName("西南科技大学");
		testObj.setContactName("西南科技大学");
		testObj.setContactTel("11111111");	
		testObj.setPinyin("xnkjdx");
		testObj.setCode("4444");
		testObj.setUpdaterName("系统管理员");
		testService.save(testObj);
	}
	 
	@Test
	@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		Customer testObj=testService.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		Customer testObj = testService.getById("1");
		testObj.setContactTel("124677");
		testService.editById(testObj);
	} 
	
	@Test
    @Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		Customer testObj=new Customer();
		testService.delById("caf27690b3d411e88a4100163e0a39a2");
	}
}
   