package com.myqm.service.gs.basic;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.base.ParserMinimalBase;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.vo.gs.basic.SysSetQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class SysSetServiceTest {
	@Autowired
	SysSetService testService;

	@Test
	//@Transactional
	public void testGetById() {
		SysSet testObj = testService.getById("2");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}

	@Test
	//@Transactional
	public void testEditById() {
		SysSet testObj = testService.getById("3");
		testObj.setSetvalue2("4");
		testService.editById(testObj);
	}

	@Test
	//@Transactional
	public void testList() {
		SysSetQuery param = new SysSetQuery();
		param.setId("2");
		List<SysSet> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(SysSet  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
}
