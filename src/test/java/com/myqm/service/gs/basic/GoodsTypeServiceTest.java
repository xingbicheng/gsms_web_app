package com.myqm.service.gs.basic;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsTypeQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest  
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GoodsTypeServiceTest {
	@Autowired
   	GoodsTypeService testService;
	static String id;
	
	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testAASave() {
		GoodsType testObj=new GoodsType();
		testObj.setCode("特别的类");
		testObj.setGoodsType("超级大大龙虾");
		testObj.setId("10241024");
		testObj.setMemo("暂无");
		testObj.setPinyin("cjddlx");
		testObj.setUpdaterName("一只小白");
		testService.save(testObj);
		id=testObj.getId();
		System.out.println(testObj.getId());
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testCCGetById() {
		GoodsType testObj=testService.getById(id);
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testDDEditById() {
		GoodsType testObj = testService.getById(id);
		testObj.setCode("不特别的类");
		testObj.setGoodsType("超级小小龙虾");
		testObj.setId("10241024");
		testObj.setMemo("暂时无");
		testObj.setPinyin("cjxxlx");
		testObj.setUpdaterName("两只小白");
		testService.save(testObj); 
		System.out.println("GoodsType: "+testObj.getGoodsType());
		testService.delById(id);
		testService.editById(testObj);
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testBBList() {
		GoodsTypeQuery param = new GoodsTypeQuery();
		param.setCodePy("rl");
		param.setUpdaterName("系统管理员");
		param.setMemo(null);
		param.setGoodsType("肉类");
		param.setCode("2");
		List<GoodsType> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(GoodsType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsType());
		}
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testFFListAll() {
		GoodsTypeQuery param = new GoodsTypeQuery();
		List<GoodsType> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(GoodsType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsType());
		}
	}
	
	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testEEDelById() {
		testService.delById(id);
	}

}
