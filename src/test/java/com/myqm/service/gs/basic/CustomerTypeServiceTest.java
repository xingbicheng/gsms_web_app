package com.myqm.service.gs.basic;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.vo.gs.basic.CustomerTypeQuery;
import com.myqm.vo.gs.basic.GoodsQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class CustomerTypeServiceTest {
 	@Autowired
   	CustomerTypeService testService;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		CustomerTypeQuery param = new CustomerTypeQuery();
		param.setCustomerType("三台");
		List<CustomerType> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(CustomerType  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		CustomerType testObj=new CustomerType();
		testObj.setCustomerType("4");
		testObj.setCode("zero");
		testObj.setId("2345");
		testObj.setMemo("zero");
		testObj.setPinyin("zero");
		testObj.setUpdaterName("zero");
		testObj.setProperty("7");
		testService.save(testObj);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		CustomerType testObj=testService.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		CustomerType testObj = testService.getById("7");
		testObj.setMemo("123");
		testService.editById(testObj);
	} 
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void deleById()
	{
		CustomerType testObj=new CustomerType();
		testService.delById("8");
	} 
}
