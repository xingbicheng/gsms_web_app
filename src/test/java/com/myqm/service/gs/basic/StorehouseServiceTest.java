package com.myqm.service.gs.basic;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.vo.gs.basic.StorehouseQuery;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class StorehouseServiceTest {
 	@Autowired
   	StorehouseService testService;
   
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		StorehouseQuery param = new StorehouseQuery();
		param.setStoreTypeId("10");
		List<Storehouse> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(Storehouse  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		Storehouse testObj=new Storehouse();
		testObj.setStoreName("8号仓库");
		testObj.setCode("8");
		testObj.setStoreTypeId("2");
		testObj.setUpdaterName("8");
		testService.save(testObj);
	}
	 
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		Storehouse testObj=testService.getById("4");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		Storehouse testObj = testService.getById("6");
		testObj.setCode("8");
		testService.editById(testObj);
	} 
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		Storehouse testObj=new Storehouse();
		testService.delById("10");
	}
}
