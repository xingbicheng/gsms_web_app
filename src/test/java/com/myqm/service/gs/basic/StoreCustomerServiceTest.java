package com.myqm.service.gs.basic;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.dao.gs.basic.StoreCustomerDao;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.vo.gs.basic.StoreCustomerQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class StoreCustomerServiceTest {

	@Autowired
	StoreCustomerService testService;
	
	@Test
	//@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testList() {
		StoreCustomerQuery param = new StoreCustomerQuery();
		param.setCustomerTypeId("2");
		List<StoreCustomer> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(StoreCustomer  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId());
		}
	}
	
	@Test
	//@Transactional
	/**测试新增数据,支持回滚*/
	public void testSave()
	{
		StoreCustomer testObj=new StoreCustomer();
		testObj.setCreaterId("2");
		testObj.setStoreId("1");
		testObj.setCustomerTypeId("1");
		testObj.setMemo("3");
		testObj.setUpdaterId("4");
		testService.save(testObj);
	}
	
	@Test
	//@Transactional
	/**测试根据id查询数据,支持回滚*/
	public void testGetById()
	{
		StoreCustomer testObj=testService.getById("7");
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}
	
	@Test
	//@Transactional
	/**测试编辑数据,支持回滚*/ 
	public void testEditById()
	{
		StoreCustomer testObj =testService.getById("2");
		testObj.setCreaterId("8");
		testService.editById(testObj);
	}
	
	@Test
    //@Transactional
    /**测试编辑数据，支持回滚*/
    public void deleById()
    {
		StoreCustomer testObj=new StoreCustomer();
		testService.delById("1");
	}
}