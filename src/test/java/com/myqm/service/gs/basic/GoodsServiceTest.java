package com.myqm.service.gs.basic;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsQuery;

@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GoodsServiceTest {
	@Autowired
   	GoodsService testService;	
 	static String goodsId;

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testAASave() {
		Goods testObj=new Goods();
		testObj.setGoodsName("超级大大龙虾");
		testObj.setGoodsType("大大虾类");
		testObj.setGoodsTypeId("8");
		testObj.setLossRate(100.00f);
		testObj.setId("10241024");
		testObj.setMemo("没有");
		testObj.setPinyin("cjddlx");
		testObj.setWarningAmount(200.00f);
		testObj.setPrice(new BigDecimal(22.2));
		//testObj.setPickUp(true);
		testObj.setUpdaterName("一只小白");
		testObj.setUnitName("大只");
		testObj.setInternal("2");
		testObj.setCode("222");
		testObj.setWarningPrice(new BigDecimal(33.3));
		testService.save(testObj);
		goodsId = testObj.getId();
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testCCGetById() {
		Goods testObj=testService.getById(goodsId);
		if (testObj!=null) {
			System.out.println("Id:"+testObj.getId());
		}
		else
		{
			System.err.println("null");
		}
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testDDEditById() {
		Goods testObj = testService.getById(goodsId);
		testObj.setGoodsName("超级小小龙虾");
		testObj.setGoodsType("小小虾类");
		testObj.setGoodsTypeId("8");
		testObj.setLossRate(100.00f);
		testObj.setId("10241024");
		testObj.setMemo("暂时没有");
		testObj.setPinyin("cjxxlx");
		testObj.setWarningAmount(200.00f);
		testObj.setPrice(new BigDecimal(22.2));
		//testObj.setPickUp(true);
		testObj.setUpdaterName("两只小白");
		testObj.setUnitName("小只");
		testObj.setInternal("3");
		testObj.setCode("333");
		testObj.setWarningPrice(new BigDecimal(33.3));
		testService.editById(testObj);
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testBBList() {
		GoodsQuery param = new GoodsQuery();
    	param.setUnitName("斤");
    	param.setCodePy("m");
    	//param.setGoodsName("墨鱼");
    	param.setUpdaterName("一只小白");
    	param.setAlternateName(null);
    	List<Goods> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(Goods  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsName());
		}
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testFFListAll() {
		GoodsQuery param = new GoodsQuery();
		List<Goods> list = testService.listAll(param); 
    	System.out.println(list.size());
    	for(Goods  testObj : list)
		{
			System.out.println("Id:"+ testObj.getId()+" "+testObj.getGoodsName());
		}
	}

	@Test
//	@Transactional
	/**测试查询所有数据,支持回滚*/
	public void testEEDelById() {
		testService.delById(goodsId);
	}

}
