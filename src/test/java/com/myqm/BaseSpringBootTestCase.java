package com.myqm;
 

import java.lang.Exception;
import org.junit.Before; 
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class) 
@SpringBootTest
public class BaseSpringBootTestCase {
	protected MockMvc mockMvc;
	@Autowired  
	private WebApplicationContext wac;  
	@Before
	public void setUp() throws Exception {
		//MockMvcBuilders使用构建MockMvc对象   （项目拦截器有效） 
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();  
		//单个类  拦截器无效
		//this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build(); 
	}



}
