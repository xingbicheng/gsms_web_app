//package com.myqm.controller.sysfun;
//
//import static org.junit.Assert.*;
// 
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.myqm.pojo.ResultResponse;
//import com.myqm.pojo.sysfun.Permission; 
// 
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class PermissionControllerTest {
//	private MockMvc mockMvc;
//	 @Autowired  
//	private WebApplicationContext wac;  
//	@Autowired
//	PermissionController testobject;
//	
//	
//	@Before
//	public void setUp() throws Exception { 
//		//MockMvcBuilders使用构建MockMvc对象   （项目拦截器有效）
//
//		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();  
//
//		//单个类  拦截器无效
//		//this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build();
//	}
//	
//	
//	//@Test
//	public void testSave() throws Exception {
//		Permission p=new Permission();
//		 
//		p.setHref("wwww");
//		p.setName("wwww");
//		p.setPermission("wwww");
//		p.setSort(11);
//		p.setType(1);
//		p.setParentId(100L);
//		testobject.save(p);
//		
//		String jsonStr = JSONObject.toJSONString(p);
//
//		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/permissions");
//		builder.contentType(MediaType.APPLICATION_JSON);
//		builder.header("token", "e9bcd690-dfb5-40e8-860d-9e228952ed73");
//		builder.content(jsonStr);
//		MvcResult result = mockMvc.perform(builder).andReturn();
//		 
//		
//		assertEquals(200, result.getResponse().getStatus());
////		
////	   String st="";
////	   st = result.getResponse().getContentAsString(); 
////	   System.out.println(st);
////	   Map<String, Class> classMap = new HashMap<String, Class>();
////		classMap.put("rows", Permission.class);
////		
////	   ResultResponse pe = JSONObject.parseObject(st,ResultResponse.class); 
////		assertNotNull(pe.getData());
////	  
//		String jsonString = JSON.toJSONString(p);  
//	    System.out.println("jsonString:" + jsonString);  
//	   // List<Permission> pe = JSON.parseArray(jsonString, Permission.class);  
//	    ResultResponse pe = JSONObject.parseObject(jsonString,ResultResponse.class); 
//	    assertNotNull(pe.getData());
//		
//	}
//
//	@Test
//	public void testGet() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testUpdate() {
//		Permission p=new Permission();
//		 
//		p.setHref("MEIMEIw");
//		p.setName("MEIMEI");
//		p.setPermission("MEIMEI");
//		p.setSort(11);
//		p.setType(1);
//		p.setParentId(100L);
//		p.setId(71L);
//		testobject.edit(p);
//		
//		String jsonStr = JSONObject.toJSONString(p);
//
//		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/permissions");
//		builder.contentType(MediaType.APPLICATION_JSON);
//		builder.header("token", "e9bcd690-dfb5-40e8-860d-9e228952ed73");
//		builder.content(jsonStr);
//		//MvcResult result = mockMvc.perform(builder).andReturn();
//		 
//		
//		//assertEquals(200, result.getResponse().getStatus());
//	}
//
//	@Test
//	public void testDelete() {
//		fail("Not yet implemented");
//	}
//
//}
