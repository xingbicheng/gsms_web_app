package com.myqm.controller.xxb.meeting;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.alibaba.fastjson.JSONObject;

import com.myqm.vo.xxb.meeting.MeetingQuery;
import com.myqm.pojo.xxb.meeting.Meeting;
import com.myqm.service.xxb.meeting.MeetingService;
import com.myqm.controller.xxb.meeting.MeetingController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MeetingControllerTest {

    @Autowired
	public MeetingService meetingService;

    @Autowired
	public MeetingController testObject;

    private MockMvc mockMvc;

    @Before
	public void setUp() throws Exception {
		 MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build();
	}

    @After
    public void tearDown() throws Exception {
    }

    @Test
	public void testSave() throws Exception {
		Meeting t = new Meeting();

		String jsonStr = JSONObject.toJSONString(t);

		//访问路径需要手动填写  如/api/dicts
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/xxx/xxx");
		builder.contentType(MediaType.APPLICATION_JSON);
		builder.header("token", "e9bcd690-dfb5-40e8-860d-9e228952ed73");
		builder.content(jsonStr);
		MvcResult result = mockMvc.perform(builder).andReturn();
		System.out.println(result);

	}

}