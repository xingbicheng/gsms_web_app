package com.myqm.controller.dev;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.myqm.pojo.dev.GenerateInput;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GenerateControllerTest {
	@Autowired
	GenerateController testObject;
	private MockMvc mockMvc;
	@Autowired  
	private WebApplicationContext wac;  
	@Before
	public void setUp() throws Exception { 
		//MockMvcBuilders使用构建MockMvc对象   （项目拦截器有效） 
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();  
		//单个类  拦截器无效
		//this.mockMvc = MockMvcBuilders.standaloneSetup(testObject).build();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave() throws Exception {
 		String tables[]={"audit_log"};  //表名
		GenerateInput t = new GenerateInput();
		t.setBeanPackageName("com.myqm.pojo.gs.basic");
		t.setDaoPackageName("com.myqm.dao.gs.basic");
		t.setServicePackageName("com.myqm.service.gs.basic");
		t.setControllerPkgName("com.myqm.controller.gs.basic");
		t.setDaotestPackageName("com.myqm.gs.test.dao"); //包
		t.setControllertestName("com.myqm.test.controller"); //包
		t.setMybatisMapperPackageName("mybatis-mappers");//目录

		for(String table : tables)
		{
			t.setTableName(table);
			String jsonStr = JSONObject.toJSONString(t); 
			MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/generate/createCode");
			builder.contentType(MediaType.APPLICATION_JSON); 
			builder.content(jsonStr);
			MvcResult result = mockMvc.perform(builder).andReturn();
			  
			byte[] outstr = result.getResponse().getContentAsByteArray();
			String srt2=new String(outstr,"UTF-8");
			System.out.println(srt2);
		}
		
	} 
}