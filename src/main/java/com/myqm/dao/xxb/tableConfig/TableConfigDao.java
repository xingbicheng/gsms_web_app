package com.myqm.dao.xxb.tableConfig;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.tableConfig.TableConfigQuery;
import com.myqm.pojo.xxb.tableConfig.TableConfig;

@Mapper
public interface TableConfigDao {

    /**
     *根据新增TableConfig,id为字符
     */
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(TableConfig record);


    /**
     *根据id删除TableConfig
     */
    //@Delete("delete from table_config where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);


    /**
     *根据id查询TableConfig
     */
    TableConfig getById(String id);

    /**
     *根据id更新TableConfig
     */
    int editById(TableConfig record);

    /**
     *分页查询所有TableConfig
     */
    List<TableConfig> list(TableConfigQuery param);

}