package com.myqm.dao.xxb.article;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.article.ArticleQuery;
import com.myqm.pojo.xxb.article.Article;

@Mapper
public interface ArticleDao {
	
	/**
    *根据新增Article,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(Article record);
    
    
   /**
	*根据id删除Article
   */
 	//@Delete("delete from article where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询Article
     */
    Article getById(String id);

    /**
     *根据id更新Article
     */ 
    int editById(Article record);

    /**
     *分页查询所有Article
     */    
    List<Article> list(ArticleQuery param); 
        
}