package com.myqm.dao.xxb.meeting;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import com.myqm.pojo.xxb.meeting.Meeting;

@Mapper
public interface MeetingDao {

    /**
     *根据新增Meeting,id为字符
     */
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(Meeting record);


    /**
     *根据id删除Meeting
     */
    //@Delete("delete from meeting where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);


    /**
     *根据id查询Meeting
     */
    Meeting getById(String id);

    /**
     *根据id更新Meeting
     */
    int editById(Meeting record);

    /**
     *分页查询所有Meeting
     */
    List<Meeting> list(MeetingQuery param);

}