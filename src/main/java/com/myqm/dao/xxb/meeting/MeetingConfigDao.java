package com.myqm.dao.xxb.meeting;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.meeting.MeetingConfigQuery;
import com.myqm.pojo.xxb.meeting.MeetingConfig;

@Mapper
public interface MeetingConfigDao {

    /**
     *根据新增MeetingConfig,id为字符
     */
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(MeetingConfig record);


    /**
     *根据id删除MeetingConfig
     */
    //@Delete("delete from meeting_config where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);


    /**
     *根据id查询MeetingConfig
     */
    MeetingConfig getById(String id);

    /**
     *根据id更新MeetingConfig
     */
    int editById(MeetingConfig record);

    /**
     *分页查询所有MeetingConfig
     */
    List<MeetingConfig> list(MeetingConfigQuery param);

}