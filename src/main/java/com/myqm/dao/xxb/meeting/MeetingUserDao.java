package com.myqm.dao.xxb.meeting;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.xxb.meeting.MeetingUserQuery;
import com.myqm.pojo.xxb.meeting.MeetingUser;

@Mapper
public interface MeetingUserDao {

    /**
     *根据新增MeetingUser,id为字符
     */
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(MeetingUser record);


    /**
     *根据id删除MeetingUser
     */
    //@Delete("delete from meeting_user where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);


    /**
     *根据id查询MeetingUser
     */
    MeetingUser getById(String id);

    /**
     *根据id更新MeetingUser
     */
    int editById(MeetingUser record);

    /**
     *分页查询所有MeetingUser
     */
    List<MeetingUser> list(MeetingUserQuery param);

}