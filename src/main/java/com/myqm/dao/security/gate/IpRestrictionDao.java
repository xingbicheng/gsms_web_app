package com.myqm.dao.security.gate;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.security.gate.IpRestriction;

@Mapper
public interface IpRestrictionDao {
	@Select("select * from sys_ip_restriction where ip = #{ip}")
	public IpRestriction findByIp(String ip);

	public void update(IpRestriction ipinfo);

	@Delete("delete from sys_ip_restriction where where ip = #{ip}")
	public void delIp(String ip);

	@Insert("insert into sys_ip_restriction(ip) values(#{ip})")
	int save(String ip);
}
