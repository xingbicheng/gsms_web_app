package com.myqm.dao.security.authorization;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.myqm.pojo.sysfun.TokenModel;

@Mapper
public interface TokenDao {
	@Insert("insert into sys_token(id,token,val, expire_time, create_time, update_time) values (replace(uuid(),'-',''), #{token}, #{val}, #{expireTime}, #{createTime}, #{updateTime})")
	int save(TokenModel model);

	TokenModel getById(Integer id);

	TokenModel getByToken(String token);

	@Update("update sys_token set val = #{val}, expire_time = #{expireTime},update_time = #{updateTime} where token = #{token}")
	int update(TokenModel model);

	@Delete("delete from sys_token where token = #{token}")
	int delete(String token);

	@Update("update sys_token set token = #{token} where id = #{id}")
	int updateToken(@Param("id") Long id, @Param("token") String token);

}
