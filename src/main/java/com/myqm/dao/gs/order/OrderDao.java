package com.myqm.dao.gs.order;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.pojo.gs.order.Order;

@Mapper
public interface OrderDao {

	/**
	 * 根据新增Order,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(Order record);

	/**
	 * 根据id删除Order
	 */
	// @Delete("delete from order where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询Order
	 */
	Order getById(String id);

	/**
	 * 根据id查询Order
	 */
	List<Order> getByIds(List<String> ids);

	/**
	 * 根据id更新Order
	 */
	int editById(Order record);


//    @Select("select store_id \n" +
//            "from gorder a\n" +
//            "left join article b on a.customer_id=b.id \n" +
//            "left join store_customer c on c.customer_type_id=b.customer_type_id\n" +
//            "where a.id=#{orderId}")
//    String getStoreIdByOrderId(@Param("orderId") String orderId);

    @Select("select company_name \n" +
            " from gorder a\n" +
            " left join article b on a.customer_id=b.id \n" +
            " left join store_customer c on c.customer_type_id=b.customer_type_id\n" +
            " left join storehouse d on d.id=store_id\n" +
            " where a.id=#{orderId}")
    String getCompanyNameByOrderId(@Param("orderId") String orderId);
	/**
	 * 分页查询所有Order
	 */
	List<Order> list(OrderQuery param);

	@Update("update gorder set state=#{state} where id =#{id}")
	int editStatedById(@Param("state") String state, @Param("id") String id);

	int auditById(@Param("auditor_id") String auditorId, @Param("id") String id, @Param("auditMemo") String auditMemo,
			@Param("audited") String audited);

	@Update("update gorder set state='1', audit_memo = '', auditor_time = null,verified='0',auditor_id =null where id =#{id}")
	int unauditById(@Param("id") String id);

    @Update("UPDATE gorder SET state=#{toState} WHERE id=#{id}")
    int changeState(@Param("id") String id,@Param("toState")String toState);



	String getNext(OrderQuery param);

	String getPre(OrderQuery param);

	 
	int setOuter(List<String> ids);
    int cancelOuter(List<String> ids);
    
    
    @Select("select store_id \n" +
            "from gorder a\n" +
            "left join article b on a.customer_id=b.id \n" +
            "left join store_customer c on c.customer_type_id=b.customer_type_id\n" +
            "where a.id=#{orderId}")
    String getStoreIdByOrderId(@Param("orderId") String orderId);

    
    
    /**
     * 根据订单明细状态修改为预采购
     * @param detailId
     */
    void batchUpdateOrderPrePurchase(List<String> detailIds);
    
    List<String> getDistinctOrderId(List<String> detailIds);

   int  addPrint(List<String> ids);
}