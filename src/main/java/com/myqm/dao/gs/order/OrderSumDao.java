package com.myqm.dao.gs.order;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderSum;
import com.myqm.pojo.gs.order.OrderTotal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderSumDao {
   List<OrderSum> sum(OrderTotal orderTotal);
}
