package com.myqm.dao.gs.order;

import java.util.List;

import com.myqm.pojo.gs.order.OrderDetailStatistic;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.vo.gs.order.OrderDetailQuery;
import com.myqm.pojo.gs.order.BatchDetailStateInfo;
import com.myqm.pojo.gs.order.OrderDetail;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface OrderDetailDao {

	/**
	 * 根据新增OrderDetail,id为字符
	 */

	int save(OrderDetail record);

	/**
	 * 根据id删除OrderDetail
	 */
	int delById(String id);

	/**
	 * 根据id删除OrderDetail
	 */
	// @Delete("delete from order_detail where order_id = #{orderId}")
	int delByOrderId(String orderId);

	/**
	 * 根据id查询OrderDetail
	 */
	OrderDetail getById(String id);

	/**
	 * 根据id更新OrderDetail
	 */
	int editById(OrderDetail record);

	/**
	 * 分页查询所有OrderDetail
	 */
	List<OrderDetail> list(OrderDetailQuery param);
	
	@Select("select * from order_detail ")
	List<OrderDetail> listAll();
	List<OrderDetail>listByOrderId(String orderId);

	int editStateById(@Param("state") String state, @Param("id") String id);
	
	int resetReceiveInfo(@Param("id") String id);
	
	int auditByOrderId(@Param("orderId") String orderId);

	int unauditByOrderId(@Param("orderId") String orderId);

	int delFlagByOrderId(@Param("orderId") String orderId);
	
	int setOuter(List<String> orderIds);
    int cancelOuter(List<String> orderIds);
	
	@Select("select count(*) from order_detail where state in ('7','8','9','a','b','c')  and order_id=#{orderId}")
	int getReceiveAmount(@Param("orderId") String orderId);
	
	@Select("select count(*) from order_detail where state!='d' and order_id=#{orderId}")
	int getdetailAmount(@Param("orderId") String orderId);
	
	@Select("select count(*) from order_detail where state='8' and order_id=#{orderId}")
	int getPartReceivedAmount(@Param("orderId") String orderId);
	
	@Update("update order_detail set receive_amount=#{receiveAmount} , receive_price=unit_price , receive_sum =#{receiveAmount}* unit_price, state = #{state} where id=#{detailId}")
	int editReceiveAmount(@Param("detailId") String detailId,@Param("receiveAmount") Double receiveAmount, @Param("state")String state);
	
	int editReceiveInfo(List<OrderDetail>orderDetails);
	
	int setReceiveInfo(List<String>detailIds);

	@Update("UPDATE order_detail SET state=#{toState} WHERE id=#{id}")
	int changeState(@Param("id") String id,@Param("toState")String toState);
	
	int batchChangeState(BatchDetailStateInfo stateInfo);
	

    @Select("select count(*) from order_detail where state=#{state} and order_id=#{orderId} and state != 'd'")
    int getCountByState(@Param("state") String state,@Param("orderId") String orderId);

    @Select("select count(*) from order_detail where order_id=#{orderId} and state != 'd'")
    int getCount(@Param("orderId") String orderId);

    @Select("select distinct(order_id) from order_detail where id=#{detailId}")
    String getOrderId(@Param("detailId")String detailId);
    
    @Update("UPDATE order_detail SET source=#{source} WHERE id=#{id}")
	int editSourceById(@Param("source")String source,@Param("id") String id);

    @Update("update order_detail set state=#{toState} " +
            "where id in(select order_detail_id from order_detail_purchase " +
            "where purchase_detail_id in " +
            "(select purchase_detail_id from stroehouse_in " +
            "where id=#{stroehouseId}))")
    int setStateByStroehouseInId(@Param("stroehouseId") String stroehouseId,
                                 @Param("toState") String toState );

    List<OrderDetail> getDetailByOrderId(String orderId);
    
    @Update("update order_detail set in_id=#{inId},back_provider_id=#{backProviderId} where id=#{detailId}")
    int setProviderBackInfo(@Param("detailId")String detailId,@Param("inId")String inId,
    		@Param("backProviderId")String backProviderId);
    
    @Update("update order_detail set in_id=#{inId},new_in_id=#{newInId},"
    		+ "storehouse_back_id=#{storehouseBackId} where id=#{detailId}")
    int setStoreBackInfo(@Param("detailId")String detailId,@Param("inId")String inId,
    		@Param("newInId")String newInId,@Param("storehouseBackId")String storehouseBackId);

    @Update("update order_detail set in_id=#{inId} , loss_id=#{lossId} where id=#{detailId}")
    int setLossInfo(@Param("detailId")String detailId,@Param("inId")String inId,
    		@Param("lossId")String lossId);

    List<OrderDetailStatistic> getOrderDetailStatistic(String orderId);
    
    List<OrderDetail> getTypeDetailByOrderId(String orderId);
    
    
     
    int setReceiveByOrderId(List<String> ids);

}