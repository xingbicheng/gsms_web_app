package com.myqm.dao.gs.order;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.gs.order.Car;

@Mapper
public interface CarDao {
	
	/**
	 * 根据客户删除
	 * @param customerId 客户ID
	 * @return 删除记录数量
	 */
	@Delete("delete from car where customer_id = #{customerId}")
	int delById(String customerId);

	/**
	 * 获取商品数量
	 * @param customerId 客户ID
	 * @param internaltradeId 商品类型Id
	 * @return 商品数量
	 */
	@Select("select * from car where customer_id = #{customerId} and goods_id = #{goodsId}")
	Car get(@Param("customerId")String customerId, @Param("goodsId")String goodsId);
	
	/**
	 * 新增记录
	 * @param record
	 * @return 新增记录数量
	 */
	int add(Car record);

    /**
     * 通过Id编辑记录
     * @param record
     * @return 编辑记录数量
     */
    int editById(Car record);
    
    /**
     * 通过用户Id获取购物车列表
     * @param customerId 用户Id
     * @return 记录数量列表
     */
    List<Car> listByCustomerId(@Param("customerId")String customerId);
    
	/**
	 * 删除商品
	 * @param customerId 客户ID
	 * @param internaltradeId 商品类型Id
	 * @return 删除记录数量
	 */
	@Delete("delete from car where customer_id = #{customerId} and goods_id = #{goodsId}")
    int del(@Param("customerId")String customerId, @Param("goodsId")String goodsId);
}
