package com.myqm.dao.gs.order;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.vo.gs.order.OrderDetailQuery;
import com.myqm.pojo.gs.order.OrderDetail;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface WebOrderDetailDao {

	/**
	 * 根据新增OrderDetail,id为字符
	 */
	int delByOrderId(String orderId);

	int save(OrderDetail record);

	/**
	 * 根据id查询OrderDetail
	 */
	OrderDetail getById(String id);

	/**
	 * 分页查询所有OrderDetail
	 */
	List<OrderDetail> list(OrderDetailQuery param);

	int saveAll(List<OrderDetail> list);
}