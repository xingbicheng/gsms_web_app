package com.myqm.dao.gs.order;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.pojo.gs.order.Order;

@Mapper
public interface WebOrderDao {

	/**
	 * 根据新增Order,id为字符
	 */
	int save(Order record);

	//@Delete("delete from web_order where id = #{id,jdbcType=CHAR}")
	int delById(String id);

	/**
	 * 根据id查询Order
	 */
	Order getById(String id);

	/**
	 * 分页查询所有Order
	 */
	List<Order> list(OrderQuery param);

	String getNext(OrderQuery param);

	String getPre(OrderQuery param);

	int saveAll(List<Order> list);
}