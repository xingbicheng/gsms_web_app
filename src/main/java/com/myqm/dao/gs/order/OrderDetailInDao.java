package com.myqm.dao.gs.order;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.gs.order.OrderDetailInQuery;
import com.myqm.pojo.gs.in.CountOrderDetailStateByIn;
import com.myqm.pojo.gs.order.OrderDetailIn;

@Mapper
public interface OrderDetailInDao {
	
	/**
    *根据新增OrderDetailIn,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(OrderDetailIn record);
    
    
   /**
	*根据id删除OrderDetailIn
   */
 	//@Delete("delete from order_detail_in where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询OrderDetailIn
     */
    OrderDetailIn getById(String id);

    /**
     *根据id更新OrderDetailIn
     */ 
    int editById(OrderDetailIn record);

    /**
     *分页查询所有OrderDetailIn
     */    
    List<OrderDetailIn> list(OrderDetailInQuery param); 

    List<OrderDetailIn> getByOrderId(String orderId);

    int delByOrderId(String orderId);
    
    @Delete("DELETE from order_detail_in where in_id in (select id from stroehouse_in where purchase_detail_id = #{id})")
    int delByPurchaseDetailId(String id);
   
     
    
  
}