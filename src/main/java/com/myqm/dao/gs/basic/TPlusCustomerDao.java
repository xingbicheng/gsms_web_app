package com.myqm.dao.gs.basic;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.basic.TPlusCustomer;
import com.myqm.pojo.gs.basic.TPlusStorehouse;

@Mapper
public interface TPlusCustomerDao {
	int editCById(TPlusCustomer tPlusCustomer);
	int editCByIdType(TPlusCustomer tPlusCustomer);
	int saveCustomer(TPlusCustomer tPlusCustomer);
	int saveCustomerType(TPlusCustomer tPlusCustomer);
	int editPById(TPlusCustomer tPlusCustomer);
	int editPByIdType(TPlusCustomer tPlusCustomer);
	int saveProviderType(TPlusCustomer tPlusCustomer);
	int saveProvider(TPlusCustomer tPlusCustomer);
}
