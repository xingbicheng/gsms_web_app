package com.myqm.dao.gs.basic;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.vo.gs.basic.CustomerQuery;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface CustomerDao {

	/**
	 * 根据新增Customer,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(Customer record);

	/**
	 * 根据id删除Customer
	 */
	// @Delete("delete from article where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询Customer
	 */
	Customer getById(String id);

	/**
	 * 根据id更新Customer
	 */
	int editById(Customer record);

	/**
	 * 分页查询所有Customer
	 */
	List<Customer> list(CustomerQuery param);


}