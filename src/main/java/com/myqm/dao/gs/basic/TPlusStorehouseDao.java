package com.myqm.dao.gs.basic;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.basic.TPlusStorehouse;

@Mapper
public interface TPlusStorehouseDao {

	int editById(TPlusStorehouse tPlusCustomer);

	int saveStorehouse(TPlusStorehouse tPlusCustomer);
	
	
}
