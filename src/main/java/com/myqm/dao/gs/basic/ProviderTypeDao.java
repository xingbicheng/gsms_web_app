package com.myqm.dao.gs.basic;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.gs.basic.ProviderTypeQuery;
import com.myqm.pojo.gs.basic.ProviderType;

@Mapper
public interface ProviderTypeDao {

	/**
	 * 根据新增ProviderType,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(ProviderType record);

	/**
	 * 根据id删除ProviderType
	 */
	// @Delete("delete from provider_type where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询ProviderType
	 */
	ProviderType getById(String id);

	/**
	 * 根据id更新ProviderType
	 */
	int editById(ProviderType record);

	/**
	 * 分页查询所有ProviderType
	 */
	List<ProviderType> list(ProviderTypeQuery param);

}