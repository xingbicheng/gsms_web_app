package com.myqm.dao.gs.basic;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.vo.gs.basic.SysSetQuery;

@Mapper
public interface SysSetDao {
	
	/**
	 * 根据id查询SysSet
	 */
	SysSet getById(String id);

	/**
	 * 根据id更新SysSet
	 */
	int editById(SysSet record);

	/**
	 * 分页查询所有StorehouseType
	 */
	List<SysSet> list(SysSetQuery param);

}
