package com.myqm.dao.gs.basic;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.gs.basic.GoodsTypeQuery;
import com.myqm.pojo.gs.basic.GoodsType;

@Mapper
public interface GoodsTypeDao {

	/**
	 * 根据新增GoodsType,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(GoodsType record);

	/**
	 * 根据id删除GoodsType
	 */
	// @Delete("delete from goods_type where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询GoodsType
	 */
	GoodsType getById(String id);

	/**
	 * 根据id更新GoodsType
	 */
	int editById(GoodsType record);

	/**
	 * 分页查询所有GoodsType
	 */
	List<GoodsType> list(GoodsTypeQuery param);

}