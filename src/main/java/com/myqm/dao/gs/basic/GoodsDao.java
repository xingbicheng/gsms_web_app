package com.myqm.dao.gs.basic;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.*;
import com.github.pagehelper.PageInfo;

import com.myqm.vo.gs.basic.GoodsQuery;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.basic.TPlusGoods;
import org.springframework.security.access.method.P;

@Mapper
public interface GoodsDao {

	/**
	 * 根据新增Goods,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(Goods record);

	/**
	 * 根据id删除Goods
	 */
	// @Delete("delete from goods where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询Goods
	 */
	Goods getById(String id);

	/**
	 * 根据id更新Goods
	 */
	int editById(Goods record);

	/**
	 * 分页查询所有Goods
	 */
	List<Goods> list(GoodsQuery param);

	@Update("update goods set price=#{price} where id=#{id}")
	int updatePrice(@Param("id")String id,@Param("price") double price);

}