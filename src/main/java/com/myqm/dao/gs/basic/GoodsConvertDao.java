package com.myqm.dao.gs.basic;

import java.util.List;

import com.myqm.pojo.gs.basic.GoodsConvert;
import com.myqm.vo.gs.basic.GoodsConvertQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GoodsConvertDao {

	/**
	 * 根据新增GoodsConvert,id为字符
	 */
	 
	int save(GoodsConvert record);

	/**
	 * 根据id删除GoodsConvert
	 */
	// @Delete("delete from goods_convert where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询GoodsConvert
	 */
	GoodsConvert getById(String id);

	/**
	 * 根据id更新GoodsConvert
	 */
	int editById(GoodsConvert record);

	/**
	 * 分页查询所有GoodsConvert
	 */
	List<GoodsConvert> list(GoodsConvertQuery param);

	int saveall(List<GoodsConvert> record);

}