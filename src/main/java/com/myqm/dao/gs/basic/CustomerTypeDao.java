package com.myqm.dao.gs.basic;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.vo.gs.basic.CustomerTypeQuery;

import org.apache.ibatis.annotations.Param;

@Mapper
public interface CustomerTypeDao {

	/**
	 * 根据新增CustomerType,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(CustomerType record);

	/**
	 * 根据id删除CustomerType
	 */
	// @Delete("delete from customer_type where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询CustomerType
	 */
	CustomerType getById(String id);

	/**
	 * 根据id更新CustomerType
	 */
	int editById(CustomerType record);

	/**
	 * 分页查询所有CustomerType
	 */
	List<CustomerType> list(CustomerTypeQuery param);

}