package com.myqm.dao.gs.basic;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.gs.basic.AuditLogQuery;
import com.myqm.pojo.gs.basic.AuditLog;

@Mapper
public interface AuditLogDao {
	
	/**
    *根据新增AuditLog,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(AuditLog record);
    
    
   /**
	*根据id删除AuditLog
   */
 	//@Delete("delete from audit_log where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询AuditLog
     */
    AuditLog getById(String id);

    /**
     *根据id更新AuditLog
     */ 
    int editById(AuditLog record);

    /**
     *分页查询所有AuditLog
     */    
    List<AuditLog> list(AuditLogQuery param); 
        
}