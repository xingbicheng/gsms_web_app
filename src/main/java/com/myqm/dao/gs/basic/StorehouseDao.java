package com.myqm.dao.gs.basic;

import java.util.List;

import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.vo.gs.basic.StorehouseQuery;
import org.apache.ibatis.annotations.*;
import com.github.pagehelper.PageInfo;

@Mapper
public interface StorehouseDao {

	/**
	 * 根据新增Storehouse,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(Storehouse record);

	/**
	 * 根据id删除Storehouse
	 */
	// @Delete("delete from storehouse where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询Storehouse
	 */
	Storehouse getById(String id);

	/**
	 * 根据id更新Storehouse
	 */
	int editById(Storehouse record);

	/**
	 * 分页查询所有Storehouse
	 */
	List<Storehouse> list(StorehouseQuery param);

	@Select("select is_center from storehouse where id=#{id}")
	String getIsCenter(@Param("id") String id);

}