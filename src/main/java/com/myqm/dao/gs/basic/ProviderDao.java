package com.myqm.dao.gs.basic;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.gs.basic.ProviderQuery;
import com.myqm.pojo.gs.basic.Provider;

@Mapper
public interface ProviderDao {

	/**
	 * 根据新增Provider,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(Provider record);

	/**
	 * 根据id删除Provider
	 */
	// @Delete("delete from provider where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询Provider
	 */
	Provider getById(String id);

	/**
	 * 根据id更新Provider
	 */
	int editById(Provider record);

	/**
	 * 分页查询所有Provider
	 */
	List<Provider> list(ProviderQuery param);

}