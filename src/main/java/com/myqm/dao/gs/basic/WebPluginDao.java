package com.myqm.dao.gs.basic;

import java.util.List;

import com.myqm.pojo.gs.basic.WebPlugin;
import com.myqm.vo.gs.basic.WebPluginQuery;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WebPluginDao {

	/**
	 * 根据新增WebPlugin,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(WebPlugin record);

	/**
	 * 根据id删除WebPlugin
	 */
	// @Delete("delete from web_plugin where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询WebPlugin
	 */
	WebPlugin getById(String id);

	/**
	 * 根据id更新WebPlugin
	 */
	int editById(WebPlugin record);

	/**
	 * 分页查询所有WebPlugin
	 */
	List<WebPlugin> list(WebPluginQuery param);

}