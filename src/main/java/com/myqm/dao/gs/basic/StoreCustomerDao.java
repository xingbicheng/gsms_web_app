package com.myqm.dao.gs.basic;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.vo.gs.basic.StoreCustomerQuery;

@Mapper
public interface StoreCustomerDao {
	/**
	    *根据新增StoreCustomer,id为字符
	    */
		//@Options(useGeneratedKeys = true, keyProperty = "id")
	    int save(StoreCustomer record);
	    
	    
	   /**
		*根据id删除StoreCustomer
	   */
	 	//@Delete("delete from StoreCustomer where id = #{id,jdbcType=VARCHAR}")
	    int delById(String id);
	     
	    
	    /**
	     *根据id查询StoreCustomer
	     */
	    StoreCustomer getById(String id);

	    /**
	     *根据id更新StoreCustomer
	     */ 
	    int editById(StoreCustomer record);

	    /**
	     *分页查询所有StoreCustomer
	     */    
	    List<StoreCustomer> list(StoreCustomerQuery param);
}
