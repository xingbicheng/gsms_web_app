package com.myqm.dao.gs.basic;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.basic.TPlusGoods;

@Mapper
public interface TPlusGoodsDao {
	
	int editById(TPlusGoods tPlusGoods);
	int editByIdType(TPlusGoods tPlusGoods);
	int saveGoods(TPlusGoods tPlusGoods);
	int saveGoodsType(TPlusGoods tPlusGoods);
}
