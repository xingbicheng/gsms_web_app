package com.myqm.dao.gs.basic;

import java.util.List;

import com.myqm.pojo.gs.basic.StorehouseType;
import com.myqm.vo.gs.basic.StorehouseTypeQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StorehouseTypeDao {

	/**
	 * 根据新增StorehouseType,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(StorehouseType record);

	/**
	 * 根据id删除StorehouseType
	 */
	// @Delete("delete from storehouse_type where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询StorehouseType
	 */
	StorehouseType getById(String id);

	/**
	 * 根据id更新StorehouseType
	 */
	int editById(StorehouseType record);

	/**
	 * 分页查询所有StorehouseType
	 */
	List<StorehouseType> list(StorehouseTypeQuery param);

}