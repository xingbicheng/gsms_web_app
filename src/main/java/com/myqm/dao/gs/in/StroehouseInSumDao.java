package com.myqm.dao.gs.in;

import com.myqm.pojo.gs.in.StroehouseInSum;
import com.myqm.pojo.gs.in.StroehouseInTotal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StroehouseInSumDao {
    List<StroehouseInSum> sum(StroehouseInTotal stroehouseInTotal);
}
