package com.myqm.dao.gs.in;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.gs.in.StoreLossTypeQuery;
import com.myqm.pojo.gs.in.StoreLossType;

@Mapper
public interface StoreLossTypeDao {
	
	/**
    *根据新增StoreLossType,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(StoreLossType record);
    
    
   /**
	*根据id删除StoreLossType
   */
 	//@Delete("delete from store_loss_type where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询StoreLossType
     */
    StoreLossType getById(String id);

    /**
     *根据id更新StoreLossType
     */ 
    int editById(StoreLossType record);

    /**
     *分页查询所有StoreLossType
     */    
    List<StoreLossType> list(StoreLossTypeQuery param); 
        
}