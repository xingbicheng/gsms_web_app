package com.myqm.dao.gs.in;
import java.util.List;

import org.apache.ibatis.annotations.*;
import com.github.pagehelper.PageInfo;
import com.myqm.vo.gs.in.StoreLossQuery;
import com.myqm.pojo.gs.in.StoreLoss;
import org.springframework.security.access.method.P;

@Mapper
public interface StoreLossDao {
	
	/**
    *根据新增StoreLoss,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(StoreLoss record);
    
    
   /**
	*根据id删除StoreLoss
   */
 	//@Delete("delete from store_loss where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询StoreLoss
     */
    StoreLoss getById(String id);

    /**
     *根据id更新StoreLoss
     */ 
    int editById(StoreLoss record);

    /**
     *分页查询所有StoreLoss
     */    
    List<StoreLoss> list(StoreLossQuery param);

    /**
     * 验证 是否一键损耗
     * @return
     */
    @Select("select * from store_loss where in_id=#{storeId} and loss_type=1")
    StoreLoss cheakOneKeyLoss(@Param("storeId") String storeId);
        
}