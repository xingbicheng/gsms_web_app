package com.myqm.dao.gs.in;

import com.myqm.dto.gs.in.SetTransferInfo;
import com.myqm.pojo.gs.in.StoreAndOrderDet;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.purchase.ProviderInfo;
import com.myqm.vo.gs.in.StroehouseInQuery;


import org.apache.ibatis.annotations.*;

import java.sql.Date;
import java.util.List;


@Mapper
public interface StroehouseInDao {
    /**
     * 根据新增StroehouseIn,id为字符
     */
    int save(StroehouseIn record);

    /**
     * 根据id删除StroehouseIn
     */
    int delById(String id);

    /**
     * 根据id查询StroehouseIn
     */
    StroehouseIn getById(String id);



    List<StroehouseIn> getByStoreIdAndDetailId(StoreAndOrderDet storeAndOrderDet);

    @Select("SELECT * FROM stroehouse_in WHERE from_storehouse_id=#{fromId}")
    List<StroehouseIn> getByFromId(@Param("fromId") String fromId);

    /**
     * 检查是否存在这条信息
     */
    StroehouseIn checkHas(String id);
    /**
     * 根据id更新StroehouseIn
     */
    int editById(StroehouseIn record);

    /**
     *  查询所有StroehouseIn
     */
    List<StroehouseIn> list(StroehouseInQuery param);

    /**
     * 设置供应商信息
     * @param providerInfo 供应商信息
     * @return
     */
    int setProviderInfo(ProviderInfo providerInfo);

    /**
     * 设置中央库调入信息
     * @param setInfo
     * @return
     */
    int setTransferInfo(SetTransferInfo setInfo);

    /**
     * 取消入库
     */
    int reInStorehouse(String purchaseDetailId);
    /**
     * 一键入库信息
     * @param Ids
     * @return
     */
    int inStorehouse(String id);

    List<StroehouseIn> uses(String goodsid);

    List<StroehouseIn>getProviderByPDId(@Param("purchaseDetailId")String purchaseDetailId,
    		@Param("storehouseId")String storehouseId);

    /**
     * 设置中央库调入信息
     * @param setInfo
     * @return
     */
    @Update("update stroehouse_in set available_amount = available_amount - #{amount} where id = #{id}")
    int setFromTransferInfo(@Param("id") String id, @Param("amount") double amount);


    List<StroehouseIn> getByIds(List<String> ids);
    @Update("update stroehouse_in set surplus = #{surplus, jdbcType=DOUBLE}, available_amount = "
            + "#{available, jdbcType=DOUBLE} where id = #{id, jdbcType=CHAR}")
    int calculation(@Param("surplus")Double surplus,
                    @Param("available")Double available, @Param("id")String id);

    @Delete("delete from stroehouse_in where purchase_detail_id=#{puchaseDetailId,jdbcType=CHAR}")
    int delByPurchaseId(@Param("puchaseDetailId") String puchaseDetailId);

    /**
     *  通过 fromInId删除stroehouse_back
     */
    @Delete("delete from stroehouse_back where form_in_id=#{fromInId,jdbcType=CHAR}")
    int stroehouseBackdelByFromInId(@Param("fromInId") String fromInId);
    @Select("select id from stroehouse_in where purchase_detail_id=#{detailId,jdbcType=CHAR}")
    String getByPurchaseDetailId(@Param("detailId") String detailId);
    List<StroehouseIn> getAllByDetailId(String purchaseDetailId);
    /**
     * 更改状态
     */
    @Update("update stroehouse_in set returnable_amount=returnable_amount - #{amount}, return_amount =return_amount + #{amount}  where id=#{inId}")
    int setReturnAmount(@Param("inId") String inId,@Param("amount") double amount);
    
    
    @Update("update stroehouse_in set returnable_amount= returnable_amount + #{amount}, return_amount = return_amount - #{amount}  where id=#{inId}")
    int resetReturnAmount(@Param("inId") String inId,@Param("amount") double amount);
   
    
    
    @Update("UPDATE stroehouse_in SET state=#{toState} WHERE purchase_detail_id=#{detailId}")
    int changeStateByDetailId(@Param("detailId") String detailId,@Param("toState") String toState);

    @Update("UPDATE stroehouse_in SET state=#{toState} WHERE id=#{id}")
    int changeStateById(@Param("id") String id,@Param("toState") String toState);

    @Select("SELECT count(*) from stroehouse_in where state='1' and purchase_detail_id=#{purchaseId}")
    int countAuditedIn(@Param("purchaseId") String purchaseId);
    
    
    @Select("SELECT count(*) from stroehouse_in where state='2' and purchase_detail_id=#{purchaseId}")
    int countIn(@Param("purchaseId") String purchaseId);
    
    

    @Update("update stroehouse_in set storehouse_id=#{stroehouseId} where purchase_detail_id=#{purchaseDetailId}")
    int editStroehouseIdByPD(@Param("purchaseDetailId")String purchaseDetailId,@Param("stroehouseId")String stroehouseId);

    @Update("update stroehouse_in set surplus=surplus-#{outerAmount},available_amount=available_amount-#{outerAmount} where id=#{inId}")
    int setOuterByInId(@Param("inId")String inId,@Param("outerAmount") double outerAmount);

    @Update("update stroehouse_in set surplus=surplus+#{outerAmount},available_amount=available_amount-#{outerAmount} where id=#{inId}")
    int reOuterByInId(@Param("inId")String inId,@Param("outerAmount") double outerAmount);

    @Update("update stroehouse_in set purchase_date= #{purchaseDate} where purchase_detail_id=#{purchaseDetailId}")
    int setPuchaseDate(@Param("purchaseDetailId")String purchaseDetailId,@Param("purchaseDate")Date  purchaseDate );

    @Update("update stroehouse_in set purchase_date= #{purchaseDate} where purchase_detail_id in (select id from purchase_order_detail where order_id = #{purchaseOrderId})")
    int setPuchaseDateByPOrderId(@Param("purchaseOrderId")String orderId,@Param("purchaseDate")Date  purchaseDate );
        
    int addPrint(List<String> ids);

    List<StroehouseIn> listByIds(List<String> ids); 
}