package com.myqm.dao.gs.purchase;

import com.myqm.pojo.gs.purchase.PurchaseOrderSum;
import com.myqm.pojo.gs.purchase.PurchaseOrderTotal;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PurchaseOrderSumDao {
    List<PurchaseOrderSum> sum(PurchaseOrderTotal purchaseOrderTotal);
}
