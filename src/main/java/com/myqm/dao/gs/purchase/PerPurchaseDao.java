package com.myqm.dao.gs.purchase;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.pojo.gs.purchase.PrePurchaseDetail;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;

@Mapper
public interface PerPurchaseDao {
	List<PrePurchaseDetail> preList(PerPurchaseQuery param);

}
