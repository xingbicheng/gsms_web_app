package com.myqm.dao.gs.purchase;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.gs.purchase.PurchaseOderClassStatistics;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.pojo.gs.purchase.StoreCustomerType;

@Mapper
public interface PlannedStorageDao {

 	int updatePurchaseDetailPrice(PurchaseOrderDetail detail);
 
 	PurchaseOrderDetail getAveragePriceByPurchaseDetail(String pdid);
	
	
	//int setPurchaseDetailAverPrice(String purchaseOrderDetailId);

	List<PurchaseOderClassStatistics> statisticsGroupByStore(String detailId);

	@Delete("delete from stroehouse_in where purchase_detail_id = #{detailId}")
	int delStoreIn(String detailId);

}
