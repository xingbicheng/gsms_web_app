package com.myqm.dao.gs.purchase;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.purchase.CustomerTypeStorehouse;
import com.myqm.pojo.gs.purchase.OrderDetailPurchase;
import com.myqm.pojo.gs.purchase.PrePurchaseCount;
import com.myqm.pojo.gs.purchase.PurchaseOderClassStatistics;
import org.springframework.security.access.method.P;

@Mapper
public interface OrderDetailPurchaseDao {
	/**
	 * 批量插入
	 */
	int batchSave(List<OrderDetailPurchase> record);

	@Delete("delete from order_detail_purchase where purchase_detail_id = #{purchaseDetailId,jdbcType=CHAR}")
    int delByPDetailId(@Param("purchaseDetailId") String purchaseDetailId);

	//修改采购单明细为预采购状态
	int batchUpdateOrderDetailPrePurchase(List<String> orderDetailIds);

	int batchUnupdateOrderDetailPrePurchase(String pDetailId);

	List<OrderDetail> getOrderDetailByPDId(String pDetailId);

	int setOrderPrePurchaseByOrderDetailIds(List<String> orderDetailIds);

	// 根据采购单明细id 获取涉及到的订单为预采购状态 的 所有订单id
	List<String> getPrePurchaseOrderIdByPurchaseDetailIds(String pDetailId);

	// 统计订单下所有明细为预采用状态
	List<PrePurchaseCount> countOrderIdPrePurchase(List<String> orderIds);

	// 清除订单状态为预采购
	//int clearOrderPrePurchase(List<String> orderIds);

	/**
	 * 查询所有OrderDetailPurchase
	 */
	List<OrderDetailPurchase> list(@Param("orderDetailId") String orderDetailId, @Param("pDetailId") String pDetailId);

	/*
	 * 采购单明细分用户类型统计
	 */

	List<PurchaseOderClassStatistics> statisticsGroupByCustomerType(@Param("orderDetailId") String orderDetailId);
	@Select("select purchase_detail_id from order_detail_purchase where order_detail_id=#{orderDetailId}")
	String getPDByODId(@Param("orderDetailId") String orderDetailId);
	
	
	CustomerTypeStorehouse getCustomerTypeStorehouse(@Param("id") String customerId);

	@Select("select order_detail_id from order_detail_purchase where purchase_detail_id =#{id} ")
	List<String> getOrderDetailIdsByPdId(@Param("id") String id);
	
	 
	
	@Select(" select store_id from store_customer where  customer_type_id in "+
			"(select customer_type_id from article where id in "+
			"(select customer_id from gorder where id in "+
			"(select order_id from order_detail where id =  #{id})))")
	
	String getStorehouseIdByPdId(@Param("id") String id);
}