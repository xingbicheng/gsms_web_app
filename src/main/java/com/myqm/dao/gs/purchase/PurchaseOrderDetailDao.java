package com.myqm.dao.gs.purchase;

import java.util.List;

import com.myqm.vo.gs.purchase.OrderAndDetailRes;
import com.myqm.vo.gs.purchase.PurchaseOrderDetailQuery;

//import com.myqm.vo.gs.purchase.OrderInfoQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import org.springframework.security.access.method.P;

@Mapper
public interface PurchaseOrderDetailDao {

	/**
	 * 根据新增PurchaseOrderDetail,id为字符
	 */
	int save(PurchaseOrderDetail record);

	/**
	 * 根据明细id删除采购单明细
	 */
	int delById(String id);


	/**
	 * 获取单条采购单明细
	 * 
	 * @param detailId
	 * @return
	 */
	PurchaseOrderDetail getById(String detailId);

	/**
	 * 根据采购单id删除采购单明细
	 */
	int delByOrderId(String orderId);

	/**
	 *
	 * @param pDetailId
	 * @return
	 */
	@Update("update purchase_order_detail set loss_amount=#{lossAmount}" +
			" where id=#{id,jdbcType=CHAR}")
	int editLoss(@Param("id") String id,@Param("lossAmount") Double lossAmount);

    int delByPDetailId(String pDetailId);
	/**
	 * 根据采购单id标记删除采购单明细
	 * 
	 * @param orderId
	 * @return
	 */
	@Update("update purchase_order_detail set state ='d' where order_id = #{orderId}")
	int delFlagByOrderId(String orderId);

    /**
     * 改变细节状态
     * @param id
     * @param toState
     * @return
     */
	@Update("UPDATE purchase_order_detail SET state=#{toState} WHERE id=#{id}")
            int changeState(@Param("id") String id, @Param("toState") String toState);
	/**
	 * list查询
	 * 
	 * @param recod
	 * @return
	 */
	List<PurchaseOrderDetail> list(PurchaseOrderDetailQuery orderId);

	/**
	 * 根据采购单id查询采购单明细
	 */
	List<PurchaseOrderDetail> getByOrderId(String orderId);
	
	/**
	 * 根据计划采购单id查询采购单明细
	 */
	List<PurchaseOrderDetail> getByPlanOrderId(String orderId);
	

	/**
	 * 根据id更新PurchaseOrderDetail
	 */
	int editById(PurchaseOrderDetail record);

	/**
	 * 审核通过采购单明细
	 * 
	 * @param id
	 * @param memo
	 * @return
	 */
	@Update("update purchase_order_detail set verified = '1', state = '1', memo = #{memo} where id = #{id}")
	int auditedById(@Param("id") String id, @Param("memo") String memo);

	
	/**
	 * 审核入库计划单
	 * @param id
	 * @param memo
	 * @return
	 */
	@Update("update purchase_order_detail set state = '1'  where id = #{purchaseDetailId}")
	int auditedInById(@Param("purchaseDetailId") String purchaseDetailId);

	
	
	/**
	 * 审核不通过采购单明细
	 * 
	 * @param id 
	 * @return
	 */
	@Update("update purchase_order_detail set verified = '0', memo = #{memo},audit_time=now() where id = #{id}")
	int notAuditedById(@Param("id") String id, @Param("memo") String memo);

	/**
	 * 弃审采购单明细
	 * 
	 * @param id
	 * @return
	 */
	@Update("update purchase_order_detail set verified = null, state = '0', memo = null where id = #{id}")
	int unauditedById(@Param("id") String id);
	/**
	 * 弃核入库计划单
	 * @param id 
	 * @return
	 */
	@Update("update purchase_order_detail set state = '0'  where id = #{purchaseDetailId}")
	int unauditedInById(@Param("purchaseDetailId") String purchaseDetailId);

	@Update("update purchase_order_detail set statistic_amount=#{statisticAmount} where id={id}")
	int setStatisticAmount(@Param("id") String id,@Param("statisticAmount") double statisticAmount);
	 
	
	@Select("select count(*) from purchase_order_detail where order_id =  #{orderId} and state = #{state} ")
	int countByState(@Param("orderId") String orderId,@Param("state") String state);
	
}