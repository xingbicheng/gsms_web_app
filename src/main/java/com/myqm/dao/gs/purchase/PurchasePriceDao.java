package com.myqm.dao.gs.purchase;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;


@Mapper
public interface PurchasePriceDao {

	/**
	 * 根据新增PurchasePrice,id为字符
	 */
	// @Options(useGeneratedKeys = true, keyProperty = "id")
	int save(PurchasePrice record);

	/**
	 * 根据id删除PurchasePrice
	 */
	// @Delete("delete from PurchasePrice where id = #{id,jdbcType=VARCHAR}")
	int delById(String id);

	/**
	 * 根据id查询PurchasePrice
	 */
	PurchasePrice getById(String id);

	/**
	 * 根据id更新PurchasePrice
	 */
	int editById(PurchasePrice record);

	/**
	 * 分页查询所有PurchasePrice
	 */
	List<PurchasePrice> list(PurchasePriceQuery param);

	int editByIds(List<PurchasePrice> record);

	int saveAll(List<PurchasePrice> record);

	int delFlagById(@Param("id") String state);
	
	@Update("update purchase_price set price = #{price}, state ='3' where id = #{id} ")
	int  priceById(@Param("id") String id, @Param("price")  BigDecimal price);

	@Update("update purchase_price set state=#{state} where id =#{id}")
	int editStatedById(@Param("state") String state, @Param("id") String id);	
}