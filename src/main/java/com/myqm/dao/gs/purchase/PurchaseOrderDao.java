package com.myqm.dao.gs.purchase;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.gs.purchase.PurchaseOrderQuery;
import com.myqm.pojo.gs.purchase.PurchaseLinePrint;
import com.myqm.pojo.gs.purchase.PurchaseOrder;

@Mapper
public interface PurchaseOrderDao {

	/**
	 * 根据新增PurchaseOrder,id为字符
	 */
	int save(PurchaseOrder record);

	/**
	 * 根据id单纯地删除PurchaseOrder
	 */
	int delById(String id);

	@Update("update purchase_order set state ='d' where id = #{id}")
	int delFlagById(String id);

	/**
	 * 根据id查询PurchaseOrder
	 */
	PurchaseOrder getById(String id);

	/**
	 * 根据id更新PurchaseOrder
	 */
	int editById(PurchaseOrder record);

	/**
	 * 修改采购单采购人id和时间信息
	 */
	@Update("update purchase_order set purchaser_id=#{purchaserId,jdbcType=CHAR}," +
			"purchase_date=#{purchaserDate,jdbcType=DATE}" +
			" where id=#{id,jdbcType=CHAR}")
	int editPurchaserIdAndMore(@Param("id") String id,@Param("purchaserId") String purchaserId, @Param("purchaserDate") Date purchaserDate);

	/**
	 * 分页查询所有PurchaseOrder
	 */
	List<PurchaseOrder> list(PurchaseOrderQuery param);

	/**
	 * 获取上一条Id
	 */
	String pre(PurchaseOrderQuery param);

	/**
	 * 获取下一条Id
	 */
	String next(PurchaseOrderQuery param);
	
	
	@Update("update purchase_order set state =#{state} where id = #{id}")
	int updateStateById(@Param("id") String id,@Param("state") String state);
	
	 
	@Update("update  purchase_order set print_count = print_count + 1 where id = #{id }") 
  	int addPrint(String id) ;
	
	List<PurchaseLinePrint> getPrintDetail(String id);
}