package com.myqm.dao.gs.tplus;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myqm.pojo.gs.in.CountOrderDetailStateByIn;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.TPlusGoodsLine;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.tplus.TPlusProvider;
import com.myqm.vo.gs.in.StroehouseInQuery;
@Mapper
public interface TPlusInDao {
    int setTplusState(@Param("ids")List<String> ids, @Param("tplusNum")String tplusNum);
    List<StroehouseIn> listByIds(List<String> ids);
    
    
    
    
    int setInLockByInIds(List<String> ids);
    //状态（d删除 1下单，2审核通过，3预采购、4采购 5入库 6出库 7收货8退货不收入9 退货到中央库房 a 退货到中央库（商品留原库 ）b 退货到供应商 c 计入损耗 f Tplus导入锁定 e导出
    @Update("update order_detail set state = 'f' where (state = '7' or state = '8' or state = '9' or state = 'a' or state = 'b' or state = 'c')"
    		+ " and id in(select detail_id from order_detail_in where in_id = #{stroehouoseInId})")
    int setOrderDetailLockByInId(String stroehouoseInId);
  
    /**
     *  查询所有StroehouseIn
     */
    List<StroehouseIn> listStoreIn(StroehouseInQuery param);
    /**
     *  根据订单状态统计
     */
    
    @Select("select  state ,count(*) as stateCount from order_detail where id in(select detail_id from order_detail_in where in_id = #{stroehouoseInId}) GROUP BY state")
    List<CountOrderDetailStateByIn>  countStateByInId(String stroehouoseInId);
    /**
     *  查询所涉及到到订单id
     */
    @Select("select distinct order_id from order_detail where id in(select detail_id from order_detail_in where in_id = #{stroehouoseInId})")
    List<String> getOrderIdByInId(String stroehouoseInId);
    
    
    List<Order> listOrderByIds(List<String> orderIds);
    
    List<TPlusGoodsLine> listByOrderId(String orderId);
   
    @Update("update gorder set state = 'a' where state = '9' and id = #{orderId}")
    int changeOrderTEById( String  orderId);
    
    @Update("update order_detail set state = 'e' where state = 'f' and order_id = #{orderId}")
    int changeOrderDetalTEByOrderId(String orderId);


    int changeOrderTplusExport(List<String> orderIds);
    int TEById(List<String> orderIds);
 
    @Select("select distinct a.provider_id as id , b.provider_name as providerName from stroehouse_in a "
    		+ "join provider b on a.provider_id = b.id where "
    		+ " a.in_time >= #{timeBegin,jdbcType=DATE} "
    		+ " and a.in_time <= date_add(#{timeEnd,jdbcType=DATE}, interval 1 day) "
    		+ " and a.state ='3' and a.can_back ='1' "
    		+ " and a.storehouse_id = #{storeId}")
    List<TPlusProvider> listProvider(@Param("storeId")String storeId
    		, @Param("timeBegin")Date timeBegin, @Param("timeEnd")Date timeEnd);
   
    
    @Select("select distinct a.provider_id as id , b.provider_name as providerName from stroehouse_in a "
    		+ "join provider b on a.provider_id = b.id where "
    		+ " a.in_time >= #{timeBegin,jdbcType=DATE} "
    		+ " and a.in_time <= date_add(#{timeEnd,jdbcType=DATE}, interval 1 day) "
    		+ " and a.state ='5' and a.can_back ='1' "
    		+ " and a.storehouse_id = #{storeId}")
    List<TPlusProvider> listLockProvider(@Param("storeId")String storeId
    		, @Param("timeBegin")Date timeBegin, @Param("timeEnd")Date timeEnd);

    
    
}
