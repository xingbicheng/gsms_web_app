package com.myqm.dao.gs.receive;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.gs.receive.StroehouseBack;
import com.myqm.vo.gs.receive.StroehouseBackQuery;

@Mapper
public interface StroehouseBackDao {
	
	/**
    *根据新增StroehouseBack,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(StroehouseBack record);
    
    
   /**
	*根据id删除StroehouseBack
   */
 	//@Delete("delete from stroehouse_back where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询StroehouseBack
     */
    StroehouseBack getById(String id);

    /**
     *根据id更新StroehouseBack
     */ 
    int editById(StroehouseBack record);

    /**
     *分页查询所有StroehouseBack
     */    
    List<StroehouseBack> list(StroehouseBackQuery param); 
    
    
    @Select("select sum(amount) as amount FROM stroehouse_back where form_in_id  =#{inId}")
    Double getBackSum(String inId);
        
}