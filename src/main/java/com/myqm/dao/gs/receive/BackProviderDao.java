package com.myqm.dao.gs.receive;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.pojo.gs.receive.BackProvider;
import com.myqm.vo.gs.receive.BackProviderQuery;
import com.myqm.vo.gs.receive.StroehouseBackQuery;

@Mapper
public interface BackProviderDao {
	
	/**
    *根据新增BackProvider,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(BackProvider record);
    
    
   /**
	*根据id删除BackProvider
   */
 	//@Delete("delete from back_provider where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询BackProvider
     */
    BackProvider getById(String id);

    /**
     *根据id更新BackProvider
     */ 
    int editById(BackProvider record);

    /**
     *分页查询所有BackProvider
     */    
    List<BackProvider> list(BackProviderQuery param); 
    
    @Select("select sum(amount) as amount FROM back_provider where in_id = #{inId}")
    Double getBackSum(String inId);
    
    
        
}