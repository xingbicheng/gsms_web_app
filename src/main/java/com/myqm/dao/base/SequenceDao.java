package com.myqm.dao.base;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SequenceDao {

	@Select("select currval(#{seqName})")
	int getCurrentValue(String seqName);

	@Select("select nextval(#{seqName})")
	int getNextValue(String seqName);
	

}
