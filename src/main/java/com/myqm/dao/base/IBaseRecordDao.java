package com.myqm.dao.base;

import java.util.List;

import com.myqm.pojo.base.BaseRecord;
import com.myqm.vo.base.BaseQueryParam;

public interface IBaseRecordDao<T extends BaseRecord, QP extends BaseQueryParam> {
	/**
	 * 删除记录
	 * 
	 * @param id
	 *            主键id
	 * @return 删除记录数量
	 */
	int delById(String id);

	/**
	 * 插入记录
	 * 
	 * @param record
	 *            基于基本信息的记录
	 * @return 插入记录数量
	 */
	int save(T record);

	/**
	 * 根据主键查询记录
	 * 
	 * @param id
	 *            主键id
	 * @return 查询出的记录
	 */
	T getById(String id);

	/**
	 * 根据ID更新更新
	 * 
	 * @param record
	 *            记录
	 * @return 更新记录数量
	 */
	int editById(T record);

	/**
	 * 分页查询记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	List<T> list(QP param);

	/**
	 * 分页查询简要记录类别
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	List<T> listBrif(QP param);

	/**
	 * 批量删除
	 * 
	 * @param 记录列表ID数组
	 * @return 删除记录数量
	 */
	int delAll(List<String> list);

}
