package com.myqm.dao.base;

import com.myqm.pojo.base.BaseInfo;
import com.myqm.vo.base.BaseQueryParam;

public interface IBaseInfoDao<T extends BaseInfo, QP extends BaseQueryParam> extends IBaseRecordDao<T, QP> {

}
