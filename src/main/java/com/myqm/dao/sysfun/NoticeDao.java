package com.myqm.dao.sysfun;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.pojo.sysfun.Notice;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.vo.sysfun.QueryNotice;

@Mapper
public interface NoticeDao extends IBaseInfoDao<Notice, QueryNotice> {

	Notice getBrifById(String id);

	/**
	 * 保存已读公告
	 * 
	 * @param noticeId
	 *            记录id
	 * @param userId
	 *            用户id
	 * @return 公告数量
	 */
	@Insert("insert ignore into sys_notice_read(notice_id, user_id, create_time) values(#{noticeId}, #{userId}, now())")
	int saveReadRecord(@Param("noticeId") String noticeId, @Param("userId") String userId);

	@Delete("delete from sys_notice_read where notice_id = #{noticeId}")
	int delReadRecord(String noticeId);

	/**
	 * 已读用户
	 * 
	 * @param noticeId
	 *            公告id
	 * @return 用户数量
	 */
	List<SysUser> listReadUsers(String noticeId);

	/**
	 * 未读数量
	 * 
	 * @param userId
	 *            用户id
	 * @return 未读数量
	 */
	@Select("select count(1) from sys_notice left join sys_notice_read r on r.notice_id = id and r.user_id = #{userId} where status = 1 and r.user_id is null")
	int countUnread(String userId);

	@Select("select count(1) from sys_notice_read where notice_id = #{noticeId} ")
	int countRead(String noticeId);

	@Select("select count(1) from sys_notice_read where notice_id = #{noticeId} and user_id = #{userId} ")
	int countUserRead(@Param("noticeId") String noticeId, @Param("userId") String userId);

	@Select("select status from sys_notice where id = #{id} ")
	int getStatus(String id);

}