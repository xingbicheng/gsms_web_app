package com.myqm.dao.sysfun;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.myqm.pojo.sysfun.Notice;
import com.myqm.vo.sysfun.QueryNotice;

@Mapper
public interface PrivateNoticeDao {
	@Insert("insert ignore into sys_notice_private(notice_id, user_id) values(#{noticeId}, #{userId})")
	int save(@Param("noticeId") String noticeId, @Param("userId") String userId);

	@Delete("delete from sys_notice_private where notice_id = #{noticeId}")
	int delByNoticeId(String noticeId);

	/**
	 * 分页查询记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	List<Notice> list(QueryNotice param);

	/**
	 * 分页查询简要记录类别
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	List<Notice> listBrif(QueryNotice param);
}
