package com.myqm.dao.sysfun;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import com.myqm.vo.sysfun.DepartmentQuery;
import com.myqm.pojo.sysfun.Department;

@Mapper
public interface DepartmentDao {
	
	/**
    *根据新增Department,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(Department record);
    
    
   /**
	*根据id删除Department
   */
 	//@Delete("delete from department where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询Department
     */
    Department getById(String id);

    /**
     *根据id更新Department
     */ 
    int editById(Department record);

    /**
     *分页查询所有Department
     */    
    List<Department> list(DepartmentQuery param); 
        
}