package com.myqm.dao.sysfun;

import java.util.List;
import java.util.Set;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.vo.sysfun.QueryPermission;

@Mapper
public interface PermissionDao extends IBaseRecordDao<Permission, QueryPermission> {

	List<Permission> listParents();
	List<Permission> rootMenu();

	/**
	 * 根据userId查询列表
	 * 
	 * @param userId
	 *            用户id
	 * @return 查询出来的列表
	 */
	List<Permission> listByUserId(String userId);

	/**
	 * 根据角色id查询公告
	 * 
	 * @param roleId
	 *            角色的id
	 * @return 查询出的公告
	 */
	List<Permission> listByRoleId(String roleId);

	/**
	 * 根据ParentId删除记录
	 * 
	 * @param id
	 *            主键id
	 */
	@Delete("delete from sys_permission where parent_id = #{id}")
	int deleteByParentId(String id);

	/**
	 * 根据权限id删除记录
	 * 
	 * @param permissionId
	 *            权限id
	 */
	@Delete("delete from sys_role_permission where permission_id = #{permissionId}")
	int deleteRolePermission(String permissionId);

	/**
	 * 根据权限permissionId查询列表
	 * 
	 * @param permissionId
	 *            权限id
	 * @return 查询出的列表
	 */
	@Select("select ru.user_id from sys_role_permission rp inner join sys_role_user ru on ru.role_id = rp.role_id where rp.permission_id = #{permissionId}")
	Set<String> listUserIds(String permissionId);

	/**
	 * 根据sort查询权限
	 * 
	 * @return 查询出的权限列表
	 */
	@Select("select id, name, mpid, bpid from sys_permission sys order by sys.sort")
	List<Permission> listAll();

    List<Permission> getByUserId(String userId);
}
