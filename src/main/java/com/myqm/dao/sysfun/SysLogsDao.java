package com.myqm.dao.sysfun;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.vo.sysfun.QuerySysLogs;

import java.util.List;

@Mapper
public interface SysLogsDao extends IBaseRecordDao<SysLogs, QuerySysLogs> {

	@Delete("delete from sys_logs where YEAR(create_time) = #{y} and Month(create_time) >= #{s} and Month(create_time) <= #{e}")
	int deleteLogs(@Param("y") int year, @Param("s") int start, @Param("e") int end);
	int insertMany(List<SysLogs> syslogs);
	//@Select("select * from  sys_logs where module_id = #{moduleId}")
	List<SysLogs> getLogsByModuleId(String moduleId);

}
