package com.myqm.dao.sysfun;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.sysfun.Role;
import com.myqm.vo.sysfun.QueryRole;

import javax.annotation.security.RolesAllowed;

@Mapper
public interface RoleDao extends IBaseInfoDao<Role, QueryRole> {

	/**
	 * 根据userId查询列表
	 * 
	 * @param userId
	 *            用户id
	 * @return 查询出来的列表
	 */
	List<Role> listByUserId(String userId);

	/**
	 * 根据roleId查询列表
	 * 
	 * @param roleId
	 *            角色id
	 * @return 查询出来的列表
	 */
	@Delete("delete from sys_role_permission where role_id = #{roleId}")
	int deleteRolePermission(String roleId);

	/**
	 * 保存
	 */
	int saveRolePermission(@Param("roleId") String roleId, @Param("permissionIds") List<String> permissionIds);

	/**
	 * 删除用户
	 * 
	 * @param roleId
	 *            角色id
	 */
	@Delete("delete from sys_role_user where role_id = #{roleId}")
	int deleteRoleUser(String roleId);

	@Select("select count(0) from sys_role_user where role_id = #{roleId}")
	int countRoleUser(String roleId);

    int editByActivation(Role role);

    List<Role> listByParam(QueryRole queryRole);
}
