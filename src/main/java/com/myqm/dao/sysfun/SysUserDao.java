package com.myqm.dao.sysfun;

import java.util.List;

import org.apache.ibatis.annotations.*;

import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.vo.sysfun.QuerySysUser;

@Mapper
public interface SysUserDao extends IBaseInfoDao<SysUser, QuerySysUser> {

	@Update("update sys_user  set status = #{status} where id = #{id}")
	int setState(@Param("status") int status, @Param("id") String id);

	@Update("update sys_user set password = #{password} where id = #{id}")
	int changePassword(@Param("id") String id, @Param("password") String password);

	@Delete("delete from sys_role_user where user_id = #{userId}")
	int deleteUserRole(String userId);

	@Select("select nickname from sys_user where id = #{userId}")
	String getUserNameById(String userId);

	/**
	 * 根据userId和roleIds在sys_role_user增加信息
	 * 
	 * @param userId
	 * @param roleIds
	 * @return
	 */
	int setUserRoles(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);

	SysUser getByName(String username);

	/**
	 * 根据用户姓名查找用户信息
	 * @param username
	 * @return
	 */
	SysUser getUser(String username);

	@Insert("INSERT INTO sys_role_user(user_id,role_id) values(#{userId},#{roleId})")
	int saveUserRole(@Param("userId") String userId,@Param("roleId") String roleId);

    @Update("REPLACE sys_role_user SET role_id=#{roleId},user_id=#{userId}")
    int editUserRole(@Param("userId") String userId,@Param("roleId") String roleId);


}
