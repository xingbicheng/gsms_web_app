package com.myqm.dao.sysfun;

import org.apache.ibatis.annotations.Mapper;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.sysfun.FileInfo;
import com.myqm.vo.sysfun.QueryFileInfo;

@Mapper
public interface FileInfoDao extends IBaseRecordDao<FileInfo, QueryFileInfo> {

	FileInfo getByMd5(String fileMd5);

}
