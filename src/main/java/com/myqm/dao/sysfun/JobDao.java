package com.myqm.dao.sysfun;

import org.apache.ibatis.annotations.Mapper;

import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.pojo.sysfun.JobModel;
import com.myqm.vo.sysfun.QueryJobModel;

@Mapper
public interface JobDao extends IBaseInfoDao<JobModel, QueryJobModel> {
}
