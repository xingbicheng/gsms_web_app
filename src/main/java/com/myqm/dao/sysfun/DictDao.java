package com.myqm.dao.sysfun;

import org.apache.ibatis.annotations.Mapper;
import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.vo.sysfun.QueryDict;

@Mapper
public interface DictDao extends IBaseInfoDao<Dict, QueryDict> {
}
