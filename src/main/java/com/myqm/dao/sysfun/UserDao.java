package com.myqm.dao.sysfun;

import org.apache.ibatis.annotations.Mapper;
import com.myqm.pojo.sysfun.SysUser;

@Mapper
public interface UserDao {

	/**
	 * 根据用户姓名查找用户信息
	 * 
	 * @param username
	 * @return
	 */
	SysUser getUser(String username);

}
