package com.myqm.dao.sysfun;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Options;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myqm.vo.sysfun.SysWxUserQuery;
import com.myqm.pojo.sysfun.SysWxUser;

@Mapper
public interface SysWxUserDao {
	
	/**
    *根据新增SysWxUser,id为字符
    */
	//@Options(useGeneratedKeys = true, keyProperty = "id")
    int save(SysWxUser record);
    
    
   /**
	*根据id删除SysWxUser
   */
 	//@Delete("delete from sys_wx_user where id = #{id,jdbcType=VARCHAR}")
    int delById(String id);
     
    
    /**
     *根据id查询SysWxUser
     */
    SysWxUser getById(String id);

    /**
     *根据openid查询SysWxUser
     */
    @Select("select * from sys_wx_user where open_id = #{id} ")
    SysWxUser getByOpenId(String id);
    /**
     *根据id更新SysWxUser
     */ 
    int editById(SysWxUser record);

    /**
     *分页查询所有SysWxUser
     */    
    List<SysWxUser> list(SysWxUserQuery param);

    @Update("update sys_wx_user set status ='1',user_id=#{userId} where open_id=#{openId}")
	void updateState(@Param("openId")String openId,@Param("userId")String userId);

    @Update("update sys_wx_user set status ='2' where open_id=#{openId}")
	void unauditedUpdateState(String openId); 
        
}