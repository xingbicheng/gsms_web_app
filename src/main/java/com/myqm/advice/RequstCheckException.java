package com.myqm.advice;

public class RequstCheckException extends Exception {
	/** 
	 *  
	 */
	private static final long serialVersionUID = 5996913071452260665L;

	private int code = 0;
	private Object data = null;

	public RequstCheckException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public RequstCheckException(int code, String msg, Object data) {
		super(msg);
		this.code = code;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
