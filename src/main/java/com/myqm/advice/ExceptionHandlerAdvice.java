package com.myqm.advice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import com.myqm.pojo.ResultResponse;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * springmvc异常处理
 * 
 */

@RestControllerAdvice
public class ExceptionHandlerAdvice {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@ExceptionHandler({ IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResultResponse<Object> badRequestException(IllegalArgumentException exception) {
		return new ResultResponse<Object>(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler({ AccessDeniedException.class })
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ResultResponse<Object> badRequestException(AccessDeniedException exception) {
		return new ResultResponse<Object>(HttpStatus.FORBIDDEN.value(), exception.getMessage());
	}

	@ExceptionHandler({ MissingServletRequestParameterException.class, HttpMessageNotReadableException.class,
			UnsatisfiedServletRequestParameterException.class, MethodArgumentTypeMismatchException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResultResponse<Object> badRequestException(Exception exception) {
		return new ResultResponse<Object>(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler({ DuplicateKeyException.class })
	@ResponseStatus(HttpStatus.OK)
	public ResultResponse<Object> keyException(HttpServletRequest req, DuplicateKeyException throwable) {
		log.error("数据库唯一异常", throwable);
		String message = throwable.getCause().getMessage();
		String[] split = message.split("'");
		return new ResultResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR.value(), split[1] + "已经存在");
	}
	
	@ExceptionHandler(MySQLIntegrityConstraintViolationException.class )
	@ResponseStatus(HttpStatus.OK)
	public ResultResponse<Object> keyException2(HttpServletRequest req,
			DataIntegrityViolationException throwable) {
		log.error("数据库重复异常", throwable);
		return new ResultResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "数据重复");
	}

	@ExceptionHandler({ DataIntegrityViolationException.class })
	@ResponseStatus(HttpStatus.OK)
	public ResultResponse<Object> violationException(HttpServletRequest req,
			DataIntegrityViolationException throwable) {
		log.error("数据库外键约束异常", throwable);
		return new ResultResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "存在其他关联错误");
	}
	

	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResultResponse<Object> exception(Throwable throwable) {
		log.error("系统异常", throwable);
		return new ResultResponse<Object>(HttpStatus.INTERNAL_SERVER_ERROR.value(), throwable.getMessage());
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.OK)
	public ResultResponse<Object> violationException(HttpServletRequest req, ConstraintViolationException throwable) {
		log.error("数据验证错误", throwable);
		String errorMsg = "";
		for (ConstraintViolation<?> constraintViolation : throwable.getConstraintViolations()) {
			errorMsg += constraintViolation.getPropertyPath().toString() + ":" + constraintViolation.getMessage() + ";";
		}
		return new ResultResponse<Object>(222, errorMsg);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.OK)
	public ResultResponse<Object> bindException(HttpServletRequest req, MethodArgumentNotValidException throwable) {
		log.error("数据验证错误", throwable);
		String errorMsg = "";
		for (FieldError error : throwable.getBindingResult().getFieldErrors()) {
			errorMsg += error.getField() + ":" + error.getDefaultMessage() + ";";
		}
		return new ResultResponse<Object>(222, errorMsg);
	}
}
