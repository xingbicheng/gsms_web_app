package com.myqm.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.filter.authorization.TokenFilter;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.security.authorization.Token;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.security.authorization.TokenService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.ResponseUtil;
import com.myqm.utils.UserUtil;

/**
 * spring security处理器
 * 
 */
@Configuration
public class SecurityHandlerConfig {

	@Autowired
	private TokenService tokenService;

	@Autowired
	private SysLogsService logService;
	@Autowired
	private SysLogsDao sysLogsDao;

	/**
	 * 登陆成功，返回Token
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationSuccessHandler loginSuccessHandler() {
		return new AuthenticationSuccessHandler() {
			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws IOException, ServletException {
				// LoginUser loginUser = (LoginUser)
				// authentication.getPrincipal();
				// Token token = tokenService.saveToken(loginUser);
				// ResultResponse<Token> info = new
				// ResultResponse<Token>(token);
				// ResponseUtil.responseJson(response, HttpStatus.OK.value(),
				// info);
			}
		};
	}

	/**
	 * 登陆失败
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationFailureHandler loginFailureHandler() {
		return new AuthenticationFailureHandler() {

			@Override
			public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
					AuthenticationException exception) throws IOException, ServletException {
				String msg = null;
				if (exception instanceof BadCredentialsException) {
					msg = "password error!";
				} else {
					msg = exception.getMessage();
				}
				ResultResponse<Object> info = new ResultResponse<Object>(HttpStatus.UNAUTHORIZED.value(), msg);
				ResponseUtil.responseJson(response, HttpStatus.UNAUTHORIZED.value(), info);
			}
		};

	}

	/**
	 * 未登录，返回401
	 */
	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		return new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response,
					AuthenticationException authException) throws IOException, ServletException {
				ResultResponse<Object> info = new ResultResponse<Object>(HttpStatus.UNAUTHORIZED.value(),
						"Please login first！");
				ResponseUtil.responseJson(response, HttpStatus.UNAUTHORIZED.value(), info);
			}
		};
	}

	/**
	 * 退出处理
	 * 
	 * @return
	 */
	@Bean
	public LogoutSuccessHandler logoutSussHandler() {
		return new LogoutSuccessHandler() {

			@Override
			public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws IOException, ServletException {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				String temp = request.getHeader("Origin");
				httpServletResponse.setHeader("Access-Control-Allow-Origin", temp);
				// 允许的访问方法
				httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
				// Access-Control-Max-Age 用于 CORS 相关配置的缓存
				httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
				httpServletResponse.setHeader("Access-Control-Allow-Headers",
						"Origin, X-Requested-With, Content-Type, Accept,token");
				httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");

				ResultResponse<Object> info = new ResultResponse<Object>(0, "sign out");

				String token = TokenFilter.getToken(request);
				if (token != null) {

					LoginUser loginUser = tokenService.getLoginUser(token);

					if (loginUser != null) {
						tokenService.deleteToken(token);
					}
				}

				ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
			}
		};

	}

}
