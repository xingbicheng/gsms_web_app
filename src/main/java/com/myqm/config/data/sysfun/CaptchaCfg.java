package com.myqm.config.data.sysfun;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:captcha.properties")
@ConfigurationProperties(prefix = "captcha")
public class CaptchaCfg {
	private int CaptchaExpries = 180; // 时间
	private int CaptchaWidth = 120;
	private int CaptchaHeight = 60;
	private String CaptchaCookieName = "CaptchaCode";

	public int getCaptchaExpries() {
		return CaptchaExpries;
	}

	public void setCaptchaExpries(int captchaExpries) {
		CaptchaExpries = captchaExpries;
	}

	public int getCaptchaWidth() {
		return CaptchaWidth;
	}

	public void setCaptchaWidth(int captchaWidth) {
		CaptchaWidth = captchaWidth;
	}

	public int getCaptchaHeight() {
		return CaptchaHeight;
	}

	public void setCaptchaHeight(int captchaHeight) {
		CaptchaHeight = captchaHeight;
	}

	public String getCaptchaCookieName() {
		return CaptchaCookieName;
	}

	public void setCaptchaCookieName(String captchaCookieName) {
		CaptchaCookieName = captchaCookieName;
	}

}
