package com.myqm.config.data.security.ipfreqcheck;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/* 
 * IP地址信息
 */
@Component
@PropertySource("classpath:gateway.properties")
@ConfigurationProperties(prefix = "ip.restriction")
public class IpRestrictionCfg {
	private String redisIpListName;
	private int limteTime;
	private int limitLockTime;
	private int lockCount; // 连续访问锁定次数
	private int maxCount;

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public int getLockCount() {
		return lockCount;
	}

	public void setLockCount(int lockCount) {
		this.lockCount = lockCount;
	}

	public int getLimitLockTime() {
		return limitLockTime;
	}

	public void setLimitLockTime(int limitLockTime) {
		this.limitLockTime = limitLockTime;
	}

	public int getLimteTime() {
		return limteTime;
	}

	public void setLimteTime(int limteTime) {
		this.limteTime = limteTime;
	}

	public String getRedisIpListName() {
		return redisIpListName;
	}

	public void setRedisIpListName(String redisIpListName) {
		this.redisIpListName = redisIpListName;
	}

}