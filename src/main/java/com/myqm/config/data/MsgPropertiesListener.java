package com.myqm.config.data;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;

import com.myqm.pojo.AllResultMsg;

@SuppressWarnings("deprecation")
public class MsgPropertiesListener implements ApplicationListener<ApplicationStartedEvent> {

	private String propertyFileName;

	public MsgPropertiesListener(String propertyFileName) {
		this.propertyFileName = propertyFileName;
	}

	@Override
	public void onApplicationEvent(ApplicationStartedEvent event) {
		AllResultMsg.loadAllProperties(propertyFileName);
	}
}