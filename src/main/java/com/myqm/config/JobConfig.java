package com.myqm.config;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.myqm.pojo.sysfun.JobModel;
import com.myqm.service.sysfun.JobService;

//@Configuration
public class JobConfig {

	public static final String KEY = "applicationContextSchedulerContextKey";

	@Bean("adminQuartzScheduler")
	public SchedulerFactoryBean quartzScheduler(DataSource dataSource) {
		SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();

		try {
			quartzScheduler.setQuartzProperties(
					PropertiesLoaderUtils.loadProperties(new ClassPathResource("quartz.properties")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		quartzScheduler.setDataSource(dataSource);
		quartzScheduler.setOverwriteExistingJobs(true);
		quartzScheduler.setApplicationContextSchedulerContextKey(KEY);
		quartzScheduler.setStartupDelay(10);

		return quartzScheduler;
	}

	@Autowired
	private JobService jobService;
	@Autowired
	private TaskExecutor taskExecutor;

	/**
	 * 初始化一个定时删除日志的任务
	 */
	// @PostConstruct
	// public void initDeleteLogsJob() {
	// taskExecutor.execute(() -> {
	// JobModel jobModel = new JobModel();
	// jobModel.setId(1L);
	// jobModel.setJobName("delete-logs-job");
	// jobModel.setCron("0 0/2 * * * ?");
	// jobModel.setDescription("定时删除三个月前日志");
	// jobModel.setSpringBeanName("sysLogsServiceImpl");
	// jobModel.setMethodName("deleteLogs");
	// jobModel.setIsSysJob(true);
	// jobModel.setStatus(1);
	// jobService.save(jobModel);
	// });
	// }

	/**
	 * 初始化一个定时更新汇率
	 */

	// @PostConstruct
	// public void initGetExchangeRateJob() {
	// taskExecutor.execute(() -> {
	// JobModel jobModel = new JobModel();
	// jobModel.setId(2L);
	// jobModel.setJobName("get-exchagne-rate-job");
	// jobModel.setCron("0 0/30 * * * ?");
	// jobModel.setDescription("定时获取交易汇率");
	// jobModel.setSpringBeanName("currExchangeRateServiceImpl");
	// jobModel.setMethodName("setCurrExchangeRate");
	// jobModel.setIsSysJob(true);
	// jobModel.setStatus(1);
	// jobService.save(jobModel);
	// });
	// }

}
