package com.myqm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.myqm.interceptor.iprestrition.IpRestrictionInterceptor;

//@Configuration
public class IpRestrictionConfig extends WebMvcConfigurerAdapter {
	@Autowired(required = true)
	IpRestrictionInterceptor ipInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(ipInterceptor).addPathPatterns("/*");
	}
}
