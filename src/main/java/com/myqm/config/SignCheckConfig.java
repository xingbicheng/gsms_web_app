package com.myqm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.myqm.interceptor.sign.SignCheckInterceptor;

//@Configuration
public class SignCheckConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private SignCheckInterceptor interceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(interceptor).addPathPatterns("/api/**"); // 用户API
	}
}
