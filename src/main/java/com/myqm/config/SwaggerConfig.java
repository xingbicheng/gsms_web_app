package com.myqm.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger文档
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	static List<Parameter> getParamList() {
		List<Parameter> pars = new ArrayList<Parameter>();
		ParameterBuilder tokenPar = new ParameterBuilder();
		tokenPar.name("token").description("令牌").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();

		ParameterBuilder signPar = new ParameterBuilder();
		signPar.name("sign").description("数据签名").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();
		ParameterBuilder timePar = new ParameterBuilder();
		timePar.name("timestamp").description("时间戳").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();

		pars.add(tokenPar.build());
		pars.add(signPar.build());
		pars.add(timePar.build());
		return pars;
	}

	@Bean
	public Docket sysDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("系统基础功能")
				.apiInfo(new ApiInfoBuilder().title("系统基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.sysfun")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket gsDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("高水二期基础功能")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.basic")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket gsPurchase() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("采购订单")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.purchase")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket gsOrder() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("原始订单")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.order")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket gsInOrder() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("入库单")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.in")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket gsOutOrder() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("出库单")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.out")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}
	
	@Bean
	public Docket gsReceiveOrder() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("结算单")
				.apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.receive")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

    @Bean
    public Docket gsWx() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("微信")
                .apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
                        .contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
                        .build())
                .select().paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.wx")).build()
                .globalOperationParameters(SwaggerConfig.getParamList());
    }
    @Bean
    public Docket tPlus() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("TPlus")
                .apiInfo(new ApiInfoBuilder().title("高水二期基础功能-接口文档")
                        .contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
                        .build())
                .select().paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.myqm.controller.gs.tplus")).build()
                .globalOperationParameters(SwaggerConfig.getParamList());
    }
    @Bean
	public Docket generateDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("代码自动生成")
				.apiInfo(new ApiInfoBuilder().title("SpringBoot基础功能-代码自动生成")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.dev")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket meetingDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("会议管理")
				.apiInfo(new ApiInfoBuilder().title("Xxb-会议管理")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.xxb.meeting")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket articleDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("文章管理")
				.apiInfo(new ApiInfoBuilder().title("Xxb-文章管理")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.xxb.article")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}

	@Bean
	public Docket tableConfigDocket() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("表单管理")
				.apiInfo(new ApiInfoBuilder().title("Xxb-表单管理")
						.contact(new Contact("西南科技大学", "www.swust.edu.cn", "huizhuoli@foxmail.com")).version("1.0")
						.build())
				.select().paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.myqm.controller.xxb.tableConfig")).build()
				.globalOperationParameters(SwaggerConfig.getParamList());
	}
}
