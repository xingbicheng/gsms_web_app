package com.myqm.dto.gs.purchase;

import java.sql.Date;
import java.util.List;

import com.myqm.pojo.gs.purchase.PrePurchaseDetail;
import io.swagger.annotations.ApiModelProperty;

public class PrePurchaseGoods {
	String goodsId;
	String goodsName;
	String goodsTypeId;
	String unitName;
	String goodsType;
    Date realDistributionDate;
    Date distributionDate;

	double amount;
	double lossRate;
	double lossAmount;
	double statisticAmount;

	List<PrePurchaseDetail> details;

    public Date getRealDistributionDate() {
        return realDistributionDate;
    }

    public void setRealDistributionDate(Date realDistributionDate) {
        this.realDistributionDate = realDistributionDate;
    }

    public Date getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(Date distributionDate) {
        this.distributionDate = distributionDate;
    }

    public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public List<PrePurchaseDetail> getDetails() {
		return details;
	}

	public void setDetails(List<PrePurchaseDetail> details) {
		this.details = details;
	}

	public double getLossRate() {
		return lossRate;
	}

	public void setLossRate(double lossRate) {
		this.lossRate = lossRate;
	}

	public double getStatisticAmount() {
		return statisticAmount;
	}

	public void setStatisticAmount(double statisticAmount) {
		this.statisticAmount = statisticAmount;
	}

	public double getLossAmount() {
		return lossAmount;
	}

	public void setLossAmount(double lossAmount) {
		this.lossAmount = lossAmount;
	}

}
