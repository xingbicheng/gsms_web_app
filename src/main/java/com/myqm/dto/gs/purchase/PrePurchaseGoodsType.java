package com.myqm.dto.gs.purchase;

import java.util.List;

public class PrePurchaseGoodsType {
	String goodsTypeId;
	String goodsType;

	List<PrePurchaseGoods> goodsGruoup;

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public List<PrePurchaseGoods> getGoodsGruoup() {
		return goodsGruoup;
	}

	public void setGoodsGruoup(List<PrePurchaseGoods> goodsGruoup) {
		this.goodsGruoup = goodsGruoup;
	}

}
