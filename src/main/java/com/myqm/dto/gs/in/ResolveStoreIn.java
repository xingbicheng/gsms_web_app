package com.myqm.dto.gs.in;

import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class ResolveStoreIn {
	@ApiModelProperty(value = "入库单id")
	@NotNull
	private String storehouseInId;
	@ApiModelProperty(value = "分解数据")
	@NotNull
	private List<Double> amount;

	public String getStorehouseInId() {
		return storehouseInId;
	}

	public void setStorehouseInId(String storehouseInId) {
		this.storehouseInId = storehouseInId;
	}

	public List<Double> getAmount() {
		return amount;
	}

	public void setAmount(List<Double> amount) {
		this.amount = amount;
	}

}
