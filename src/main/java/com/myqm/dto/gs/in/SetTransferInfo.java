package com.myqm.dto.gs.in;

import java.math.BigDecimal; 

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

public class SetTransferInfo {
	
	@ApiModelProperty(value = "中央库入库id 调出库房入库单id")
	@NotNull
	private String fromStorehouseInId;
	
	@ApiModelProperty(value = "调入 入库单id")
	@NotNull
	private String toStorehouseInId;

	@JsonIgnore
	@ApiModelProperty(value = "中央库id 调出库房id")
	@NotNull
	private String fromStorehouseId;

    @JsonIgnore
	@ApiModelProperty(value = "调入 库房id")
	@NotNull
	private String toStorehouseId;

    @JsonIgnore
	@ApiModelProperty(value = "供应商id")
	@NotNull
	private String providerId;

    @JsonIgnore
	@ApiModelProperty(value = "供应商单价")
	@NotNull
	private BigDecimal price;

    @JsonIgnore
	@ApiModelProperty(value = "包装说明")
	@NotNull
	private String packingMemo;
    
    private boolean canBack= false;

	public boolean getCanBack() {
		return canBack;
	}

	public void setCanBack(boolean canBack) {
		this.canBack = canBack;
	}

	public String getFromStorehouseInId() {
		return fromStorehouseInId;
	}

	public void setFromStorehouseInId(String fromStorehouseInId) {
		this.fromStorehouseInId = fromStorehouseInId;
	}

	public String getToStorehouseInId() {
		return toStorehouseInId;
	}

	public void setToStorehouseInId(String toStorehouseInId) {
		this.toStorehouseInId = toStorehouseInId;
	}

	public String getFromStorehouseId() {
		return fromStorehouseId;
	}

	public void setFromStorehouseId(String fromStorehouseId) {
		this.fromStorehouseId = fromStorehouseId;
	}

	public String getToStorehouseId() {
		return toStorehouseId;
	}

	public void setToStorehouseId(String toStorehouseId) {
		this.toStorehouseId = toStorehouseId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPackingMemo() {
		return packingMemo;
	}

	public void setPackingMemo(String packingMemo) {
		this.packingMemo = packingMemo;
	} 
}
