package com.myqm.dto.gs.in;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;


public class SetLoss {
    @ApiModelProperty(value = "入库单编号(字符长度为1-32)")
    @Size(min=1, max=32,message="入库单编号字符长度为1-32")
    @Excel(name="入库单编号",orderNum="1")
    private String inId;

    @ApiModelProperty(value = "损耗数量")
    private Double amount;

    @ApiModelProperty(value = "损耗类型(字符长度为1)")
    @Size(min=1, max=32,message="损耗类型字符长度为1")
    @Excel(name="损耗类型",orderNum="11")
    private String lossType;

    @ApiModelProperty(value="0冲正为，1冲负")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInId() {
        return inId;
    }

    public void setInId(String inId) {
        this.inId = inId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getLossType() {
        return lossType;
    }

    public void setLossType(String lossType) {
        this.lossType = lossType;
    }
}
