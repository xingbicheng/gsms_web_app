package com.myqm.dto.gs.in;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class SetProviderInfo {
	@ApiModelProperty(value = "入库单id")
	@NotNull
	private List<String> storehouseInId;
	@ApiModelProperty(value = "供应商id")
	@NotNull
	private String providerId;

	@ApiModelProperty(value = "供应商单价")
	@NotNull
	private BigDecimal price;

	@ApiModelProperty(value = "包装说明")
	@NotNull
	private String packingMemo;
	
	

	public List<String> getStorehouseInId() {
		return storehouseInId;
	}

	public void setStorehouseInId(List<String> storehouseInId) {
		this.storehouseInId = storehouseInId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPackingMemo() {
		return packingMemo;
	}

	public void setPackingMemo(String packingMemo) {
		this.packingMemo = packingMemo;
	}

}
