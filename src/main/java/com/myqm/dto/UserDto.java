package com.myqm.dto;

import java.util.List;

import com.myqm.pojo.sysfun.SysUser;

public class UserDto extends SysUser {

	private List<Long> roleIds;

	public List<Long> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}

}
