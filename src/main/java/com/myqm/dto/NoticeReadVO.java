package com.myqm.dto;

import java.sql.Date;
import javax.validation.constraints.NotNull;

import com.myqm.pojo.sysfun.Notice;

import io.swagger.annotations.ApiModelProperty;

public class NoticeReadVO extends Notice {

	private static final long serialVersionUID = -3842182350180882396L;

	@ApiModelProperty(value = "用户id")
	@NotNull
	private Long userId;

	@ApiModelProperty(value = "阅读时间")
	@NotNull
	private Date readTime;

	@ApiModelProperty(value = "是否阅读")
	@NotNull
	private Boolean isRead;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}
}
