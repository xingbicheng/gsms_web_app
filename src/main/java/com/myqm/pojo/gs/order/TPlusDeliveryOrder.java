package com.myqm.pojo.gs.order;

import com.myqm.pojo.gs.in.TPlusGoodsLine;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "deliveryOrder")
@XmlAccessorType(value = XmlAccessType.PROPERTY)  
public class TPlusDeliveryOrder implements Serializable, Cloneable {
	
	@XmlElement 
	private String deliveryOrderCode;//单据编号
	@XmlElement
	private String warehouseCode;//仓库编码
	@XmlElement
	private String operateTime;//单据日期 2018-11-22
	@XmlElement
	private String ownerCode;//客户编码
	
	@XmlElementWrapper(name = "orderLines") 
	//@XmlElement(name = "orderLine", type =TPlusGoods.class) 
	@XmlElementRef
	private List<TPlusGoodsLine> orderLines;
	public TPlusDeliveryOrder(){
		
	}
	
	public TPlusDeliveryOrder(String deliveryOrderCode, String warehouseCode,
                              String ownerCode, String operateTime){
        this.deliveryOrderCode = deliveryOrderCode;
        this.warehouseCode = warehouseCode;
        this.ownerCode = ownerCode;
        this.operateTime = operateTime;
	}

    @XmlTransient
    public String getDeliveryOrderCode() {
        return deliveryOrderCode;
    }
    public void setDeliveryOrderCode(String deliveryOrderCode) {
        this.deliveryOrderCode = deliveryOrderCode;
    }
	@XmlTransient
	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}
	@XmlTransient
	public String getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	@XmlTransient
	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}
	@XmlTransient
	public List<TPlusGoodsLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<TPlusGoodsLine> orderLines) {
		this.orderLines = orderLines;
	} 
	 public Object clone() throws CloneNotSupportedException{
	         return super.clone();
	     }
	 
}
