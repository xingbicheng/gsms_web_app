package com.myqm.pojo.gs.order;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "request") 
@XmlAccessorType(value = XmlAccessType.PROPERTY)  
public class TPlusDeliveryOrderList implements Serializable{
	@XmlElementRef
	List<TPlusDeliveryOrder> list = new ArrayList<TPlusDeliveryOrder>();
	@XmlTransient
	public List<TPlusDeliveryOrder> getList() {
		return list;
	}

	public void setList(List<TPlusDeliveryOrder> list) {
		this.list = list;
	}
	
	
	

}
