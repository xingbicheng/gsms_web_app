package com.myqm.pojo.gs.order;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;
import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderTotal {

    @ApiModelProperty(value = "按用户类型分类")
    private Boolean classificationCustomer=false;

    @ApiModelProperty(value = "收货开始时间")
    private Date receiveTimeStart;

    @ApiModelProperty(value = "收货结束时间")
    private Date receiveTimeEnd;


    @ApiModelProperty(value = "按内部商品分类")
    private boolean classificationGoods=false;


    @ApiModelProperty(value = "出库开始时间")
    private Date outTimeStart;

    @ApiModelProperty(value = "出库结束时间")
    private Date outTimeEnd;



    @ApiModelProperty(value = "状态（d删除 1下单，2审核通过，3采购 4入库 5出库 6收货7退货入库 8 退货不收入 9 退货到中央库房 ）(字符长度为1-1)")
    @Size(min = 1, max = 1, message = "状态（d删除 1下单，2审核通过，3采购 4入库 5出库 6收货7退货入库 8 退货不收入 9 退货到中央库房 ）字符长度为1-1")
    private String state ;


    public Date getOutTimeStart() {
        return outTimeStart;
    }

    public void setOutTimeStart(Date outTimeStart) {
        this.outTimeStart = outTimeStart;
    }

    public Date getOutTimeEnd() {
        return outTimeEnd;
    }

    public void setOutTimeEnd(Date outTimeEnd) {
        this.outTimeEnd = outTimeEnd;
    }

    public Date getReceiveTimeStart() {
        return receiveTimeStart;
    }

    public void setReceiveTimeStart(Date receiveTimeStart) {
        this.receiveTimeStart = receiveTimeStart;
    }

    public Date getReceiveTimeEnd() {
        return receiveTimeEnd;
    }

    public void setReceiveTimeEnd(Date receiveTimeEnd) {
        this.receiveTimeEnd = receiveTimeEnd;
    }

    public Boolean getClassificationCustomer() {
        return classificationCustomer;
    }

    public void setClassificationCustomer(Boolean classificationCustomer) {
        this.classificationCustomer = classificationCustomer;
    }

    public boolean isClassificationGoods() {
        return classificationGoods;
    }

    public void setClassificationGoods(boolean classificationGoods) {
        this.classificationGoods = classificationGoods;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
