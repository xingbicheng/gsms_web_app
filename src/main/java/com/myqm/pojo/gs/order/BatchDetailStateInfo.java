package com.myqm.pojo.gs.order;

import java.util.List;

public class BatchDetailStateInfo {
	private List<String> detailIds;
	private String state;
	public List<String> getDetailIds() {
		return detailIds;
	}
	public void setDetailIds(List<String> detailIds) {
		this.detailIds = detailIds;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}
