package com.myqm.pojo.gs.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order.OrderOrigin;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** OrderDetail */
@JsonInclude(Include.NON_NULL)
public class OrderDetail implements Serializable {
	@JsonProperty("orderDetailId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "订单编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "订单编号字符长度为1-32")
	@Excel(name = "订单编号", orderNum = "1")
	private String orderId;

	@ApiModelProperty(value = "采购单明细id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "订单编号字符长度为1-32")
	@Excel(name = "采购单明细编号", orderNum = "1")
	private String purchaseDetailId;
	
	@ApiModelProperty(value = "单价")
	private BigDecimal unitPrice;
	
	@ApiModelProperty(value = "收货单价")
	private BigDecimal receivePrice;

	@ApiModelProperty(value = "总数量")
	private Double amount;

	@ApiModelProperty(value = "单位名称(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "单位名称字符长度为1-20")
	@Excel(name = "单位名称", orderNum = "4")
	private String unitName;

    private String originalUnit;

	@ApiModelProperty(value = "总价")
	private BigDecimal sum;
	
	@ApiModelProperty(value = "收货总价")
	private BigDecimal receiveSum;

	@ApiModelProperty(value = "原始名(字符长度为1-50)")
	@Size(min = 1, max = 50, message = "原始名字符长度为1-50")
	@Excel(name = "原始名", orderNum = "6")
	private String goodsOriginalName;

	@ApiModelProperty(value = "自费数量")
	private Double selfAmount;

	@ApiModelProperty(value = "免费数量")
	private Double freeAmount;

	@ApiModelProperty(value = "自费钱")
	private BigDecimal selfSum;

	@ApiModelProperty(value = "免费金额")
	private BigDecimal freeSum;

	@ApiModelProperty(value = "来源标志，0 原始订单，1 放弃购买，2新增购买 3 修改购买 主要针对web订单和微信订单的修改")
	@Size(min = 1, max = 1, message = "来源标志，0 原始订单，1 放弃购买，2新增购买 3 修改购买 主要针对web订单和微信订单的修改")
	@Excel(name = "来源标志，0 原始订单，1 放弃购买，2新增购买 3 修改购买 主要针对web订单和微信订单的修改", orderNum = "11")
	private String source = "0";

	@ApiModelProperty(value = "状态（d删除 1下单，2审核通过，3预采购、4采购 5入库 6出库 7收货8退货不收入9 退货到中央库房 a 退货到中央库（商品留原库 ）b 退货到供应商 c 计入损耗(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "状态（d删除 1下单，2审核通过，3采购 4入库 5出库 6收货7退货入库 8 退货不收入 9 退货到中央库房 ）字符长度为1-1")
	@Excel(name = "状态（d删除 1下单，2审核通过，3采购 4入库 5出库 6收货7退货入库 8 退货不收入 9 退货到中央库房 ）", orderNum = "12")
	private String state = "1";

	@ApiModelProperty(value = "备注(字符长度为1-200)")
	@Excel(name = "备注", orderNum = "13")
	private String memo;

	@ApiModelProperty(value = "数据异常 0 正常 1内部商品名异常 2单价区间异常3大数量 4单位异常(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "数据异常 0 正常 1内部商品名异常 2单价区间异常3大数量 4单位异常字符长度为1-1")
	@Excel(name = "数据异常 0 正常 1内部商品名异常 2单价区间异常3大数量 4单位异常", orderNum = "14")
	private String flag = "0";

	@ApiModelProperty(value = "收货时间")
	private Date receiveTime;

	@ApiModelProperty(value = "内部商品_id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "内部商品_id字符长度为1-32")
	@Excel(name = "内部商品_id", orderNum = "16")
	private String inGoodsId;
	
	@ApiModelProperty(value = "内部商品_code") 
	@Excel(name = "内部商品_code", orderNum = "16")
	private String inGoodsCode;
	

	@ApiModelProperty(value = "外部商品_id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "外部商品_id字符长度为1-32")
	@Excel(name = "外部商品_id", orderNum = "17")
	private String outGoodsId;

	@ApiModelProperty(value = "内部产品名称(新增编辑不需要此字段)")
	@Excel(name = "内部产品名称", orderNum = "1")
	private String inGoodsName;

	@ApiModelProperty(value = "内部商品分类(新增编辑不需要此字段)")
	@Excel(name = "内部商品分类", orderNum = "2")
	private String inGoodsTypeId;

	@ApiModelProperty(value = "内部商品分类名称(新增编辑不需要此字段)")
	@Excel(name = "内部商品分类名称", orderNum = "2")
	private String inGoodsType;

	@ApiModelProperty(value = "内部商品单位(新增编辑不需要此字段)")
	@Excel(name = "内部商品单位", orderNum = "2")
	private String inUnitName;

	@ApiModelProperty(value = "编辑修改标志 n不修改   a 新增  e 修改  d 删除")
	private String editFlag;

	@ApiModelProperty(value = "出货数量")
	private Double outAmount;

	@ApiModelProperty(value = "接收数量")
	private Double receiveAmount;
	@ApiModelProperty(value = "出库时间")
	private Date outTime;
	
	
	@ApiModelProperty(value = "客户类型")
	private String customerType;
	
	@ApiModelProperty(value = "仓库名称")
	private String storeName;
	
	@ApiModelProperty(value = "入库单id")
	private String inId;
	
	@ApiModelProperty(value = "入库单id(退货后产生)")
	private String newInId;
	
	@ApiModelProperty(value = "退货供应商记录id")
	private String backProviderId;
	
	@ApiModelProperty(value = "退货中央库记录id")
	private String storehouseBackId;
	@ApiModelProperty(value = "退货损耗id")
	private String lossId;

    @ApiModelProperty("打印订单专用")
	private String companyName;

    @ApiModelProperty("打印订单专用")
    private String specifications;

    @ApiModelProperty("打印订单专用")
    private String goodsId;

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public String getInGoodsCode() {
		return inGoodsCode;
	}

	public void setInGoodsCode(String inGoodsCode) {
		this.inGoodsCode = inGoodsCode;
	}

	public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    private List<StroehouseIn> stroehouseInList;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLossId() {
		return lossId;
	}

    public List<StroehouseIn> getStroehouseInList() {
        return stroehouseInList;
    }

    public void setStroehouseInList(List<StroehouseIn> stroehouseInList) {
        this.stroehouseInList = stroehouseInList;
    }

    public void setLossId(String lossId) {
		this.lossId = lossId;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getOriginalUnit() {
        return originalUnit;
    }

    public void setOriginalUnit(String originalUnit) {
        this.originalUnit = originalUnit;
    }

    public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public Double getOutAmount() {
		return outAmount;
	}

	public void setOutAmount(Double outAmount) {
		this.outAmount = outAmount;
	}

	public Double getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(Double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	// ==========================采购单查询明细时使用字段===========================
	@ApiModelProperty(value = "原始订单编号(字符长度为1-20),查询使用")
	@Excel(name = "原始订单编号", orderNum = "1")
	private String orderNo;

	@ApiModelProperty(value = "2.微信 1为网络数据，0为手工数据(字符长度为1-1)")
	@Excel(name = "2.微信 1为网络数据，0为手工数据", orderNum = "11")
	private String origin = OrderOrigin.webOrder;
	@ApiModelProperty(value = "真实配送时间,查询使用")
	private Date realDistributionDate;
    @ApiModelProperty(value = "配送时间,查询使用")
    private Date distributionDate;

	@ApiModelProperty(value = "订货单位名称,查询使用 ")
	@Excel(name = "订货单位名称", orderNum = "2")
	private String customerName;
	@ApiModelProperty(value = "订货单位id, 查询使用")
	private String customerId;

	// ==========================采购单查询明细时使用字段===========================

	private static final long serialVersionUID = 1L;

	public interface OrderEditFlag {
		String none = "n";
		String add = "a";
		String edit = "e";
		String del = "d";
	}

    public Date getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(Date distributionDate) {
        this.distributionDate = distributionDate;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId == null ? null : orderId.trim();
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName == null ? null : unitName.trim();
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public String getGoodsOriginalName() {
		return goodsOriginalName;
	}

	public void setGoodsOriginalName(String goodsOriginalName) {
		this.goodsOriginalName = goodsOriginalName == null ? null : goodsOriginalName.trim();
	}

	public Double getSelfAmount() {
		return selfAmount;
	}

	public void setSelfAmount(Double selfAmount) {
		this.selfAmount = selfAmount;
	}

	public Double getFreeAmount() {
		return freeAmount;
	}

	public void setFreeAmount(Double freeAmount) {
		this.freeAmount = freeAmount;
	}

	public BigDecimal getSelfSum() {
		return selfSum;
	}

	public void setSelfSum(BigDecimal selfSum) {
		this.selfSum = selfSum;
	}

	public BigDecimal getFreeSum() {
		return freeSum;
	}

	public void setFreeSum(BigDecimal freeSum) {
		this.freeSum = freeSum;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source == null ? null : source.trim();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag == null ? null : flag.trim();
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getInGoodsId() {
		return inGoodsId;
	}

	public void setInGoodsId(String inGoodsId) {
		this.inGoodsId = inGoodsId == null ? null : inGoodsId.trim();
	}

	public String getOutGoodsId() {
		return outGoodsId;
	}

	public void setOutGoodsId(String outGoodsId) {
		this.outGoodsId = outGoodsId == null ? null : outGoodsId.trim();
	}

	public String getInUnitName() {
		return inUnitName;
	}

	public void setInUnitName(String inUnitName) {
		this.inUnitName = inUnitName;
	}

	public String getInGoodsName() {
		return inGoodsName;
	}

	public void setInGoodsName(String inGoodsName) {
		this.inGoodsName = inGoodsName;
	}

	public String getInGoodsTypeId() {
		return inGoodsTypeId;
	}

	public void setInGoodsTypeId(String inGoodsTypeId) {
		this.inGoodsTypeId = inGoodsTypeId;
	}

	public String getInGoodsType() {
		return inGoodsType;
	}

	public void setInGoodsType(String inGoodsType) {
		this.inGoodsType = inGoodsType;
	}

	public String getEditFlag() {
		return editFlag;
	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Date getRealDistributionDate() {
		return realDistributionDate;
	}

	public void setRealDistributionDate(Date realDistributionDate) {
		this.realDistributionDate = realDistributionDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	

	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}
	
	



	public BigDecimal getReceivePrice() {
		return receivePrice;
	}

	public void setReceivePrice(BigDecimal receivePrice) {
		this.receivePrice = receivePrice;
	}

	public BigDecimal getReceiveSum() {
		return receiveSum;
	}

	public void setReceiveSum(BigDecimal receiveSum) {
		this.receiveSum = receiveSum;
	}

    



	public String getInId() {
		return inId;
	}

	public void setInId(String inId) {
		this.inId = inId;
	}

	public String getNewInId() {
		return newInId;
	}

	public void setNewInId(String newInId) {
		this.newInId = newInId;
	}

	public String getBackProviderId() {
		return backProviderId;
	}

	public void setBackProviderId(String backProviderId) {
		this.backProviderId = backProviderId;
	}

	public String getStorehouseBackId() {
		return storehouseBackId;
	}

	public void setStorehouseBackId(String storehouseBackId) {
		this.storehouseBackId = storehouseBackId;
	}





	public interface OrderDetailState {
		// 状态（d删除 1下单，2审核通过，3预采购、4采购 5入库 6出库
		// 7收货8退货不收入9 退货到中央库房 a 退货到中央库（商品留原库 ）
		String del = "d";
		String place = "1";
		String audited = "2";
		String prePurchase = "3";
		String Purchase = "4";
		String in = "5";
		String out = "6";
		String received = "7";
		String back = "8";//未使用
		String backGoodsToCenter = "9";
		String backToCenter = "a";
		String backToProvider="b";
		String recordLoss="c";
		String tplusLock ="f";
		String tplusExport ="e";
		
		
	}

	public interface OrderDetailSource {
		// 来源标志，0 原始订单，1 放弃购买，
		// 2新增购买 3 修改购买 主要针对web订单和微信订单的修改
		String original = "0";
		String abandon = "1";
		String newDetail = "2";
		String eidtDetail = "3";
	}

	public interface OrderDetailError {
		// 数据异常 0 正常 1内部商品名异常 2单价区间异常3大数量4订货单位异常
		String noError = "0";
		String goodsIdError = "1";
		String goodsPricesError = "2";
		String goodsAmountError = "3";
		String customerIdError = "3";
	}

}