package com.myqm.pojo.gs.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;
import javax.validation.constraints.Size;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** Order */
@JsonInclude(Include.NON_NULL)
public class Order implements Serializable , Cloneable{
	@JsonProperty("orderId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "原始订单编号(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "原始订单编号字符长度为1-20")
	@Excel(name = "原始订单编号", orderNum = "1")
	private String orderNo;

	@ApiModelProperty(value = "配送单位id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "配送单位id字符长度为1-32")
	@Excel(name = "配送单位id", orderNum = "2")
	private String customerId;

	@ApiModelProperty(value = "配送时间")
	private Date distributionDate;

	@ApiModelProperty(value = "总价")
	private BigDecimal lumpSum;
	
	@ApiModelProperty(value = "自费")
	private BigDecimal selfSum = null;
	
	@ApiModelProperty(value = "免费")
	private BigDecimal freeSum = null;

	@ApiModelProperty(value = "建单时间(字符长度为1-30)")
	@Size(min = 1, max = 30, message = "建单时间字符长度为1-30")
	@Excel(name = "建单时间", orderNum = "5")
	@JSONField(format = "yyyy-MM-dd")
	private String createTime;

	@ApiModelProperty(value = "状态（d删除 1下单，2审核通过，3预采购(不使用) 4采购 (不使用) 5入库(不使用) 6出库 7收货8部分收货）(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "状态（d删除 1下单，2审核通过，3预采购(不使用) 4采购(不使用)  5入库(不使用) 6出库 7收货8部分收货）字符长度为1-1")
	@Excel(name = "状态（d删除 1下单，2审核通过，3预采购 4采购  5入库 6出库 7收货8部分收货）", orderNum = "6")
	private String state = OrderState.place;

	@ApiModelProperty(value = "审核人 ")
	@Size(min = 1, max = 20, message = "审核人 ")
	@Excel(name = "审核人", orderNum = "7")
	private String auditorName;

	@ApiModelProperty(value = "审核人Id ")
	@Size(min = 1, max = 32, message = "审核人Id ")
	@Excel(name = "审核人", orderNum = "7")
	private String auditorId;

	@ApiModelProperty(value = "审核时间(字符长度为1-12)")
	@Size(min = 1, max = 12, message = "审核时间字符长度为1-12")
	@Excel(name = "审核时间", orderNum = "8")
	private String auditorTime;

	@ApiModelProperty(value = "打印计数")
	private int printCount = 0;

	@ApiModelProperty(value = "备注(字符长度为1-200)")
	@Excel(name = "备注", orderNum = "10")
	private String memo;

	@ApiModelProperty(value = "2.微信 1为网络数据，0为手工数据(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "2.微信 1为网络数据，0为手工数据字符长度为1-1")
	@Excel(name = "2.微信 1为网络数据，0为手工数据", orderNum = "11")
	private String origin = OrderOrigin.webOrder;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "建立人(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "建立人字符长度为1-32")
	@Excel(name = "建立人", orderNum = "14")
	private String createrId;

	@ApiModelProperty(value = "修改人(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "修改人字符长度为1-32")
	@Excel(name = "修改人", orderNum = "15")
	private String updaterId;

	@ApiModelProperty(value = "数据异常 0 正常 1订单明细异常 2 订单重复异常(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "数据异常 0 正常 1订单明细异常 2 订单重复异常字符长度为1-1")
	@Excel(name = "数据异常 0 正常 1订单明细异常 2 订单重复异常", orderNum = "16")
	private String flag = OrderError.noError;

	@ApiModelProperty(value = "真实配送时间")
	private Date realDistributionDate;

	@ApiModelProperty(value = "审核意见(字符长度为1-255)")
	@Excel(name = "审核意见", orderNum = "18")
	private String auditMemo;

	@ApiModelProperty(value = "0不通过， 1 通过")
	private Long verified;

	@ApiModelProperty(value = "订单细节")
	List<OrderDetail> orderDetailList;

	@ApiModelProperty(value = "订货单位名称,新增修改不用填写")
	@Excel(name = "订货单位名称", orderNum = "2")
	private String customerName;

	@ApiModelProperty(value = "订货单位类型名称 ,新增修改不用填写")
	@Excel(name = "订货单位类型", orderNum = "2")
	private String customerType;

	@ApiModelProperty(value = "订货单位类型id ,新增修改不用填写")
	@Excel(name = "订货单位类型id", orderNum = "2")
	private String customerTypeId;

	@ApiModelProperty(value = "出货人id")
	@Excel(name = "出货人id", orderNum = "2")
	private String outerId;

	@ApiModelProperty(value = "出货人姓名")
	@Excel(name = "出货人姓名", orderNum = "2")
	private String outerName;

	
 
	 
	public String getOuterName() {
		return outerName;
	}

	public void setOuterName(String outerName) {
		this.outerName = outerName;
	}

	@ApiModelProperty(value = "收获时间")
	@Excel(name = "收获时间", orderNum = "2")
	private Date receiveTime;

	@ApiModelProperty(value = "出货时间")
	@Excel(name = "出货时间", orderNum = "2")
	private Date outTime;

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public String getOuterId() {
		return outerId;
	}

	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}

	private static final long serialVersionUID = 1L;

	
	public BigDecimal getSelfSum() {
		if ((selfSum == null)&&(freeSum == null))
			return  this.lumpSum;
		else
			return selfSum;
	}

	public void setSelfSum(BigDecimal selfSum) {
		this.selfSum = selfSum;
	}

	public BigDecimal getFreeSum() {
		if (freeSum == null)
			return new BigDecimal(0);
		else
			return freeSum;
	}

	public void setFreeSum(BigDecimal freeSum) {
		this.freeSum = freeSum;
	}

	/**
	 * 更新人Name
	 */
	@ApiModelProperty(value = "更新人Name,新增修改不用填写")
	protected String updaterName;

	/**
	 * 新建人Name
	 */
	@ApiModelProperty(value = "新建人Name,新增修改不用填写")
	protected String createrName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo == null ? null : orderNo.trim();
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId == null ? null : customerId.trim();
	}

	public Date getDistributionDate() {
		return distributionDate;
	}

	public void setDistributionDate(Date distributionDate) {
		this.distributionDate = distributionDate;
	}

	public BigDecimal getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(BigDecimal lumpSum) {
		this.lumpSum = lumpSum;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	public String getAuditorTime() {
		return auditorTime;
	}

	public void setAuditorTime(String auditorTime) {
		this.auditorTime = auditorTime == null ? null : auditorTime.trim();
	}

	public int getPrintCount() {
		return printCount;
	}

	public void setPrintCount(int printCount) {
		this.printCount = printCount;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin == null ? null : origin.trim();
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId == null ? null : createrId.trim();
	}

	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId == null ? null : updaterId.trim();
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag == null ? null : flag.trim();
	}

	public Date getRealDistributionDate() {
		return realDistributionDate;
	}

	public void setRealDistributionDate(Date realDistributionDate) {
		this.realDistributionDate = realDistributionDate;
	}

	public String getAuditMemo() {
		return auditMemo;
	}

	public void setAuditMemo(String auditMemo) {
		this.auditMemo = auditMemo == null ? null : auditMemo.trim();
	}

	public Long getVerified() {
		return verified;
	}

	public void setVerified(Long verified) {
		this.verified = verified;
	}

	public List<OrderDetail> getOrderDetailList() {
		return orderDetailList;
	}

	public void setOrderDetailList(List<OrderDetail> orderDetailList) {
		this.orderDetailList = orderDetailList;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public String getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}

	
	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNo=" + orderNo + ", customerId=" + customerId + ", distributionDate="
				+ distributionDate + ", lumpSum=" + lumpSum + ", selfSum=" + selfSum + ", freeSum=" + freeSum
				+ ", createTime=" + createTime + ", state=" + state + ", auditorName=" + auditorName + ", auditorId="
				+ auditorId + ", auditorTime=" + auditorTime + ", printCount=" + printCount + ", memo=" + memo
				+ ", origin=" + origin + ", updateTime=" + updateTime + ", createrId=" + createrId + ", updaterId="
				+ updaterId + ", flag=" + flag + ", realDistributionDate=" + realDistributionDate + ", auditMemo="
				+ auditMemo + ", verified=" + verified + ", orderDetailList=" + orderDetailList + ", customerName="
				+ customerName + ", customerType=" + customerType + ", customerTypeId=" + customerTypeId + ", outerId="
				+ outerId + ", outerName=" + outerName + ", receiveTime=" + receiveTime + ", outTime=" + outTime
				+ ", updaterName=" + updaterName + ", createrName=" + createrName + "]";
	}

	@Override
	public Object clone() {
		Order o = null;
		try {
			o = (Order) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return o;
	}

	public interface OrderState {
		// 状态（d删除 1下单，2审核通过，3预采购 4采购 5入库 6出库 7收货8部分收货）

		String del = "d";
		String place = "1";
		String audited = "2";
		String prePurchase = "3";
		String Purchase = "4";
		String in = "5";
		String out = "6";
		String received = "7";
		String partReceived = "8";
		String tplusLock ="9";
		String tplusExport ="a";
	}

	public interface OrderOrigin {
		// 2.微信 1为网络数据，0为手工数据
		String manual = "0";
		String webOrder = "1";
		String weixin = "2";
	}

	public interface OrderError {
		// 数据异常 0 正常 1订单明细异常
		// 2 订单重复异常 3订单明细中有商品没有内部商品名，
		// 4 订单中单位存在问题
		String noError = "0";
		String detailError = "1";
		String repeatError = "2";
		String goodsIdError = "3";
		String customerIdError = "3";
	}

}