package com.myqm.pojo.gs.order;

public class OrderDetailStatistic {

    private String state;
    private int total;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
