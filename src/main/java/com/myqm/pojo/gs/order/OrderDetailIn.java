package com.myqm.pojo.gs.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**OrderDetailIn*/
@JsonInclude(Include.NON_NULL)
public class OrderDetailIn implements Serializable {

	@ApiModelProperty(value = "(字符长度为1-32)")
	@Size(min=1, max=32,message="字符长度为1-32")
	@Excel(name="",orderNum="0")
	private String detailId;

	@ApiModelProperty(value = "(字符长度为1-32)")
	@Size(min=1, max=32,message="字符长度为1-32")
	@Excel(name="",orderNum="1")
	private String inId;

	@ApiModelProperty(value = "数量")
	private Double amount;


    private static final long serialVersionUID = 1L;

    public String getDetailId () {   
    	 return detailId;
    }

    public void setDetailId (String detailId) {
    	 this.detailId= detailId == null ? null : detailId.trim();
    }

    public String getInId () {   
    	 return inId;
    }

    public void setInId (String inId) {
    	 this.inId= inId == null ? null : inId.trim();
    }

	public Double getAmount () {   
		  return amount;
	}
	
	public void setAmount (Double amount) {
		  this.amount= amount;
	}


	public void difference(OrderDetailIn obj) {
		String defString = "";
	   if(!this.detailId.equals(obj.getDetailId())) {
			 defString += "从" + this.detailId +"修改为" +obj.getDetailId();
		}

	   if(!this.inId.equals(obj.getInId())) {
			 defString += "从" + this.inId +"修改为" +obj.getInId();
		}

		if (Math.abs(this.amount - obj.getAmount()) > 0 ) {
			defString += "数量从" + this.amount + "修改为" + obj.getAmount();
		}


    }
}