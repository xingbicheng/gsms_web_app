package com.myqm.pojo.gs.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderSum implements Serializable {
    @ApiModelProperty(value = "统计一个类的总价")
    private BigDecimal total;
    @ApiModelProperty(value = "统计一个类的总数量")
    private double sumAmount;
    @ApiModelProperty(value = "内部商品名")
    private String inGoodsId;

    @ApiModelProperty(value="统计一个类的收货数量总和")
    private double sumReceiverAmount;

    @ApiModelProperty(value="统计一个类的出库数量总和")
    private double sumOutAmount;

    @ApiModelProperty(value="统计一个类的收货总价总和")
    private BigDecimal totalReceiveSum;



    @ApiModelProperty(value="统计一个类的收货总价总和")
    private BigDecimal customerTypeId;


    public BigDecimal getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(BigDecimal customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public double getSumReceiverAmount() {
        return sumReceiverAmount;
    }

    public void setSumReceiverAmount(double sumReceiverAmount) {
        this.sumReceiverAmount = sumReceiverAmount;
    }

    public double getSumOutAmount() {
        return sumOutAmount;
    }

    public void setSumOutAmount(double sumOutAmount) {
        this.sumOutAmount = sumOutAmount;
    }

    public BigDecimal getTotalReceiveSum() {
        return totalReceiveSum;
    }

    public void setTotalReceiveSum(BigDecimal totalReceiveSum) {
        this.totalReceiveSum = totalReceiveSum;
    }

    public double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(double sumAmount) {
        this.sumAmount = sumAmount;
    }

    public String getInGoodsId() {
        return inGoodsId;
    }

    public void setInGoodsId(String inGoodsId) {
        this.inGoodsId = inGoodsId;
    }
}
