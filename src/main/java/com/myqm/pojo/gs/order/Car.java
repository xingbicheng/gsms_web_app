package com.myqm.pojo.gs.order;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;

public class Car implements Serializable{ 
	@JsonIgnoreProperties
	@ApiModelProperty(value = "客户Id")
	private String customerId;
	
	@ApiModelProperty(value = "商品Id")
	@NotNull
	private String goodsId;
	
	@ApiModelProperty(value = "数量")
	@NotNull
	private Double count;
	
	@ApiModelProperty(value = "商品名称")
	private String goodsName;
	
	@ApiModelProperty(value = "商品分类")
	private String goodsType;
	
	@ApiModelProperty(value = "计量单位")
	private String unitName;
	
	private static final long serialVersionUID = 1L;

	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
}
