package com.myqm.pojo.gs.purchase;

import io.swagger.annotations.ApiModelProperty;

public class StoreCustomerType {

	private String storeId;
	private String customerTypeId;

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

}
