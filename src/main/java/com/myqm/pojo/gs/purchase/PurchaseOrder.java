package com.myqm.pojo.gs.purchase;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.time.DateUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/** PurchaseOrder */
@JsonInclude(Include.NON_NULL)
public class PurchaseOrder implements Serializable {
	@JsonProperty("purchaseOrderId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "字符长度为1-20")
	@Excel(name = "", orderNum = "1")
	private String orderNo;


	@ApiModelProperty(value = "采购时间")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date purchaseDate;

	@ApiModelProperty(value = "采购人")
	private String purchaserId;

	@ApiModelProperty(value = "采购人姓名 新增修改不用填写")
	private String purchaserName;

	@ApiModelProperty(value = "d 删除 0下单 1审核中 2审核通过，3采购中-入库中 4入库完成(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "d 删除 0下单 1审核中 2审核通过，3采购中-入库中 4入库完成字符长度为1-1")
	@Excel(name = "d 删除 0下单 1审核中 2审核通过，3采购中-入库中 4入库完成", orderNum = "4")
	private String state = PurchaseOrderState.place;


	private String purchaseType = PurchaseType.byOrder;

	@ApiModelProperty(value = "建单时间")
	private Timestamp createTime;

	@ApiModelProperty(value = "更新时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "建单人")
	private String createrId;

	@ApiModelProperty(value = "修改人")
	private String updaterId;

	@ApiModelProperty(value = "实际采购总价")
	private BigDecimal sum;

	@ApiModelProperty(value = "打印次数")
	private int printCount = 0;

	@ApiModelProperty(value = "备注(字符长度为1-200)")
	@Excel(name = "备注", orderNum = "9")
	private String memo;

	@ApiModelProperty(value = "商品类型（如蔬菜+肉类）(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "商品类型（如蔬菜+肉类）字符长度为1-255")
	@Excel(name = "商品类型（如蔬菜+肉类）", orderNum = "10")
	private String goodsTypes;

	@ApiModelProperty(value = "所有客户分类 如（涪城+游仙）(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "所有客户分类 如（涪城+游仙）字符长度为1-255")
	@Excel(name = "所有客户分类 如（涪城+游仙）", orderNum = "11")
	private String customerTypes;

	private List<PurchaseOrderDetail> orderDetailList;

	/**
	 * 更新人Name
	 */
	@ApiModelProperty(value = "更新人Name,新增修改不用填写")
	protected String updaterName;

	/**
	 * 新建人Name
	 */
	@ApiModelProperty(value = "新建人Name,新增修改不用填写")
	protected String createrName;

	public List<PurchaseOrderDetail> getOrderDetailList() {
		return orderDetailList;
	}

	public void setOrderDetailList(List<PurchaseOrderDetail> details) {
		this.orderDetailList = details;
	}

	private static final long serialVersionUID = 1L;

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo == null ? null : orderNo.trim();
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getPurchaserId() {
		return purchaserId;
	}

	public void setPurchaserId(String purchaserId) {
		this.purchaserId = purchaserId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public int getPrintCount() {
		return printCount;
	}

	public void setPrintCount(int printCount) {
		this.printCount = printCount;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getGoodsTypes() {
		return goodsTypes;
	}

	public void setGoodsTypes(String goodsTypes) {
		this.goodsTypes = goodsTypes == null ? null : goodsTypes.trim();
	}

	public String getCustomerTypes() {
		return customerTypes;
	}

	public void setCustomerTypes(String customerTypes) {
		this.customerTypes = customerTypes == null ? null : customerTypes.trim();
	}

	public String getPurchaserName() {
		return purchaserName;
	}

	public void setPurchaserName(String purchaserName) {
		this.purchaserName = purchaserName;
	}

	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId;
	}

	public String difference(PurchaseOrder obj) {
		String defString = "";
		try {

            if(this.purchaseDate!=null&&obj.getPurchaseDate()!=null)
                if ((!DateUtils.isSameDay(purchaseDate, createTime) || !DateUtils.isSameDay(purchaseDate, updateTime))) {
                    defString += "采购时间从" + this.purchaseDate + "修改为" + obj.getPurchaseDate();
                }

            if (!Objects.equals(this.purchaserId, obj.getPurchaserId())) {
                defString += "采购人从" + this.purchaserId + "修改为" + obj.getPurchaserId();
            }

            if (!this.state.equals(obj.getState())) {
                String[] s={this.state,obj.getState()};
                int i=0;
                while(i<2){
                    switch (s[i]){
                        case "d":s[i]="删除";break;
                        case "0":s[i]="下单";break;
                        case "1":s[i]="审核中";break;
                        case "2":s[i]="审核通过";break;
                        case "3":s[i]="采购中-入库中";break;
                        case "4":s[i]="入库完成";break;
                    }
                    i++;
                }
                defString += "状态从" + s[0] + "修改为" + s[1];
            }

            if (!Objects.equals(this.createrId, obj.getCreaterId())) {
                defString += "建单人从" + this.createrId + "修改为" + obj.getCreaterId();
            }

            if(this.sum!=null&&obj.getSum()!=null)
                if (this.sum.compareTo(obj.getSum()) != 0) {
                    defString += "实际采购总价从" + this.sum + "修改为" + obj.getSum();
                }

            if (!Objects.equals(this.printCount, obj.getPrintCount())) {
                defString += "打印次数从" + this.printCount + "修改为" + obj.getPrintCount();
            }

            if(this.memo!=null&&obj.getMemo()!=null)
                if (!this.memo.equals(obj.getMemo())) {
                    defString += "备注从" + this.memo + "修改为" + obj.getMemo();
                }

            if(this.goodsTypes!=null&&obj.getGoodsTypes()!=null)
                if (!this.goodsTypes.equals(obj.getGoodsTypes())) {
                    defString += "商品类型（如蔬菜+肉类）从" + this.goodsTypes + "修改为" + obj.getGoodsTypes();
                }

            if(this.customerTypes!=null&&obj.getCustomerTypes()!=null)
                if (!this.customerTypes.equals(obj.getCustomerTypes())) {
                    defString += "所有客户分类 如（涪城+游仙）从" + this.customerTypes + "修改为" + obj.getCustomerTypes();
                }
        }
        catch (Exception e){
		    defString+="日志ERROR!";
        }
		return defString;
	}

	// public static void main(String[] args)
	//
	// {
	// SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
	// try {
	// Date time1 = (Date) dateformat.parse("2016-6-19");
	// Date time2 = (Date) dateformat.parse("2017-6-19");
	// System.out.println(DateUtils.isSameDay(time1,time2));
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	@Override
	public String toString() {
		return "PurchaseOrder{" + "id='" + id + '\'' + ", orderNo='" + orderNo + '\'' + ", purchaseDate=" + purchaseDate
				+ ", purchaserId=" + purchaserId + ", state='" + state + '\'' + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", createrId=" + createrId + ", sum=" + sum + ", printCount="
				+ printCount + ", memo='" + memo + '\'' + ", goodsTypes='" + goodsTypes + '\'' + ", customerTypes='"
				+ customerTypes + '\'' + ", details=" + orderDetailList + '}';
	}

	public interface PurchaseOrderState {
		// d 删除 0下单 1审核通过，2入库 
		String del = "d";
		String place = "0"; 
		String audited = "1"; 
		String in = "2";
	}

    public interface PurchaseType {
        // 采购方式（0订单采购    1计划采购）
        String byOrder = "0";
        String byPlan = "1";

    }
}