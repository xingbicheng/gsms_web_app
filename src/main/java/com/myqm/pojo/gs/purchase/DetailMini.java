package com.myqm.pojo.gs.purchase;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailMini{
    @JsonProperty("purchaseOrderDetailId")
    private String detailId;

    private Double lossAmount;

    private String storeId;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Double getLossAmount() {
        return lossAmount;
    }

    public void setLossAmount(Double lossAmount) {
        this.lossAmount = lossAmount;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }
}
