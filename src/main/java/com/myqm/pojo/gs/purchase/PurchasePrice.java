package com.myqm.pojo.gs.purchase;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import net.sf.ehcache.statistics.sampled.SampledCacheStatistics;

/** PurchasePrice */
@JsonInclude(Include.NON_NULL)
public class PurchasePrice implements Serializable,Cloneable  {

	@JsonProperty("purchasePriceId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "供应商编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "供应商编号字符长度为1-32")
	@Excel(name = "供应商编号", orderNum = "1")
	private String providerId;
	
	@ApiModelProperty(value = "询问货物的id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "询问货物的id字符长度为1-32")
	@Excel(name = "询问货物的id", orderNum = "2")
	private String goodsId;

	@ApiModelProperty(value = "采购单明细(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "采购单明细字符长度为1-32")
	@Excel(name = "采购单明细", orderNum = "3")
	private String purchaseDetailId;
	
	@ApiModelProperty(value = "预购数量")
	private BigDecimal amount;
	
	@ApiModelProperty(value = "提供商品价格")
	private BigDecimal price;
	
	@ApiModelProperty(value = "供货商更改价格时间")
	private Timestamp updateTime;
	
	@ApiModelProperty(value = "供货商创建价格时间")
	private Timestamp createTime;
	 
	
 
	
	@ApiModelProperty(value = "d删除1草稿 2正式询价3已经报价(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "d删除1草稿 2正式询价3已经报价字符长度为1-1")
	@Excel(name = "d删除1草稿 2正式询价3已经报价", orderNum = "9")
	private String state = "1";
	
	@ApiModelProperty(value ="限制时间(最迟报价时间)")
	private String limitDate;
	
	@ApiModelProperty(value ="建立人")	
	@Size(min = 1, max = 32,message ="询问建立人")
	@Excel(name = "建立人", orderNum = "11")
	private String createrId;
	
	@ApiModelProperty(value ="建立人")	 
	@Excel(name = "建立人姓名", orderNum = "11")
	private String createrName;
 
	
	@ApiModelProperty(value = "供应商名称(字符长度为1-20),新增不用填写")
	@Excel(name = "供应商名称", orderNum = "1")
	private String providerName;
	
	@ApiModelProperty(value = "内部产品名称(字符长度为1-50),新增不用填写")
	@Excel(name = "内部产品名称", orderNum = "2")
	private String goodsName;

	@ApiModelProperty(value = "采购单编号(字符长度为1-32),新增不用填写")
	@Excel(name = "采购单编号", orderNum = "3")
	private String orderId;
	 
	
	private static final long serialVersionUID = 1L;
@Override
	 public Object clone() throws CloneNotSupportedException {
		         return super.clone();
	    }


	public String getCreaterName() {
	return createrName;
}

		public void setCreaterName(String createrName) {
			this.createrName = createrName;
		}
 
	public interface PurchasePriceState{
		//d删除1草稿 2正式询价3已经报价
		
		String del = "d";
		String draft = "1";
		String quotation = "2";
		String  quoted = "3";
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId == null ? null : providerId.trim();
	}

	
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId == null ? null : goodsId.trim();
	}

	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId == null ? null : purchaseDetailId.trim();
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

 
	
 
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}
	
	public String getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(String limitDate) {
		this.limitDate = limitDate;
	}
	
	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId == null ? null : createrId.trim();
	}

	 
	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public void difference(PurchasePrice obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}
		
		if (!this.providerId.equals(obj.getProviderId())) {
			defString += "供应商从" + this.providerId + "修改为" + obj.getProviderId();
		}
		
		if (!this.goodsId.equals(obj.getGoodsId())) {
			defString += "询问货物从" + this.goodsId + "修改为" + obj.getGoodsId();
		}
		
		if (!this.purchaseDetailId.equals(obj.getPurchaseDetailId())) {
			defString += "采购单明细从" + this.purchaseDetailId + "修改为" + obj.getPurchaseDetailId();
		}
		
		if (!this.amount.equals(obj.getAmount())) {
			defString += "预购数量从" + this.amount + "修改为" + obj.getAmount();
		}
		
		if (this.price.compareTo(obj.getPrice()) != 0) {
			defString += "提供商品价格从" + this.price + "修改为" + obj.getPrice();
		} 
		if (!this.state.equals(obj.getState())) {
			defString += "d删除1草稿 2正式询价3已经报价从" + this.state + "修改为" + obj.getState();
		}
		
		if(!this.createrId.equals(obj.getCreaterId())) {
			defString += "建立人" + this.createrId + "修改为" +obj.getCreaterId();
		}
			
		if(!this.createrId.equals(obj.getCreaterId())) {
			defString += "修改人" + this.createrId + "修改为" +obj.getCreaterId();
		}
    }

	
}

