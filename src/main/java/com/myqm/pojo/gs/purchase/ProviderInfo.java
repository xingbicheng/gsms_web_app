package com.myqm.pojo.gs.purchase;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size; 
import io.swagger.annotations.ApiModelProperty;

public class ProviderInfo {
	@ApiModelProperty(value = "实际采购单价")
	@NotNull
	private BigDecimal price;
	
	@ApiModelProperty(value = "供货商id")
	@NotNull
	private String providerId;
	
	@ApiModelProperty(value = "包装备注(字符长度为1-200)")
	@Size(min = 1, max = 200, message = "包装备注字符长度为1-200") 
	private String packingMemo;
	
	private boolean canBack = false;
	
	
	
	public boolean isCanBack() {
		return canBack;
	}

	public void setCanBack(boolean canBack) {
		this.canBack = canBack;
	}

	private List<String> inIds;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getPackingMemo() {
		return packingMemo;
	}

	public void setPackingMemo(String packingMemo) {
		this.packingMemo = packingMemo;
	}

	public List<String> getInIds() {
		return inIds;
	}

	public void setInIds(List<String> inIds) {
		this.inIds = inIds;
	}
	
}
