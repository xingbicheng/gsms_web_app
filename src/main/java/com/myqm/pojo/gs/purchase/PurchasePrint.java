package com.myqm.pojo.gs.purchase;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List; 
import io.swagger.annotations.ApiModelProperty;

public class PurchasePrint {
	@ApiModelProperty(value = "订单编号") 
	String orderNo;
	@ApiModelProperty(value = "采购员") 	
	String puchaseName;
	@ApiModelProperty(value = "采购时间") 
	Date puchaseDate; 
	@ApiModelProperty(value = "采购明细") 
	List<PurchaseLinePrint> list;
	
	
	public PurchasePrint(String no,	String pname,Date pdate )
	{
		orderNo = no;
		puchaseName = pname;
		puchaseDate = pdate;
		
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getPuchaseName() {
		return puchaseName;
	}
	public void setPuchaseName(String puchaseName) {
		this.puchaseName = puchaseName;
	}
	public Date getPuchaseDate() {
		return puchaseDate;
	}
	public void setPuchaseDate(Date puchaseDate) {
		this.puchaseDate = puchaseDate;
	}
	public List<PurchaseLinePrint> getList() {
		return list;
	}
	public void setList(List<PurchaseLinePrint> list) {
		this.list = list;
	} 
}
