package com.myqm.pojo.gs.purchase;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.myqm.pojo.base.DoubleSerialize;
import io.swagger.annotations.ApiModelProperty;

/** PurchaseOrderDetail */
@JsonInclude(Include.NON_NULL)
public class PurchaseOrderDetail implements Serializable {
	@JsonProperty("purchaseOrderDetailId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "采购单编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "采购单编号字符长度为1-32")
	@Excel(name = "采购单编号", orderNum = "1")
	private String orderId;

    @JsonSerialize(using = DoubleSerialize.class)
	@ApiModelProperty(value = "统计采购数量")
	private Double statisticAmount;

    @JsonSerialize(using = DoubleSerialize.class)
	@ApiModelProperty(value = "损耗量")
	private Double lossAmount;

	@ApiModelProperty(value = "总价")
	private BigDecimal sum;

	@ApiModelProperty(value = "实际采购均单价，系统自动计算")
	private BigDecimal averagePrice;

	@ApiModelProperty(value = "采购方式如：（中央库调拨+订单采购+计划采购）(字符长度为1-50)")
	@Size(min = 1, max = 50, message = "采购方式如：（中央库调拨+订单采购+计划采购）字符长度为1-50")
	@Excel(name = "采购方式如：（中央库调拨+订单采购+计划采购）", orderNum = "6")
	private String purchaseType;

	@ApiModelProperty(value = "状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）字符长度为1-1")
	@Excel(name = "状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）", orderNum = "7")
	private String state = PurchaseOrderDetailState.place;

	@ApiModelProperty(value = "内部商品名id")
	private String goodsId;

    @JsonSerialize(using = DoubleSerialize.class)
	@ApiModelProperty(value = "总采购数量")
	private Double amount;

	@ApiModelProperty(value = "审计人")
	private String auditorId;

	@ApiModelProperty(value = "审计备注(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "审计备注字符长度为1-255")
	@Excel(name = "审计备注", orderNum = "11")
	private String auditMemo;

	@ApiModelProperty(value = "备注(字符长度为1-200)")
	@Excel(name = "备注", orderNum = "12")
	private String memo;

	@ApiModelProperty(value = "0不通过， 1 通过(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "0不通过， 1 通过字符长度为1-1")
	@Excel(name = "0不通过， 1 通过", orderNum = "13")
	private String verified;

	@ApiModelProperty(value = "审计时间")
	private Timestamp auditTime;

	@ApiModelProperty(value = "商品类型id，新增删除不需要")
	private String goodsTypeId;
	@ApiModelProperty(value = "商品名称，新增删除不需要")
	private String goodsName;
	@ApiModelProperty(value = "商品单位，新增删除不需要")
	private String unitName;
	@ApiModelProperty(value = "商品类型，新增删除不需要")
	private String goodsType;

	@ApiModelProperty(value = "审核人姓名")
	private String auditName;

	@ApiModelProperty(value = "实际采购量，系统自动计算")
	private double realAmount;

	@ApiModelProperty(value = "实际采购总价，系统自动计算")
	private BigDecimal realSum;

	@ApiModelProperty(value = "订单明细编号,新增时必填，修改时如果不填写代表没有做任何修改")
	private List<String> orderDetailIds;

    @ApiModelProperty(value = "库房id（计划采购单填写）")
    private String storeId;

    @ApiModelProperty(value = "库房名称（计划采购单填写）")
    private String storeName;
    
	@ApiModelProperty(value = "编辑修改标志 n不修改   a 新增  e 修改  d 删除")
	private String editFlag;
    
	@ApiModelProperty(value = "损耗率")
	private Float lossRate;

	public Float getLossRate() {
		return lossRate;
	}

	public void setLossRate(Float lossRate) {
		this.lossRate = lossRate;
	}

	public String getEditFlag() {
		return editFlag;
	}

	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}

	private static final long serialVersionUID = 1L;

    public String getStoreId() {
        return storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId == null ? null : orderId.trim();
	}

	public Double getStatisticAmount() {
		return statisticAmount;
	}

	public void setStatisticAmount(Double statisticAmount) {
		this.statisticAmount = statisticAmount;
	}

	public Double getLossAmount() {
		return lossAmount;
	}

	public void setLossAmount(Double lossAmount) {
		this.lossAmount = lossAmount;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public BigDecimal getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType == null ? null : purchaseType.trim();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}

	public String getAuditMemo() {
		return auditMemo;
	}

	public void setAuditMemo(String auditMemo) {
		this.auditMemo = auditMemo == null ? null : auditMemo.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getVerified() {
		return verified;
	}

	public void setVerified(String verified) {
		this.verified = verified == null ? null : verified.trim();
	}

	public Timestamp getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Timestamp auditTime) {
		this.auditTime = auditTime;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	// public String getEditFlag() {
	// return editFlag;
	// }
	//
	// public void setEditFlag(String editFlag) {
	// this.editFlag = editFlag;
	// }

	public double getRealAmount() {
		return realAmount;
	}

	public void setRealAmount(double realAmount) {
		this.realAmount = realAmount;
	}

	public String difference(PurchaseOrderDetail obj) {
		String defString = "";
		try{
            if (!Objects.equals(this.id, obj.getId())) {
                defString += "从" + this.id + "修改为" + obj.getId();
            }

            if (!this.orderId.equals(obj.getOrderId())) {
                defString += "采购单编号从" + this.orderId + "修改为" + obj.getOrderId();
            }

            if (Math.abs(this.statisticAmount - obj.getStatisticAmount()) > 0) {
                defString += "统计采购数量从" + this.statisticAmount + "修改为" + obj.getStatisticAmount();
            }

            if (Math.abs(this.lossAmount - obj.getLossAmount()) > 0) {
                defString += "损耗量从" + this.lossAmount + "修改为" + obj.getLossAmount();
            }

            if (this.sum.compareTo(obj.getSum()) != 0) {
                defString += "总价从" + this.sum + "修改为" + obj.getSum();
            }


            if (!Objects.equals(this.averagePrice,obj.getAveragePrice())) {
                defString += "实际采购均单价从" + this.averagePrice + "修改为" + obj.getAveragePrice();
            }

            if (!this.purchaseType.equals(obj.getPurchaseType())) {
                defString += "采购方式如：（中央库调拨+订单采购+计划采购）从" + this.purchaseType + "修改为" + obj.getPurchaseType();
            }

            if (!this.state.equals(obj.getState())) {
                defString += "状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）从" + this.state + "修改为" + obj.getState();
            }

            if (!Objects.equals(this.goodsId, obj.getGoodsId())) {
                defString += "内部商品名id从" + this.goodsId + "修改为" + obj.getGoodsId();
            }

            if (Math.abs(this.amount - obj.getAmount()) > 0) {
                defString += "总采购数量从" + this.amount + "修改为" + obj.getAmount();
            }

            if (!Objects.equals(this.auditorId, obj.getAuditorId())) {
                defString += "审计人从" + this.auditorId + "修改为" + obj.getAuditorId();
            }

            if (!this.auditMemo.equals(obj.getAuditMemo())) {
                defString += "审计备注从" + this.auditMemo + "修改为" + obj.getAuditMemo();
            }

            if (!this.memo.equals(obj.getMemo())) {
                defString += "备注从" + this.memo + "修改为" + obj.getMemo();
            }

            if (!this.verified.equals(obj.getVerified())) {
                defString += "0不通过， 1 通过从" + this.verified + "修改为" + obj.getVerified();
            }
        }catch (Exception e){
            defString="日志ERROR";
        }

		return defString;
	}

	public interface PurchaseOrderDetailState {
		// 状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）(字符长度为1-1)")
		String del = "d";
		String place = "0";
		String audited = "1";
		String in = "2";
		String allIn = "3";
	}
	
 

	public List<String> getOrderDetailIds() {
		return orderDetailIds;
	}

	public void setOrderDetailIds(List<String> orderIds) {
		this.orderDetailIds = orderIds;
	}

	public String getAuditName() {
		return auditName;
	}

	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}

	public BigDecimal getRealSum() {
		return realSum;
	}

	public void setRealSum(BigDecimal realSum) {
		this.realSum = realSum;
	}

}