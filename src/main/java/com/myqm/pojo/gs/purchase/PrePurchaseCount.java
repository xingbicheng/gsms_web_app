package com.myqm.pojo.gs.purchase;

import io.swagger.annotations.ApiModelProperty;

public class PrePurchaseCount {
	@ApiModelProperty(value = "订单id(字符长度为1-32)")
	private String orderId;

	@ApiModelProperty(value = "订单id(字符长度为1-32)")
	private int prePurchaseCount;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getPrePurchaseCount() {
		return prePurchaseCount;
	}

	public void setPrePurchaseCount(int prePurchaseCount) {
		this.prePurchaseCount = prePurchaseCount;
	}

}
