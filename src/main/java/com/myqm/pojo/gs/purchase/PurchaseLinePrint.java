package com.myqm.pojo.gs.purchase;

import io.swagger.annotations.ApiModelProperty;

public class PurchaseLinePrint {
	@ApiModelProperty(value = "单位名称")
	private String unitName;

	@ApiModelProperty(value = "内部产品名称(新增编辑不需要此字段)")
	private String goodsName;

	@ApiModelProperty(value = "入库库房id")
	private String storehouseId;
	
	@ApiModelProperty(value = "库房名字")
	private String storeName;

	@ApiModelProperty(value = "内部商品名id")
	private String goodsId;

	@ApiModelProperty(value = "采购数量")
	private Double amount;

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getStorehouseId() {
		return storehouseId;
	}

	public void setStorehouseId(String storehouseId) {
		this.storehouseId = storehouseId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	} 
}
