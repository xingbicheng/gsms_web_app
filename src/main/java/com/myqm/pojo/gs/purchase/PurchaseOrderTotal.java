package com.myqm.pojo.gs.purchase;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;
import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseOrderTotal {
    @ApiModelProperty(value = "按内部商品分类")
    private boolean classificationGoods=false;
    @ApiModelProperty(value = "按订单编号分类")
    private boolean classificationOrderNo=false;
    @ApiModelProperty(value = "按商品类型分类")
    private boolean classificationGoodsTypes=false;
    @ApiModelProperty(value = "按客户分类")
    private boolean classificationCustomerTypes=false;

    @ApiModelProperty(value = "出库开始时间")
    private Date auditTimeStart;

    @ApiModelProperty(value = "出库结束时间")
    private Date auditTimeEnd;

    @ApiModelProperty(value = "状态")
    private String state ;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isClassificationGoods() {
        return classificationGoods;
    }

    public void setClassificationGoods(boolean classificationGoods) {
        this.classificationGoods = classificationGoods;
    }

    public boolean isClassificationOrderNo() {
        return classificationOrderNo;
    }

    public void setClassificationOrderNo(boolean classificationOrderNo) {
        this.classificationOrderNo = classificationOrderNo;
    }

    public boolean isClassificationGoodsTypes() {
        return classificationGoodsTypes;
    }

    public void setClassificationGoodsTypes(boolean classificationGoodsTypes) {
        this.classificationGoodsTypes = classificationGoodsTypes;
    }

    public boolean isClassificationCustomerTypes() {
        return classificationCustomerTypes;
    }

    public void setClassificationCustomerTypes(boolean classificationCustomerTypes) {
        this.classificationCustomerTypes = classificationCustomerTypes;
    }

    public Date getAuditTimeStart() {
        return auditTimeStart;
    }

    public void setAuditTimeStart(Date auditTimeStart) {
        this.auditTimeStart = auditTimeStart;
    }

    public Date getAuditTimeEnd() {
        return auditTimeEnd;
    }

    public void setAuditTimeEnd(Date auditTimeEnd) {
        this.auditTimeEnd = auditTimeEnd;
    }
}
