package com.myqm.pojo.gs.purchase;

import io.swagger.annotations.ApiModelProperty;

public class PurchaseOderClassStatistics {
	@ApiModelProperty(value = "仓库id")
	String storeId;

	@ApiModelProperty(value = "采购单明细商品ID")
	String goodsId;

	@ApiModelProperty(value = "采购单明细商品数量")
	double amount;

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
