package com.myqm.pojo.gs.purchase;

import java.io.Serializable;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** OrderDetailPurchase */
@JsonInclude(Include.NON_NULL)
public class OrderDetailPurchase implements Serializable {

	@ApiModelProperty(value = "订单明细id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "字符长度为1-32")
	@Excel(name = "", orderNum = "0")
	private String orderDetailId;

	@ApiModelProperty(value = "采购单明细id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "字符长度为1-32")
	@Excel(name = "", orderNum = "1")
	private String purchaseDetailId;

	private static final long serialVersionUID = 1L;

	public OrderDetailPurchase(String odId, String pdId) {
		orderDetailId = odId;
		purchaseDetailId = pdId; 
	}

	public String getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(String detailId) {
		this.orderDetailId = detailId == null ? null : detailId.trim();
	}

	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId == null ? null : purchaseDetailId.trim();
	}

}