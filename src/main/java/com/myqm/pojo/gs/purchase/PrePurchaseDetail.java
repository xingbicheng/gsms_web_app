package com.myqm.pojo.gs.purchase;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import java.sql.Date;

public class PrePurchaseDetail {
	@JsonProperty("purchaseDetailId")
	@ApiModelProperty(value = "采购单明细ID")
	String id;
	@JsonProperty("purchaseOrderId")
	@ApiModelProperty(value = "采购单ID")
	String orderId;
	@ApiModelProperty(value = "客户名称")
	String customerName;
	@ApiModelProperty(value = "客户类型")
	String customerType;
	@ApiModelProperty(value = "采购单明细状态")
	@JsonProperty("purchaseDetailState")
	String state;
	@ApiModelProperty(value = "采购单明细商品ID")
	String goodsId;
	@ApiModelProperty(value = "采购单明细商品名称")
	String goodsName;
	@ApiModelProperty(value = "采购单明细商品类型id")
	String goodsTypeId;
	@ApiModelProperty(value = "采购单明细商品单位")
	String unitName;
	@ApiModelProperty(value = "采购单明细商品类型")
	String goodsType;
	@ApiModelProperty(value = "采购单明细商品数量")
	double amount;
	@ApiModelProperty(value = "采购单明细商品损耗率")
	double lossRate;
    @ApiModelProperty(value = "真实配送时间,查询使用")
    private Date realDistributionDate;
    @ApiModelProperty(value = "配送时间,查询使用")
    private Date distributionDate;

    public Date getRealDistributionDate() {
        return realDistributionDate;
    }

    public void setRealDistributionDate(Date realDistributionDate) {
        this.realDistributionDate = realDistributionDate;
    }

    public Date getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(Date distributionDate) {
        this.distributionDate = distributionDate;
    }

    public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public double getLossRate() {
		return lossRate;
	}

	public void setLossRate(double lossRate) {
		this.lossRate = lossRate;
	}

}
