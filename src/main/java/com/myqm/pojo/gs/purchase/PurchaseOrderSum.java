package com.myqm.pojo.gs.purchase;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseOrderSum {
    @ApiModelProperty(value = "内部商品名")
    private String goodsId;
    @ApiModelProperty(value = "统计采购数量")
    private double sumStaticAmount;
    @ApiModelProperty(value = "统计损耗量")
    private BigDecimal sumLossAmount;
    @ApiModelProperty(value = "统计总价")
    private BigDecimal total;
    @ApiModelProperty(value = "统计总采购数量")
    private double sumAmount;
    @ApiModelProperty(value = "统计实际采购数量")
    private double sumRealAmount;
    @ApiModelProperty(value = "统计实际总价")
    private BigDecimal realTotal;
    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public double getSumStaticAmount() {
        return sumStaticAmount;
    }

    public void setSumStaticAmount(double sumStaticAmount) {
        this.sumStaticAmount = sumStaticAmount;
    }

    public BigDecimal getSumLossAmount() {
        return sumLossAmount;
    }

    public void setSumLossAmount(BigDecimal sumLossAmount) {
        this.sumLossAmount = sumLossAmount;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(double sumAmount) {
        this.sumAmount = sumAmount;
    }

    public double getSumRealAmount() {
        return sumRealAmount;
    }

    public void setSumRealAmount(double sumRealAmount) {
        this.sumRealAmount = sumRealAmount;
    }

    public BigDecimal getRealTotal() {
        return realTotal;
    }

    public void setRealTotal(BigDecimal realTotal) {
        this.realTotal = realTotal;
    }


}
