package com.myqm.pojo.gs.purchase;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.util.List;

public class PurchaseOrderTs {

    @JsonProperty("purchaseOrderId")
    private String orderId;

    @ApiModelProperty(value = "采购员id")
    private String purchaserId;

    @ApiModelProperty(value = "采购时间")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date purchaseDate;

    List<DetailMini> detailsEdit;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPurchaserId() {
        return purchaserId;
    }

    public void setPurchaserId(String purchaserId) {
        this.purchaserId = purchaserId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public List<DetailMini> getDetailsEdit() {
        return detailsEdit;
    }

    public void setDetailsEdit(List<DetailMini> detailsEdit) {
        this.detailsEdit = detailsEdit;
    }
}
