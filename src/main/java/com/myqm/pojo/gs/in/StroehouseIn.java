package com.myqm.pojo.gs.in;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/** StroehouseIn */
@JsonInclude(Include.NON_NULL)
public class StroehouseIn implements Serializable {

    @ApiModelProperty(value = "入库单id")
    private String id;

    @ApiModelProperty(value = "采购单编号(字符长度为1-32)")
    @Size(min = 1, max = 32, message = "采购单编号字符长度为1-32")
    @Excel(name = "采购单编号", orderNum = "1")
    private String purchaseDetailId;

    @ApiModelProperty(value = "总价")
    private BigDecimal sum;

    @ApiModelProperty(value = "实际采购单价")
    private BigDecimal price;

    @ApiModelProperty(value = "采购方式 1中央库调拨 2，订单采购 3.计划采购 4.分库退货入库(字符长度为1-1)")
    @Size(min = 1, max = 1, message = "采购方式 1中央库调拨 2，订单采购 3.计划采购 4.分库退货入库字符长度为1-1")
    @Excel(name = "采购方式 1中央库调拨 2，订单采购 3.计划采购 4.分库退货入库", orderNum = "4")
    private String purchaseType;

    @ApiModelProperty(value = "状态（状态（0 下单、 1审核通过,采购中，2入库完成 3.出库中，4出库完毕)(字符长度为1-1)")
    @Size(min = 1, max = 1, message = "状态（0 下单、 1审核通过,采购中，2入库完成 3.出库中，4出库完毕）字符长度为1-1")
    @Excel(name = "状态（0 下单、 1审核通过,采购中，2入库完成 3.出库中，4出库完毕）", orderNum = "5")
    private String state;

    @ApiModelProperty(value = "内部商品名id")
    private String goodsId;

    @ApiModelProperty(value = "采购数量")
    private Double amount;
    
    
    @ApiModelProperty(value = "总退货数量，系统统计")
    private Double returnAmount;
    

    @ApiModelProperty(value = "入库时间")
    private Timestamp inTime;

    @ApiModelProperty(value = "供货商id")
    private String providerId;

    @ApiModelProperty(value = "入库库房id")
    private String storehouseId;

    @ApiModelProperty(value = "包装备注(字符长度为1-200)")
    @Size(min = 1, max = 200, message = "包装备注字符长度为1-200")
    @Excel(name = "包装备注", orderNum = "11")
    private String packingMemo;

    @ApiModelProperty(value = "备注(字符长度为1-200)")
    @Size(min = 1, max = 200, message = "包装备注字符长度为1-200")
    @Excel(name = "备注", orderNum = "12")
    private String memo;

    @ApiModelProperty(value = "调拨或退货库房id")
    private String fromStorehouseId;

    @ApiModelProperty(value = "调拨或退货库房入库单号")
    private String fromStorehouseInId;


    @ApiModelProperty(value = "剩余数量")
    private Double surplus;

    @ApiModelProperty(value = "商品名字")
    private String goodsName;
    
    
    @ApiModelProperty(value = "打印次数")
    private int printCount;
    
    
    @ApiModelProperty(value = "商品Code")
    private String goodsCode;
    @ApiModelProperty(value = "商品单位")
    private String unitName;
    @ApiModelProperty(value = "是否可以退货")
    private boolean canBack= false;
 
    
    @ApiModelProperty(value = "商品类型")
    private String goodsTypeId;

    @ApiModelProperty(value = "商品类型名称")
    private String goodsTypeName;

    @ApiModelProperty(value = "提供者名字")
    private String providerName;

    @ApiModelProperty(value = "库房名字")
    private String storeName;

    @ApiModelProperty(value = "真实入库数量")
    private Double realAmount;

    @ApiModelProperty(value = "采购时间")
    private Date purchaseDate;


     @ApiModelProperty(value = "损耗量")
    private Double lossAmount;

    @ApiModelProperty(value="可用量")
    private Double availableAmount;

    @ApiModelProperty(value="可退货量")
    private Double returnableAmount; 

    private String blackPrice;
    @ApiModelProperty(value = "统计数量")
    private double statisticAmount;
    
    @ApiModelProperty(value="T+单据号码")
    private String tplusNum;


    public Double getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(Double returnAmount) {
		this.returnAmount = returnAmount;
	}

	public String getGoodsTypeName() {
        return goodsTypeName;
    }

    public void setGoodsTypeName(String goodsTypeName) {
        this.goodsTypeName = goodsTypeName;
    }

    public boolean getCanBack() {
		return canBack;
	}

	public void setCanBack(boolean canBack) {
		this.canBack = canBack;
	}
	

    public String getTplusNum() {
		return tplusNum;
	}

	public void setTplusNum(String tplusNum) {
		this.tplusNum = tplusNum;
	}

	public Double getReturnableAmount() {
        return returnableAmount==null?amount:returnableAmount;
    }

    public void setReturnableAmount(Double returnableAmount) {
        this.returnableAmount = returnableAmount;
    }

    public double getStatisticAmount() {
        return statisticAmount;
    }

    public void setStatisticAmount(double statisticAmount) {
        this.statisticAmount = statisticAmount;
    }

    public String getBlackPrice() {
        return blackPrice;
    }

    public void setBlackPrice(String blackPrice) {
        this.blackPrice = blackPrice;
    }

    private List<FromStroehouseInfo> toStroehouseInfos;

    public List<FromStroehouseInfo> getToStroehouseInfos() {
        return toStroehouseInfos;
    }

    public void setToStroehouseInfos(List<FromStroehouseInfo> toStroehouseInfos) {
        this.toStroehouseInfos = toStroehouseInfos;
    }

    public String getFromStorehouseInId() {
        return fromStorehouseInId;
    }

    public void setFromStorehouseInId(String fromStorehouseInId) {
        this.fromStorehouseInId = fromStorehouseInId;
    }

    public Timestamp getInTime() {
        return inTime;
    }

    public void setInTime(Timestamp inTime) {
        this.inTime = inTime;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Double getLossAmount() {
        return lossAmount;
    }

    public void setLossAmount(Double lossAmount) {
        this.lossAmount = lossAmount;
    }

    public Double getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(Double realAmount) {
        this.realAmount = realAmount;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsTypeId() {
        return goodsTypeId;
    }

    public void setGoodsTypeId(String goodsTypeId) {
        this.goodsTypeId = goodsTypeId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getPurchaseDetailId() {
        return purchaseDetailId;
    }

    public void setPurchaseDetailId(String purchaseDetailId) {
        this.purchaseDetailId = purchaseDetailId == null ? null : purchaseDetailId.trim();
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType == null ? null : purchaseType.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getStorehouseId() {
        return storehouseId;
    }

    public void setStorehouseId(String storehouseId) {
        this.storehouseId = storehouseId;
    }

    public String getPackingMemo() {
        return packingMemo;
    }

    public void setPackingMemo(String packingMemo) {
        this.packingMemo = packingMemo == null ? null : packingMemo.trim();
    }



    public String getFromStorehouseId() {
        return fromStorehouseId;
    }

    public void setFromStorehouseId(String fromStorehouseId) {
        this.fromStorehouseId = fromStorehouseId;
    }

    public Double getSurplus() {
        return surplus;
    }

    public void setSurplus(Double surplus) {
        this.surplus = surplus;
    }

    public Double getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(Double availableAmount) {
        this.availableAmount = availableAmount;
    }


    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getPrintCount() {
		return printCount;
	}

	public void setPrintCount(int printCount) {
		this.printCount = printCount;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public void difference(StroehouseIn obj) {
        String defString = "";
        if (!Objects.equals(this.id, obj.getId())) {
            defString += "从" + this.id + "修改为" + obj.getId();
        }

        if (!this.purchaseDetailId.equals(obj.getPurchaseDetailId())) {
            defString += "采购单编号从" + this.purchaseDetailId + "修改为" + obj.getPurchaseDetailId();
        }

        if (this.sum.compareTo(obj.getSum()) != 0) {
            defString += "总价从" + this.sum + "修改为" + obj.getSum();
        }

        if (this.price.compareTo(obj.getPrice()) != 0) {
            defString += "实际采购单价从" + this.price + "修改为" + obj.getPrice();
        }

        if (!this.purchaseType.equals(obj.getPurchaseType())) {
            defString += "采购方式 1中央库调拨 2，订单采购 3.计划采购 4.分库退货入库从" + this.purchaseType + "修改为" + obj.getPurchaseType();
        }

        if (!this.state.equals(obj.getState())) {
            defString += "状态（0采购中，1入库完成 2.出库中，3出库完毕）从" + this.state + "修改为" + obj.getState();
        }

        if (!Objects.equals(this.goodsId, obj.getGoodsId())) {
            defString += "内部商品名id从" + this.goodsId + "修改为" + obj.getGoodsId();
        }

        if (Math.abs(this.amount - obj.getAmount()) > 0) {
            defString += "采购数量从" + this.amount + "修改为" + obj.getAmount();
        }

        if (!Objects.equals(this.providerId, obj.getProviderId())) {
            defString += "供货商id从" + this.providerId + "修改为" + obj.getProviderId();
        }

        if (!Objects.equals(this.storehouseId, obj.getStorehouseId())) {
            defString += "入库库房id从" + this.storehouseId + "修改为" + obj.getStorehouseId();
        }

        if (!this.packingMemo.equals(obj.getPackingMemo())) {
            defString += "包装备注从" + this.packingMemo + "修改为" + obj.getPackingMemo();
        }

        if (!Objects.equals(this.fromStorehouseId, obj.getFromStorehouseId())) {
            defString += "调拨或退货库房id从" + this.fromStorehouseId + "修改为" + obj.getFromStorehouseId();
        }

        if (Math.abs(this.surplus - obj.getSurplus()) > 0) {
            defString += "剩余数量从" + this.surplus + "修改为" + obj.getSurplus();
        }

    }

    public interface StroehouseInState {
        // 状态（0 下单、1、审核通过 2入库完成、3损耗、4清空  5 t+导入）
        String place = "0";
        String audited = "1";
        String in = "2";
        String loss ="3";
        String clean="4";
        String tplusLock="5";
        String tplusImport="6";
        
    }

    public interface PurchaseType
    {
        //1中央库调拨 2，订单采购 3.计划采购 4.分库退货入库"
        String transaction ="1";
        String orderPurchase="2";
        String planPurchase="3";
        String backStore="4";

    }


    @Override
    public String toString() {
        return "StroehouseIn{" +
                "id='" + id + '\'' +
                ", purchaseDetailId='" + purchaseDetailId + '\'' +
                ", sum=" + sum +
                ", price=" + price +
                ", purchaseType='" + purchaseType + '\'' +
                ", state='" + state + '\'' +
                ", goodsId='" + goodsId + '\'' +
                ", amount=" + amount +
                ", inTime=" + inTime +
                ", providerId='" + providerId + '\'' +
                ", storehouseId='" + storehouseId + '\'' +
                ", packingMemo='" + packingMemo + '\'' +
                ", fromStorehouseId=" + fromStorehouseId +
                ", surplus=" + surplus +
                ", goodsName='" + goodsName + '\'' +
                ", goodsTypeId='" + goodsTypeId + '\'' +
                ", providerName='" + providerName + '\'' +
                ", storeName='" + storeName + '\'' +
                ", realAmount=" + realAmount +
                ", purchaseDate=" + purchaseDate +
                ", lossAmount=" + lossAmount +
                ", availableAmount=" + availableAmount +
                '}';
    }
}