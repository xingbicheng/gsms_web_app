package com.myqm.pojo.gs.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StroehouseInSum {
    @ApiModelProperty(value = "统计总价")
    private BigDecimal total;
    @ApiModelProperty(value = "统计采购数量")
    private double sumAmount;
    @ApiModelProperty(value = "统计剩余数量")
    private double sumSurplus;
    @ApiModelProperty(value = "统计真实入库数量")
    private double sumRealAmount;
    @ApiModelProperty(value = "统计损耗量")
    private double sumLossAmount;
    @ApiModelProperty(value = "统计可用量")
    private double sumAvailableAmount;
    
    @ApiModelProperty(value = "统计退货用量")
    private double sumReturnAmount;
    

    @ApiModelProperty(value = "内部商品名")
    private String goodsId;
    @ApiModelProperty(value = "库房类型")
    private String storeType;
    @ApiModelProperty(value = "提供商类型")
    private String providerType;
    @ApiModelProperty(value = "用户类型")
    private String customerType;

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(double sumAmount) {
        this.sumAmount = sumAmount;
    }

    public double getSumSurplus() {
        return sumSurplus;
    }

    public void setSumSurplus(double sumSurplus) {
        this.sumSurplus = sumSurplus;
    }

    public double getSumRealAmount() {
        return sumRealAmount;
    }

    public void setSumRealAmount(double sumRealAmount) {
        this.sumRealAmount = sumRealAmount;
    }

    public double getSumLossAmount() {
        return sumLossAmount;
    }

    public void setSumLossAmount(double sumLossAmount) {
        this.sumLossAmount = sumLossAmount;
    }

    public double getSumAvailableAmount() {
        return sumAvailableAmount;
    }

    public void setSumAvailableAmount(double sumAvailableAmount) {
        this.sumAvailableAmount = sumAvailableAmount;
    }

	public double getSumReturnAmount() {
		return sumReturnAmount;
	}

	public void setSumReturnAmount(double sumReturnAmount) {
		this.sumReturnAmount = sumReturnAmount;
	}
    
}
