package com.myqm.pojo.gs.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StroehouseInTotal {
    @ApiModelProperty(value = "按内部商品名分类")
    private boolean classificationGoods;

    @ApiModelProperty(value = "按库房类型分类")
    private boolean classificationStoreType;

    @ApiModelProperty(value = "按用户类型分类")
    private boolean classificationCustomerType;

    @ApiModelProperty(value = "按供应商类型分类")
    private boolean classificationProviderType;

    @ApiModelProperty(value = "入库开始时间")
    private Timestamp inTimeStart;

    @ApiModelProperty(value = "入库结束时间")
    private Timestamp inTimeEnd;

    @ApiModelProperty(value = "状态")
    private String state;

    public boolean isClassificationGoods() {
        return classificationGoods;
    }

    public void setClassificationGoods(boolean classificationGoods) {
        this.classificationGoods = classificationGoods;
    }

    public boolean isClassificationStoreType() {
        return classificationStoreType;
    }

    public void setClassificationStoreType(boolean classificationStoreType) {
        this.classificationStoreType = classificationStoreType;
    }

    public boolean isClassificationCustomerType() {
        return classificationCustomerType;
    }

    public void setClassificationCustomerType(boolean classificationCustomerType) {
        this.classificationCustomerType = classificationCustomerType;
    }

    public boolean isClassificationProviderType() {
        return classificationProviderType;
    }

    public void setClassificationProviderType(boolean classificationProviderType) {
        this.classificationProviderType = classificationProviderType;
    }

    public Timestamp getInTimeStart() {
        return inTimeStart;
    }

    public void setInTimeStart(Timestamp inTimeStart) {
        this.inTimeStart = inTimeStart;
    }

    public Timestamp getInTimeEnd() {
        return inTimeEnd;
    }

    public void setInTimeEnd(Timestamp inTimeEnd) {
        this.inTimeEnd = inTimeEnd;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
