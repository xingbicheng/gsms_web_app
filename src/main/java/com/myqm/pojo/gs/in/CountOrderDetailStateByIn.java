package com.myqm.pojo.gs.in;

public class CountOrderDetailStateByIn {
	String state;
	int stateCount;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getStateCount() {
		return stateCount;
	}
	public void setStateCount(int stateCount) {
		this.stateCount = stateCount;
	}
	 
	

}
