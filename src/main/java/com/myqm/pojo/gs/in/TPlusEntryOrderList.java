package com.myqm.pojo.gs.in;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "request") 
@XmlAccessorType(value = XmlAccessType.PROPERTY)  
public class TPlusEntryOrderList implements Serializable{ 
	@XmlElementRef
	List<TPlusEntryOrder> list = new ArrayList<TPlusEntryOrder>();
	@XmlTransient
	public List<TPlusEntryOrder> getList() {
		return list;
	}

	public void setList(List<TPlusEntryOrder> list) {
		this.list = list;
	}
	
	
	

}
