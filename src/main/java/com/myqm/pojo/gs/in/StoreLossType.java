package com.myqm.pojo.gs.in;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**StoreLossType*/
@JsonInclude(Include.NON_NULL)
public class StoreLossType implements Serializable {

	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "损耗类型(字符长度为1-20)")
	@Size(min=1, max=20,message="损耗类型字符长度为1-20")
	@Excel(name="损耗类型",orderNum="1")
	private String lossType;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "备注(字符长度为1-255)")
	@Size(min=1, max=255,message="备注字符长度为1-255")
	@Excel(name="备注",orderNum="3")
	private String memo;

	@ApiModelProperty(value = "拼音字段(字符长度为1-100)")
	@Excel(name="拼音字段",orderNum="4")
	private String pinyin;

	@ApiModelProperty(value = "编码(字符长度为1-20)")
	@Size(min=1, max=20,message="编码字符长度为1-20")
	@Excel(name="编码",orderNum="5")
	private String code;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min=1, max=20,message="修改人字符长度为1-20")
	@Excel(name="修改人",orderNum="6")
	private String updaterName;


    private static final long serialVersionUID = 1L;

	public String getId () {   
		return id;
	}
		
	public void setId (String id) {
		this.id= id ;
	}

    public String getLossType () {   
    	 return lossType;
    }

    public void setLossType (String lossType) {
    	 this.lossType= lossType == null ? null : lossType.trim();
    }

    public String getMemo () {   
    	 return memo;
    }

    public void setMemo (String memo) {
    	 this.memo= memo == null ? null : memo.trim();
    }

    public String getPinyin () {   
    	 return pinyin;
    }

    public void setPinyin (String pinyin) {
    	 this.pinyin= pinyin == null ? null : pinyin.trim();
    }

    public String getCode () {   
    	 return code;
    }

    public void setCode (String code) {
    	 this.code= code == null ? null : code.trim();
    }

    public String getUpdaterName () {   
    	 return updaterName;
    }

    public void setUpdaterName (String updaterName) {
    	 this.updaterName= updaterName == null ? null : updaterName.trim();
    }


	public void difference(StoreLossType obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.lossType.equals(obj.getLossType())) {
			 defString += "损耗类型从" + this.lossType +"修改为" +obj.getLossType();
		}

	   if(!this.memo.equals(obj.getMemo())) {
			 defString += "备注从" + this.memo +"修改为" +obj.getMemo();
		}

	   if(!this.pinyin.equals(obj.getPinyin())) {
			 defString += "拼音字段从" + this.pinyin +"修改为" +obj.getPinyin();
		}

	   if(!this.code.equals(obj.getCode())) {
			 defString += "编码从" + this.code +"修改为" +obj.getCode();
		}

	   if(!this.updaterName.equals(obj.getUpdaterName())) {
			 defString += "修改人从" + this.updaterName +"修改为" +obj.getUpdaterName();
		}


    }
}