package com.myqm.pojo.gs.in;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(value = XmlAccessType.PROPERTY) 
@XmlRootElement(name="orderLine") 
public class TPlusGoodsLine implements Serializable{
	@XmlElement 
	private String itemCode;//商品编码
	@XmlElement 
	private double actualQty;//数量
	@XmlElement 
	private BigDecimal price;//单价
	
	@XmlTransient
	private String id;	
	@XmlTransient
	private String goodsTypeId;
	@XmlTransient
	private String state;
	@XmlTransient
	private String goodsId;
	
	
	public TPlusGoodsLine(){
		
	}
	public TPlusGoodsLine(String goodsCode, double qty, BigDecimal p){
		itemCode = goodsCode;
		actualQty = qty;
		price = p;
	}
	
	 
	public String getGoodsId() {
		return goodsId;
	}
	 
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
 
	public String getState() {
		return state;
	}
	 
	public void setState(String state) {
		this.state = state;
	}
 
	public String getId() {
		return id;
	}
 
	public void setId(String id) {
		this.id = id;
	}
	 
	public String getGoodsTypeId() {
		return goodsTypeId;
	}
	 
	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}
	
	
	
	
	@XmlTransient
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	@XmlTransient
	public double getActualQty() {
		return actualQty;
	}
	public void setActualQty(double actualQty) {
		this.actualQty = actualQty;
	}
	@XmlTransient
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
