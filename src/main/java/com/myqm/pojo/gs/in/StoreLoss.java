package com.myqm.pojo.gs.in;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/**StoreLoss*/
@JsonInclude(Include.NON_NULL)
public class StoreLoss implements Serializable {
	
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "入库单编号(字符长度为1-32)")
	@Size(min=1, max=32,message="入库单编号字符长度为1-32")
	@Excel(name="入库单编号",orderNum="1")
	private String inId;

	@ApiModelProperty(value = "总价")
	private BigDecimal sum;

	@ApiModelProperty(value = "实际采购单价")
	private BigDecimal price;

	@ApiModelProperty(value = "内部商品名id(字符长度为1-32)")
	@Size(min=1, max=32,message="内部商品名id字符长度为1-32")
	@Excel(name="内部商品名id",orderNum="4")
	private String goodsId;

	@ApiModelProperty(value = "损耗数量")
	private Double amount;

	@ApiModelProperty(value = "损耗时间")
	private Timestamp lossTime;

	@ApiModelProperty(value = "供货商id(字符长度为1-32)")
	@Size(min=1, max=32,message="供货商id字符长度为1-32")
	@Excel(name="供货商id",orderNum="7")
	private String providerId;

	@ApiModelProperty(value = "包装备注(字符长度为1-200)")
	@Size(min=1, max=200,message="包装备注字符长度为1-200")
	@Excel(name="包装备注",orderNum="8")
	private String packingMemo;

	@ApiModelProperty(value = "库房id(字符长度为1-32)")
	@Size(min=1, max=32,message="库房id字符长度为1-32")
	@Excel(name="库房id",orderNum="9")
	private String storehouseId;
	
	@ApiModelProperty(value = "库房名称(字符长度为1-32)") 
	@Excel(name="库房名称",orderNum="9")
	private String storeName;

	@ApiModelProperty(value = "0冲正1冲负(字符长度为1)")
	@Size(min=1, max=32,message="0冲正1冲负字符长度为1")
	@Excel(name="0冲正1冲负",orderNum="10")
	private String type;
	
	@ApiModelProperty(value = "损耗类型(字符长度为1)")
	@Size(min=1, max=32,message="损耗类型字符长度为1")
	@Excel(name="损耗类型",orderNum="11")
	private String lossType;

    private  String lossTypeName;
    private  String goodsName;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getLossTypeName() {
        return lossTypeName;
    }

    public void setLossTypeName(String lossTypeName) {
        this.lossTypeName = lossTypeName;
    }

    private static final long serialVersionUID = 1L;

	public String getId () {   
		return id;
	}
		
	public void setId (String id) {
		this.id= id ;
	}

    public String getInId () {   
    	 return inId;
    }

    public void setInId (String inId) {
    	 this.inId= inId == null ? null : inId.trim();
    }

	public BigDecimal getSum () {   
	    return sum;
	}

	public void setSum (BigDecimal sum) {
	    this.sum= sum;
	}

	public BigDecimal getPrice () {   
	    return price;
	}

	public void setPrice (BigDecimal price) {
	    this.price= price;
	}

    public String getGoodsId () {   
    	 return goodsId;
    }

    public void setGoodsId (String goodsId) {
    	 this.goodsId= goodsId == null ? null : goodsId.trim();
    }

	public Double getAmount () {   
		  return amount;
	}
	
	public void setAmount (Double amount) {
		  this.amount= amount;
	}

    public String getProviderId () {   
    	 return providerId;
    }

    public void setProviderId (String providerId) {
    	 this.providerId= providerId == null ? null : providerId.trim();
    }

    public String getPackingMemo () {   
    	 return packingMemo;
    }

    public void setPackingMemo (String packingMemo) {
    	 this.packingMemo= packingMemo == null ? null : packingMemo.trim();
    }

    public String getStorehouseId () {   
    	 return storehouseId;
    }

    public void setStorehouseId (String storehouseId) {
    	 this.storehouseId= storehouseId == null ? null : storehouseId.trim();
    }

	public Timestamp getLossTime() {
		return lossTime;
	}

	public void setLossTime(Timestamp lossTime) {
		this.lossTime = lossTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLossType() {
		return lossType;
	}

	public void setLossType(String lossType) {
		this.lossType = lossType;
	}



	public interface StoreLossType {
		String positive = "0";
		String negative = "1";
	}
}