package com.myqm.pojo.gs.in;

import java.io.Serializable;
import java.sql.Date;
import java.util.List; 
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper; 
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.myqm.pojo.gs.basic.TPlusGoods;

@XmlRootElement(name = "entryOrder") 
@XmlAccessorType(value = XmlAccessType.PROPERTY)  
public class TPlusEntryOrder implements Serializable{
	
	@XmlElement 
	private String entryOrderCode;//单据编号
	@XmlElement
	private String warehouseCode;//仓库编码
	@XmlElement
	private String operateTime;//单据日期 2018-11-22
	@XmlElement
	private String ownerCode;//供应商编码
	
	@XmlElementWrapper(name = "orderLines") 
	//@XmlElement(name = "orderLine", type =TPlusGoods.class) 
	@XmlElementRef
	private List<TPlusGoodsLine> orderLines;
	public TPlusEntryOrder(){
		
	}
	
	public TPlusEntryOrder(String oc, String wc, String owc, String ot){
		entryOrderCode = oc;
		warehouseCode = wc;
		ownerCode = owc; 
		operateTime = ot; 
	}
	@XmlTransient
	public String getEntryOrderCode() {
		return entryOrderCode;
	}

	public void setEntryOrderCode(String entryOrderCode) {
		this.entryOrderCode = entryOrderCode;
	}
	@XmlTransient
	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}
	@XmlTransient
	public String getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	@XmlTransient
	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}
	@XmlTransient
	public List<TPlusGoodsLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<TPlusGoodsLine> orderLines) {
		this.orderLines = orderLines;
	} 
}
