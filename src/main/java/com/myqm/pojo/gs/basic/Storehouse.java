package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** Storehouse */
@JsonInclude(Include.NON_NULL)
public class Storehouse implements Serializable {
	@JsonProperty("storeId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "库房名称(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "库房名称字符长度为1-255")
	@Excel(name = "库房名称", orderNum = "1")
	private String storeName;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "备注(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "备注字符长度为1-255")
	@Excel(name = "备注", orderNum = "3")
	private String memo;

	@ApiModelProperty(value = "拼音字段(字符长度为1-100)")
	@Size(min = 1, max = 100, message = "拼音字段字符长度为1-100")
	@Excel(name = "拼音字段", orderNum = "4")
	private String pinyin;

	@ApiModelProperty(value = "编码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "编码字符长度为1-20")
	@Excel(name = "编码", orderNum = "5")
	private String code;

	@ApiModelProperty(value = "仓库类型编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "仓库类型编号字符长度为1-32")
	@Excel(name = "仓库类型编号", orderNum = "6")
	private String storeTypeId;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "修改人字符长度为1-20")
	@Excel(name = "修改人", orderNum = "7")
	private String updaterName;
	
	@ApiModelProperty(value = "是否是中央库（0不是 1 是）")
	@Size(min = 1, max = 1, message = "是否是中央库（0不是 1 是）")
	@Excel(name = "是否是中央库（0不是 1 是）", orderNum = "8")
	private String isCenter;
	
	@ApiModelProperty(value = "开票公司名称(字符长度为1-225)")
	@Size(min = 1, max = 225, message = "开票公司名称字符长度为1-225")
	@Excel(name = "开票公司名称(字符长度为1-225)", orderNum = "9")
	private String companyName;
	
	@ApiModelProperty(value = "仓库类型名称(字符长度为1-20),新增不用填写")
	@Excel(name = "仓库类型名称", orderNum = "1")
	private String storeType;

	@ApiModelProperty(value = "T+账套信息")
	@Excel(name = "T+账套信息", orderNum = "1")
	private String tplusAccount;
	
	@ApiModelProperty(value = "T+账套导出仓库") 
	private String  toStoreCode;
	
	
	private static final long serialVersionUID = 1L;

	
	public String getToStoreCode() {
		return toStoreCode;
	}

	public void setToStoreCode(String toStoreCode) {
		this.toStoreCode = toStoreCode;
	}

	public String getIsCenter() {
		return isCenter;
	}

	public void setIsCenter(String isCenter) {
		this.isCenter = isCenter;
	}

	public String getTplusAccount() {
		return tplusAccount;
	}

	public void setTplusAccount(String tplusAccount) {
		this.tplusAccount = tplusAccount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName == null ? null : storeName.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin == null ? null : pinyin.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public String getStoreTypeId() {
		return storeTypeId;
	}

	public void setStoreTypeId(String storeTypeId) {
		this.storeTypeId = storeTypeId == null ? null : storeTypeId.trim();
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName == null ? null : updaterName.trim();
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	
	public String getBeCenter() {
		return isCenter;
	}

	public void setBeCenter(String isCenter) {
		this.isCenter = isCenter == null ? null : isCenter.trim();
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName == null ? null : companyName.trim();
	}
	
	public void difference(Storehouse obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.storeName.equals(obj.getStoreName())) {
			defString += "库房名称从" + this.storeName + "修改为" + obj.getStoreName();
		}

		if (!this.memo.equals(obj.getMemo())) {
			defString += "备注从" + this.memo + "修改为" + obj.getMemo();
		}

		if (!this.pinyin.equals(obj.getPinyin())) {
			defString += "拼音字段从" + this.pinyin + "修改为" + obj.getPinyin();
		}

		if (!this.code.equals(obj.getCode())) {
			defString += "编码从" + this.code + "修改为" + obj.getCode();
		}

		if (!this.storeTypeId.equals(obj.getStoreTypeId())) {
			defString += "仓库类型编号从" + this.storeTypeId + "修改为" + obj.getStoreTypeId();
		}

		if (!this.updaterName.equals(obj.getUpdaterName())) {
			defString += "修改人从" + this.updaterName + "修改为" + obj.getUpdaterName();
		}
		
		if (!this.isCenter.equals(obj.getBeCenter())) {
			defString += "是否是中央库（0不是 1 是）从" + this.isCenter + "修改为" + obj.getBeCenter();
		}
		
		if (!this.companyName.equals(obj.getCompanyName())) {
			defString += "开票公司名称从" + this.companyName + "修改为" + obj.getCompanyName();
		}

	}
}