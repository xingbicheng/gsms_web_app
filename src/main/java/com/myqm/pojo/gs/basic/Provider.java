package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** Provider */
@JsonInclude(Include.NON_NULL)
public class Provider implements Serializable {
	@JsonProperty("providerId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "供应商名称(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "供应商名称字符长度为1-20")
	@Excel(name = "供应商名称", orderNum = "1")
	private String providerName;

	@ApiModelProperty(value = "供应商类型编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "供应商类型编号字符长度为1-32")
	@Excel(name = "供应商类型编号", orderNum = "2")
	private String providerTypeId;

	@ApiModelProperty(value = "联系人姓名(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "联系人姓名字符长度为1-20")
	@Excel(name = "联系人姓名", orderNum = "3")
	private String contactName;

	@ApiModelProperty(value = "联系人电话(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "联系人电话字符长度为1-20")
	@Excel(name = "联系人电话", orderNum = "4")
	private String contactTel;

	@ApiModelProperty(value = "备注(字符长度为1-200)")
	@Size(min = 1, max = 200, message = "备注字符长度为1-200")
	@Excel(name = "备注", orderNum = "5")
	private String memo;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "修改人字符长度为1-20")
	@Excel(name = "修改人", orderNum = "7")
	private String updaterName;

	@ApiModelProperty(value = "拼音字段(字符长度为1-100)")
	@Size(min = 1, max = 100, message = "拼音字段字符长度为1-100")
	@Excel(name = "拼音字段", orderNum = "8")
	private String pinyin;

	@ApiModelProperty(value = "编码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "编码字符长度为1-20")
	@Excel(name = "编码", orderNum = "9")
	private String code;

	@ApiModelProperty(value = "供货商类型名称(字符长度为1-20),新增不用填写")
	@Excel(name = "供货商类型名称", orderNum = "1")
	private String providerType;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName == null ? null : providerName.trim();
	}

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId == null ? null : providerTypeId.trim();
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName == null ? null : contactName.trim();
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel == null ? null : contactTel.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName == null ? null : updaterName.trim();
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin == null ? null : pinyin.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public void difference(Provider obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.providerName.equals(obj.getProviderName())) {
			defString += "供应商名称从" + this.providerName + "修改为" + obj.getProviderName();
		}

		if (!this.providerTypeId.equals(obj.getProviderTypeId())) {
			defString += "供应商类型编号从" + this.providerTypeId + "修改为" + obj.getProviderTypeId();
		}

		if (!this.contactName.equals(obj.getContactName())) {
			defString += "联系人姓名从" + this.contactName + "修改为" + obj.getContactName();
		}

		if (!this.contactTel.equals(obj.getContactTel())) {
			defString += "联系人电话从" + this.contactTel + "修改为" + obj.getContactTel();
		}

		if (!this.memo.equals(obj.getMemo())) {
			defString += "备注从" + this.memo + "修改为" + obj.getMemo();
		}

		if (!this.updaterName.equals(obj.getUpdaterName())) {
			defString += "修改人从" + this.updaterName + "修改为" + obj.getUpdaterName();
		}

		if (!this.pinyin.equals(obj.getPinyin())) {
			defString += "拼音字段从" + this.pinyin + "修改为" + obj.getPinyin();
		}

		if (!this.code.equals(obj.getCode())) {
			defString += "编码从" + this.code + "修改为" + obj.getCode();
		}

	}
}