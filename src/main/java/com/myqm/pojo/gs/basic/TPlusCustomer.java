package com.myqm.pojo.gs.basic;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.myqm.utils.GetPinyinUtil;

@XmlAccessorType(value = XmlAccessType.PROPERTY) 
@XmlRootElement(name="CustomerItem") 
//@XmlType(propOrder={"code","name","shorThand","unitCode","unitName","invcCode","invcName"})
 public class TPlusCustomer implements Serializable{

	@XmlElement(name = "code")
	private String code ;

	@XmlElement(name = "name")
	private String name ;

	@XmlElement(name = "classCode")
	private String classCode ;//往来单位分类编码
	
	@XmlElement(name = "className")
	private String className ;//往来单位分类名称
	
	@XmlElement(name = "isCustomer")
	private String isCustomer ;//往来单位性质
	
	@XmlElement(name = "MobilePhone")
	private String MobilePhone ;//往来单位性质
	
	private String classNamePinyin ;//往来姓名分类拼音
	
	
	@XmlTransient
	public String getIsCustomer() {
		return isCustomer;
	}

	public void setIsCustomer(String isCustomer) {
		this.isCustomer = isCustomer;
	}

	@XmlTransient
	public String getMobilePhone() {
		return MobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		MobilePhone = mobilePhone;
	}

	public String getClassNamePinyin() {
		if (name!=null)
		return GetPinyinUtil.getPinYinHeadChar(name);
		else
			return null;
	}

	public void setClassNamePinyin(String classNamePinyin) {
		this.classNamePinyin = classNamePinyin;
	}

	public TPlusCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TPlusCustomer(String code, String name, String classCode, String className) {
		super();
		this.code = code;
		this.name = name;
		this.classCode = classCode;
		this.className = className;
	}
	
	@XmlTransient
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@XmlTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@XmlTransient
	public String getClassCode() {
		return classCode.toLowerCase();
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
	@XmlTransient
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	

	
}
