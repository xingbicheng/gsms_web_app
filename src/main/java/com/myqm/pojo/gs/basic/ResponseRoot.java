package com.myqm.pojo.gs.basic;


import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
 
@XmlRootElement(name = "response") 
@XmlAccessorType(value = XmlAccessType.PROPERTY)  
public class ResponseRoot<T extends Object> implements Serializable {

	@XmlElement(name = "flag")
	private String flag;
	@XmlElement(name = "code")
	private String code;
	@XmlElement(name = "message")
	private String message;
	
	@XmlElementWrapper(name = "items") 
	 @XmlElements(
			 { @XmlElement(name = "GoodsItem", type = TPlusGoods.class) 
			 ,@XmlElement(name = "CustomerItem", type = TPlusCustomer.class) 
			 ,@XmlElement(name = "HouseItem", type = TPlusStorehouse.class) 
				 }) 
	//@XmlElement(name = "item", type =TPlusGoods.class) 
	private List<T> items;
	
	
	public ResponseRoot() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ResponseRoot(String flag, String code, String message,  List<T> items) {
		super();
		this.flag = flag;
		this.code = code;
		this.message = message;
		this.items = items;
	}
	@XmlTransient
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@XmlTransient
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@XmlTransient
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@XmlTransient
	public List<T> getItems() {
		return items;
	}
	public void setItems(List<T> items) {
		this.items = items;
	}

	
}
