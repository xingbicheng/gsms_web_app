package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** GoodsConvert */
@JsonInclude(Include.NON_NULL)
public class GoodsConvert implements Serializable {
	@JsonProperty("goodsConvertId")
	@ApiModelProperty(value = "")
	private String id;

    @JsonProperty("goodsOriginalName")
	@ApiModelProperty(value = "原始产品名称(字符长度为1-200)")
	@Size(min = 1, max = 200, message = "原始产品名称字符长度为1-200")
	@Excel(name = "原始产品名称", orderNum = "1")
	private String originalName;

	@ApiModelProperty(value = "原始计量单位(字符长度为1-10)")
	@Size(min = 1, max = 10, message = "原始计量单位字符长度为1-10")
	@Excel(name = "原始计量单位", orderNum = "2")
	private String originalUnit;

	@ApiModelProperty(value = "单位编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "单位编号字符长度为1-32")
	@Excel(name = "单位编号", orderNum = "3")
	private String customerTypeId;


	@ApiModelProperty(value = "内部商品_id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "内部商品_id字符长度为1-32")
	@Excel(name = "内部商品_id", orderNum = "4")
	private String inGoodsId;

	@ApiModelProperty(value = "外部商品_id(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "外部商品_id字符长度为1-32")
	@Excel(name = "外部商品_id", orderNum = "5")
	private String outGoodsId;

	@ApiModelProperty(value = "内部商品名字,新增不用填写")
	@Excel(name = "内部商品名字")
	private String inGoodsName;

	@ApiModelProperty(value = "外部商品名字,新增不用填写")
	@Excel(name = "外部商品名字")
	private String outGoodsName;
	
	@ApiModelProperty(value = "单位编号(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "单位编号字符长度为1-20")
	@Excel(name = "客户类型", orderNum = "6")
	private String customerType;
	
	@ApiModelProperty(value = "单位编号(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "单位编号字符长度为1-20")
	@Excel(name = "客户类型", orderNum = "6")
	private String unitName;
	
	@ApiModelProperty(value = "单位编号(字符长度为1-20) 0为新增，1为修改")
	private String flag ;
	


	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getOutGoodsName() {
		return outGoodsName;
	}

	public void setOutGoodsName(String outGoodsName) {
		this.outGoodsName = outGoodsName;
	}

	public String getInGoodsName() {
		return inGoodsName;
	}

	public void setInGoodsName(String inGoodsName) {
		this.inGoodsName = inGoodsName;
	}

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName == null ? null : originalName.trim();
	}

	public String getOriginalUnit() {
		return originalUnit;
	}

	public void setOriginalUnit(String originalUnit) {
		this.originalUnit = originalUnit == null ? null : originalUnit.trim();
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId == null ? null : customerTypeId.trim();
	}

	public String getInGoodsId() {
		return inGoodsId;
	}

	public void setInGoodsId(String inGoodsId) {
		this.inGoodsId = inGoodsId == null ? null : inGoodsId.trim();
	}

	public String getOutGoodsId() {
		return outGoodsId;
	}

	public void setOutGoodsId(String outGoodsId) {
		this.outGoodsId = outGoodsId == null ? null : outGoodsId.trim();
	}
	
	

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public void difference(GoodsConvert obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.originalName.equals(obj.getOriginalName())) {
			defString += "原始产品名称从" + this.originalName + "修改为" + obj.getOriginalName();
		}

		if (!this.originalUnit.equals(obj.getOriginalUnit())) {
			defString += "原始计量单位从" + this.originalUnit + "修改为" + obj.getOriginalUnit();
		}

		if (!this.customerTypeId.equals(obj.getCustomerTypeId())) {
			defString += "单位编号从" + this.customerTypeId + "修改为" + obj.getCustomerTypeId();
		}

		if (!this.inGoodsId.equals(obj.getInGoodsId())) {
			defString += "内部商品_id从" + this.inGoodsId + "修改为" + obj.getInGoodsId();
		}

		if (!this.outGoodsId.equals(obj.getOutGoodsId())) {
			defString += "外部商品_id从" + this.outGoodsId + "修改为" + obj.getOutGoodsId();
		}
		
		if (!this.customerType.equals(obj.getCustomerType())) {
			defString += "客户类型从" + this.customerType + "修改为" + obj.getCustomerType();
		}

	}
}