package com.myqm.pojo.gs.basic;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.myqm.utils.GetPinyinUtil;

@XmlAccessorType(value = XmlAccessType.PROPERTY) 
@XmlRootElement(name="GoodsItem") 
//@XmlType(propOrder={"code","name","shorThand","unitCode","unitName","invcCode","invcName"})
 public class TPlusGoods implements Serializable{

	private String id ;

	@XmlElement(name = "code")
	private String code ;

	@XmlElement(name = "name")
	private String name ;

	@XmlElement(name = "shorthand")
	private String shorThand;//：检索码

	@XmlElement(name = "unitcode")
	private String unitCode;//:计量单位编码

	@XmlElement(name = "unitname")
	private String unitName;//:计量单位名称
	
	@XmlElement(name = "invccode")
	private String invcCode;//：商品分类编码

	@XmlElement(name = "invcname")
	private String invcName;//：商品分类名称

	@XmlElement(name = "specification")
	private String specification;//：商品分类名称
	
	private String invcNamePinyin;//：商品分类名称拼音
	
	public String getInvcNamePinyin() {
		if (invcName!=null)
			return GetPinyinUtil.getPinYinHeadChar(invcName);
			else
				return null;
	}

	public void setInvcNamePinyin(String invcNamePinyin) {
		this.invcNamePinyin = invcNamePinyin;
	}

	public TPlusGoods() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TPlusGoods(String code, String name, String shorThand, String unitCode, String unitName, String invcCode,
			String invcName,String specification ) {
		super();
		this.code = code;
		this.name = name;
		this.shorThand = shorThand;
		this.unitCode = unitCode;
		this.unitName = unitName;
		this.invcCode = invcCode;
		this.invcName = invcName;
		this.specification=specification;
	}
	@XmlTransient
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	@XmlTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlTransient
	public String getShorThand() {
		return shorThand.toLowerCase();
	}

	public void setShorThand(String shorThand) {
		this.shorThand = shorThand;
	}
	@XmlTransient
	public String getUnitCode() {
		return unitCode;
	}
	
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	@XmlTransient
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	@XmlTransient
	public String getInvcCode() {
		return invcCode;
	}

	public void setInvcCode(String invcCode) {
		this.invcCode = invcCode;
	}
	@XmlTransient
	public String getInvcName() {
		return invcName;
	}

	public void setInvcName(String invcName) {
		this.invcName = invcName;
	}
	@XmlTransient
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
