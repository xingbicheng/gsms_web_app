package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** CustomerType */
@JsonInclude(Include.NON_NULL)
public class CustomerType implements Serializable {
	@JsonProperty("customerTypeId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "客户类型(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "客户类型字符长度为1-20")
	@Excel(name = "客户类型", orderNum = "1")
	private String customerType;

	@ApiModelProperty(value = "后定价否， 0 先定价， 1后定价(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "后定价否， 0 先定价， 1后定价字符长度为1-1")
	@Excel(name = "后定价否， 0 先定价， 1后定价", orderNum = "2")
	private String property;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "备注(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "备注字符长度为1-255")
	@Excel(name = "备注", orderNum = "4")
	private String memo;

	@ApiModelProperty(value = "拼音字段(字符长度为1-100)")
	@Size(min = 1, max = 100, message = "拼音字段字符长度为1-100")
	@Excel(name = "拼音字段", orderNum = "5")
	private String pinyin;

	@ApiModelProperty(value = "存货编码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "存货编码字符长度为1-20")
	@Excel(name = "存货编码", orderNum = "6")
	private String code;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "修改人字符长度为1-20")
	@Excel(name = "修改人", orderNum = "7")
	private String updaterName;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType == null ? null : customerType.trim();
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property == null ? null : property.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin == null ? null : pinyin.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName == null ? null : updaterName.trim();
	}

	public void difference(CustomerType obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.customerType.equals(obj.getCustomerType())) {
			defString += "客户类型从" + this.customerType + "修改为" + obj.getCustomerType();
		}

		if (!this.property.equals(obj.getProperty())) {
			defString += "后定价否， 0 先定价， 1后定价从" + this.property + "修改为" + obj.getProperty();
		}

		if (!this.memo.equals(obj.getMemo())) {
			defString += "备注从" + this.memo + "修改为" + obj.getMemo();
		}

		if (!this.pinyin.equals(obj.getPinyin())) {
			defString += "拼音字段从" + this.pinyin + "修改为" + obj.getPinyin();
		}

		if (!this.code.equals(obj.getCode())) {
			defString += "存货编码从" + this.code + "修改为" + obj.getCode();
		}

		if (!this.updaterName.equals(obj.getUpdaterName())) {
			defString += "修改人从" + this.updaterName + "修改为" + obj.getUpdaterName();
		}

	}
}