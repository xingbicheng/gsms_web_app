package com.myqm.pojo.gs.basic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.myqm.utils.GetPinyinUtil;

@XmlAccessorType(value = XmlAccessType.PROPERTY) 
@XmlRootElement(name="HouseItem") 
public class TPlusStorehouse {
	@XmlElement(name = "code")
	private String code ;

	@XmlElement(name = "name")
	private String name ;
	
	private String namePinyin;
	
	public String getNamePinyin() {
		if (name!=null)
			return GetPinyinUtil.getPinYinHeadChar(name);
			else
				return null;
	}

	public void setNamePinyin(String namePinyin) {
		this.namePinyin = namePinyin;
	}

	public TPlusStorehouse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TPlusStorehouse(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	@XmlTransient
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	@XmlTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
