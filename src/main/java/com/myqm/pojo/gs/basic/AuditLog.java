package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**AuditLog*/
@JsonInclude(Include.NON_NULL)
public class AuditLog implements Serializable {

	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "操作人的用户id(字符长度为1-32)")
	@Size(min=1, max=32,message="操作人的用户id字符长度为1-32")
	@Excel(name="操作人的用户id",orderNum="1")
	private String userName;

	@ApiModelProperty(value = "记录类型 0订单审核，1成本审核(字符长度为1-1)")
	@Size(min=1, max=1,message="记录类型 0订单审核，1成本审核字符长度为1-1")
	@Excel(name="记录类型 0订单审核，1成本审核",orderNum="2")
	private String type;

	@ApiModelProperty(value = "审核条数")
	private Long count;

	@ApiModelProperty(value = "审核时间")
	private Timestamp auditTime;



    private static final long serialVersionUID = 1L;

    public Timestamp getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Timestamp auditTime) {
        this.auditTime = auditTime;
    }

    public String getId () {
		return id;
	}
		
	public void setId (String id) {
		this.id= id ;
	}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType () {
    	 return type;
    }

    public void setType (String type) {
    	 this.type= type == null ? null : type.trim();
    }

	public Long getCount () {   
		return count;
	}
		
	public void setCount (Long count) {
		this.count= count ;
	}



    public interface AuditLogType{
	    //记录类型 0订单审核，1成本审核
       String ORDER="0";
       String COST="1";
    }

}

