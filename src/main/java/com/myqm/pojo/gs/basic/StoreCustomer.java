package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**StoreCustomer*/
@JsonInclude(Include.NON_NULL)
public class StoreCustomer implements Serializable {
	@JsonProperty("storeCustomerId")
	@ApiModelProperty(value = "")
	private String id;
	
	@ApiModelProperty(value = "库房编号(字符长度为1-32)")
	@Size(min=1, max=32,message="库房编号字符长度为1-32")
	@Excel(name="库房编号",orderNum="1")
	private String storeId;

	@ApiModelProperty(value = "用户类型(字符长度为1-32)")
	@Size(min=1, max=32,message="用户类型字符长度为1-32")
	@Excel(name="用户类型",orderNum="2")
	private String customerTypeId;
	
	@ApiModelProperty(value="备注(字符长度为1-225)")
	@Size(min=1,max=20,message="备注字符长度为1-225")
	@Excel(name="备注",orderNum="3")
	private String memo;
	
	@ApiModelProperty(value = "修改时间")
	private Timestamp createTime;
	
	@ApiModelProperty(value="建立人(字符长度为1-225)")
	@Size(min=1,max=32,message="建立人字符长度为1-225")
	@Excel(name="建立人",orderNum="5")
	private String createrId;
	
	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;
	
	@ApiModelProperty(value="修改人(字符长度为1-225)")
	@Size(min=1,max=32,message="修改人字符长度为1-225")
	@Excel(name="修改人",orderNum="7")
	private String updaterId;
	
	@ApiModelProperty(value = "库房名称(字符长度为1-255),新增不用填写")
	@Excel(name = "库房名称", orderNum = "1")
	private String storeName;

	@ApiModelProperty(value = "仓库类型编号(字符长度为1-32),新增不用填写")
	@Excel(name = "仓库类型编号", orderNum = "1")
	private String storeTypeId;
	
	@ApiModelProperty(value = "客户类型名称(字符长度为1-20),新增不用填写")
	@Excel(name = "客户类型名称", orderNum = "2")
	private String customerType;
	
	
	private static final long serialVersionUID = 1L;
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id=id;
	}
	
	public String getStoreId() {
		return storeId;
	}
	
	public void setStoreId(String storeId) {
		this.storeId= storeId == null ? null : storeId.trim();
	}
	
	public String getCustomerTypeId() {
		return customerTypeId;
	}
	
	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId= customerTypeId == null ? null : customerTypeId.trim();
	}
	
	public String getMemo() {
		return memo;
	}
	
	public void setMemo(String memo) {
		this.memo= memo == null ? null : memo.trim();
	}
	
	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId == null ? null : createrId.trim();
	}
	
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId == null ? null : updaterId.trim();
	}
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public String getStoreTypeId() {
		return storeTypeId;
	}
	
	public void setStoreTypeId(String storeTypeId) {
		this.storeTypeId = storeTypeId;
	}
	
	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	public void difference(StoreCustomer obj) {
		String defString = "";
		if(!Objects.equals(this.id,obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}
		
		if(!this.storeId.equals(obj.getStoreId())) {
			 defString += "库房编号从" + this.storeId +"修改为" +obj.getStoreId();
		}

		
		if(!this.customerTypeId.equals(obj.getCustomerTypeId())) {
			 defString += "用户类型从" + this.customerTypeId +"修改为" +obj.getCustomerTypeId();
		}
		
		 if(!this.memo.equals(obj.getMemo())) {
			 defString += "备注从" + this.memo +"修改为" +obj.getMemo();
		}
		 
		 if(!this.createrId.equals(obj.getCreaterId())) {
			 defString += "从" + this.createrId + "修改为" + obj.getCreaterId();
		 }
		 
		 if(!this.updaterId.equals(obj.getUpdaterId())) {
			 defString += "从" + this.updaterId + "修改为" + obj.getUpdaterId();
		 }
	}
}
