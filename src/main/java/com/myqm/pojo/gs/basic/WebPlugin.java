package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** WebPlugin */
@JsonInclude(Include.NON_NULL)
public class WebPlugin implements Serializable {

	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "插件描述(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "插件描述字符长度为1-255")
	@Excel(name = "插件描述", orderNum = "1")
	private String description;

	@ApiModelProperty(value = "路径(字符长度为1-50)")
	@Size(min = 1, max = 50, message = "路径字符长度为1-50")
	@Excel(name = "路径", orderNum = "2")
	private String path;

	@ApiModelProperty(value = "插件名称(字符长度为1-25)")
	@Size(min = 1, max = 25, message = "插件名称字符长度为1-25")
	@Excel(name = "插件名称", orderNum = "3")
	private String name;

	@ApiModelProperty(value = "插件类名(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "插件类名字符长度为1-255")
	@Excel(name = "插件类名", orderNum = "4")
	private String className;

	@ApiModelProperty(value = "创建时间")
	private Timestamp createTime;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "建立人(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "建立人字符长度为1-32")
	@Excel(name = "建立人", orderNum = "7")
	private String createrId;

	@ApiModelProperty(value = "修改人(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "修改人字符长度为1-32")
	@Excel(name = "修改人", orderNum = "8")
	private String updaterId;

	@ApiModelProperty(value = "网站地址(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "网站地址字符长度为1-255")
	@Excel(name = "网站地址", orderNum = "9")
	private String url;

	@ApiModelProperty(value = "学校网站用户名(字符长度为1-50)")
	@Size(min = 1, max = 50, message = "学校网站用户名字符长度为1-50")
	@Excel(name = "学校网站用户名", orderNum = "10")
	private String username;

	@ApiModelProperty(value = "密码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "密码字符长度为1-20")
	@Excel(name = "密码", orderNum = "11")
	private String password;

	@ApiModelProperty(value = "创建人")
	@Excel(name = "创建人", orderNum = "12")
	private String creatername;

	@ApiModelProperty(value = "修改人")
	@Excel(name = "创建人", orderNum = "13")
	private String updatername;

	public String getCreatername() {
		return creatername;
	}

	public void setCreatername(String creatername) {
		this.creatername = creatername;
	}

	public String getUpdatername() {
		return updatername;
	}

	public void setUpdatername(String updatername) {
		this.updatername = updatername;
	}

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? null : description.trim();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className == null ? null : className.trim();
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId == null ? null : createrId.trim();
	}

	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId == null ? null : updaterId.trim();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url == null ? null : url.trim();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void difference(WebPlugin obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.description.equals(obj.getDescription())) {
			defString += "插件描述从" + this.description + "修改为" + obj.getDescription();
		}

		if (!this.path.equals(obj.getPath())) {
			defString += "路径从" + this.path + "修改为" + obj.getPath();
		}

		if (!this.name.equals(obj.getName())) {
			defString += "插件名称从" + this.name + "修改为" + obj.getName();
		}

		if (!this.className.equals(obj.getClassName())) {
			defString += "插件类名从" + this.className + "修改为" + obj.getClassName();
		}

		if (!this.createrId.equals(obj.getCreaterId())) {
			defString += "建立人从" + this.createrId + "修改为" + obj.getCreaterId();
		}

		if (!this.updaterId.equals(obj.getUpdaterId())) {
			defString += "修改人从" + this.updaterId + "修改为" + obj.getUpdaterId();
		}

		if (!this.url.equals(obj.getUrl())) {
			defString += "网站地址从" + this.url + "修改为" + obj.getUrl();
		}

		if (!this.username.equals(obj.getUsername())) {
			defString += "学校网站用户名从" + this.username + "修改为" + obj.getUsername();
		}

		if (!this.password.equals(obj.getPassword())) {
			defString += "密码从" + this.password + "修改为" + obj.getPassword();
		}

	}
}