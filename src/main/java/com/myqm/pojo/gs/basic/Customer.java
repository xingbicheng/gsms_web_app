package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** Customer */
@JsonInclude(Include.NON_NULL)
public class Customer implements Serializable {
	@JsonProperty("customerId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "客户类型(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "客户类型字符长度为1-32")
	@Excel(name = "客户类型", orderNum = "1")
	private String customerTypeId;

	@ApiModelProperty(value = "订货单位名称(字符长度为1-40)")
	@Size(min = 1, max = 40, message = "订货单位名称字符长度为1-40")
	@Excel(name = "订货单位名称", orderNum = "2")
	private String customerName;

	@ApiModelProperty(value = "联系人姓名(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "联系人姓名字符长度为1-20")
	@Excel(name = "联系人姓名", orderNum = "3")
	private String contactName;

	@ApiModelProperty(value = "联系人电话(字符长度为1-15)")
	@Size(min = 1, max = 15, message = "联系人电话字符长度为1-15")
	@Excel(name = "联系人电话", orderNum = "4")
	private String contactTel;

	@ApiModelProperty(value = "拼音字段(字符长度为1-100)")
	@Size(min = 1, max = 100, message = "拼音字段字符长度为1-100")
	@Excel(name = "拼音字段", orderNum = "5")
	private String pinyin;

	@ApiModelProperty(value = "咨询电话1(字符长度为1-15)")
	@Size(min = 1, max = 15, message = "咨询电话1字符长度为1-15")
	@Excel(name = "咨询电话1", orderNum = "6")
	private String advisoryTel1;

	@ApiModelProperty(value = "咨询电话2(字符长度为1-15)")
	@Size(min = 1, max = 15, message = "咨询电话2字符长度为1-15")
	@Excel(name = "咨询电话2", orderNum = "7")
	private String advisoryTel2;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "备注(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "备注字符长度为1-255")
	@Excel(name = "备注", orderNum = "9")
	private String memo;

	@ApiModelProperty(value = "客户编码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "客户编码字符长度为1-20")
	@Excel(name = "客户编码", orderNum = "10")
	private String code;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "修改人字符长度为1-20")
	@Excel(name = "修改人", orderNum = "11")
	private String updaterName;

	@ApiModelProperty(value = "客户类型(字符长度为1-20),新增不用填写")

	@Excel(name = "客户类型", orderNum = "1")
	private String customerType;



	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName == null ? null : customerName.trim();
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName == null ? null : contactName.trim();
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel == null ? null : contactTel.trim();
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin == null ? null : pinyin.trim();
	}

	public String getAdvisoryTel1() {
		return advisoryTel1;
	}

	public void setAdvisoryTel1(String advisoryTel1) {
		this.advisoryTel1 = advisoryTel1 == null ? null : advisoryTel1.trim();
	}

	public String getAdvisoryTel2() {
		return advisoryTel2;
	}

	public void setAdvisoryTel2(String advisoryTel2) {
		this.advisoryTel2 = advisoryTel2 == null ? null : advisoryTel2.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName == null ? null : updaterName.trim();
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public void difference(Customer obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.customerTypeId.equals(obj.getCustomerTypeId())) {
			defString += "客户类型从" + this.customerTypeId + "修改为" + obj.getCustomerTypeId();
		}

		if (!this.customerName.equals(obj.getCustomerName())) {
			defString += "订货单位名称从" + this.customerName + "修改为" + obj.getCustomerName();
		}

		if (!this.contactName.equals(obj.getContactName())) {
			defString += "联系人姓名从" + this.contactName + "修改为" + obj.getContactName();
		}

		if (!this.contactTel.equals(obj.getContactTel())) {
			defString += "联系人电话从" + this.contactTel + "修改为" + obj.getContactTel();
		}

		if (!this.pinyin.equals(obj.getPinyin())) {
			defString += "拼音字段从" + this.pinyin + "修改为" + obj.getPinyin();
		}

		if (!this.advisoryTel1.equals(obj.getAdvisoryTel1())) {
			defString += "咨询电话1从" + this.advisoryTel1 + "修改为" + obj.getAdvisoryTel1();
		}

		if (!this.advisoryTel2.equals(obj.getAdvisoryTel2())) {
			defString += "咨询电话2从" + this.advisoryTel2 + "修改为" + obj.getAdvisoryTel2();
		}

		if (!this.memo.equals(obj.getMemo())) {
			defString += "备注从" + this.memo + "修改为" + obj.getMemo();
		}

		if (!this.code.equals(obj.getCode())) {
			defString += "客户编码从" + this.code + "修改为" + obj.getCode();
		}

		if (!this.updaterName.equals(obj.getUpdaterName())) {
			defString += "修改人从" + this.updaterName + "修改为" + obj.getUpdaterName();
		}
	}

}