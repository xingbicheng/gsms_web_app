package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** SysSet */
@JsonInclude(Include.NON_NULL)
public class SysSet implements Serializable{
	
	@JsonProperty("Id")
	@ApiModelProperty(value = "")
	private String id;
	
	@ApiModelProperty(value = "设置项名称(字符长度为1-100)")
	@Size(min = 1, max = 10, message = "设置项名称字符长度为1-100")
	@Excel(name = "设置项名称", orderNum = "1")
	private String setname;
	
	@ApiModelProperty(value = " ")
	@Size(min = 1, max = 10, message = " ")
	@Excel(name = " ", orderNum = "2")
	private String setvalue1;
	
	@ApiModelProperty(value = " ")
	@Size(min = 1, max = 10, message = " ")
	@Excel(name = " ", orderNum = "3")
	private String setvalue2;
	
	@ApiModelProperty(value = " ")
	@Size(min = 1, max = 10, message = " ")
	@Excel(name = " ", orderNum = "4")
	private String setvalue3;
	
	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getSetname() {
		return setname;
	}
	
	public void setSetname(String setname) {
		this.setname = setname;
	}
	
	public String getSetvalue1() {
		return setvalue1;
	}
	
	public void setSetvalue1(String setvalue1) {
		this.setvalue1 = setvalue1;
	}
	
	public String getSetvalue2() {
		return setvalue2;
	}
	
	public void setSetvalue2(String setvalue2) {
		this.setvalue2 = setvalue2;
	}
	
	public String getSetvalue3() {
		return setvalue3;
	}
	
	public void setSetvalue3(String setvalue3) {
		this.setvalue3 = setvalue3;
	}

	public void difference(SysSet obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}
     }
}
