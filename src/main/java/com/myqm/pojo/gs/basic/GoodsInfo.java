package com.myqm.pojo.gs.basic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class GoodsInfo {
	
	@JsonIgnoreProperties 
 	private String customerId;
	
	private String goodsId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	
}
