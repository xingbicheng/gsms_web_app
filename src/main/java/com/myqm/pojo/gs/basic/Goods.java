package com.myqm.pojo.gs.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

/** Goods */
@JsonInclude(Include.NON_NULL)
public class Goods implements Serializable {
	@JsonProperty("goodsId")
	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "内部产品名称(字符长度为1-50)")
	@Size(min = 1, max = 50, message = "内部产品名称字符长度为1-50")
	@Excel(name = "内部产品名称", orderNum = "1")
	private String goodsName;

	@ApiModelProperty(value = "内部商品分类(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "内部商品分类字符长度为1-32")
	@Excel(name = "内部商品分类", orderNum = "2")
	private String goodsTypeId;

	@ApiModelProperty(value = "计量单位(字符长度为1-10)")
	@Size(min = 1, max = 10, message = "计量单位字符长度为1-10")
	@Excel(name = "计量单位", orderNum = "3")
	private String unitName;

	@ApiModelProperty(value = "定价")
	private BigDecimal price;

	@ApiModelProperty(value = "(字符长度为1-100)")
	@Size(min = 1, max = 100, message = "字符长度为1-100")
	@Excel(name = "", orderNum = "5")
	private String pinyin;

	@ApiModelProperty(value = "存货编码(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "存货编码字符长度为1-20")
	@Excel(name = "存货编码", orderNum = "6")
	private String code;

	@ApiModelProperty(value = "0为内部 1为外部(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "0为内部 1为外部字符长度为1-1")
	@Excel(name = "0为内部 1为外部", orderNum = "7")
	private String internal;

	@ApiModelProperty(value = "备用名(字符长度为0-50)")
	@Size(min = 0, max = 50, message = "备用名字符长度为0-50")
	@Excel(name = "备用名", orderNum = "8")
	private String alternateName;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;

	@ApiModelProperty(value = "备注(字符长度为1-255)")
	@Size(min = 1, max = 255, message = "备注字符长度为1-255")
	@Excel(name = "备注", orderNum = "10")
	private String memo;

	@ApiModelProperty(value = "修改人(字符长度为1-20)")
	@Size(min = 1, max = 20, message = "修改人字符长度为1-20")
	@Excel(name = "修改人", orderNum = "11")
	private String updaterName;

	@ApiModelProperty(value = "损耗率")
	private Float lossRate;

	@ApiModelProperty(value = "是否收整为斤 0 不收整， 1为收整(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "是否收整为斤 0 不收整， 1为收整字符长度为1-1")
	@Excel(name = "是否收整为斤 0 不收整， 1为收整", orderNum = "13")
	private String pickUp;

	@ApiModelProperty(value = "预警单据 ，高于此单价后预警")
	private BigDecimal warningPrice = null;

	@ApiModelProperty(value = "数量预警，高于该数量预警")
	private Float warningAmount = null;

	@ApiModelProperty(value = "内部商品分类(字符长度为1-20),新增不用填写")
	private String goodsType;
	
	@ApiModelProperty(value = "默认供应商(字符长度为1-50)")
	@Size(min = 1, max = 32, message = "默认供应商字符长度为1-32")
	@Excel(name = "默认供应商", orderNum = "17")
	private String providerId;
	
	@ApiModelProperty(value = "默认供应商名字(字符长度为1-50)")
	@Size(min = 1, max = 32, message = "默认供应商名字为1-32")
	@Excel(name = "默认供应商名字", orderNum = "17")
	private String providerName;

	@ApiModelProperty(value = "最近价格")
	private BigDecimal currentPrice;

	
	@ApiModelProperty(value = "是否允许为空价0 不允许， 1为允许(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "是否允许为空价 0 不允许， 1为允许字符长度为1-1")
	@Excel(name = "是否允许为空价 0 不允许， 1为允许", orderNum = "19")
	private String blackPrice;

	
	@ApiModelProperty(value = "规格")
 	@Excel(name = "规格", orderNum = "19")
	private String specifications;
	
	private static final long serialVersionUID = 1L;

    public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsTypeId() {
		return goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId == null ? null : goodsTypeId.trim();
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin == null ? null : pinyin.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public String getInternal() {
		return internal;
	}

	public void setInternal(String internal) {
		this.internal = internal == null ? null : internal.trim();
	}

	public String getAlternateName() {
		return alternateName;
	}

	public void setAlternateName(String alternateName) {
		this.alternateName = alternateName == null ? null : alternateName.trim();
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName == null ? null : updaterName.trim();
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Float getLossRate() {
		return lossRate;
	}

	public void setLossRate(Float lossRate) {
		this.lossRate = lossRate;
	}

	public String getPickUp() {
		return pickUp;
	}

	public void setPickUp(String pickUp) {
		this.pickUp = pickUp;
	}

	public BigDecimal getWarningPrice() {
		return warningPrice;
	}

	public void setWarningPrice(BigDecimal warningPrice) {
		this.warningPrice = warningPrice;
	}

	public Float getWarningAmount() {
		return warningAmount;
	}

	public void setWarningAmount(Float warningAmount) {
		this.warningAmount = warningAmount;
	}

	
	
	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	public String getBlackPrice() {
		return blackPrice;
	}

	public void setBlackPrice(String blackPrice) {
		this.blackPrice = blackPrice;
	}

	public void difference(Goods obj) {
		String defString = "";
		if (!Objects.equals(this.id, obj.getId())) {
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if (!this.goodsName.equals(obj.getGoodsName())) {
			defString += "内部产品名称从" + this.goodsName + "修改为" + obj.getGoodsName();
		}

		if (!this.goodsTypeId.equals(obj.getGoodsTypeId())) {
			defString += "内部商品分类从" + this.goodsTypeId + "修改为" + obj.getGoodsTypeId();
		}

		if (!this.unitName.equals(obj.getUnitName())) {
			defString += "计量单位从" + this.unitName + "修改为" + obj.getUnitName();
		}

		if (Math.abs(this.price.doubleValue() - obj.getPrice().doubleValue()) > 0) {
			defString += "定价从" + this.price + "修改为" + obj.getPrice();
		}

		if (!this.pinyin.equals(obj.getPinyin())) {
			defString += "拼音从" + this.pinyin + "修改为" + obj.getPinyin();
		}

		if (!this.code.equals(obj.getCode())) {
			defString += "存货编码从" + this.code + "修改为" + obj.getCode();
		}

		if (!this.internal.equals(obj.getInternal())) {
			defString += "0为内部 1为外部从" + this.internal + "修改为" + obj.getInternal();
		}

		if (!this.alternateName.equals(obj.getAlternateName())) {
			defString += "备用名从" + this.alternateName + "修改为" + obj.getAlternateName();
		}

		if (!this.memo.equals(obj.getMemo())) {
			defString += "备注从" + this.memo + "修改为" + obj.getMemo();
		}

		if (!this.updaterName.equals(obj.getUpdaterName())) {
			defString += "修改人从" + this.updaterName + "修改为" + obj.getUpdaterName();
		}

		if (Math.abs(this.lossRate - obj.getLossRate()) > 0) {
			defString += "损耗率从" + this.lossRate + "修改为" + obj.getLossRate();
		}

		if (!this.pickUp.equals(obj.getPickUp())) {
			defString += "是否收整为斤 0 不收整， 1为收整从" + this.pickUp + "修改为" + obj.getPickUp();
		}

		if (this.warningPrice.compareTo(obj.getWarningPrice()) != 0) {
			defString += "预警单据 ，高于此单价后预警从" + this.warningPrice + "修改为" + obj.getWarningPrice();
		}

		if (Math.abs(this.warningAmount - obj.getWarningAmount()) > 0) {
			defString += "数量预警，高于该数量预警从" + this.warningAmount + "修改为" + obj.getWarningAmount();
		}
		
		if (!this.currentPrice.equals(obj.getCurrentPrice())) {
			defString += "最近价格从" + this.currentPrice + "修改为" + obj.getCurrentPrice();
		}

		if (!this.blackPrice.equals(obj.getBlackPrice())) {
			defString += "是否允许为空价 0 不允许， 1为允许" + this.blackPrice + "修改为" + obj.getBlackPrice();
		}


	}

}