package com.myqm.pojo.gs.tplus;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class TPlusLockInfo {
	@ApiModelProperty(value = "锁定订单信息")
	List<TPlusOrderCustomer> orders = new ArrayList<TPlusOrderCustomer>();
	@ApiModelProperty(value = "锁定涉及到的供应商信息")
	List<TPlusProvider> providers;// = new ArrayList<TPlusProvider>();
	public List<TPlusOrderCustomer> getOrders() {
		return orders;
	}
	public void setOrders(List<TPlusOrderCustomer> orders) {
		this.orders = orders;
	}
	public List<TPlusProvider> getProviders() {
		return providers;
	}
	public void setProviders(List<TPlusProvider> providers) {
		this.providers = providers;
	}
	 
	
}
