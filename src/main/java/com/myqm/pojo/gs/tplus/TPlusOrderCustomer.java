package com.myqm.pojo.gs.tplus;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class TPlusOrderCustomer { 
	
	@ApiModelProperty(value = "客户编号")
	@NotNull
	String customerId;
	@ApiModelProperty(value = "客户姓名")
	String customerName;
	
	@ApiModelProperty(value = "仓库编号")
	@NotNull
	String storeId;
	
	 
	@ApiModelProperty(value = "客户订单NO")
	List<String> orderNos;
	@ApiModelProperty(value = "客户订单Ids")
	@NotNull
	@Size(min= 1)
	List<String> orderIds;
	
	
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	 
	public List<String> getOrderNos() {
		return orderNos;
	}
	public void setOrderNos(List<String> orderNos) {
		this.orderNos = orderNos;
	}
	public List<String> getOrderIds() {
		return orderIds;
	}
	public void setOrderIds(List<String> orderIds) {
		this.orderIds = orderIds;
	}
	

}
