package com.myqm.pojo.gs.tplus;
 
import io.swagger.annotations.ApiModelProperty;

public class TPlusProvider { 
	@ApiModelProperty(value = "供应商ID")
	private String id;

	@ApiModelProperty(value = "供应商名称")
	private String providerName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	
	
}
