package com.myqm.pojo.gs.receive;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**StroehouseBack*/
@JsonInclude(Include.NON_NULL)
public class StroehouseBack implements Serializable {

	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "中央库入库单编号(字符长度为1-32)")
	@Size(min=1, max=32,message="中央库入库单编号字符长度为1-32")
	@Excel(name="中央库入库单编号",orderNum="1")
	private String toInId;

	@ApiModelProperty(value = "总价")
	private BigDecimal sum;

	@ApiModelProperty(value = "实际采购单价")
	private BigDecimal price;

	@ApiModelProperty(value = "内部商品名id(字符长度为1-32)")
	@Size(min=1, max=32,message="内部商品名id字符长度为1-32")
	@Excel(name="内部商品名id",orderNum="4")
	private String goodsId;

	@ApiModelProperty(value = "采购数量")
	private Double amount;

	@ApiModelProperty(value = "入库时间")
	private Timestamp outTime;

	@ApiModelProperty(value = "供货商id(字符长度为1-32)")
	@Size(min=1, max=32,message="供货商id字符长度为1-32")
	@Excel(name="供货商id",orderNum="7")
	private String providerId;

	@ApiModelProperty(value = "包装备注(字符长度为1-200)")
	@Size(min=1, max=200,message="包装备注字符长度为1-200")
	@Excel(name="包装备注",orderNum="8")
	private String packingMemo;

	@ApiModelProperty(value = "调拨库房id(字符长度为1-32)")
	@Size(min=1, max=32,message="调拨库房id字符长度为1-32")
	@Excel(name="调拨库房id",orderNum="9")
	private String fromStorehouseId;

	@ApiModelProperty(value = "调拨库房id(字符长度为1-32)")
	@Size(min=1, max=32,message="调拨库房id字符长度为1-32")
	@Excel(name="调拨库房id",orderNum="10")
	private String toStorehouseId;

	@ApiModelProperty(value = "分库入库单编号(字符长度为1-32)")
	@Size(min=1, max=32,message="分库入库单编号字符长度为1-32")
	@Excel(name="分库入库单编号",orderNum="11")
	private String formInId;

	@ApiModelProperty(value = "移库方式，0 退货到中央库 2 退货到中央库，但货品不退回(字符长度为1-1)")
	@Size(min=1, max=1,message="移库方式，0 退货到中央库 2 退货到中央库，但货品不退回字符长度为1-1")
	@Excel(name="移库方式，0 退货到中央库 2 退货到中央库，但货品不退回",orderNum="12")
	private String type;


    private static final long serialVersionUID = 1L;

	

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getOutTime() {
		return outTime;
	}

	public void setOutTime(Timestamp outTime) {
		this.outTime = outTime;
	}

	public String getToInId () {   
    	 return toInId;
    }

    public void setToInId (String toInId) {
    	 this.toInId= toInId == null ? null : toInId.trim();
    }

	public BigDecimal getSum () {   
	    return sum;
	}

	public void setSum (BigDecimal sum) {
	    this.sum= sum;
	}

	public BigDecimal getPrice () {   
	    return price;
	}

	public void setPrice (BigDecimal price) {
	    this.price= price;
	}

    public String getGoodsId () {   
    	 return goodsId;
    }

    public void setGoodsId (String goodsId) {
    	 this.goodsId= goodsId == null ? null : goodsId.trim();
    }

	public Double getAmount () {   
		  return amount;
	}
	
	public void setAmount (Double amount) {
		  this.amount= amount;
	}

    public String getProviderId () {   
    	 return providerId;
    }

    public void setProviderId (String providerId) {
    	 this.providerId= providerId == null ? null : providerId.trim();
    }

    public String getPackingMemo () {   
    	 return packingMemo;
    }

    public void setPackingMemo (String packingMemo) {
    	 this.packingMemo= packingMemo == null ? null : packingMemo.trim();
    }

    public String getFromStorehouseId () {   
    	 return fromStorehouseId;
    }

    public void setFromStorehouseId (String fromStorehouseId) {
    	 this.fromStorehouseId= fromStorehouseId == null ? null : fromStorehouseId.trim();
    }

    public String getToStorehouseId () {   
    	 return toStorehouseId;
    }

    public void setToStorehouseId (String toStorehouseId) {
    	 this.toStorehouseId= toStorehouseId == null ? null : toStorehouseId.trim();
    }

    public String getFormInId () {   
    	 return formInId;
    }

    public void setFormInId (String formInId) {
    	 this.formInId= formInId == null ? null : formInId.trim();
    }

    public String getType () {   
    	 return type;
    }

    public void setType (String type) {
    	 this.type= type == null ? null : type.trim();
    }


	public void difference(StroehouseBack obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.toInId.equals(obj.getToInId())) {
			 defString += "中央库入库单编号从" + this.toInId +"修改为" +obj.getToInId();
		}

		if (this.sum.compareTo(obj.getSum()) != 0){
			defString += "总价从" + this.sum + "修改为" + obj.getSum();
		}

		if (this.price.compareTo(obj.getPrice()) != 0){
			defString += "实际采购单价从" + this.price + "修改为" + obj.getPrice();
		}

	   if(!this.goodsId.equals(obj.getGoodsId())) {
			 defString += "内部商品名id从" + this.goodsId +"修改为" +obj.getGoodsId();
		}

		if (Math.abs(this.amount - obj.getAmount()) > 0 ) {
			defString += "采购数量从" + this.amount + "修改为" + obj.getAmount();
		}

	   if(!this.providerId.equals(obj.getProviderId())) {
			 defString += "供货商id从" + this.providerId +"修改为" +obj.getProviderId();
		}

	   if(!this.packingMemo.equals(obj.getPackingMemo())) {
			 defString += "包装备注从" + this.packingMemo +"修改为" +obj.getPackingMemo();
		}

	   if(!this.fromStorehouseId.equals(obj.getFromStorehouseId())) {
			 defString += "调拨库房id从" + this.fromStorehouseId +"修改为" +obj.getFromStorehouseId();
		}

	   if(!this.toStorehouseId.equals(obj.getToStorehouseId())) {
			 defString += "调拨库房id从" + this.toStorehouseId +"修改为" +obj.getToStorehouseId();
		}

	   if(!this.formInId.equals(obj.getFormInId())) {
			 defString += "分库入库单编号从" + this.formInId +"修改为" +obj.getFormInId();
		}

	   if(!this.type.equals(obj.getType())) {
			 defString += "移库方式，0 退货到中央库 2 退货到中央库，但货品不退回从" + this.type +"修改为" +obj.getType();
		}


    }
}