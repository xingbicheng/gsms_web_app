package com.myqm.pojo.gs.receive;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**BackProvider*/
@JsonInclude(Include.NON_NULL)
public class BackProvider implements Serializable {

	@ApiModelProperty(value = "")
	private String id;

	@ApiModelProperty(value = "入库单编号(字符长度为1-32)")
	@Size(min=1, max=32,message="入库单编号字符长度为1-32")
	@Excel(name="入库单编号",orderNum="1")
	private String inId;

	@ApiModelProperty(value = "总价")
	private BigDecimal sum;

	@ApiModelProperty(value = "实际采购单价")
	private BigDecimal price;

	@ApiModelProperty(value = "内部商品名id(字符长度为1-32)")
	@Size(min=1, max=32,message="内部商品名id字符长度为1-32")
	@Excel(name="内部商品名id",orderNum="4")
	private String goodsId;

	@ApiModelProperty(value = "采购数量")
	private Double amount;

	@ApiModelProperty(value = "退回时间")
	private Timestamp backTime;


	@ApiModelProperty(value = "供货商id(字符长度为1-32)")
	@Size(min=1, max=32,message="供货商id字符长度为1-32")
	@Excel(name="供货商id",orderNum="7")
	private String providerId;

	@ApiModelProperty(value = "包装备注(字符长度为1-200)")
	@Size(min=1, max=200,message="包装备注字符长度为1-200")
	@Excel(name="包装备注",orderNum="8")
	private String packingMemo;

	@ApiModelProperty(value = "库房id(字符长度为1-32)")
	@Size(min=1, max=32,message="库房id字符长度为1-32")
	@Excel(name="库房id",orderNum="9")
	private String storehouseId;
	
	@ApiModelProperty(value = "供货商名字(字符长度为1-32)")
	private String providerName;
	
	@ApiModelProperty(value = "货物名字(字符长度为1-32)")
	private String goodsName;
	
	@ApiModelProperty(value = "库房名字(字符长度为1-32)")
	private String  storeName;
    private static final long serialVersionUID = 1L;

    
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getId () {
		return id;
	}
		
	public void setId (String id) {
		this.id= id ;
	}

    public String getInId () {   
    	 return inId;
    }

    public void setInId (String inId) {
    	 this.inId= inId == null ? null : inId.trim();
    }

	public BigDecimal getSum () {   
	    return sum;
	}

	public void setSum (BigDecimal sum) {
	    this.sum= sum;
	}

	public BigDecimal getPrice () {   
	    return price;
	}

	public void setPrice (BigDecimal price) {
	    this.price= price;
	}

    public String getGoodsId () {   
    	 return goodsId;
    }

    public void setGoodsId (String goodsId) {
    	 this.goodsId= goodsId == null ? null : goodsId.trim();
    }

	public Double getAmount () {   
		  return amount;
	}
	
	public void setAmount (Double amount) {
		  this.amount= amount;
	}

    public String getProviderId () {   
    	 return providerId;
    }

    public void setProviderId (String providerId) {
    	 this.providerId= providerId == null ? null : providerId.trim();
    }

    public String getPackingMemo () {   
    	 return packingMemo;
    }

    public void setPackingMemo (String packingMemo) {
    	 this.packingMemo= packingMemo == null ? null : packingMemo.trim();
    }

    public String getStorehouseId () {   
    	 return storehouseId;
    }

    public void setStorehouseId (String storehouseId) {
    	 this.storehouseId= storehouseId == null ? null : storehouseId.trim();
    }


	public void difference(BackProvider obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.inId.equals(obj.getInId())) {
			 defString += "入库单编号从" + this.inId +"修改为" +obj.getInId();
		}

		if (this.sum.compareTo(obj.getSum()) != 0){
			defString += "总价从" + this.sum + "修改为" + obj.getSum();
		}

		if (this.price.compareTo(obj.getPrice()) != 0){
			defString += "实际采购单价从" + this.price + "修改为" + obj.getPrice();
		}

	   if(!this.goodsId.equals(obj.getGoodsId())) {
			 defString += "内部商品名id从" + this.goodsId +"修改为" +obj.getGoodsId();
		}

		if (Math.abs(this.amount - obj.getAmount()) > 0 ) {
			defString += "采购数量从" + this.amount + "修改为" + obj.getAmount();
		}

	   if(!this.providerId.equals(obj.getProviderId())) {
			 defString += "供货商id从" + this.providerId +"修改为" +obj.getProviderId();
		}

	   if(!this.packingMemo.equals(obj.getPackingMemo())) {
			 defString += "包装备注从" + this.packingMemo +"修改为" +obj.getPackingMemo();
		}

	   if(!this.storehouseId.equals(obj.getStorehouseId())) {
			 defString += "库房id从" + this.storehouseId +"修改为" +obj.getStorehouseId();
		}


    }
}