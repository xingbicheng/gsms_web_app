package com.myqm.pojo.base;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class BackInfo {
	@ApiModelProperty(value="订单明细id")
	@NotNull
	private String detailId;
	
	@ApiModelProperty(value="入库单id(退回中央库时需要)")
	@NotNull
	private String inId;
	
	@ApiModelProperty(value="库房id")
	private String storehouseId;
	
	@ApiModelProperty(value="退货数量")
	@NotNull
	private Double amount;
	
	@ApiModelProperty(value="b:退回供应商9:退回中央库a:退回中央库(货品留在分库)c:计入损耗")
	@NotNull
	private String type;

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getInId() {
		return inId;
	}

	public void setInId(String inId) {
		this.inId = inId;
	}

	public String getStorehouseId() {
		return storehouseId;
	}

	public void setStorehouseId(String storehouseId) {
		this.storehouseId = storehouseId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	

}
