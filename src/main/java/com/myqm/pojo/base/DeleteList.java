package com.myqm.pojo.base;

import java.util.List;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class DeleteList {
	@ApiModelProperty(value = "删除编号的数组")
	@Size(min = 1)
	List<String> ids;

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

}
