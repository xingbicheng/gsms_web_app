package com.myqm.pojo.base;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@XmlAccessorType(value = XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseInfo

		extends BaseRecord {
	/**
	 * 备注
	 */
	@XmlTransient
	@Excel(name = "memo", orderNum = "1")
	@ApiModelProperty(value = "备注")
	protected String memo;

	/**
	 * 更新人Id
	 */
	@XmlTransient
	@ApiModelProperty(value = "更新人Id")
	protected String updaterId;

	/**
	 * 更新人Name
	 */
	@XmlTransient
	@ApiModelProperty(value = "更新人Name")
	protected String updaterName;

	/**
	 * 新建人Id
	 */
	@XmlTransient
	@ApiModelProperty(value = "新建人Id")
	protected String createrId;

	/**
	 * 新建人Name
	 */
	@XmlTransient
	@ApiModelProperty(value = "新建人Name")
	protected String createrName;
	/**
	 * 修改时间
	 */
	@XmlTransient
	@ApiModelProperty(value = "更新时间")
	protected Timestamp updateTime;

	/**
	 * 建单时间
	 */
	@XmlTransient
	@ApiModelProperty(value = "建单时间")
	protected Timestamp createTime;

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId;
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}
