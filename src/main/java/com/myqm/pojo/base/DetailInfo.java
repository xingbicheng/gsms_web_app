package com.myqm.pojo.base;


import java.util.List;

import javax.validation.constraints.NotNull;



import io.swagger.annotations.ApiModelProperty;

public class DetailInfo {
		@ApiModelProperty(value = "订单id")
		@NotNull
		String orderId;
		
//		@ApiModelProperty(value = "订货明细 ")
//		List<OrderDetail>details;

		@ApiModelProperty(value = "订货明细ids ")
		@NotNull
		List<String>detailIds;
		
		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public List<String> getDetailIds() {
			return detailIds;
		}

		public void setDetailIds(List<String> detailIds) {
			this.detailIds = detailIds;
		}

//		public List<OrderDetail> getDetails() {
//			return details;
//		}
//
//		public void setDetails(List<OrderDetail> details) {
//			this.details = details;
//		}
		
		
		
		
}
