package com.myqm.pojo.base;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import io.swagger.annotations.ApiModelProperty;

@XmlAccessorType(value = XmlAccessType.FIELD)
public class BaseRecord {

	@XmlTransient
	@ApiModelProperty(value = "基本信息id编号主键,新建记录无id")
	@NotNull
	protected String id = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
