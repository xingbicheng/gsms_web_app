package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.sql.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseInfo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class SysUser extends BaseInfo implements Serializable {

	private static final long serialVersionUID = -6525908145032868837L;

	@Size(min = 1, max = 11)
	@ApiModelProperty(value = "登陆用户")
	@Excel(name = "username", orderNum = "1")
	private String username;

	@Size(min = 1, max = 60)
	@JsonIgnore
	@ApiModelProperty(value = "登陆密码")
	private String password;

	@Size(min = 1)
	@ApiModelProperty(value = "昵称")
	@Excel(name = "nickname", orderNum = "1")
	private String nickname;
 

	@ApiModelProperty(value = "修改头像")
	@Excel(name = "headImgUr1", orderNum = "1")
	private String headImgUrl;

	@Size(min = 1, max = 11)
	@ApiModelProperty(value = "电话")
	@Excel(name = "phone", orderNum = "1")
	private String phone;

	@ApiModelProperty(value = "部门")
	@Excel(name = "部门名称", orderNum = "1")
	private String departmentName;


	@ApiModelProperty(value = "部门ID")
	@Excel(name = "departmentId", orderNum = "1")
	private String departmentId;


	// @NotNull
	@Size(min = 1, max = 30)
	@ApiModelProperty(value = "用户手机")
	@Excel(name = "telephone", orderNum = "1")
	private String telephone;

	@Email
	@Size(min = 0, max = 50)
	@ApiModelProperty(value = "邮箱")
	@Excel(name = "email", orderNum = "1")
	private String email;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "用户生日")
	@Excel(name = "birthday", databaseFormat = "yyMMddHHmmss", format = "yyyy-MM-dd", orderNum = "1")
	private Date birthday;

	@ApiModelProperty(value = "性别")
	@Excel(name = "sex", orderNum = "1")
	@Pattern(regexp = "[FM]")
	@Size(min = 1, max = 1)
	private String sex;

	@ApiModelProperty(value = "pc 状态:不可用 0;可用 1;锁定2; 微信 用户状态（0申请中，1审核通过，2审核不通过）")
	@Excel(name = "status", orderNum = "1")
	private Integer status;
	
	
	@ApiModelProperty(value = "申请的类型(0审核员，1提供者，2消费者)(字符长度为1-2)")
	@Size(min=1, max=2,message="申请的类型(0审核员，1提供者，2消费者)字符长度为1-2")
	@Excel(name="申请的类型(0审核员，1提供者，2消费者)",orderNum="4")
	private String type;

	@ApiModelProperty(value = "生成的openid(字符长度为1-32)")
	@Size(min=1, max=40,message="生成的openid字符长度为1-32")
	@Excel(name="生成的userId",orderNum="6")
	private String userId;





    private String roleId;

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }



    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }



    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	 
 

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public interface Status {
		int DISABLED = 0;
		int VALID = 1;
		int LOCKED = 2;
	}
	
	public interface Type {
		String Auditing = "0";
		String Provider = "1";
		String Consumer = "2";
	}
}
