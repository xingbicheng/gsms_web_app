package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**Department*/
@JsonInclude(Include.NON_NULL)
public class Department implements Serializable {

	@ApiModelProperty(value = "部门id")
	private String id;

	@ApiModelProperty(value = "部门名称(字符长度为1-40)")
	@Size(min=1, max=40,message="部门名称字符长度为1-40")
	@Excel(name="部门名称",orderNum="1")
	private String departmentName;

	@ApiModelProperty(value = "修改时间")
	private Timestamp updateTime;
	
	@ApiModelProperty(value = "修改时间")
	private Timestamp createTime;
	

	@ApiModelProperty(value = "修改人(字符长度为1-20)") 
	@Excel(name="修改人",orderNum="3")
	private String updaterName;
	
	@ApiModelProperty(value = "建立人(字符长度为1-20)") 
	@Excel(name="建立人",orderNum="3")
	private String createrName;

	@ApiModelProperty(value = "建立人(字符长度为1-32)")
	@Size(min=1, max=32,message="建立人字符长度为1-32")
	@Excel(name="建立人",orderNum="4")
	private String createrId;

	@ApiModelProperty(value = "修改人(字符长度为1-32)")
	@Size(min=1, max=32,message="修改人字符长度为1-32")
	@Excel(name="修改人",orderNum="5")
	private String updaterId;


    public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCreaterName() {
		return createrName;
	}

	 


	private static final long serialVersionUID = 1L;

	public String getId () {   
		return id;
	}
		
	public void setId (String id) {
		this.id= id ;
	}

    public String getDepartmentName () {   
    	 return departmentName;
    }

    public void setDepartmentName (String departmentName) {
    	 this.departmentName= departmentName == null ? null : departmentName.trim();
    }

    public String getUpdaterName () {   
    	 return updaterName;
    }

    public void setUpdaterName (String updaterName) {
    	 this.updaterName= updaterName == null ? null : updaterName.trim();
    }

    public String getCreaterId () {   
    	 return createrId;
    }

    public void setCreaterId (String createrId) {
    	 this.createrId= createrId == null ? null : createrId.trim();
    }

    public String getUpdaterId () {   
    	 return updaterId;
    }

    public void setUpdaterId (String updaterId) {
    	 this.updaterId= updaterId == null ? null : updaterId.trim();
    }


	public void difference(Department obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "部门id从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.departmentName.equals(obj.getDepartmentName())) {
			 defString += "部门名称从" + this.departmentName +"修改为" +obj.getDepartmentName();
		}

	   if(!this.updaterName.equals(obj.getUpdaterName())) {
			 defString += "修改人从" + this.updaterName +"修改为" +obj.getUpdaterName();
		}

	   if(!this.createrId.equals(obj.getCreaterId())) {
			 defString += "建立人从" + this.createrId +"修改为" +obj.getCreaterId();
		}

	   if(!this.updaterId.equals(obj.getUpdaterId())) {
			 defString += "修改人从" + this.updaterId +"修改为" +obj.getUpdaterId();
		} 
    }

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
}