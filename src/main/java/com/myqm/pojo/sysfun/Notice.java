package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseInfo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Notice extends BaseInfo implements Serializable {

	private static final long serialVersionUID = -4401913568806243090L;

	@ApiModelProperty(value = "标题")
	@NotNull
	@Size(min = 1, max = 128)
	@Excel(name = "title", orderNum = "1")
	private String title;

	@ApiModelProperty(value = "内容")
	@NotNull
	@Excel(name = "content", orderNum = "1")
	private String content;

	@ApiModelProperty(value = "状态")
	// @NotNull
	@Max(1)
	@Excel(name = "status", orderNum = "1")
	private Long status = 0L;

	@ApiModelProperty(value = "私信否")
	// @NotNull
	@Max(1)
	@Excel(name = "isPrivated", orderNum = "1")
	private String isPrivated = "1";

	@ApiModelProperty(value = "私信阅读者")
	@Excel(name = "canReadUserIds", orderNum = "1")
	List<String> canReadUserIds;

	public List<String> getCanReadUserIds() {
		return canReadUserIds;
	}

	public void setCanReadUserIds(List<String> canReadUserIds) {
		this.canReadUserIds = canReadUserIds;
	}

	public String getIsPrivated() {
		return isPrivated;
	}

	public void setIsPrivated(String isPrivated) {
		this.isPrivated = isPrivated;
	}

	@ApiModelProperty(value = "是否读过")
	private boolean readed = false;

	public boolean isReaded() {
		return readed;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public interface Status {
		int DRAFT = 0;
		int PUBLISH = 1;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		Notice other = (Notice) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
				&& (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
				&& (this.getCreateTime() == null ? other.getCreateTime() == null
						: this.getCreateTime().equals(other.getCreateTime()))
				&& (this.getUpdateTime() == null ? other.getUpdateTime() == null
						: this.getUpdateTime().equals(other.getUpdateTime()))
				&& (this.getMemo() == null ? other.getMemo() == null : this.getMemo().equals(other.getMemo()))
				&& (this.getCreateTime() == null ? other.getCreateTime() == null
						: this.getCreateTime().equals(other.getCreateTime()))
				&& (this.getUpdateTime() == null ? other.getUpdateTime() == null
						: this.getUpdateTime().equals(other.getUpdateTime()))
				&& (this.getCreaterId() == null ? other.getCreaterId() == null
						: this.getCreaterId().equals(other.getCreateTime()))
				&& (this.getUpdaterId() == null ? other.getUpdaterId() == null
						: this.getUpdaterId().equals(other.getUpdateTime()))
				&& (this.getMemo() == null ? other.getMemo() == null : this.getMemo().equals(other.getMemo()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
		result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
		result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
		result = prime * result + ((getMemo() == null) ? 0 : getMemo().hashCode());
		result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
		result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
		result = prime * result + ((getCreaterId() == null) ? 0 : getCreaterId().hashCode());
		result = prime * result + ((getUpdaterId() == null) ? 0 : getUpdaterId().hashCode());
		result = prime * result + ((getMemo() == null) ? 0 : getMemo().hashCode());
		return result;
	}

}
