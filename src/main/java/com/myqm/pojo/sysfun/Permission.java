package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseRecord;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Permission extends BaseRecord implements Serializable {
	private static final long serialVersionUID = 6180869216498363919L;
	@Excel(name = "parentId", orderNum = "0")
	@ApiModelProperty(value = "上级菜单id")
//	@Min(0) 因为id不再是标准的数字，所以在验证时会出现误判
	private String parentId;

	@Excel(name = "name", orderNum = "1")
	@ApiModelProperty(value = "名称")
	@NotNull
	@Size(min = 1, max = 50)
	private String name;

	@Excel(name = "icon", orderNum = "2")
	@ApiModelProperty(value = "icon")
	private String icon;

	@Excel(name = "href", orderNum = "3")
	@ApiModelProperty(value = "链接")
	private String href;

	@Excel(name = "route", orderNum = "4")
	@ApiModelProperty(value = "链接")
	private String route;

	@Excel(name = "mpid", orderNum = "5")
	@ApiModelProperty(value = "mpid")
	private Long mpid;

	@Excel(name = "bpid", orderNum = "6")
	@ApiModelProperty(value = "bpid")
	private Long bpid;

	@Excel(name = "type", orderNum = "7")
	@ApiModelProperty(value = "类型")
	private Integer type;

	@Excel(name = "permission", orderNum = "8")
	@ApiModelProperty(value = "权限")
	private String permission;

	@Excel(name = "sort", orderNum = "9")
	@ApiModelProperty(value = "排序")
	@Min(1)
	private Integer sort;

	private List<Permission> child;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<Permission> getChild() {
		return child;
	}

	public void setChild(List<Permission> child) {
		this.child = child;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Long getMpid() {
		return mpid;
	}

	public void setMpid(Long mpid) {
		this.mpid = mpid;
	}

	public Long getBpid() {
		return bpid;
	}

	public void setBpid(Long bpid) {
		this.bpid = bpid;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		Permission other = (Permission) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getParentId() == null ? other.getParentId() == null
						: this.getParentId().equals(other.getParentId()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getIcon() == null ? other.getIcon() == null : this.getIcon().equals(other.getIcon()))
				&& (this.getRoute() == null ? other.getRoute() == null : this.getRoute().equals(other.getRoute()))
				&& (this.getBpid() == null ? other.getBpid() == null : this.getBpid().equals(other.getBpid()))
				&& (this.getMpid() == null ? other.getMpid() == null : this.getMpid().equals(other.getMpid()))
				&& (this.getHref() == null ? other.getHref() == null : this.getHref().equals(other.getHref()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getSort() == null ? other.getSort() == null : this.getSort().equals(other.getSort()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getParentId() == null) ? 0 : getParentId().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getIcon() == null) ? 0 : getIcon().hashCode());
		result = prime * result + ((getBpid() == null) ? 0 : getBpid().hashCode());
		result = prime * result + ((getMpid() == null) ? 0 : getMpid().hashCode());
		result = prime * result + ((getRoute() == null) ? 0 : getRoute().hashCode());
		result = prime * result + ((getIcon() == null) ? 0 : getIcon().hashCode());
		result = prime * result + ((getHref() == null) ? 0 : getHref().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getPermission() == null) ? 0 : getPermission().hashCode());
		result = prime * result + ((getSort() == null) ? 0 : getSort().hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", parentId=").append(parentId);
		sb.append(", name=").append(name);
		sb.append(", icon=").append(icon);
		sb.append(", route=").append(route);
		sb.append(", mpid=").append(mpid);
		sb.append(", bpid=").append(bpid);
		sb.append(", href=").append(href);
		sb.append(", type=").append(type);
		sb.append(", permission=").append(permission);
		sb.append(", sort=").append(sort);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}

}
