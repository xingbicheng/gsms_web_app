package com.myqm.pojo.sysfun;

import java.io.Serializable;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseInfo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class JobModel extends BaseInfo implements Serializable {

	private static final long serialVersionUID = -2458935535811207209L;

	@ApiModelProperty(value = "任务名")
	@Size(min = 1, max = 16)
	@Excel(name = "jobname", orderNum = "1")
	private String jobName;

	@ApiModelProperty(value = "任务描述")
	private String description;

	@ApiModelProperty(value = "任务时间，可在http://cron.qqe2.com/生成")
	@Excel(name = "cron", orderNum = "1")
	private String cron;

	@ApiModelProperty(value = "Spring Bean 名")
	@Excel(name = "springBeanName", orderNum = "1")
	private String springBeanName;

	@ApiModelProperty(value = "Spring Bean 方法名")
	private String methodName;

	@ApiModelProperty(value = "是否为系统Job")
	@Excel(name = "isSysJob", orderNum = "1")
	private Boolean isSysJob;

	@ApiModelProperty(value = "状态")
	@Excel(name = "status", orderNum = "1")
	private Integer status;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getSpringBeanName() {
		return springBeanName;
	}

	public void setSpringBeanName(String springBeanName) {
		this.springBeanName = springBeanName;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Boolean getIsSysJob() {
		return isSysJob;
	}

	public void setIsSysJob(Boolean isSysJob) {
		this.isSysJob = isSysJob;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "JobModel [jobName=" + jobName + ", description=" + description + ", cron=" + cron + ", springBeanName="
				+ springBeanName + ", methodName=" + methodName + ", isSysJob=" + isSysJob + ", status=" + status + "]";
	}

}
