package com.myqm.pojo.sysfun;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseInfo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Role extends BaseInfo implements Serializable {

	private static final long serialVersionUID = -3802292814767103648L;
	@Excel(name = "name", orderNum = "0")
	@ApiModelProperty(value = "角色名字")
	@Size(min = 1, max = 50)
	private String name;

	@Excel(name = "description", orderNum = "1")
	@ApiModelProperty(value = "描述")
	@Size(min = 1, max = 100)
	private String description;

	@ApiModelProperty(value = "交易可见性，1 可见，0不可见 ")
	@Pattern(regexp = "[01]")
	private String visible;

	@ApiModelProperty(value = "激活状态 1角色使用中，0角色停用 ")
	@Pattern(regexp = "[01]")
	private String activation ;


    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
