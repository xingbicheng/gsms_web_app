package com.myqm.pojo.sysfun;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseInfo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class Dict extends BaseInfo implements Serializable {

	private static final long serialVersionUID = -2431140186410912787L;
	@NotNull
	@ApiModelProperty(value = "字典类型名")
	@Size(min = 1, max = 16)
	@Excel(name = "type", orderNum = "1")
	private String type;

	@NotNull
	@Size(min = 1, max = 16)
	@ApiModelProperty(value = "字典键值")
	@Excel(name = "k", orderNum = "1")
	private String k;

	@NotNull
	@Size(min = 1, max = 64)
	@ApiModelProperty(value = "字典value")
	@Excel(name = "val", orderNum = "1")
	private String val;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = k;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

}
