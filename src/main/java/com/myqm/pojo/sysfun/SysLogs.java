package com.myqm.pojo.sysfun;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseRecord;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class SysLogs extends BaseRecord {

	@Size(min = 1, max = 32)
	@ApiModelProperty(value = "模块")
	@Excel(name = "module", orderNum = "1")
	private String module;

    @ApiModelProperty(value = "模块id")
    @Excel(name = "moduleId", orderNum = "1")
    private String moduleId;

	@NotNull
	@ApiModelProperty(value = "成功失败")
	@Excel(name = "flag", orderNum = "1")
	private Boolean flag;

	@ApiModelProperty(value = "异常备注")
	@Excel(name = "remark", orderNum = "1")
	private String remark;

	@ApiModelProperty(value = "创建时间")
	@Excel(name = "createTime", databaseFormat = "yyMMddHHmmss", format = "yyyy-MM-dd", orderNum = "1")
	private Timestamp createTime;

	@ApiModelProperty(value = "用户id")
	@Excel(name = "userId", orderNum = "1")
	private String userId;

	@ApiModelProperty(value = "用户号")
	@Excel(name = "username", orderNum = "1")
	private String username;

	@ApiModelProperty(value = "用户别名")
	@Excel(name = "nickname", orderNum = "1")
	private String nickname;


//    public SysLogs(String module,String userId) {
//        this.module = module;
//        this.userId = userId;
//    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userid) {
		this.userId = userid;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark!=null) {
			if(remark.length()>400) {
				remark=remark.substring(0, 399);
			}
		}
		this.remark = remark;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		SysLogs other = (SysLogs) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
				&& (this.getFlag() == null ? other.getFlag() == null : this.getFlag().equals(other.getFlag()))
				&& (this.getCreateTime() == null ? other.getCreateTime() == null
						: this.getCreateTime().equals(other.getCreateTime()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
		result = prime * result + ((getFlag() == null) ? 0 : getFlag().hashCode());
		result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", userId=").append(userId);
		sb.append(", module=").append(module);
		sb.append(", flag=").append(flag);
		sb.append(", createTime=").append(createTime);
		sb.append(", remark=").append(remark);
		sb.append("]");
		return sb.toString();
	}
}
