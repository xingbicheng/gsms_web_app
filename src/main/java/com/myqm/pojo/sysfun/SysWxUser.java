package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**SysWxUser*/
@JsonInclude(Include.NON_NULL)
public class SysWxUser implements Serializable {

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "微信名字(字符长度为1-40)")
	@Size(min=1, max=40,message="微信名字字符长度为1-40")
	@Excel(name="微信名字",orderNum="1")
	private String weixinName;

	@ApiModelProperty(value = "名字(字符长度为1-40)")
	@Size(min=1, max=40,message="名字字符长度为1-40")
	@Excel(name="名字",orderNum="2")
	private String name;

	@ApiModelProperty(value = "生成的openid(字符长度为1-40)")
	@Size(min=1, max=40,message="生成的openid字符长度为1-40")
	@Excel(name="生成的openid",orderNum="3")
	private String openId;

	@ApiModelProperty(value = "申请的类型(0审核员，1提供者，2消费者)(字符长度为1-2)")
	@Size(min=1, max=2,message="申请的类型(0审核员，1提供者，2消费者)字符长度为1-2")
	@Excel(name="申请的类型(0审核员，1提供者，2消费者)",orderNum="4")
	private String type;

	@ApiModelProperty(value = "用户状态（0申请中，1审核通过，2审核不通过）(字符长度为1-2)")
	@Size(min=1, max=2,message="用户状态（0申请中，1审核通过，2审核不通过）字符长度为1-2")
	@Excel(name="用户状态（0申请中，1审核通过，2审核不通过）",orderNum="5")
	private String status;

	@ApiModelProperty(value = "创建时间")
	private Timestamp createTime;


	@ApiModelProperty(value = "关联的用户id(字符长度为1-32)")
	@Size(min=1, max=40,message="关联的用户id字符长度为1-32")
	@Excel(name="生成的userId",orderNum="6")
	private String userId;
	
	@ApiModelProperty(value = "电话(字符长度为1-32)")
	@Size(min=1, max=40,message="电话字符长度为1-32")
	@Excel(name="phone",orderNum="7")
	private String phone;
	
	@ApiModelProperty(value = "绑定用户姓名")
	private String userName;
	

	
    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	private static final long serialVersionUID = 1L;

	public Long getId () {   
		return id;
	}
		
	public void setId (Long id) {
		this.id= id ;
	}

    public String getWeixinName () {   
    	 return weixinName;
    }

    public void setWeixinName (String weixinName) {
    	 this.weixinName= weixinName == null ? null : weixinName.trim();
    }

    public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }

    public String getOpenId () {   
    	 return openId;
    }

    public void setOpenId (String openId) {
    	 this.openId= openId == null ? null : openId.trim();
    }

    public String getType () {   
    	 return type;
    }

    public void setType (String type) {
    	 this.type= type == null ? null : type.trim();
    }


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	public void difference(SysWxUser obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.weixinName.equals(obj.getWeixinName())) {
			 defString += "微信名字从" + this.weixinName +"修改为" +obj.getWeixinName();
		}

	   if(!this.name.equals(obj.getName())) {
			 defString += "名字从" + this.name +"修改为" +obj.getName();
		}

	   if(!this.openId.equals(obj.getOpenId())) {
			 defString += "生成的openid从" + this.openId +"修改为" +obj.getOpenId();
		}

	   if(!this.type.equals(obj.getType())) {
			 defString += "申请的类型(0审核员，1提供者，2消费者)从" + this.type +"修改为" +obj.getType();
		}

	   if(!this.status.equals(obj.getStatus())) {
			 defString += "用户状态（0申请中，1审核通过，2审核不通过）从" + this.status +"修改为" +obj.getStatus();
		}


    }
}