package com.myqm.pojo.sysfun;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.quartz.ExecuteInJTATransaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.myqm.pojo.base.BaseRecord;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL)
public class FileInfo extends BaseRecord implements Serializable {

	private static final long serialVersionUID = -5761547882766615438L;

	@ApiModelProperty(value = "文件原文件名")
	@Size(min = 1, max = 50)
	@Excel(name = "filename", orderNum = "1")
	private String fileName;

	@ApiModelProperty(value = "文件MD5")
	@Excel(name = "fileMd5", orderNum = "1")
	private String fileMd5;

	@ApiModelProperty(value = "文件大小")
	@Min(1)
	@Excel(name = "size", orderNum = "1")
	private Long size;

	@ApiModelProperty(value = "文件存储路径")
	@Size(min = 1)
	@Excel(name = "path", orderNum = "1")
	@JsonIgnore
	private String path;

	@ApiModelProperty(value = "文件链接")
	@Size(min = 1, max = 1024)
	@Excel(name = "url", orderNum = "1")
	@JsonIgnore
	private String url;

	@ApiModelProperty(value = "文件类型")
	@NotNull
	@Excel(name = "type", orderNum = "1")
	private String type;

	@ApiModelProperty(value = "创建时间")
	@Excel(name = "createTime", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-mm-dd")
	private Timestamp createTime;

	@ApiModelProperty(value = "创建人Id")
	@Excel(name = "createrId", orderNum = "1")
	private String createrId;

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFileMd5() {
		return fileMd5;
	}

	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		FileInfo other = (FileInfo) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getFileMd5() == null ? other.getFileMd5() == null
						: this.getFileMd5().equals(other.getFileMd5()))
				&& (this.getFileName() == null ? other.getFileName() == null
						: this.getFileName().equals(other.getFileName()))
				&& (this.getSize() == null ? other.getSize() == null : this.getSize().equals(other.getSize()))
				&& (this.getPath() == null ? other.getPath() == null : this.getPath().equals(other.getPath()))
				&& (this.getUrl() == null ? other.getUrl() == null : this.getUrl().equals(other.getUrl()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getCreaterId() == null ? other.getCreaterId() == null
						: this.getCreaterId().equals(other.getCreateTime()))
				&& (this.getCreateTime() == null ? other.getCreateTime() == null
						: this.getCreateTime().equals(other.getCreateTime()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getFileName() == null) ? 0 : getFileName().hashCode());
		result = prime * result + ((getFileMd5() == null) ? 0 : getFileMd5().hashCode());
		result = prime * result + ((getSize() == null) ? 0 : getSize().hashCode());
		result = prime * result + ((getPath() == null) ? 0 : getPath().hashCode());
		result = prime * result + ((getUrl() == null) ? 0 : getUrl().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
		result = prime * result + ((getCreaterId() == null) ? 0 : getCreaterId().hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", fileMd5=").append(fileMd5);
		sb.append(", fileName=").append(fileName);
		sb.append(", size=").append(size);
		sb.append(", path=").append(path);
		sb.append(", url=").append(url);
		sb.append(", type=").append(type);
		sb.append(", createTime=").append(createTime);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}
}
