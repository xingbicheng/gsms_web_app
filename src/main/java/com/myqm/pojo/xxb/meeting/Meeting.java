package com.myqm.pojo.xxb.meeting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**Meeting*/
@JsonInclude(Include.NON_NULL)
public class Meeting implements Serializable {

    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "(字符长度为1-20)")
    @Size(min=1, max=20,message="字符长度为1-20")
    @Excel(name="",orderNum="1")
    private String username;

    @ApiModelProperty(value = "")
    private Long age;

    @ApiModelProperty(value = "(字符长度为1-10)")
    @Size(min=1, max=10,message="字符长度为1-10")
    @Excel(name="",orderNum="3")
    private String gender;


    private static final long serialVersionUID = 1L;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id= id ;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername (String username) {
        this.username= username == null ? null : username.trim();
    }

    public Long getAge () {
        return age;
    }

    public void setAge (Long age) {
        this.age= age ;
    }

    public String getGender () {
        return gender;
    }

    public void setGender (String gender) {
        this.gender= gender == null ? null : gender.trim();
    }


    public void difference(Meeting obj) {
        String defString = "";
        if (!Objects.equals(this.id,obj.getId())){
            defString += "从" + this.id + "修改为" + obj.getId();
        }

        if(!this.username.equals(obj.getUsername())) {
            defString += "从" + this.username +"修改为" +obj.getUsername();
        }

        if (!Objects.equals(this.age,obj.getAge())){
            defString += "从" + this.age + "修改为" + obj.getAge();
        }

        if(!this.gender.equals(obj.getGender())) {
            defString += "从" + this.gender +"修改为" +obj.getGender();
        }


    }
}