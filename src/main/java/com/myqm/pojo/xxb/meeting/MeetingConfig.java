package com.myqm.pojo.xxb.meeting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**MeetingConfig*/
@JsonInclude(Include.NON_NULL)
public class MeetingConfig implements Serializable {

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="0")
	private String fieldName;

	@ApiModelProperty(value = "")
	private Long isAddKey;

	@ApiModelProperty(value = "")
	private Long isAddRequiredKey;

	@ApiModelProperty(value = "")
	private Long isAddDisabledKey;

	@ApiModelProperty(value = "")
	private Long isEditKey;

	@ApiModelProperty(value = "")
	private Long isEditRequiredKey;

	@ApiModelProperty(value = "")
	private Long isEditDisabledKey;

	@ApiModelProperty(value = "")
	private Long isSearchKey;

	@ApiModelProperty(value = "")
	private Long isSearchRequiredKey;

	@ApiModelProperty(value = "")
	private Long isSearchDisabledKey;

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="11")
	private String type;


	private static final long serialVersionUID = 1L;

	public String getFieldName () {
		return fieldName;
	}

	public void setFieldName (String fieldName) {
		this.fieldName= fieldName == null ? null : fieldName.trim();
	}

	public Long getIsAddKey () {
		return isAddKey;
	}

	public void setIsAddKey (Long isAddKey) {
		this.isAddKey= isAddKey ;
	}

	public Long getIsAddRequiredKey () {
		return isAddRequiredKey;
	}

	public void setIsAddRequiredKey (Long isAddRequiredKey) {
		this.isAddRequiredKey= isAddRequiredKey ;
	}

	public Long getIsAddDisabledKey () {
		return isAddDisabledKey;
	}

	public void setIsAddDisabledKey (Long isAddDisabledKey) {
		this.isAddDisabledKey= isAddDisabledKey ;
	}

	public Long getIsEditKey () {
		return isEditKey;
	}

	public void setIsEditKey (Long isEditKey) {
		this.isEditKey= isEditKey ;
	}

	public Long getIsEditRequiredKey () {
		return isEditRequiredKey;
	}

	public void setIsEditRequiredKey (Long isEditRequiredKey) {
		this.isEditRequiredKey= isEditRequiredKey ;
	}

	public Long getIsEditDisabledKey () {
		return isEditDisabledKey;
	}

	public void setIsEditDisabledKey (Long isEditDisabledKey) {
		this.isEditDisabledKey= isEditDisabledKey ;
	}

	public Long getIsSearchKey () {
		return isSearchKey;
	}

	public void setIsSearchKey (Long isSearchKey) {
		this.isSearchKey= isSearchKey ;
	}

	public Long getIsSearchRequiredKey () {
		return isSearchRequiredKey;
	}

	public void setIsSearchRequiredKey (Long isSearchRequiredKey) {
		this.isSearchRequiredKey= isSearchRequiredKey ;
	}

	public Long getIsSearchDisabledKey () {
		return isSearchDisabledKey;
	}

	public void setIsSearchDisabledKey (Long isSearchDisabledKey) {
		this.isSearchDisabledKey= isSearchDisabledKey ;
	}

	public Long getId () {
		return id;
	}

	public void setId (Long id) {
		this.id= id ;
	}

	public String getType () {
		return type;
	}

	public void setType (String type) {
		this.type= type == null ? null : type.trim();
	}


	public void difference(MeetingConfig obj) {
		String defString = "";
		if(!this.fieldName.equals(obj.getFieldName())) {
			defString += "从" + this.fieldName +"修改为" +obj.getFieldName();
		}

		if (!Objects.equals(this.isAddKey,obj.getIsAddKey())){
			defString += "从" + this.isAddKey + "修改为" + obj.getIsAddKey();
		}

		if (!Objects.equals(this.isAddRequiredKey,obj.getIsAddRequiredKey())){
			defString += "从" + this.isAddRequiredKey + "修改为" + obj.getIsAddRequiredKey();
		}

		if (!Objects.equals(this.isAddDisabledKey,obj.getIsAddDisabledKey())){
			defString += "从" + this.isAddDisabledKey + "修改为" + obj.getIsAddDisabledKey();
		}

		if (!Objects.equals(this.isEditKey,obj.getIsEditKey())){
			defString += "从" + this.isEditKey + "修改为" + obj.getIsEditKey();
		}

		if (!Objects.equals(this.isEditRequiredKey,obj.getIsEditRequiredKey())){
			defString += "从" + this.isEditRequiredKey + "修改为" + obj.getIsEditRequiredKey();
		}

		if (!Objects.equals(this.isEditDisabledKey,obj.getIsEditDisabledKey())){
			defString += "从" + this.isEditDisabledKey + "修改为" + obj.getIsEditDisabledKey();
		}

		if (!Objects.equals(this.isSearchKey,obj.getIsSearchKey())){
			defString += "从" + this.isSearchKey + "修改为" + obj.getIsSearchKey();
		}

		if (!Objects.equals(this.isSearchRequiredKey,obj.getIsSearchRequiredKey())){
			defString += "从" + this.isSearchRequiredKey + "修改为" + obj.getIsSearchRequiredKey();
		}

		if (!Objects.equals(this.isSearchDisabledKey,obj.getIsSearchDisabledKey())){
			defString += "从" + this.isSearchDisabledKey + "修改为" + obj.getIsSearchDisabledKey();
		}

		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if(!this.type.equals(obj.getType())) {
			defString += "从" + this.type +"修改为" +obj.getType();
		}


	}
}