package com.myqm.pojo.xxb.meeting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**MeetingUser*/
@JsonInclude(Include.NON_NULL)
public class MeetingUser implements Serializable {

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="1")
	private String name;

	@ApiModelProperty(value = "")
	private Long age;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="3")
	private String gender;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="4")
	private String address;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="5")
	private String memo;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="6")
	private String phone;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="7")
	private String parent;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="8")
	private String school;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="9")
	private String work;

	@ApiModelProperty(value = "")
	private Date brithday;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="11")
	private String test1;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="12")
	private String test2;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="13")
	private String test3;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="14")
	private String test4;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="15")
	private String test5;

	@ApiModelProperty(value = "(字符长度为1-255)")
	@Size(min=1, max=255,message="字符长度为1-255")
	@Excel(name="",orderNum="16")
	private String test6;


	private static final long serialVersionUID = 1L;

	public Long getId () {
		return id;
	}

	public void setId (Long id) {
		this.id= id ;
	}

	public String getName () {
		return name;
	}

	public void setName (String name) {
		this.name= name == null ? null : name.trim();
	}

	public Long getAge () {
		return age;
	}

	public void setAge (Long age) {
		this.age= age ;
	}

	public String getGender () {
		return gender;
	}

	public void setGender (String gender) {
		this.gender= gender == null ? null : gender.trim();
	}

	public String getAddress () {
		return address;
	}

	public void setAddress (String address) {
		this.address= address == null ? null : address.trim();
	}

	public String getMemo () {
		return memo;
	}

	public void setMemo (String memo) {
		this.memo= memo == null ? null : memo.trim();
	}

	public String getPhone () {
		return phone;
	}

	public void setPhone (String phone) {
		this.phone= phone == null ? null : phone.trim();
	}

	public String getParent () {
		return parent;
	}

	public void setParent (String parent) {
		this.parent= parent == null ? null : parent.trim();
	}

	public String getSchool () {
		return school;
	}

	public void setSchool (String school) {
		this.school= school == null ? null : school.trim();
	}

	public String getWork () {
		return work;
	}

	public void setWork (String work) {
		this.work= work == null ? null : work.trim();
	}

	public Date getBrithday () {
		return brithday;
	}

	public void setBrithday (Date brithday) {
		this.brithday= brithday;
	}

	public String getTest1 () {
		return test1;
	}

	public void setTest1 (String test1) {
		this.test1= test1 == null ? null : test1.trim();
	}

	public String getTest2 () {
		return test2;
	}

	public void setTest2 (String test2) {
		this.test2= test2 == null ? null : test2.trim();
	}

	public String getTest3 () {
		return test3;
	}

	public void setTest3 (String test3) {
		this.test3= test3 == null ? null : test3.trim();
	}

	public String getTest4 () {
		return test4;
	}

	public void setTest4 (String test4) {
		this.test4= test4 == null ? null : test4.trim();
	}

	public String getTest5 () {
		return test5;
	}

	public void setTest5 (String test5) {
		this.test5= test5 == null ? null : test5.trim();
	}

	public String getTest6 () {
		return test6;
	}

	public void setTest6 (String test6) {
		this.test6= test6 == null ? null : test6.trim();
	}


	public void difference(MeetingUser obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

		if(!this.name.equals(obj.getName())) {
			defString += "从" + this.name +"修改为" +obj.getName();
		}

		if (!Objects.equals(this.age,obj.getAge())){
			defString += "从" + this.age + "修改为" + obj.getAge();
		}

		if(!this.gender.equals(obj.getGender())) {
			defString += "从" + this.gender +"修改为" +obj.getGender();
		}

		if(!this.address.equals(obj.getAddress())) {
			defString += "从" + this.address +"修改为" +obj.getAddress();
		}

		if(!this.memo.equals(obj.getMemo())) {
			defString += "从" + this.memo +"修改为" +obj.getMemo();
		}

		if(!this.phone.equals(obj.getPhone())) {
			defString += "从" + this.phone +"修改为" +obj.getPhone();
		}

		if(!this.parent.equals(obj.getParent())) {
			defString += "从" + this.parent +"修改为" +obj.getParent();
		}

		if(!this.school.equals(obj.getSchool())) {
			defString += "从" + this.school +"修改为" +obj.getSchool();
		}

		if(!this.work.equals(obj.getWork())) {
			defString += "从" + this.work +"修改为" +obj.getWork();
		}

//		if((!brithday.equals("createTime") || !brithday.equals("updateTime")) && this.brithday.compareTo(obj.getBrithday() != 0 )){
//			defString += ""从" + this.brithday + "修改为" + obj.getBrithday();
//		}
		if(!this.test1.equals(obj.getTest1())) {
			defString += "从" + this.test1 +"修改为" +obj.getTest1();
		}

		if(!this.test2.equals(obj.getTest2())) {
			defString += "从" + this.test2 +"修改为" +obj.getTest2();
		}

		if(!this.test3.equals(obj.getTest3())) {
			defString += "从" + this.test3 +"修改为" +obj.getTest3();
		}

		if(!this.test4.equals(obj.getTest4())) {
			defString += "从" + this.test4 +"修改为" +obj.getTest4();
		}

		if(!this.test5.equals(obj.getTest5())) {
			defString += "从" + this.test5 +"修改为" +obj.getTest5();
		}

		if(!this.test6.equals(obj.getTest6())) {
			defString += "从" + this.test6 +"修改为" +obj.getTest6();
		}


	}
}