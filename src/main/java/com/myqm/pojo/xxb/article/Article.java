package com.myqm.pojo.xxb.article;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**Article*/
@JsonInclude(Include.NON_NULL)
public class Article implements Serializable {

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="0")
	private String content;

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "(字符长度为1-30)")
	@Size(min=1, max=30,message="字符长度为1-30")
	@Excel(name="",orderNum="2")
	private String memo;

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="3")
	private String name;

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="4")
	private String title;


    private static final long serialVersionUID = 1L;

    public String getContent () {   
    	 return content;
    }

    public void setContent (String content) {
    	 this.content= content == null ? null : content.trim();
    }

	public Long getId () {   
		return id;
	}
		
	public void setId (Long id) {
		this.id= id ;
	}

    public String getMemo () {   
    	 return memo;
    }

    public void setMemo (String memo) {
    	 this.memo= memo == null ? null : memo.trim();
    }

    public String getName () {   
    	 return name;
    }

    public void setName (String name) {
    	 this.name= name == null ? null : name.trim();
    }

    public String getTitle () {   
    	 return title;
    }

    public void setTitle (String title) {
    	 this.title= title == null ? null : title.trim();
    }


	public void difference(Article obj) {
		String defString = "";
	   if(!this.content.equals(obj.getContent())) {
			 defString += "从" + this.content +"修改为" +obj.getContent();
		}

		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.memo.equals(obj.getMemo())) {
			 defString += "从" + this.memo +"修改为" +obj.getMemo();
		}

	   if(!this.name.equals(obj.getName())) {
			 defString += "从" + this.name +"修改为" +obj.getName();
		}

	   if(!this.title.equals(obj.getTitle())) {
			 defString += "从" + this.title +"修改为" +obj.getTitle();
		}


    }
}