package com.myqm.pojo.xxb.tableConfig;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


/**TableConfig*/
@JsonInclude(Include.NON_NULL)
public class TableConfig implements Serializable {

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "(字符长度为1-20)")
	@Size(min=1, max=20,message="字符长度为1-20")
	@Excel(name="",orderNum="1")
	private String tableName;

	@ApiModelProperty(value = "(字符长度为1-100)")
	@Size(min=1, max=100,message="字符长度为1-100")
	@Excel(name="",orderNum="2")
	private String isSearch;


    private static final long serialVersionUID = 1L;

	public Long getId () {   
		return id;
	}
		
	public void setId (Long id) {
		this.id= id ;
	}

    public String getTableName () {   
    	 return tableName;
    }

    public void setTableName (String tableName) {
    	 this.tableName= tableName == null ? null : tableName.trim();
    }

    public String getIsSearch () {   
    	 return isSearch;
    }

    public void setIsSearch (String isSearch) {
    	 this.isSearch= isSearch == null ? null : isSearch.trim();
    }


	public void difference(TableConfig obj) {
		String defString = "";
		if (!Objects.equals(this.id,obj.getId())){
			defString += "从" + this.id + "修改为" + obj.getId();
		}

	   if(!this.tableName.equals(obj.getTableName())) {
			 defString += "从" + this.tableName +"修改为" +obj.getTableName();
		}

	   if(!this.isSearch.equals(obj.getIsSearch())) {
			 defString += "从" + this.isSearch +"修改为" +obj.getIsSearch();
		}
    }
}