package com.myqm.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class PageTableData<T extends Object> {

	@ApiModelProperty(value = "当前页")
	int pageNumber;
	@ApiModelProperty(value = "总页数")
	int totalPage;
	@ApiModelProperty(value = "每页条数")
	int pageSize;
	@ApiModelProperty(value = "总查询条数")
	long total;

	@JsonProperty("rows")
	@ApiModelProperty(value = "查询数据")
	List<T> rows;

	public PageTableData(List<T> datas) {
		rows = datas;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int currentPage) {
		this.pageNumber = currentPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int showRows) {
		this.pageSize = showRows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

}
