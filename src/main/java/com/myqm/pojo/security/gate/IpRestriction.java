package com.myqm.pojo.security.gate;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class IpRestriction implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String ip; // ip地址
	private Timestamp lastAccessTime; // 日期类型
	private boolean isBlack; // 是否为黑名单
	private boolean isLocked; // 是否被锁定
	private int lockCount = 0; // 频繁访问计数
	private Timestamp lastCounterTime;// 频繁访问第一次计数
	private boolean isCounter;// 频繁访问开始计数

	// 构造函数
	/**
	 * 
	 * @param ip
	 *            ip地址
	 * @param iptime
	 *            访问时间
	 * @param isblack
	 *            是否为黑名单
	 */
	public IpRestriction(String ip, Timestamp iptime, boolean isblack) {
		this.lockCount = 0;
		this.ip = ip;
		this.lastAccessTime = iptime;
		this.isBlack = isblack;
		this.isLocked = false;
		this.isCounter = false;
	}

	public boolean isCounter() {
		return isCounter;
	}

	public void setCounter(boolean isCounter) {
		this.isCounter = isCounter;
	}

	public Date getLastCounterTime() {
		return lastCounterTime;
	}

	public void setLastCounterTime(Timestamp lastCounterTime) {
		this.lastCounterTime = lastCounterTime;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public Date getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(Timestamp lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isBlack() {
		return isBlack;
	}

	public void setBlack(boolean isBlack) {
		this.isBlack = isBlack;
	}

	public int getLockCount() {
		return lockCount;
	}

	public void setLockCount(int lockCount) {
		this.lockCount = lockCount;
	}

	public void addLockCount() {
		this.lockCount++;
	}

	public void resetLockCount() {
		this.lockCount = 0;
	}
}
