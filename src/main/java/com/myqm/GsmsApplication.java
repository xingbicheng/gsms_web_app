package com.myqm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.myqm.config.data.MsgPropertiesListener;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * 启动类
 */
@EnableSwagger2
@SpringBootApplication
// 扫描接口类，这个配置只能扫描该包下的接口，不能扫描mapper文件
@MapperScan(basePackages = "com.myqm.dao")
@EnableTransactionManagement
@EnableScheduling
public class GsmsApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GsmsApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(GsmsApplication.class);
		application.addListeners(new MsgPropertiesListener("sys-msg.properties"));
		application.run(args);

		// SpringApplication.run(IftApplication.class, args);

	}

}
