package com.myqm.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Method;

/**
 * excel工具类
 */
public class ExcelUtil {

	public static void excelLocal(String path, String fileName, String[] headers, List<Object[]> datas) {
		Workbook workbook = getWorkbook(headers, datas);
		if (workbook != null) {
			ByteArrayOutputStream byteArrayOutputStream = null;
			FileOutputStream fileOutputStream = null;
			try {
				byteArrayOutputStream = new ByteArrayOutputStream();
				workbook.write(byteArrayOutputStream);

				String suffix = ".xls";
				File file = new File(path + File.separator + fileName + suffix);
				if (!file.getParentFile().exists()) {
					file.getParentFile().mkdirs();
				}

				fileOutputStream = new FileOutputStream(file);
				fileOutputStream.write(byteArrayOutputStream.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (fileOutputStream != null) {
						fileOutputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					if (byteArrayOutputStream != null) {
						byteArrayOutputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void excelListExport(String fileName, String[] headers, String[] fieldNames, List<Object> listdata,
			HttpServletResponse response) throws Exception {

		List<Object[]> datas = convertListOb(listdata, fieldNames);

		Workbook workbook = getWorkbook(headers, datas);
		if (workbook != null) {
			ByteArrayOutputStream byteArrayOutputStream = null;
			try {
				// byteArrayOutputStream = new ByteArrayOutputStream();
				// workbook.write(byteArrayOutputStream);

				// File file = new File("d:/" + File.separator + fileName +
				// ".xls");
				// if (!file.getParentFile().exists()) {
				// file.getParentFile().mkdirs();
				// }
				// FileOutputStream fileOutputStream = null;
				// fileOutputStream = new FileOutputStream(file);
				// fileOutputStream.write(byteArrayOutputStream.toByteArray());
				// if (fileOutputStream != null) {
				// fileOutputStream.close();
				// }
				String test = "fdsafda";
				String suffix = ".xls";
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + new String((fileName + suffix).getBytes(), "iso-8859-1"));
				// response.addHeader("Content-Length", "" +
				// byteArrayOutputStream.toByteArray().length);

				OutputStream outputStream = response.getOutputStream();
				workbook.write(outputStream);
				// outputStream.write(byteArrayOutputStream.toByteArray());
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (byteArrayOutputStream != null) {
						byteArrayOutputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 导出excel
	 * 
	 * @param fileName
	 * @param headers
	 * @param datas
	 * @param response
	 */
	public static void excelExport(String fileName, String[] headers, List<Object[]> datas,
			HttpServletResponse response) {
		Workbook workbook = getWorkbook(headers, datas);
		if (workbook != null) {
			ByteArrayOutputStream byteArrayOutputStream = null;
			try {
				byteArrayOutputStream = new ByteArrayOutputStream();
				workbook.write(byteArrayOutputStream);

				String suffix = ".xls";
				response.setContentType("application/vnd.ms-excel;charset=utf-8");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + new String((fileName + suffix).getBytes(), "iso-8859-1"));

				OutputStream outputStream = response.getOutputStream();
				outputStream.write(byteArrayOutputStream.toByteArray());
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (byteArrayOutputStream != null) {
						byteArrayOutputStream.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 * @param headers
	 *            列头
	 * @param datas
	 *            数据
	 * @return
	 */
	public static Workbook getWorkbook(String[] headers, List<Object[]> datas) {
		Workbook workbook = new HSSFWorkbook();

		Sheet sheet = workbook.createSheet();
		Row row = null;
		Cell cell = null;
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER_SELECTION);

		Font font = workbook.createFont();

		int line = 0, maxColumn = 0;
		if (headers != null && headers.length > 0) {// 设置列头
			row = sheet.createRow(line++);
			row.setHeightInPoints(23);
			font.setBold(true);
			font.setFontHeightInPoints((short) 13);
			style.setFont(font);

			maxColumn = headers.length;
			for (int i = 0; i < maxColumn; i++) {
				cell = row.createCell(i);
				cell.setCellValue(headers[i]);
				cell.setCellStyle(style);
			}
		}

		if (datas != null && datas.size() > 0) {// 渲染数据
			for (int index = 0, size = datas.size(); index < size; index++) {
				Object[] data = datas.get(index);
				if (data != null && data.length > 0) {
					row = sheet.createRow(line++);
					row.setHeightInPoints(20);

					int length = data.length;
					if (length > maxColumn) {
						maxColumn = length;
					}

					for (int i = 0; i < length; i++) {
						cell = row.createCell(i);
						if (data[i] != null)
							System.out.println(data[i].toString());
						cell.setCellValue(data[i] == null ? null : data[i].toString());
					}
				}
			}
		}

		for (int i = 0; i < maxColumn; i++) {
			sheet.autoSizeColumn(i);
		}

		return workbook;
	}

	/**
	 * 处理字符串 如： abc_dex ---> abcDex
	 * 
	 * @param str
	 * @return
	 */
	public static String removeLine(String str) {
		if (null != str && str.contains("_")) {
			int i = str.indexOf("_");
			char ch = str.charAt(i + 1);
			char newCh = (ch + "").substring(0, 1).toUpperCase().toCharArray()[0];
			String newStr = str.replace(str.charAt(i + 1), newCh);
			String newStr2 = newStr.replace("_", "");
			return newStr2;
		}
		return str;
	}

	/**
	 * 根据属性，获取get方法
	 * 
	 * @param ob
	 *            对象
	 * @param name
	 *            属性名
	 * @return
	 * @throws Exception
	 */
	public static Object getGetMethod(Object ob, String name) throws Exception {
		Method[] m = ob.getClass().getMethods();
		for (int i = 0; i < m.length; i++) {
			if (("get" + name).toLowerCase().equals(m[i].getName().toLowerCase())) {
				return m[i].invoke(ob);
			}
		}
		return null;
	}

	/**
	 * 根据属性，获取get方法
	 * 
	 * @param ob
	 *            对象
	 * @param name
	 *            属性名
	 * @return
	 * @throws Exception
	 */
	public static Object getGetDesMethod(Object ob, String name) throws Exception {
		Method[] m = ob.getClass().getMethods();
		for (int i = 0; i < m.length; i++) {
			if ((name + "Des").toLowerCase().equals(m[i].getName().toLowerCase())) {
				return m[i].invoke(ob);
			}
		}
		return null;
	}

	/**
	 * @param data
	 *            原数据
	 * @param fieldName
	 *            字段列表
	 * @param header
	 *            字段头
	 * @param value
	 *            字段名
	 * @throws Exception
	 */
	static List<Object[]> convertListOb(List<Object> data, String[] fieldName) throws Exception {
		String[] methodNames = new String[fieldName.length];
		for (int i = 0; i < fieldName.length; i++) {
			methodNames[i] = removeLine(fieldName[i]);
		}
		List<Object[]> valus = new ArrayList<Object[]>();
		for (int i = 0; i < data.size(); i++) {
			Object[] oneValue = new Object[fieldName.length];
			for (int j = 0; j < fieldName.length; j++) {
				oneValue[j] = getGetDesMethod(data.get(i), methodNames[j]);
				if (oneValue[j] == null)
					oneValue[j] = getGetMethod(data.get(i), methodNames[j]);
			}
			valus.add(oneValue);
		}
		return valus;
	}
	
    public static Workbook batchImport(MultipartFile file) throws IOException   {
    	 
        String fileName = file.getOriginalFilename();
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return null;
        }
        boolean isExcel2003 = true;
        if (fileName.matches("^.+\\.(?i)(xlsx)$")) {
            isExcel2003 = false;
        }
        InputStream is = file.getInputStream();
        Workbook wb = null;
        if (isExcel2003) {
           return  wb = new HSSFWorkbook(is);
        } else {
        	return wb = new XSSFWorkbook(is);
        }
    }
    
    public static String getcellvalue(Cell cell){
        String cellValue = "";
        if (null != cell) {// 判断excel单元格内容的格式，并对其进行转换，以便插入数据库
            switch (cell.getCellType()) {  
                case 0:
                    cellValue = String.valueOf((int) cell.getNumericCellValue());
                    break;
                case 1:
                    cellValue = cell.getStringCellValue();
                    break;
                case 2:
                    cellValue = cell.getNumericCellValue() + "";
                    // cellValue = String.valueOf(cell.getDateCellValue());
                    break;
                case 3:
                    cellValue = "";
                    break;
                case 4:
                    cellValue = String.valueOf(cell.getBooleanCellValue());
                    break;
                case 5:
                    cellValue = String.valueOf(cell.getErrorCellValue());
                    break;
            }
        } 
        return  cellValue.trim();
    }
}
