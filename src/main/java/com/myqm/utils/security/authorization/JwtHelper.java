package com.myqm.utils.security.authorization;

import java.security.Key;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.myqm.pojo.security.authorization.LoginUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/* 
 * 构造及解析jwt的工具类 
 */
public class JwtHelper {

	public static Claims parseJWT(String jsonWebToken, String base64Security) {
		try {
			Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
					.parseClaimsJws(jsonWebToken).getBody();
			return claims;
		} catch (Exception ex) {
			return null;
		}
	}

	public static LoginUser validate(String jsonWebToken, String base64Security) {
		try {
			Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(base64Security))
					.parseClaimsJws(jsonWebToken).getBody();

			LoginUser jwtUser = new LoginUser();
			// 设置数据
			// jwtUser.setUsername("aaa");

			jwtUser.setToken(jsonWebToken);

			jwtUser.setUsername(claims.getSubject());
			// jwtUser.setId(Long.parseLong((String) claims.get("userId")));
			// jwtUser.setRole((String) claims.get("roleId"));
			return jwtUser;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * @param userName
	 *            用户名
	 * @param userId
	 *            角色ID
	 * @param roleId
	 *            角色ID
	 * @param audience
	 *            接收者
	 * @param issuer
	 *            发行者
	 * @param TTLMillis
	 *            过期时间(毫秒)
	 * @param base64Security
	 *            密钥
	 * @return
	 */
	public static String createJWT(String userName, String audience, String issuer, long TTLMillis,
			String base64Security) {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// 生成签名密钥
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(base64Security);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// 添加构成JWT的参数
		JwtBuilder builder = Jwts.builder().setHeaderParam("type", "JWT").claim("unique_name", userName)
				.setIssuer(issuer) // jwt签发者
				.setAudience(audience) // 接收jwt的一方
				.signWith(signatureAlgorithm, signingKey);
		// 添加Token过期时间
		if (TTLMillis >= 0) {
			long expMillis = nowMillis + TTLMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp).setNotBefore(now);
		}

		// 生成JWT
		return builder.compact();
	}
}