package com.myqm.utils.security.sign;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.myqm.advice.RequstCheckException;
import com.myqm.utils.MD5Utils;

public class SignUtil {

	public static String sign(String parameter, String api_secret, String timestr) throws ServletException {
		try {
			return MD5Utils.getMD5(parameter + timestr).toUpperCase();
		} catch (Exception e) {
			throw new ServletException("Requset Data signature error!");
		}
	}

	public static String sign(TreeMap<String, Object> parameter, String api_secret, String timestr)
			throws ServletException {
		try {
			String stringToSign = StringToSign(parameter, api_secret) + timestr;
			return MD5Utils.getMD5(stringToSign).toUpperCase();
		} catch (Exception e) {
			throw new ServletException("Requset Data signature error!");
		}
	}

	@SuppressWarnings("rawtypes")
	public static String StringToSign(TreeMap<String, Object> parameter, String api_secret)
			throws RequstCheckException {
		String stringToSign = api_secret;
		Iterator it = parameter.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next().toString();
			if (key.equals("sign")) {
				continue;
			}
			stringToSign += key;

			Object value = parameter.get(key);
			if (value == null) {
				continue;
			}
			stringToSign += value.toString();
		}
		stringToSign += api_secret;
		return stringToSign;
	}

	/**
	 * 得到http请求的参数，返回排序后的map
	 * 
	 * @param request
	 * @param ignoreSignParameter
	 *            是否忽略签名字段
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static TreeMap<String, Object> getFormParameter(HttpServletRequest request) {
		Map parameterMap = request.getParameterMap();
		// 返回值Map
		TreeMap<String, Object> returnMap = new TreeMap<String, Object>();
		Iterator entries = parameterMap.entrySet().iterator();

		Map.Entry entry;
		String name = "";
		String value = "";
		while (entries.hasNext()) {
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			if (name.equals("sign")) {
				continue;
			}
			Object valueObj = entry.getValue();
			if (null == valueObj) {
				value = "";
			} else if (valueObj instanceof String[]) {
				String[] values = (String[]) valueObj;
				for (int i = 0; i < values.length; i++) {
					value = values[i] + ",";
				}
				value = value.substring(0, value.length() - 1);
			} else {
				value = valueObj.toString().trim();
			}
			returnMap.put(name, value);
		}
		return returnMap;
	}

	/**
	 * 判断是否是multipart/form-data请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isMultipartContent(HttpServletRequest request) {
		if (!"post".equals(request.getMethod().toLowerCase())) {
			return false;
		}
		String contentType = request.getContentType(); // 获取Content-Type
		if ((contentType != null) && (contentType.toLowerCase().startsWith("multipart/"))) {
			return true;
		} else {
			return false;
		}
	}

	public static TreeMap<String, Object> getMultipartParameter(HttpServletRequest request) throws ServletException {
		// 检查form中是否有enctype="multipart/form-data"
		if (isMultipartContent(request) && request.getParameterMap().isEmpty()) {
			throw new ServletException("File upload API request URL lack of necessary query parameters");
		}
		return getFormParameter(request);
	}
}
