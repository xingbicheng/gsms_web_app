package com.myqm.utils;

import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException; 
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map; 

/**
 * 通过短信接口发送短信  
 */
public class SmsUtil {

	public static void main(String[] args) {  
		sendSms("13778139866", "祝您的生活非常非常愉快888！", ""); 
	}  



	/**
	 * 给多个人发送单条短信
	 * 
	 * @param list
	 *            手机号验证码
	 */
	public static void sendSmsAll(List<HashMap> list) {
		String code;
		String mobile;
		for (int i = 0; i < list.size(); i++) {
			code = list.get(i).get("code").toString();
			mobile = list.get(i).get("mobile").toString();
			sendSms3(mobile, code, "");
		}
	}
	
	public static boolean sendSms(String mobile, String code, String times) {
        int re;
        if (times==null)
            re = sendSms3(mobile, code,  ""); 
        else
          re = sendSms3(mobile, code,  times);
        if (re > 0)
            return true;
        else
            return false;
    }
	

 
	// 短信商 ====================成都凌凯=====================================================

	/**
	 * Hppt POST请求发送方法 返回值>0 为 提交成功
	 * 
	 * @param Mobile
	 *            电话号码
	 * @param Content
	 *            发送内容
	 * @param send_time
	 *            定时发送时间，为空时，为及时发送
	 * @return
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException
	 */
	public static int sendSms3(String Mobile, String Content, String send_time) {

		Content += "【绵阳龙门批发市场】";
		String inputLine = "";
		int value = -2;

		try {
			String send_content = URLEncoder.encode(Content.replaceAll("<br/>", " "), "GBK");// 发送内容

			String strUrl = "http://mb345.com:999/WS/BatchSend2.aspx";
			String param = "CorpID=NFCP005081&Pwd=GSSC123@&Mobile=" + Mobile + "&Content="
					+ send_content + "&Cell=&SendTime=" + send_time;

			inputLine = UrlSendUtil.sendPostNoJson(strUrl, param);
			// System.out.println("开始发送短信手机号码为 ：" + Mobile); 
			value = new Integer(inputLine).intValue();

		} catch (Exception e) {

			// System.out.println("网络异常,发送短信失败！");
			value = -2;
		}

		// System.out.println(String.format("返回值：%d", value)); 
		return value;
	}
 
}
