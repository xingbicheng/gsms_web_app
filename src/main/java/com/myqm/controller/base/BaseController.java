package com.myqm.controller.base;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.utils.UserUtil;

public class BaseController {
	protected static final Logger log = LoggerFactory.getLogger("adminLogger");

	public SysUser getCurrentUser() {
		return UserUtil.getLoginUser();
	}

	public String getCurrentUserId() {
		return UserUtil.getLoginUser().getId();
	}

	public ResultResponse<String> dbOperateResult(String result) {
		ResultResponse<String> response = null;
		if (result == null)
			response = new ResultResponse<String>(-1, "数据更新或删除0条记录");
		else if (result.equals(""))
			response = new ResultResponse<String>(-1, "数据更新或删除0条记录");
		else
			response = new ResultResponse<String>(0, "OK", "数据更新或删除成功");
		return response;
	}

	public ResultResponse<String> dbOperateResult(String op, String result) {
		ResultResponse<String> response = null;
		if (result == null)
			response = new ResultResponse<String>(-1, op + "0条记录");
		else if (result.equals(""))
			response = new ResultResponse<String>(-1, op + "0条记录");
		else
			response = new ResultResponse<String>(0, "OK", op + "成功");
		return response;
	}

	public ResultResponse<String> newResult(String result) {
		ResultResponse<String> response = null;

		response = new ResultResponse<String>(0, result);

		return response;
	}

	public ResultResponse<String> dbDelOperateResult(List<String> result) {
		ResultResponse<String> response = null;
		if (result == null)
			response = new ResultResponse<String>(-1, "数据更新或删除0条记录");
		else if (result.size() == 0)
			response = new ResultResponse<String>(-1, "数据更新或删除0条记录");
		else
			response = new ResultResponse<String>(0, "OK", "数据更新或删除成功" + result.size() + "记录");
		return response;
	}
}
