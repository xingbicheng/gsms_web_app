package com.myqm.controller.xxb.meeting;


import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.xxb.meeting.MeetingUser;
import com.myqm.vo.xxb.meeting.MeetingUserQuery;
import com.myqm.service.xxb.meeting.MeetingUserService;


@Api(tags = "MeetingUser")
@RestController
@Validated
@RequestMapping("/api/meetingUser")
public class MeetingUserController extends BaseController {

    @Autowired
    public MeetingUserService meetingUserService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
            @ApiParam(required = true, value = "添加MeetingUser")
            @Valid @RequestBody MeetingUser record) {
        return super.dbOperateResult(meetingUserService.save(record));
    }

    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
            @ApiParam(required=true,value="修改MeetingUser")
            @Valid @RequestBody MeetingUser record) {
        return super.dbOperateResult(meetingUserService.editById(record));

    }

    @ApiOperation(value="删除记录")
    @GetMapping(value="/del")
    public ResultResponse<String> delById(
            @ApiParam(required=true,value="查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        return super.dbOperateResult(meetingUserService.delById(id));
    }

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<MeetingUser> getById(
            @ApiParam(required = true, value = "查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        MeetingUser record=meetingUserService.getById(id);
        return new ResultResponse<MeetingUser>(record);
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<MeetingUser>> list(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingUserQuery param) {
        return new ResultResponse<PageTableData<MeetingUser>>(
                meetingUserService.list(param.getCurrentPage(),
                        param.getPageSize(), param));
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<MeetingUser>> all(
            @ApiParam(required = false, value = "查询参数")
            @Valid  MeetingUserQuery param) {
        return new ResultResponse<List<MeetingUser>>(
                meetingUserService.listAll(param));
    }

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingUserQuery param, HttpServletResponse response)
            throws Exception {
        PageTableData <MeetingUser> listData= meetingUserService.list(
                param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "MeetingUser查询数据",
                "MeetingUser查询数据"), MeetingUser.class, listData.getRows());

        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=MeetingUser查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingUserQuery param, HttpServletResponse response)
            throws Exception {
        List<MeetingUser> listData= meetingUserService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
                new ExportParams( "MeetingUser查询数据", "MeetingUser查询数据"), MeetingUser.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=MeetingUser查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
