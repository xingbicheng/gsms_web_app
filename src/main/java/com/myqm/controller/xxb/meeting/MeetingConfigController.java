package com.myqm.controller.xxb.meeting;


import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.xxb.meeting.MeetingConfig;
import com.myqm.vo.xxb.meeting.MeetingConfigQuery;
import com.myqm.service.xxb.meeting.MeetingConfigService;


@Api(tags = "MeetingConfig")
@RestController
@Validated
@RequestMapping("/api/meetingConfig")
public class MeetingConfigController extends BaseController {

    @Autowired
    public MeetingConfigService meetingConfigService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
            @ApiParam(required = true, value = "添加MeetingConfig")
            @Valid @RequestBody MeetingConfig record) {
        return super.dbOperateResult(meetingConfigService.save(record));
    }

    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
            @ApiParam(required=true,value="修改MeetingConfig")
            @Valid @RequestBody MeetingConfig record) {
        return super.dbOperateResult(meetingConfigService.editById(record));

    }

    @ApiOperation(value="删除记录")
    @GetMapping(value="/del")
    public ResultResponse<String> delById(
            @ApiParam(required=true,value="查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        return super.dbOperateResult(meetingConfigService.delById(id));
    }

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<MeetingConfig> getById(
            @ApiParam(required = true, value = "查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        MeetingConfig record=meetingConfigService.getById(id);
        return new ResultResponse<MeetingConfig>(record);
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<MeetingConfig>> list(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingConfigQuery param) {
        return new ResultResponse<PageTableData<MeetingConfig>>(
                meetingConfigService.list(param.getCurrentPage(),
                        param.getPageSize(), param));
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<MeetingConfig>> all(
            @ApiParam(required = false, value = "查询参数")
            @Valid  MeetingConfigQuery param) {
        return new ResultResponse<List<MeetingConfig>>(
                meetingConfigService.listAll(param));
    }

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingConfigQuery param, HttpServletResponse response)
            throws Exception {
        PageTableData <MeetingConfig> listData= meetingConfigService.list(
                param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "MeetingConfig查询数据",
                "MeetingConfig查询数据"), MeetingConfig.class, listData.getRows());

        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=MeetingConfig查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
            @ApiParam(required = false, value = "查询参数")
            @Valid MeetingConfigQuery param, HttpServletResponse response)
            throws Exception {
        List<MeetingConfig> listData= meetingConfigService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
                new ExportParams( "MeetingConfig查询数据", "MeetingConfig查询数据"), MeetingConfig.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=MeetingConfig查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
