package com.myqm.controller.xxb.tableConfig;


import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.xxb.tableConfig.TableConfig;
import com.myqm.vo.xxb.tableConfig.TableConfigQuery;
import com.myqm.service.xxb.tableConfig.TableConfigService;


@Api(tags = "TableConfig")
@RestController
@Validated
@RequestMapping("/api/tableConfig")
public class TableConfigController extends BaseController {

    @Autowired
    public TableConfigService tableConfigService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
            @ApiParam(required = true, value = "添加TableConfig")
            @Valid @RequestBody TableConfig record) {
        return super.dbOperateResult(tableConfigService.save(record));
    }

    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
            @ApiParam(required=true,value="修改TableConfig")
            @Valid @RequestBody TableConfig record) {
        return super.dbOperateResult(tableConfigService.editById(record));

    }

    @ApiOperation(value="删除记录")
    @GetMapping(value="/del")
    public ResultResponse<String> delById(
            @ApiParam(required=true,value="查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        return super.dbOperateResult(tableConfigService.delById(id));
    }

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<TableConfig> getById(
            @ApiParam(required = true, value = "查询编号")
            @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        TableConfig record=tableConfigService.getById(id);
        return new ResultResponse<TableConfig>(record);
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<TableConfig>> list(
            @ApiParam(required = false, value = "查询参数")
            @Valid TableConfigQuery param) {
        return new ResultResponse<PageTableData<TableConfig>>(
                tableConfigService.list(param.getCurrentPage(),
                        param.getPageSize(), param));
    }

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<TableConfig>> all(
            @ApiParam(required = false, value = "查询参数")
            @Valid  TableConfigQuery param) {
        return new ResultResponse<List<TableConfig>>(
                tableConfigService.listAll(param));
    }

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
            @ApiParam(required = false, value = "查询参数")
            @Valid TableConfigQuery param, HttpServletResponse response)
            throws Exception {
        PageTableData <TableConfig> listData= tableConfigService.list(
                param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "TableConfig查询数据",
                "TableConfig查询数据"), TableConfig.class, listData.getRows());

        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=TableConfig查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
            @ApiParam(required = false, value = "查询参数")
            @Valid TableConfigQuery param, HttpServletResponse response)
            throws Exception {
        List<TableConfig> listData= tableConfigService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
                new ExportParams( "TableConfig查询数据", "TableConfig查询数据"), TableConfig.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=TableConfig查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
