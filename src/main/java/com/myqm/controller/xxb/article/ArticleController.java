package com.myqm.controller.xxb.article;


import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord; 
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.xxb.article.Article;
import com.myqm.vo.xxb.article.ArticleQuery;
import com.myqm.service.xxb.article.ArticleService;


@Api(tags = "Article")
@RestController
@Validated
@RequestMapping("/api/article")
public class ArticleController extends BaseController {

	@Autowired
	public ArticleService articleService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
    	@ApiParam(required = true, value = "添加Article")
    	@Valid @RequestBody Article record) {
        return super.dbOperateResult(articleService.save(record));
    }
    
    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
    	@ApiParam(required=true,value="修改Article")
		@Valid @RequestBody Article record) { 
        return super.dbOperateResult(articleService.editById(record));
 
    } 
    
	@ApiOperation(value="删除记录")
	@GetMapping(value="/del")
	public ResultResponse<String> delById(
		@ApiParam(required=true,value="查询编号")
        @RequestParam("id") @NotNull(message = "id不能为空") String id) { 
        return super.dbOperateResult(articleService.delById(id));
    } 

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<Article> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        Article record=articleService.getById(id);
        return new ResultResponse<Article>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<Article>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid ArticleQuery param) {
        return new ResultResponse<PageTableData<Article>>(
            articleService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<Article>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  ArticleQuery param) {
        return new ResultResponse<List<Article>>(
            articleService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid ArticleQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <Article> listData= articleService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "Article查询数据", 
        	"Article查询数据"), Article.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=Article查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid ArticleQuery param, HttpServletResponse response) 
    	throws Exception {
        List<Article> listData= articleService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "Article查询数据", "Article查询数据"), Article.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=Article查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
