package com.myqm.controller.dev;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.pojo.dev.GenerateInput;
import com.myqm.service.dev.GenerateService;
import com.myqm.utils.Freemarker;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 代码生成接口
 */
@Api(tags = "代码生成")
@RestController
@RequestMapping("/generate")
public class GenerateController {

	@Autowired
	private GenerateService generateService;

	@ApiOperation("生成代码")
	@PostMapping(value = "/createCode")
	public GenerateInput save(@RequestBody GenerateInput input) throws Exception {
		String tableName = generateService.upperFirstChar(input.getTableName());
		String antdName = generateService.upperFirstChar(input.getAntdName());
		if (input.getBeanName() == null)
			input.setBeanName(generateService.upperFirstChar(tableName));

		if (input.getVoName() == null)
			input.setVoName(generateService.upperFirstChar(tableName) + "Query");

		if (input.getDaoName() == null)
			input.setDaoName(generateService.upperFirstChar(tableName) + "Dao");

		if (input.getMybatisMapperName() == null)
			input.setMybatisMapperName(generateService.upperFirstChar(tableName) + "Mapper");

		if (input.getServiceName() == null)
			input.setServiceName(generateService.upperFirstChar(tableName) + "Service");

		if (input.getControllerName() == null)
			input.setControllerName(generateService.upperFirstChar(tableName) + "Controller");

		if (input.getHtmlName() == null)
			input.setHtmlName(generateService.upperFirstChar(tableName) + "Html");

		if (input.getControllertestName() == null)
			input.setHtmlName(generateService.upperFirstChar(tableName) + "TestController");

		if (input.getAntdPageName() == null)
			input.setAntdPageName(generateService.upperFirstChar(antdName) + "List");

		if (input.getAntdDetailName() == null)
			input.setAntdDetailName(generateService.upperFirstChar(antdName) + "Detail");

		if (input.getAntdModalName() == null)
			input.setAntdModalName(generateService.upperFirstChar(antdName) + "Modal");

		if (input.getAntdServiceName() == null)
			input.setAntdServiceName(generateService.upperFirstChar(antdName) + "Service");

		input.setFields(generateService.listBeanField(input.getTableName()));  // 设置一个表的所有参数

		String beanPackageName = input.getBeanPackageName();
		String voPackageName = input.getVoPackageName();
		String voName = input.getVoName();
		String beanName = input.getBeanName();
		String htmlPackageName = input.getHtmlPackageName();
		String daoPackageName = input.getDaoPackageName();
		String daotestPackageName = input.getDaotestPackageName();
		String mybatisMapperPackageName = input.getMybatisMapperPackageName();
		String servicePackageName = input.getServicePackageName();
		String controllerPackageName = input.getControllerPkgName();
		String classUrl = input.getClassUrl();
		String controllertestPackageName = input.getControllertestName();

		/*
		* html 部分
		* */

		String htmlPagePackageName = input.getHtmlPagePkgName();
		String htmlPageName = input.getHtmlPageName();

		/**
		* antdPro部分
		* */

		String antdPageName = input.getAntdPageName();
		String antdModalName = input.getAntdModalName();
		String antdServiceName = input.getAntdServiceName();
		String antdDetailName = input.getAntdDetailName();

		Map<String, Object> root = new HashMap<String, Object>();
		root.put("fieldList", input.getFields());
		root.put("beanPackageName", beanPackageName);
		root.put("beanName", beanName);
		root.put("nowDate", new Date());
		root.put("voPackageName", voPackageName);
		root.put("voName", voName);
		root.put("daoPackageName", daoPackageName);
		root.put("daotestPackageName", daotestPackageName);
		root.put("servicePackageName", servicePackageName);
		root.put("controllerPackageName", controllerPackageName);
		root.put("tableName", input.getTableName());
		root.put("classUrl", classUrl);
		root.put("controllertestPackageName", controllertestPackageName);

		/*
		* html 部分
		* */

		root.put("htmlPagePackageName", htmlPagePackageName);
		root.put("htmlPageName", htmlPageName);

		/*
		* antdPro部分
		* */

		root.put("antdName", antdName);
		root.put("antdPageName", antdPageName);
		root.put("antdDetailName", antdDetailName);
		root.put("antdModalName", antdModalName);
		root.put("antdServiceName", antdServiceName);

		String backFilePath = "admin/code/back/";
		String frontFilePath = "admin/code/front/";
		// 存放路径
		String ftlPath = "createCode/";
		/* 生成pojo */
		Freemarker.printFile("pojoTemplate.ftl", root, beanPackageName + "/" + beanName + ".java", backFilePath, ftlPath);

		/* 生成vo */
		Freemarker.printFile("voTemplate.ftl", root, voPackageName + "/" + beanName + "Query.java", backFilePath, ftlPath);

		/* 生成dao */
		Freemarker.printFile("daoTemplate.ftl", root, daoPackageName + "/" + beanName + "Dao.java", backFilePath, ftlPath);
		/* 生成dao */
		Freemarker.printFile("daotestTemplate.ftl", root, daotestPackageName + "/" + beanName + "DaoTest.java",
				backFilePath, ftlPath);

		/* 生成service */
		Freemarker.printFile("serviceTemplate.ftl", root, servicePackageName + "/" + beanName + "Service.java",
				backFilePath, ftlPath);

		/* 生成serviceImp */
		Freemarker.printFile("serviceImplTemplate.ftl", root,
				servicePackageName + "/Impl/" + beanName + "ServiceImpl.java", backFilePath, ftlPath);

		/* 生成controller */
		Freemarker.printFile("controllerTemplate.ftl", root, controllerPackageName + "/" + beanName + "Controller.java",
				backFilePath, ftlPath);

		/* 生成mapper */
		Freemarker.printFile("mybatisMapperTemplate.ftl", root,
				mybatisMapperPackageName + "/" + beanName + "Mapper.xml", backFilePath, ftlPath);
		/*
		* 生成 html
		* */

        /* 生成htmlPage */
        Freemarker.printFile("htmlPageTemplate.ftl", root, htmlPagePackageName + "/" + htmlPageName + ".html",
				frontFilePath, ftlPath);

		/* 生成htmlAddPage */
		Freemarker.printFile("htmlAddPageTemplate.ftl", root, htmlPagePackageName + "/" + htmlPageName + "-add.html",
				frontFilePath, ftlPath);

		/*
		* 生成 antd
		* */

		/* 生成antdPage */
		Freemarker.printFile("antdPageTemplate.ftl", root, antdName + "/" + antdPageName + ".js",
				frontFilePath, ftlPath);

		/* 生成antdDetail */
		Freemarker.printFile("antdDetailTemplate.ftl", root, antdName + "/" + antdDetailName + ".js",
				frontFilePath, ftlPath);

		/* 生成antdModal */
		Freemarker.printFile("antdModalTemplate.ftl", root, antdName + "/models/" + antdModalName + ".js",
				frontFilePath, ftlPath);

		/* 生成antdPageLess */
		Freemarker.printFile("antdPageLessTemplate.ftl", root, antdName + "/" + antdPageName + ".less",
				frontFilePath, ftlPath);

		/* 生成antdLess */
		Freemarker.printFile("antdLessTemplate.ftl", root, antdName + "/style.less",
				frontFilePath, ftlPath);

        /* 生成antdService */
        Freemarker.printFile("antdServiceTemplate.ftl", root, antdServiceName + "/" + antdServiceName + ".js",
                frontFilePath, ftlPath);


		// /*生成addHtmlT*/
		// Freemarker.printFile("addHtmlTemplate.ftl", root,
		// htmlPackageName+"/"+classUrl+"Add.html", filePath, ftlPath);

		// /*生成listHtml*/
		// Freemarker.printFile("listHtmlTemplate.ftl", root,
		// htmlPackageName+"/"+classUrl+"List.html", filePath, ftlPath);

		// /*生成updateHtml*/
		// Freemarker.printFile("updateHtmlTemplate.ftl", root,
		// htmlPackageName+"/"+classUrl+"Update.html", filePath, ftlPath);
		/* 生成testController */
		Freemarker.printFile("controllerTestTemplate.ftl", root,
				controllertestPackageName + "/" + beanName + "ControllerTest.java", backFilePath, ftlPath);

		// 生成antdPro文件

		return input;
	}

}
