package com.myqm.controller.gs.in;

import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseInSum;
import com.myqm.pojo.gs.in.StroehouseInTotal;
import com.myqm.service.gs.in.StroehouseInSumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api(tags = "入库单统计")
@RestController
@Validated
@RequestMapping("/api/stroehouseIn/sum")
public class StroehouseInSumController {
    @Autowired
    StroehouseInSumService stroehouseInSumService;
    @ApiOperation(value = "根据ID查询记录")
    @PutMapping(value = "/sum")
    public ResultResponse<List<StroehouseInSum>> getById(
            @ApiParam(required = true, value = "添加采购单信息") @Valid @RequestBody StroehouseInTotal stroehouseInTotal) {
        List<StroehouseInSum> stroehouseInSum=stroehouseInSumService.sum(stroehouseInTotal);
        return new ResultResponse<List<StroehouseInSum>>(stroehouseInSum);
    }
}
