package com.myqm.controller.gs.in;


import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord; 
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.gs.in.StoreLossType;
import com.myqm.vo.gs.in.StoreLossTypeQuery;
import com.myqm.service.gs.in.StoreLossTypeService;


@Api(tags = "库房损耗类型")
@RestController
@Validated
@RequestMapping("/api/storeLossType")
public class StoreLossTypeController extends BaseController {

	@Autowired
	public StoreLossTypeService storeLossTypeService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
    	@ApiParam(required = true, value = "添加StoreLossType")
    	@Valid @RequestBody StoreLossType record) {
        return super.dbOperateResult(storeLossTypeService.save(record));
 
    }
    
    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
    	@ApiParam(required=true,value="修改StoreLossType")
		@Valid @RequestBody StoreLossType record) { 
        return super.dbOperateResult(storeLossTypeService.editById(record));
 
    } 
    
	@ApiOperation(value="删除记录")
	@GetMapping(value="/del")
	public ResultResponse<String> delById(
		@ApiParam(required=true,value="查询编号")
        @RequestParam("id") @NotNull(message = "id不能为空") String id) { 
		String message = storeLossTypeService.delById(id);
        return new ResultResponse<String>(-1, message);
    } 

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<StoreLossType> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        StoreLossType record=storeLossTypeService.getById(id);
        return new ResultResponse<StoreLossType>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<StoreLossType>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid StoreLossTypeQuery param) {
        return new ResultResponse<PageTableData<StoreLossType>>(
            storeLossTypeService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<StoreLossType>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  StoreLossTypeQuery param) {
        return new ResultResponse<List<StoreLossType>>(
            storeLossTypeService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid StoreLossTypeQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <StoreLossType> listData= storeLossTypeService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "StoreLossType查询数据", 
        	"StoreLossType查询数据"), StoreLossType.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=StoreLossType查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid StoreLossTypeQuery param, HttpServletResponse response) 
    	throws Exception {
        List<StoreLossType> listData= storeLossTypeService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "StoreLossType查询数据", "StoreLossType查询数据"), StoreLossType.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=StoreLossType查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
