package com.myqm.controller.gs.in;

import java.sql.Date;
import java.util.List;
import javax.validation.Valid;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController; 
import com.myqm.vo.gs.in.StroehouseInQuery; 
import com.myqm.service.gs.in.StroehouseInService;

@Api(tags = "入库单打印")
@RestController
@Validated
@RequestMapping("/api/printIn")
public class PrintInOrderController extends BaseController {

	@Autowired
	public StroehouseInService stroehouseInService;
	 @ApiOperation(value="查询打印入库信息")
	    @GetMapping(value="/list")
	    public ResultResponse<List<StroehouseIn>> list(
	    	@ApiParam(required = true, value = "供应商id")
	    	 @RequestParam("providerId") String   providerId,
	    	@ApiParam(required = true, value = "出库时间")
	    	 @RequestParam("inDate") Date  inDate) { 
	        StroehouseInQuery record = new StroehouseInQuery();
	        record.setProviderId(providerId);
	        Date endDate = new Date(inDate.getTime()+ 86400000);  
 	        record.setInTimeBegin(inDate); 
 	        record.setInTimeEnd(endDate);  
	        record.setState(StroehouseInState.in);
			return  new ResultResponse<List<StroehouseIn>> (stroehouseInService.listAll(record));
	    }

    @ApiOperation(value="打印入库单")
    @GetMapping(value="/print")
    public ResultResponse<List<StroehouseIn>> print(
    	@ApiParam(required = true, value = "打印的编号")
    	 @RequestParam("ids")   List<String> ids) { 
    	stroehouseInService.addPrint(ids);     
        return new ResultResponse<List<StroehouseIn>> (stroehouseInService.listByIds(ids));
    }
    
 
     
}
