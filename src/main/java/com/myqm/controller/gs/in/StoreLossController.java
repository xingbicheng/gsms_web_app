package com.myqm.controller.gs.in;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;

import com.myqm.dto.gs.in.SetLoss;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.in.StoreLoss;
import com.myqm.vo.gs.in.StoreLossQuery;
import com.myqm.service.gs.in.StoreLossService;

@Api(tags = "库房损耗")
@RestController
@Validated
@RequestMapping("/api/storeLoss")
public class StoreLossController extends BaseController {

	@Autowired
	public StoreLossService storeLossService;

    @ApiOperation(value="新增记录")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
    	@ApiParam(required = true, value = "添加StoreLoss")
    	@Valid @RequestBody StoreLoss record) {
        return super.dbOperateResult(storeLossService.save(record)); 
    }
    
    @ApiOperation(value="一键批量损耗,商品正常损耗")
    @GetMapping(value="/setloss")
    public ResultResponse<String> setloss(
    	@ApiParam(required = true, value = "通过传入的inIds添加StoreLoss")
        @RequestParam("inIds") @NotNull List<String> inIds) {
        return new ResultResponse<String>(0,storeLossService.setLoss(inIds));
 
    }
    @ApiOperation(value="自定义商品损耗")
    @PostMapping(value="/setloses")
    public ResultResponse<String> setloses(
            @ApiParam(required = true, value = "通过传入的inIds添加StoreLoss")
            @Valid @RequestBody List<SetLoss> records) {
        String res=storeLossService.setLoses(records);
        int code=0;
        if("自定义损耗失败".equals(res))
            code=1;
        return new ResultResponse<String>(code,code==0?"ok":res,code==0?res:null);

    }

    @ApiOperation(value="商品损耗取消")
    @GetMapping(value="/cancelloss")
    public ResultResponse<String> cancelLoss(
            @ApiParam(required = true, value = "通过传入的商品损耗id取消损耗")
            @RequestParam("ids") @NotNull List<String> ids) {
        String res=storeLossService.cancelLoss(ids);
        int code=0;
        if("取消损耗错误".equals(res))
            code=1;
        return new ResultResponse<>(code,code==0?"ok":res,res);

    }
    
    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
    	@ApiParam(required=true,value="修改StoreLoss")
		@Valid @RequestBody StoreLoss record) { 
        return super.dbOperateResult(storeLossService.editById(record));
 
    } 
    
	@ApiOperation(value="删除记录")
	@GetMapping(value="/del")
	public ResultResponse<String> delById(
		@ApiParam(required=true,value="查询编号")
        @RequestParam("id") @NotNull(message = "id不能为空") String id) { 
        return super.dbOperateResult(storeLossService.delById(id));
    } 

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<StoreLoss> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        StoreLoss record=storeLossService.getById(id);
        return new ResultResponse<StoreLoss>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<StoreLoss>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid StoreLossQuery param) {
        return new ResultResponse<PageTableData<StoreLoss>>(
            storeLossService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<StoreLoss>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  StoreLossQuery param) {
        return new ResultResponse<List<StoreLoss>>(
            storeLossService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid StoreLossQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <StoreLoss> listData= storeLossService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "StoreLoss查询数据", 
        	"StoreLoss查询数据"), StoreLoss.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=StoreLoss查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid StoreLossQuery param, HttpServletResponse response) 
    	throws Exception {
        List<StoreLoss> listData= storeLossService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "StoreLoss查询数据", "StoreLoss查询数据"), StoreLoss.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=StoreLoss查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
