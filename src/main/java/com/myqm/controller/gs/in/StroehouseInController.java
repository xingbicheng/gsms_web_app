package com.myqm.controller.gs.in;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.service.gs.in.StroehouseInService;
import com.myqm.vo.gs.in.StroehouseInQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Api(tags = "入库单")
@RestController
@Validated
@RequestMapping("/api/stroehouseIn")
public class StroehouseInController extends BaseController {

	@Autowired
	public StroehouseInService stroehouseInService;
	//修改采购单明细状态，采购单状态；
	//修改订单明细状态，修改订单状态
	@ApiOperation(value = "一键入库")
	@GetMapping(value = "/in")
	public ResultResponse<String> inStorehouse(
			@ApiParam(required = true, value = "入库单编号") @RequestParam("ids") @NotNull(message = "id不能为空") List<String> Ids) {
		String record = stroehouseInService.inStorehouse(Ids);
		return super.newResult(record);
	}
	//修改采购单明细状态，采购单状态；
	//修改订单明细状态，修改订单状态
    @ApiOperation(value = "取消入库")
    @GetMapping(value = "/rein")
    public ResultResponse<String> reInStorehouse(
            @ApiParam(required = true, value = "入库单编号") @RequestParam("ids") @NotNull(message = "id不能为空") String id) {
        String res = stroehouseInService.reInStorehouse(id);
        int code=0;
        if("取消入库错误".equals(res))
            code=1;
        return new ResultResponse<>(code,code==0?"ok":res,code==0?res:null);
    }
 
    
//    @ApiOperation(value = "一键清空库存")
//    @GetMapping(value = "/cleanstore")
//    public ResultResponse<String> cleanStorehouseIn(
//            @ApiParam(required = true, value = "入库单编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//        String res = stroehouseInService.cleanStorehouseIn(id);
//        return new ResultResponse<String>(0,"ok",res);
//    }

    
    
	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<StroehouseIn> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		StroehouseIn record = stroehouseInService.getById(id);
		return new ResultResponse<StroehouseIn>(record);
	}

	
	//wx
	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<StroehouseIn>> list(
			@ApiParam(required = false, value = "查询参数") @Valid StroehouseInQuery param) {
		return new ResultResponse<PageTableData<StroehouseIn>>(
				stroehouseInService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<StroehouseIn>> all(
			@ApiParam(required = false, value = "查询参数") @Valid StroehouseInQuery param) {
		return new ResultResponse<List<StroehouseIn>>(stroehouseInService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid StroehouseInQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<StroehouseIn> listData = stroehouseInService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("StroehouseIn查询数据", "StroehouseIn查询数据"),
				StroehouseIn.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=StroehouseIn查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid StroehouseInQuery param,
			HttpServletResponse response) throws Exception {
		List<StroehouseIn> listData = stroehouseInService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("StroehouseIn查询数据", "StroehouseIn查询数据"),
				StroehouseIn.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=StroehouseIn查询数据.xls");
		workbook.write(response.getOutputStream());
	}
	
	
    @ApiOperation(value = "中央库可用量和剩余量批量查询")
	@GetMapping(value = "/uses")
	public ResultResponse<List<StroehouseIn>> users(
			 @ApiParam(required = true, value = "商品编号")
			 @RequestParam("goodsId") 
			 @NotNull(message = "商品id不能为空") String id){
	        List<StroehouseIn> records = stroehouseInService.uses(id);
		return new ResultResponse<List<StroehouseIn>>(records);
	}
	
}
