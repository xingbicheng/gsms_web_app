package com.myqm.controller.gs.outer;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.order.Order;
import com.myqm.service.gs.order.OrderService; 
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.vo.gs.order.OrderQuery.SysState;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "订单出库")
@RestController
@Validated
@RequestMapping("/api/outer")
public class OuterOrderController extends BaseController {
	@Autowired
	public OrderService orderService;

	//修改入库单的剩余量，可用量
	//修改入库单状态，当剩余量 = amount 入库状态， 当剩余量 = 0 出库完成  当剩余量< amount 出库中
	@ApiOperation(value = "一键出库")
	@GetMapping(value = "/outall")
	public ResultResponse<String> receiveOrders(
			@ApiParam(required = true, value = "出库编号") @RequestParam("orderIds") @NotNull(message = "id不能为空") List<String> orderIds) {
		String re = orderService.setOuter(orderIds);
        return new ResultResponse<String>("存在状态不为入库状态的订单细节".equals(re)?1:0,re);
	}

	//修改入库单的剩余量，可用量
    @ApiOperation(value = "取消出库")
    @GetMapping(value = "/reout")
    public ResultResponse<String> cancelOut(
            @ApiParam(required = true, value = "出库编号") @RequestParam("orderIds") @NotNull(message = "id不能为空") List<String> orderIds) {
        String record = orderService.reOutOuter(orderIds);
        return new ResultResponse<String>(0,record);
    }
    //缺少 部分明细出库的接口
    
    
    
//	@ApiOperation(value = "新增记录")
//	@PostMapping(value = "/add")
//	public ResultResponse<String> save(@ApiParam(required = true, value = "添加Order") @Valid @RequestBody Order record) {
//		orderService.save(record);
//		return super.dbOperateResult(orderService.save(record));
//	}
//
//	@ApiOperation(value = "修改定订单内容，")
//	@PostMapping(value = "/edit")
//	public ResultResponse<String> editById(
//			@ApiParam(required = true, value = "修改订单，明细修改需提供editFlag标志") @Valid @RequestBody Order record) {
//		return super.newResult(orderService.editById(record));
//
//	}

	@ApiOperation(value = "下一个order信息")
	@GetMapping(value = "next")
	public ResultResponse<Order> getNext(
			@Valid OrderQuery param) {
		param.setSysState(SysState.out);
		Order record = orderService.getNext(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有下一条");
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "上一个order信息")
	@GetMapping(value = "pre")
	public ResultResponse<Order> getPre(
			@Valid OrderQuery param) {
		param.setSysState(SysState.out);
		Order record = orderService.getPre(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有上一条");
		return new ResultResponse<Order>(record);
	}

//	@ApiOperation(value = "审核记录")
//	@GetMapping(value = "/audit")
//	public ResultResponse<String> auditById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id,
//			@RequestParam("audited") @NotNull(message = "id不能为空") boolean audited,
//			@RequestParam("auditMemo") @NotNull(message = "id不能为空") String auditMemo) {
//		return super.newResult(orderService.audit(id, audited, auditMemo));
//	}
//
//	@ApiOperation(value = "批量审核记录")
//	@GetMapping(value = "/auditnum")
//	public ResultResponse<String> auditByIdNum(
//			@ApiParam(required = false, value = "删除参数") @Valid OrderAuditQuery param) {
//		return super.newResult(orderService.auditNum(param));
//	}
//
//	@ApiOperation(value = "弃审记录")
//	@GetMapping(value = "/unaudit")
//	public ResultResponse<String> unauditById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		return super.newResult(orderService.unaudit(id));
//	}
//
//	@ApiOperation(value = "删除记录使用flag")
//	@GetMapping(value = "/delflag")
//	public ResultResponse<String> delFlagById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		return super.newResult(orderService.delFlagById(id));
//	}
//
//	@ApiOperation(value = "删除数据库记录")
//	@GetMapping(value = "/del")
//	public ResultResponse<String> delById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		return super.newResult(orderService.delById(id));
//	}



	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Order> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = orderService.getById(id);
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "详细列表分页查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Order>> list(
			@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		param.setSysState(SysState.out);
		return new ResultResponse<PageTableData<Order>>(
				orderService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Order>> all(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		param.setSysState(SysState.out);
		return new ResultResponse<List<Order>>(orderService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param, HttpServletResponse response)
			throws Exception {
		param.setSysState(SysState.out);
		PageTableData<Order> listData = orderService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param,
			HttpServletResponse response) throws Exception {
		param.setSysState(SysState.out);
		List<Order> listData = orderService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
