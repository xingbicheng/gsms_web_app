package com.myqm.controller.gs.outer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order;
import com.myqm.service.gs.order.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "订单打印")
@RestController
@Validated
@RequestMapping("/api/outer") 
public class PrintOrderController extends BaseController {
	
	@Autowired
	public OrderService orderService;
	
	   @ApiOperation(value="打印收货单")
	    @GetMapping(value="/print")
	    public ResultResponse<List<Order>> print(
	    	@ApiParam(required = true, value = "打印的订单编号")
	    	 @RequestParam("ids")   List<String> ids) { 
		   orderService.addPrint(ids);     
	        return new ResultResponse<List<Order>> (orderService.printInfoByIds(ids));
	    }
}
