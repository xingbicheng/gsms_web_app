package com.myqm.controller.gs.wx.provider;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.service.gs.in.StroehouseInService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.in.StroehouseInQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "微信入库单")
@RestController
@Validated
@RequestMapping("/api/wxstroehouseIn")
public class WxStroehouseInController extends BaseController{	
	
	@Autowired
	public StroehouseInService stroehouseInService;
	
    @ApiOperation(value = "微信详细列表查询")
	@GetMapping(value = "/wxlist")
	public ResultResponse<PageTableData<StroehouseIn>> wxList(
			@ApiParam(required = false, value = "查询参数") @Valid StroehouseInQuery param) {
    	param.setProviderId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<PageTableData<StroehouseIn>>(
				stroehouseInService.list(param.getCurrentPage(), param.getPageSize(), param));
	}
}