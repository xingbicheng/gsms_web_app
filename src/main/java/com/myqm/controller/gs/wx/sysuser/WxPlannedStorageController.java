package com.myqm.controller.gs.wx.sysuser;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.myqm.dto.gs.in.SetTransferInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.dto.gs.in.ResolveStoreIn;
import com.myqm.dto.gs.in.SetProviderInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.service.gs.purchase.PlannedStorageService;
import com.myqm.service.gs.purchase.PurchaseOrderDetailService;
import com.myqm.service.gs.purchase.PurchaseOrderService;
import com.myqm.vo.gs.purchase.PurchaseOrderDetailQuery;
import com.myqm.vo.gs.purchase.PurchaseOrderQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "微信计划入库单")
@RestController
@Validated
@RequestMapping("/api/wxplannedStorage")
public class WxPlannedStorageController extends BaseController{

	@Autowired
	public PurchaseOrderService purchaseOrderService;

	@Autowired
	public PlannedStorageService inService;

	@Autowired
	public PurchaseOrderDetailService purchaseDetailService;
	 // 审核

 	// 审核明细
    //分订单明细 的入库情况，设置订单明细为采购；，更新订单状态 
 	@ApiOperation(value = "采购单明细审核")
 	@GetMapping(value = "/wxaudit")
 	public ResultResponse<String> audited(
 			@ApiParam(required = true, value = "修改的采购单明细信息") @RequestParam("ids")
 			@NotNull(message = "采购单明细ids不能空") List<String> ids,
 			@RequestParam("audited") @NotNull(message = "是否通过") boolean audited, @RequestParam("memo") String memo) {
 		return super.newResult(purchaseOrderService.auditDetailById(ids, audited, memo));
 	}

 	//弃审明细
 	//删除分订单明细 的入库情况 ，设置订单明细为预采购，更新订单状态 
 	@ApiOperation(value = "采购单明细弃审")
 	@GetMapping(value = "/wxunaudit")
 	public ResultResponse<String> unaudit(
 			@ApiParam(required = true, value = "修改的采购单明细信息") @RequestParam("id") @NotNull(message = "采购单明细") String id) {
 		return super.newResult(purchaseOrderService.unauditDetailById(id));
 	}
}
