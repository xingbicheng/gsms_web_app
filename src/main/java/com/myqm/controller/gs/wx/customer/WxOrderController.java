package com.myqm.controller.gs.wx.customer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.service.gs.basic.CustomerService;
import com.myqm.service.gs.order.OrderService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.order.OrderQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "订单")
@RestController
@Validated
@RequestMapping("/api/wxorder")
public class WxOrderController {
	
	@Autowired
	public OrderService orderService; 
	
	@Autowired
	public CustomerService customerService;
	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(@ApiParam(required = true, value = "添加Order") @Valid @RequestBody Order record) {
		LoginUser user = UserUtil.getLoginUser();
		record.setCustomerId(user.getUserId());
		Customer byId = customerService.getById(user.getUserId());
		record.setCustomerName(byId.getCustomerName());
		return new ResultResponse<String>(0,"ok",orderService.save(record));
	}
	
	@ApiOperation(value = "修改定订单内容，")
	@PostMapping(value = "/wxedit")
	public ResultResponse<String> wxEditById(
			@ApiParam(required = true, value = "修改订单，明细修改需提供editFlag标志") @Valid @RequestBody Order record) {
		record.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<String>(0,orderService.editById(record),record.getId());
	}
	
	@ApiOperation(value = "详细列表分页查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Order>> wxList(
			@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		param.setCustomerId(UserUtil.getLoginUser().getUserId());
		if(param.getState()==null)
			param.setState("1");
		return new ResultResponse<PageTableData<Order>>(
				orderService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

}
