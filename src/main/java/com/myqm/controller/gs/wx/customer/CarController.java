package com.myqm.controller.gs.wx.customer;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.basic.GoodsInfo;
import com.myqm.pojo.gs.order.Car;
import com.myqm.service.gs.order.CarService;
import com.myqm.service.gs.order.OrderService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.order.QueryCar;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "购物车")
@RestController
@RequestMapping(value = "/api/car")
public class CarController extends BaseController{

	@Autowired
	private CarService carService;
	
	@Autowired
	private OrderService orderService;
	
	@ApiOperation(value="清空所有购物车数据")
	@DeleteMapping(value="/delAll")
	public ResultResponse<Integer> delById() {
		
		return new ResultResponse<Integer>(carService.delById(UserUtil.getLoginUser().getUserId()));
	}
	
	@ApiOperation(value="删除购物车记录")
	@DeleteMapping(value="/delgoods")
	public ResultResponse<Integer> del(@ApiParam(required=true,value="货物信息") 
				@RequestBody GoodsInfo goodsInfo){
		goodsInfo.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<Integer>(carService.del(goodsInfo));
	}
	
	@ApiOperation(value="新增记录")
	@PostMapping(value="/add")
	@ApiParam(required = true, value = "记录值")
	public ResultResponse<Integer> save(@Valid  @RequestBody Car record) {
		record.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<Integer>(carService.add(record));
	}
	
	@ApiParam(required = true , value = "记录内容")
	@PostMapping(value = "/edit") 
	public ResultResponse<Integer> editById(@Valid @RequestBody Car record) {
		record.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<Integer>(carService.editById(record));
	}
	//
	@ApiOperation(value = "拷贝订单到购物车") 
	@PostMapping(value = "/addToCar")
	public ResultResponse<String> copyToCar(
			@ApiParam(required = true, value = "订单id")
			@RequestBody @Valid String id) {
		return new ResultResponse<String>(0,carService.copyToCar(id));
	}
	
	@ApiOperation(value = "用户id列表查询(分页)") 
	@PostMapping("/list")
	public ResultResponse<PageInfo<Car>> listByCustomerId(@ApiParam(required = false, value = "用户id")
			@Valid @RequestBody QueryCar param){
		param.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<PageInfo<Car>>(
				carService.listByCustomerId(param.getCurrentPage(), param.getPageSize(),param));
	}
	
	@ApiOperation(value = "用户id列表查询(不分页)") 
	@PostMapping("/listAll")
	public ResultResponse<List<Car>> listAll(@ApiParam(required = false, value = "用户id")
			@Valid @RequestBody QueryCar param){
		param.setCustomerId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<List<Car>>(
				carService.listAll(param));
	}
	
	
}
