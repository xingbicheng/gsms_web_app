package com.myqm.controller.gs.wx.provider;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;
import com.myqm.service.gs.purchase.PurchasePriceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Api(tags = "微信供应商询价")
@RestController
@Validated
@RequestMapping("/api/wxpurchaseprice")
public class WxPriceController extends BaseController{
	
	@Autowired
	public PurchasePriceService purchasePriceService;
	
	@ApiOperation(value = "修改内容")
	@GetMapping(value = "/wxedit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "报价ID ")
			 @RequestParam("id")  String id, 
			 @ApiParam(required = true, value = "报价")
			 @RequestParam("price") BigDecimal price) {
		return super.dbOperateResult(purchasePriceService.priceById(id,price));

	}
	
	
	@ApiOperation(value = "微信详细列表查询")
	@GetMapping(value = "/wxlist")
	public ResultResponse<PageTableData<PurchasePrice>> wxList(
			@ApiParam(required = false, value = "查询参数") @Valid PurchasePriceQuery param) {
		param.setProviderId(UserUtil.getLoginUser().getUserId());
		return new ResultResponse<PageTableData<PurchasePrice>>(
				purchasePriceService.list(param.getCurrentPage(), param.getPageSize(), param));
	} 

}
