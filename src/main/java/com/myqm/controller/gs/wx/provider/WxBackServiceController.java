package com.myqm.controller.gs.wx.provider;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.pojo.gs.receive.BackProvider;
import com.myqm.service.gs.receive.BackService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;
import com.myqm.vo.gs.receive.BackProviderQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Api(tags = "退货")
@RestController
@Validated
@RequestMapping("/api/wxreceive")
public class WxBackServiceController {

	@Autowired 
	private BackService backService;
	
	@Autowired 
	private ProviderDao providerDao;
	@ApiOperation(value = "微信详细列表查询")
	@GetMapping(value = "/wxlist")
	public ResultResponse<PageTableData<BackProvider>> wxList(
			@ApiParam(required = false, value = "查询参数") @Valid BackProviderQuery param) {
		param.setProviderName(providerDao.getById(UserUtil.getLoginUser().getUserId()).getProviderName());
		return new ResultResponse<PageTableData<BackProvider>>(
				backService.list(param.getCurrentPage(), param.getPageSize(), param));
	}
	
}
