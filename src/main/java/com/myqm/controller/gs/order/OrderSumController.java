package com.myqm.controller.gs.order;

import com.myqm.pojo.ResultResponse;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderSum;
import com.myqm.pojo.gs.order.OrderTotal;
import com.myqm.service.gs.order.OrderSumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api(tags = "订单统计")
@RestController
@Validated
@RequestMapping("/api/order/sum")
public class OrderSumController {
    @Autowired
    OrderSumService orderSumService;
    @ApiOperation(value = "订单统计")
    @PostMapping(value = "/sum")
    public ResultResponse<List<OrderSum>> Sum(@ApiParam(required = true, value = "订单统计")
                                            @Valid @RequestBody OrderTotal orderTotal){
        List<OrderSum> orderSum=orderSumService.sum(orderTotal);
        return new ResultResponse<List<OrderSum>>(orderSum);
    }

}
