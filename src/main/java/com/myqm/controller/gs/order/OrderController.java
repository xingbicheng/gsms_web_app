package com.myqm.controller.gs.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull; 
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.myqm.pojo.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.pojo.PageTableData; 
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.vo.gs.order.Ids;
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.vo.gs.order.QueryAudit;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.sysfun.SysLogsService;

@Api(tags = "订单")
@RestController
@Validated
@RequestMapping("/api/order")
public class OrderController extends BaseController {

	@Autowired
	public OrderService orderService; 
	
	@Autowired // 自动连接
	public OrderDetailDao orderDetailDao; 
	@Autowired
	SysLogsService sysLogsService;
	@ApiOperation(value = "批量导入web订单")
	@PostMapping(value = "/addweb")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "web订单编号")
			 @RequestBody Ids ids) { 
		return super.newResult("导入"+orderService.saveWeb(ids)+"订单！");
	}
	
	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(@ApiParam(required = true, value = "添加Order") @Valid @RequestBody Order record) {
		 
		return new ResultResponse<String>(0,"ok",orderService.save(record));
	}

	@ApiOperation(value = "修改定订单内容，")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改订单，明细修改需提供editFlag标志") @Valid @RequestBody Order record) {
		return new ResultResponse<String>(0,orderService.editById(record),record.getId());
	}

	@ApiOperation(value = "下一个order信息")
	@GetMapping(value = "next")
	public ResultResponse<Order> getNext(
			@Valid OrderQuery param) {
		Order record = orderService.getNext(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有下一条");
		return new ResultResponse<Order>(record);
		
	}

	@ApiOperation(value = "上一个order信息")
	@GetMapping(value = "pre")
	public ResultResponse<Order> getPre(
			@Valid OrderQuery param) {
		Order record = orderService.getPre(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有上一条");
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "审核记录")
	@GetMapping(value = "/audit")
	public ResultResponse<String> auditById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id,
			@RequestParam("audited") @NotNull(message = "是否通过不能为空") boolean audited,
			@RequestParam("auditMemo") @NotNull(message = "审核意见不能为空") String auditMemo) {
		return super.newResult(orderService.audit(id, audited, auditMemo));
	}

	@ApiOperation(value = "批量审核记录")
	@PostMapping(value = "/auditall")
	public ResultResponse<String> auditByIdNum(
			@ApiParam(required = true, value = "批量审核记录") @Valid @RequestBody  List<QueryAudit> queryAudits) {
  		return super.newResult(orderService.audits(queryAudits));
	}

	@ApiOperation(value = "弃审记录")
	@GetMapping(value = "/unaudit")
	public ResultResponse<String> unauditById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        String res=orderService.unaudit(id);
        return new ResultResponse("订单弃审成功!".equals(res)?0:1,res);
	}

	@ApiOperation(value = "删除记录使用flag")
	@GetMapping(value = "/delflag")
	public ResultResponse<String> delFlagById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.newResult(orderService.delFlagById(id));
	}

	@ApiOperation(value = "删除数据库记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		String delById = orderService.delById(id);
		if(delById.equals("-1")) {
			return new ResultResponse<String>(-1, "订单状态不是下单状态,不能标记删除，请核实过再标记删除");
		}
		return new ResultResponse<String>(0, orderService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Order> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = orderService.getById(id);
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "详细列表分页查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Order>> list(
			@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		if(param.getState()==null)
			param.setState("1");
		return new ResultResponse<PageTableData<Order>>(
				orderService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Order>> all(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		return new ResultResponse<List<Order>>(orderService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param, HttpServletResponse response)
			throws Exception {
		PageTableData<Order> listData = orderService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param,
			HttpServletResponse response) throws Exception {
		List<Order> listData = orderService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	}
	
	@ApiOperation(value = "获取新的NUM")
	@GetMapping(value = "/getnum")
	public ResultResponse<String> getNum() {
		return new ResultResponse<String>(0, "OK", orderService.getNum());
	}
	
	@ApiOperation(value = "获取Order的日志")
	@GetMapping(value = "/getSysLog")
	public ResultResponse<List<SysLogs>> getSysLog(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id
			) {
		return new ResultResponse<List<SysLogs>>(0,"OK",sysLogsService.getLogsByModuleId(id));
	}
	
	@ApiOperation(value = "根据ID打印原始订单")
	@GetMapping(value = "/getOrginById")
	public ResultResponse<Order> printOrginById(
			@ApiParam(required = true, value = "打印编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = orderService.printOrginById(id,"0");
		return new ResultResponse<Order>(record);
	}
	
	@ApiOperation(value = "根据ID打印实际订单")
	@GetMapping(value = "/getRealById")
	public ResultResponse<Order> printRealById(
			@ApiParam(required = true, value = "打印编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = orderService.printOrginById(id,"1");
		return new ResultResponse<Order>(record);
	}


//    @ApiOperation(value = "根据IDs打印订单")
//    @GetMapping(value = "/getRealByIds")
//    public ResultResponse<List<Order>> printRealByIds(
//            @ApiParam(required = true, value = "打印编号") @RequestParam("id") @NotNull(message = "ids不能为空") List<String> ids) {
//        List<Order> record = orderService.printInfoByIds(ids);
//        return new ResultResponse<List<Order>>(record);
//    }

    @ApiOperation(value = "excel导入mysql")
	@PostMapping(value = "/importExcel")
	public ResultResponse<String> importExcel(
			MultipartFile file) {
		try {
			String importExcel = orderService.importExcel(file);
			return new ResultResponse(importExcel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new ResultResponse("导入错误");
		}
	}
}
