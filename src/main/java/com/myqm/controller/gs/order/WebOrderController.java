package com.myqm.controller.gs.order;
 
import java.sql.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.order.Order;
import com.myqm.service.gs.order.WebOrderService;
import com.myqm.vo.gs.order.OrderQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "网络订单爬取")
@RestController
@Validated
@RequestMapping("/api/order/web")
public class WebOrderController extends BaseController {

	@Autowired
	WebOrderService webService;

	@ApiOperation(value = "添加web订单")
	@GetMapping(value = "/spider")
	public ResultResponse<String> addOrder(@RequestParam("id") @NotNull(message = "id不能为空") String pluginId)
			throws Exception {
		return this.newResult(webService.saveOrder(pluginId));
	}
	
	@ApiOperation(value = "批量删除数据库记录")
	@GetMapping(value = "/dels")
	public ResultResponse<String> delByIds(
			@ApiParam(required = true, value = "查询编号") @RequestParam("ids") @NotNull(message = "ids不能为空") List<String> ids) {
		return super.newResult(webService.delete(ids));
	}

	@ApiOperation(value = "添加回顾web订单")
	@GetMapping(value = "/old/spider")
	public ResultResponse<String> addOldOrder(
			@ApiParam(required = true, value = "订单爬取插件编号") @RequestParam("id") @NotNull(message = "id不能为空") String pluginId,
			@ApiParam(required = false, value = "爬取单位名称，如果为null 爬取所有单位") @RequestParam(value = "customerName",required=false) String customerName,
			@ApiParam(required = true, value = "爬取开始时间") @RequestParam("beginDate") @NotNull(message = "开始时间不能为空") Date beginDate,
			@ApiParam(required = true, value = "爬取结束时间") @RequestParam("endDate") @NotNull(message = "结束时间不能为空") Date endDate)
			throws Exception {
		return this.newResult(webService.saveOldOrder(pluginId, customerName, beginDate, endDate));

	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Order> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = webService.getById(id);
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "下一个order信息")
	@GetMapping(value = "next")
	public ResultResponse<Order> getNext(
			@Valid OrderQuery param) {
		Order record = webService.getNext(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有下一条");
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "上一个order信息")
	@GetMapping(value = "pre")
	public ResultResponse<Order> getPre(
			@Valid OrderQuery param) {
		Order record = webService.getPre(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有上一条");
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "详细列表分页查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Order>> list(
			@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		return new ResultResponse<PageTableData<Order>>(
				webService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Order>> all(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		return new ResultResponse<List<Order>>(webService.listAll(param));
	}
	
}
