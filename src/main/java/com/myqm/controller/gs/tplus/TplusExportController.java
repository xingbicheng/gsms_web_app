package com.myqm.controller.gs.tplus;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse; 
import com.myqm.pojo.gs.basic.Provider;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.pojo.gs.tplus.TPlusLockInfo;
import com.myqm.pojo.gs.tplus.TPlusOrderCustomer;
import com.myqm.service.gs.basic.ProviderService;
import com.myqm.service.gs.tplus.TplusInService;
import com.myqm.service.gs.tplus.TplusOrderService; 
import io.swagger.annotations.Api; 
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;


@Api(tags = "采购入库、销售出库导入TPlus")
@RestController
@Validated
@RequestMapping("/api/exporttplus")
public class TplusExportController extends BaseController { 
	
	@Autowired
	public TplusInService tplusInService; 
	
	@Autowired
	public TplusOrderService tplusOrderService; 
	 
	
	@ApiOperation(value="列出当天的待导入的不可退货信息")
	    @GetMapping(value="/notBackList")
	public ResultResponse<List<StroehouseIn>> lock(
	    	@ApiParam(required = true, value = "仓库号")
	    	@RequestParam("storehouseId")   String storehouseId,
	    	@ApiParam(required = true, value = "入库时间")
	    	@RequestParam("inDate")   Date inDate, 
	    	@ApiParam(value = "状态,默认是做完损耗的单据")
	    	@RequestParam("state") String   state
	    	) {
		if(state == null)
			state = StroehouseInState.loss;
	        return new ResultResponse<List<StroehouseIn>>(tplusInService.listNotBack
	        		(storehouseId, inDate, state)); 
	    } 
	  
    @ApiOperation(value="导出到T+不可以退货的入库信息")
    @GetMapping(value="/notBackExport")
    public ResultResponse<String> notBackExport(
    		@ApiParam(required = true, value = "仓库号")
    		@RequestParam("storehouseId")   String storehouseId,
    	@ApiParam(required = true, value = "入库单ids")
    	@RequestParam("ids")List<String> ids) {
        return super.newResult(tplusInService.notBackExport(ids,storehouseId)); 
    }
    
    @ApiOperation(value="检查时间端下所有的单据是否结算完毕")
    @GetMapping(value="/check")
    public ResultResponse<String> check(
    		@ApiParam(required = true, value = "仓库号")
    		@RequestParam("storehouseId")   String storehouseId,  
    		@ApiParam(required = true, value = "入库开始时间")
	    	@RequestParam("beginDate")   Date beginDate, 
	    	@ApiParam(required = true, value = "入库结束时间")
	    	@RequestParam("endDate")   Date endDate
	    	) {
    	StringBuffer  msg=new StringBuffer("");
    	boolean result =tplusInService.checkOrder(storehouseId,beginDate,endDate,msg);
        if (result)
    	return super.newResult("检查正确"); 
        else
        	return new  ResultResponse<String>(-1,msg.toString(),null);
    }
    
    
    @ApiOperation(value="锁定单据")
    @GetMapping(value="/lock")
    public ResultResponse<TPlusLockInfo> lock(
    		@ApiParam(required = true, value = "仓库号")
    		@RequestParam("storehouseId")   String storehouseId,  
    		@ApiParam(required = true, value = "入库开始时间")
	    	@RequestParam("beginDate")   Date beginDate, 
	    	@ApiParam(required = true, value = "入库结束时间")
	    	@RequestParam("endDate")   Date endDate
	    	) {
    	TPlusLockInfo info = new TPlusLockInfo();
    	 
    	String result = tplusInService.lock(storehouseId,beginDate,endDate,info);
        return new ResultResponse<TPlusLockInfo>(0, result, info); 
    }
    
    @ApiOperation(value="获取已经锁定的数据")
    @GetMapping(value="/getLock")
    public ResultResponse<TPlusLockInfo> getLock(
    		@ApiParam(required = true, value = "仓库号")
    		@RequestParam("storehouseId")   String storehouseId,  
    		@ApiParam(required = true, value = "入库开始时间")
	    	@RequestParam("beginDate")   Date beginDate, 
	    	@ApiParam(required = true, value = "入库结束时间")
	    	@RequestParam("endDate")   Date endDate
	    	) {
    	TPlusLockInfo info = new TPlusLockInfo();
    	 
    	String result = tplusInService.getLock(storehouseId,beginDate,endDate,info);
        return new ResultResponse<TPlusLockInfo>(0, result, info); 
    }
    
    
    
     
    
    @ApiOperation(value="按供货商导出到T+可以退货的入库信息")
    @GetMapping(value="/exportIn")
    public ResultResponse<String> backExportByProvider(
    		@ApiParam(required = true, value = "仓库号")
    		@RequestParam("storehouseId")   String storehouseId,
    		@ApiParam(required = true, value = "供应商编号")
    		@RequestParam("providerId")   String providerId,  
    		@ApiParam(required = true, value = "入库开始时间")
	    	@RequestParam("beginDate")   Date beginDate, 
	    	@ApiParam(required = true, value = "入库结束时间")
	    	@RequestParam("endDate")   Date endDate
	    	) {
        return super.newResult(tplusInService.exportByProvider(storehouseId,providerId, beginDate,endDate)); 
    } 
     
    @ApiOperation(value="按供货商导出到T+可以退货的销售出库信息")
    @PostMapping(value="/exportOut")
    public ResultResponse<String> backExportByCustomer(
    		@ApiParam(required = true, value = "仓库号")
    		 @Valid @RequestBody TPlusOrderCustomer customerInfo) {
    	
        return super.newResult(tplusOrderService.exportOrder( customerInfo )); 
    } 
   
}
