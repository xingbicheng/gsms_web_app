package com.myqm.controller.gs.purchase;

import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import com.myqm.pojo.gs.purchase.PurchaseOrderTs;
import com.myqm.pojo.gs.purchase.PurchasePrint;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.sysfun.SysLogsService;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.myqm.pojo.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.purchase.PurchaseLinePrint;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.vo.gs.purchase.PurchaseOrderQuery;
import com.myqm.service.gs.purchase.PurchaseOrderService;

@Api(tags = "采购单操作")
@RestController
@Validated
@RequestMapping("/api/purchaseOrder")
public class PurchaseOrderController extends BaseController {

	@Autowired
	public PurchaseOrderService purchaseOrderService;

	@Autowired
    public SysLogsService sysLogsService;

	//修改订单状态，为预采购
	@ApiOperation(value = "新增订单采购单")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加采购单信息") @Valid @RequestBody PurchaseOrder record) {
		return super.dbOperateResult(purchaseOrderService.save(record));
	}

	//补一个计划采购的新增、修改、只能入中央
    @ApiOperation(value = "新增计划采购单")
    @PostMapping(value = "/addplan")
    public ResultResponse<String> addplan(
            @ApiParam(required = true, value = "添加采购单信息") @Valid @RequestBody PurchaseOrder record) {
        return super.dbOperateResult(purchaseOrderService.planPurchaseSave(record));
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "修改计划采购单明细")
    @PostMapping(value = "/editPlan")
    public ResultResponse<String> editPlan(
            @ApiParam(required = true, value = "修改的计划采购单明细信息")
            @Valid @RequestBody PurchaseOrder record) {
        String res=purchaseOrderService.editPlanById(record);
        return newResult(res);
    } 

    // 修改明细
	//如果修改订单明细采购，需要修改订单状态，如果orderDetailIds 为空，不修改订单状态
    //? 删除分库信息
	@SuppressWarnings("unchecked")
	@ApiOperation(value = "修改订单采购单明细")
	@PostMapping(value = "/detail/edit")
	public ResultResponse<String> editDetail(
			@ApiParam(required = true, value = "修改的采购单明细信息") 
			@Valid @RequestBody PurchaseOrderDetail record) {
		String res=purchaseOrderService.editDetailById(record);
	    return newResult(res); 
	}

	//修改 损耗量， 不修改订单状态
	@ApiOperation(value = "修改订单采购简单信息（采购员，采购时间，细目损耗量）")
	@PostMapping(value = "/detail/edits")
	public ResultResponse<String> editDetails(
			@ApiParam(required = true, value = "修改的采购单明细信息（采购员，采购时间，细目损耗量）")
			@Valid @RequestBody PurchaseOrderTs record) {
		String res=purchaseOrderService.editDetailT(record);
		if("修改成功".equals(res))
			return new ResultResponse(0,res);
		else
			return new ResultResponse(1,res); 
	}

	//修改订单状态
	@ApiOperation(value = "删除订单采购单一条明细")
	@PostMapping(value = "/detail/del")
	public ResultResponse<String> delDetail(
			@ApiParam(required = true, value = "修改的采购单明细信息") @RequestParam("id") @NotNull(message = "采购单明细") String id) {
		return super.newResult(purchaseOrderService.delDetailById(id));
	}
	//不修改订单状态
	@ApiOperation(value = "修改采购单头部信息")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改PurchaseOrder") @Valid @RequestBody PurchaseOrder record) {
		return super.dbOperateResult(purchaseOrderService.editById(record));

	}

    
	//不修改修改订单状态
	@ApiOperation(value = "删除采购单")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return new ResultResponse(purchaseOrderService.delById(id));
	}
	//修改订单状态
	@ApiOperation(value = "标记删除采购单")
	@GetMapping(value = "/delflag")
	public ResultResponse<String> delFlagById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.newResult(purchaseOrderService.delFlagById(id));
	}

	@ApiOperation(value = "根据ID查询采购单")
	@GetMapping(value = "/get")
	public ResultResponse<PurchaseOrder> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		PurchaseOrder record = purchaseOrderService.getById(id);
		return new ResultResponse<PurchaseOrder>(record);
	}

	@ApiOperation(value = "根据采购单明细ID查询所涉及到的订单明细")
	@GetMapping(value = "/get/orderdetail")
	public ResultResponse<List<OrderDetail>> getOrderDetailById(
			@ApiParam(required = true, value = "采购单明细id") @RequestParam("id") @NotNull(message = "id不能为空") String detailId) {
		List<OrderDetail> record = purchaseOrderService.getOrederDetailByPDId(detailId);
		return new ResultResponse<List<OrderDetail>>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<PurchaseOrder>> list(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param) {
		return new ResultResponse<PageTableData<PurchaseOrder>>(
				purchaseOrderService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<PurchaseOrder>> listAll(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param) {
		return new ResultResponse<List<PurchaseOrder>>(purchaseOrderService.listAll(param));
	}

	@ApiOperation(value = "上一条采购订单及详细")
	@GetMapping(value = "/pre")
	public ResultResponse<PurchaseOrder> pre(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param) {
		return new ResultResponse<PurchaseOrder>(purchaseOrderService.pre(param));
	}

	@ApiOperation(value = "下一条采购订单及详细")
	@GetMapping(value = "/next")
	public ResultResponse<PurchaseOrder> next(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param) {
		return new ResultResponse<PurchaseOrder>(purchaseOrderService.next(param));
	}

    @ApiOperation(value = "根据采购单id获取日志")
    @GetMapping(value = "/getlogs")
    public ResultResponse<List<SysLogs>> getLogs(
            @ApiParam(required = true, value = "查询记录编号") @RequestParam("record") @NotNull(message = "id不能为空") String record) {
        List<SysLogs> sysLogs=sysLogsService.getLogsByModuleId(record);
        try{
            if(sysLogs==null||sysLogs.size()==0)
                return new ResultResponse(1,"没有此采购单的日志信息");
            else
                return new ResultResponse<List<SysLogs>>(sysLogs);
        }catch (Exception e){
            return new ResultResponse(1,"没有此采购单的日志信息");
        }
    }

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<PurchaseOrder> listData = purchaseOrderService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("PurchaseOrder查询数据", "PurchaseOrder查询数据"),
				PurchaseOrder.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=PurchaseOrder查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderQuery param,
			HttpServletResponse response) throws Exception {
		List<PurchaseOrder> listData = purchaseOrderService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("PurchaseOrder查询数据", "PurchaseOrder查询数据"),
				PurchaseOrder.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=PurchaseOrder查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "获取新的NUM")
	@GetMapping(value = "/getnum")
	public ResultResponse<String> getNum() {
		return new ResultResponse<String>(0, "OK", purchaseOrderService.getNum());
	}
	@ApiOperation(value = "获取单据打印数据")
	@GetMapping(value = "/print")
	public ResultResponse<PurchasePrint> getPrint(
			@ApiParam(required = true, value = "修改的采购单明细信息") 
			@RequestParam("id") @NotNull(message = "采购单id不能空") String id){
		return new ResultResponse<PurchasePrint> ( purchaseOrderService.getPrintPurchase(id));		
	}
}
