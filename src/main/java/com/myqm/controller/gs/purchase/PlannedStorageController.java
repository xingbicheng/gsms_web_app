package com.myqm.controller.gs.purchase;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.myqm.dto.gs.in.SetTransferInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.dto.gs.in.ResolveStoreIn;
import com.myqm.dto.gs.in.SetProviderInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.service.gs.purchase.PlannedStorageService;
import com.myqm.service.gs.purchase.PurchaseOrderDetailService;
import com.myqm.service.gs.purchase.PurchaseOrderService;
import com.myqm.vo.gs.purchase.PurchaseOrderDetailQuery;
import com.myqm.vo.gs.purchase.PurchaseOrderQuery;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "计划入库单")
@RestController
@Validated
@RequestMapping("/api/plannedStorage")
public class PlannedStorageController extends BaseController {

	@Autowired
	public PurchaseOrderService purchaseOrderService;

	@Autowired
	public PlannedStorageService inService;

	@Autowired
	public PurchaseOrderDetailService purchaseDetailService;

	// 计划入库
	@ApiOperation(value = "入库计划分解")
	@PostMapping(value = "/resolve")
	public ResultResponse<String> resolve(
			@ApiParam(required = true, value = "拆分入库单id") @RequestBody @Valid ResolveStoreIn resolveInfo) {
		String re = inService.resolve(resolveInfo.getStorehouseInId(), resolveInfo.getAmount());
		return new ResultResponse<>(re=="分解成功"?0:1,re);
	}

	@ApiOperation(value = "入库计划合并")
	@GetMapping(value = "/merge")
	public ResultResponse<String> merge(@ApiParam(required = true, value = "合并计划入库单id1") String inId1,
			@ApiParam(required = true, value = "合并计划入库单id2")String inId2) {
	    String re=inService.merge(inId1, inId2);
		return new ResultResponse<>(re=="合并成功"?0:1,re);
	}

	@ApiOperation(value = "设置供货商信息")
	@PostMapping(value = "/set")
	public ResultResponse<String> setProvider(@ApiParam(required = true, value = "设置供应商信息") @RequestBody SetProviderInfo setInfo) {
		String re= inService.setProvider(setInfo);
        return new ResultResponse<>(re=="设置成功"?0:1,re);
	}

    @ApiOperation(value = "设置转库信息信息")
    @PostMapping(value = "/settran")
    public ResultResponse<String> setTransfer(@ApiParam(required = true, value = "设置转库信息")
                                              @RequestBody SetTransferInfo setInfo) {
        String re=inService.setTransferInfo(setInfo);
        return new ResultResponse<>(re=="转库成功"?0:1,re);
    }

    
    // 审核
 	// 审核明细
    //分订单明细 的入库情况，设置订单明细为采购；，更新订单状态 
 	@ApiOperation(value = "采购单明细审核")
 	@GetMapping(value = "/audit")
 	public ResultResponse<String> audited(
 			@ApiParam(required = true, value = "修改的采购单明细信息") @RequestParam("ids")
 			@NotNull(message = "采购单明细ids不能空") List<String> ids,
 			@RequestParam("audited") @NotNull(message = "是否通过") boolean audited, @RequestParam("memo") String memo) {
 		return super.newResult(purchaseOrderService.auditDetailById(ids, audited, memo));
 	}

 	//弃审明细
 	//删除分订单明细 的入库情况 ，设置订单明细为预采购，更新订单状态 
 	@ApiOperation(value = "采购单明细弃审")
 	@GetMapping(value = "/unaudit")
 	public ResultResponse<String> unaudit(
 			@ApiParam(required = true, value = "修改的采购单明细信息") @RequestParam("id") @NotNull(message = "采购单明细") String id) {
 		return super.newResult(purchaseOrderService.unauditDetailById(id));
 	}
 	
 	
	@ApiOperation(value = "根据采购明细获取入库计划")
	@GetMapping(value = "/get")
	public ResultResponse<List<StroehouseIn>> getAllInById(
			@ApiParam(required = true, value = "采购明细id") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return new ResultResponse<List<StroehouseIn>>(inService.getByPurchaseDetailId(id));
	}

	@ApiOperation(value = "查询所有采购单明细")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<PurchaseOrderDetail>> list(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderDetailQuery param) {
		return new ResultResponse<PageTableData<PurchaseOrderDetail>>(
				purchaseDetailService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "查询所有采购单明细")
	@GetMapping(value = "/all")
	public ResultResponse<List<PurchaseOrderDetail>> listAll(
			@ApiParam(required = false, value = "查询参数") @Valid PurchaseOrderDetailQuery param) {
		return new ResultResponse<List<PurchaseOrderDetail>>(purchaseDetailService.listAll(param));
	}

	
}
