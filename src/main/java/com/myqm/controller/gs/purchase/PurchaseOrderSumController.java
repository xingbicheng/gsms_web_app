package com.myqm.controller.gs.purchase;

import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.purchase.PurchaseOrderSum;
import com.myqm.pojo.gs.purchase.PurchaseOrderTotal;
import com.myqm.service.gs.purchase.PurchaseOrderSumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "采购单统计")
@RestController
@Validated
@RequestMapping("/api/purchaseOrder/sum")
public class PurchaseOrderSumController {
    @Autowired
    PurchaseOrderSumService purchaseOrderSumService;
    @ApiOperation(value = "采购单统计")
    @PostMapping(value = "/sum")
    public ResultResponse<List<PurchaseOrderSum>> save(
            @ApiParam(required = true, value = "添加采购单信息") @Valid @RequestBody PurchaseOrderTotal purchaseOrderTotal) {
        List<PurchaseOrderSum> purchaseOrderSum= purchaseOrderSumService.sum(purchaseOrderTotal);
        return new ResultResponse<List<PurchaseOrderSum>>(purchaseOrderSum);
    }
}
