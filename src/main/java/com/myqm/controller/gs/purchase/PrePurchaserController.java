package com.myqm.controller.gs.purchase;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.myqm.pojo.ResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;
import com.myqm.service.gs.purchase.PerPurchaseService;

@Api(tags = "采购单前处理")
@RestController
@Validated
@RequestMapping("/api/pre_purchase")
public class PrePurchaserController extends BaseController {

	@Autowired
	public PerPurchaseService perService;

	@ApiOperation(value = "获取采购数据")
	@GetMapping(value = "/")
	public ResultResponse<List<PrePurchaseGoodsType>> getPre(
			@ApiParam(required = true, value = "查询参数") @Valid PerPurchaseQuery param) {
		return new ResultResponse<List<PrePurchaseGoodsType>>(perService.getPurchaseDetail(param));
	}
}
