package com.myqm.controller.gs.purchase;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.pojo.gs.purchase.PurchasePrice.PurchasePriceState;
import com.myqm.service.gs.purchase.PurchasePriceService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "询问价格")
@RestController
@Validated
@RequestMapping("/api/purchaseprice")
public class PurchasePriceController extends BaseController{
	
	@Autowired
	public PurchasePriceService purchasePriceService;

	
	static class RequestPurchasePrice{ 
		 
		@ApiModelProperty(value = "询问货物的id(字符长度为1-32)")
		@Size(min = 1, max = 32, message = "询问货物的id字符长度为1-32")
		@Excel(name = "询问货物的id", orderNum = "2")
		private String goodsId;

		@ApiModelProperty(value = "采购单明细(字符长度为1-32)")
		@Size(min = 1, max = 32, message = "采购单明细字符长度为1-32")
		@Excel(name = "采购单明细", orderNum = "3")
		private String purchaseOrderDetailId;
		
		@ApiModelProperty(value = "预购数量")
		private BigDecimal amount; 
		
		@ApiModelProperty(value ="限制时间(最迟报价时间 yyyy-MM-dd HH:mm:ss)") 
		private String limitDate; 
		
		List<String> providerIds;

		public String getGoodsId() {
			return goodsId;
		}

		public void setGoodsId(String goodsId) {
			this.goodsId = goodsId;
		}


		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public String getLimitDate() {
			return limitDate;
		}

		public void setLimitDate(String limitDate) {
			 

			this.limitDate = limitDate;
		}

		public List<String> getProviderIds() {
			return providerIds;
		}

		public void setProviderIds(List<String> providerIds) {
			this.providerIds = providerIds;
		}

		public String getPurchaseOrderDetailId() {
			return purchaseOrderDetailId;
		}

		public void setPurchaseOrderDetailId(String purchaseOrderDetailId) {
			this.purchaseOrderDetailId = purchaseOrderDetailId;
		} 
		
		
		
	}
	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加purchaseprice") 
			@Valid @RequestBody RequestPurchasePrice record) throws CloneNotSupportedException {
		PurchasePrice pp = new PurchasePrice();
		pp.setAmount(record.getAmount());
		pp.setLimitDate(record.getLimitDate());
		pp.setGoodsId(record.getGoodsId()); 
		pp.setCreaterId(UserUtil.getLoginUser().getId());
		pp.setState(PurchasePriceState.quotation);
		pp.setPurchaseDetailId(record.getPurchaseOrderDetailId());
		List<PurchasePrice> list = new ArrayList<PurchasePrice>();
		if(record.getProviderIds()!=null) {
			for (String  id: record.getProviderIds()) {			
				PurchasePrice newpp =(PurchasePrice)pp.clone(); 
				newpp.setProviderId(id);
				list.add(newpp);
			} 
		}
		return super.dbOperateResult(purchasePriceService.saveAll(list));
	}
	
//	@ApiOperation(value = "批量新增记录")
//	@PostMapping(value = "/adds")
//	public ResultResponse<String> saveAll(
//			@ApiParam(required = true, value = "添加purchaseprice") @Valid @RequestBody List<PurchasePrice> record) {
//		return super.dbOperateResult(purchasePriceService.saveAll( record));
//	}
//
//	@ApiOperation(value = "修改内容")
//	@PostMapping(value = "/edit")
//	public ResultResponse<String> editById(
//			@ApiParam(required = true, value = "修改purchaseprice") @Valid @RequestBody PurchasePrice record) {
//		return super.dbOperateResult(purchasePriceService.editById(record));
//	}
//	
//	@ApiOperation(value = "批量修改内容")
//	@PostMapping(value = "/edits")
//	public ResultResponse<String> editByIds(
//			@ApiParam(required = true, value = "修改purchaseprice") @Valid @RequestBody List<PurchasePrice> record) {
//		return super.dbOperateResult(purchasePriceService.editByIds(record));
//	}
//
//	@ApiOperation(value = "删除记录")
//	@GetMapping(value = "/del")
//	public ResultResponse<String> delById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		return super.newResult(purchasePriceService.delById(id));
//	}
//	
//	@ApiOperation(value = "删除状态")
//	@GetMapping(value = "/delflag")
//	public ResultResponse<String> delFlagById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		return super.newResult(purchasePriceService.delFlagById(id));
//	}
//	
//	@ApiOperation(value = "根据ID查询记录")
//	@GetMapping(value = "/get")
//	public ResultResponse<PurchasePrice> getById(
//			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//		PurchasePrice record = purchasePriceService.getById(id);
//		return new ResultResponse<PurchasePrice>(record);
//	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<PurchasePrice>> list(
			@ApiParam(required = false, value = "查询参数") @Valid PurchasePriceQuery param) {
		return new ResultResponse<PageTableData<PurchasePrice>>(
				purchasePriceService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<PurchasePrice>> all(
			@ApiParam(required = false, value = "查询参数") @Valid PurchasePriceQuery param) {
		return new ResultResponse<List<PurchasePrice>>(purchasePriceService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid PurchasePriceQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<PurchasePrice> listData = purchasePriceService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("PurchasePrice查询数据", "PurchasePrice查询数据"),
				PurchasePrice.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=PurchasePrice查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid PurchasePriceQuery param,
			HttpServletResponse response) throws Exception {
		List<PurchasePrice> listData = purchasePriceService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("PurchasePrice查询数据", "PurchasePrice查询数据"),
				PurchasePrice.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=PurchasePrice查询数据.xls");
		workbook.write(response.getOutputStream());
	}

}
