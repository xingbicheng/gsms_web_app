package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;

import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.service.gs.basic.CustomerService;
import com.myqm.service.gs.basic.CustomerTypeService;
import com.myqm.service.gs.basic.StoreCustomerService;
import com.myqm.service.gs.basic.StorehouseService;
import com.myqm.vo.gs.basic.StoreCustomerQuery;
import com.myqm.vo.gs.basic.StorehouseQuery;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;

@Api(tags = "仓库")
@RestController
@Validated
@RequestMapping("/api/storehouse")
public class StorehouseController extends BaseController {

	@Autowired
	public StorehouseService storehouseService;
	@Autowired
	public StoreCustomerService storeCustomerService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加storehouse") @Valid @RequestBody Storehouse record) {
		
		return super.dbOperateResult(storehouseService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改storehouse") @Valid @RequestBody Storehouse record) {
		return super.dbOperateResult(storehouseService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(storehouseService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Storehouse> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Storehouse record = storehouseService.getById(id);
		return new ResultResponse<Storehouse>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Storehouse>> list(
			@ApiParam(required = false, value = "查询参数") @Valid StorehouseQuery param) {
		return new ResultResponse<PageTableData<Storehouse>>(
				storehouseService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Storehouse>> all(
			@ApiParam(required = false, value = "查询参数") @Valid StorehouseQuery param) {
		return new ResultResponse<List<Storehouse>>(storehouseService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid StorehouseQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<Storehouse> listData = storehouseService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Storehouse查询数据", "Storehouse查询数据"),
				Storehouse.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Storehouse查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid StorehouseQuery param,
			HttpServletResponse response) throws Exception {
		List<Storehouse> listData = storehouseService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Storehouse查询数据", "Storehouse查询数据"),
				Storehouse.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Storehouse查询数据.xls");
		workbook.write(response.getOutputStream());
	}
	
	

	
	@ApiOperation(value = "新增仓库的客户类型")
	@PostMapping(value = "/customeType/add")
	public ResultResponse<String> save1(
			@ApiParam(required = true, value = "添加customertype") @Valid @RequestBody StoreCustomer record) {
		 return super.dbOperateResult(storeCustomerService.save(record));
	}
	
	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/customeType/edit")
	public ResultResponse<String> edit2(
			@ApiParam(required = true, value = "修改customertype") @Valid @RequestBody StoreCustomer record) {
		return super.dbOperateResult(storeCustomerService.editById(record));
	}
		
		@ApiOperation(value="删除记录")
		@GetMapping(value="/customeType/del")
		public ResultResponse<String> delById1(
			@ApiParam(required=true,value="查询编号")
			@RequestParam("id") @NotNull(message = "id不能为空") String id) { 
	        return super.dbOperateResult(storeCustomerService.delById(id));
	    } 

	    @ApiOperation(value = "根据ID查询记录")
	    @GetMapping(value = "/customeType/get")
	    public ResultResponse<List<StoreCustomer>> getByStroeId(
	    	@ApiParam(required = true, value = "仓库id")
	    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
	    	StoreCustomerQuery q = new StoreCustomerQuery();
	    	q.setStoreId(id);
	    	List<StoreCustomer> records=storeCustomerService.listAll(q);
	        return new ResultResponse<List<StoreCustomer>>(records);
}
}
