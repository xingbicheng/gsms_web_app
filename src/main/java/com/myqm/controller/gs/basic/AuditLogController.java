package com.myqm.controller.gs.basic;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.basic.AuditLog;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.service.gs.basic.AuditLogService;
import com.myqm.service.gs.basic.CustomerService;
import com.myqm.vo.gs.basic.AuditLogQuery;
import com.myqm.vo.gs.basic.CustomerQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api(tags = "审核记录统计")
@RestController
@Validated
@RequestMapping("/api/auditlog")
public class AuditLogController extends BaseController {

    @Autowired
    public AuditLogService auditLogService;


    @ApiOperation(value = "根据ID查询审核记录")
    @GetMapping(value = "/get")
    public ResultResponse<AuditLog> getById(
            @ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
        AuditLog record = auditLogService.getById(id);
        return new ResultResponse<AuditLog>(record);
    }

    @ApiOperation(value = "按类型传参获取详细审核条数记录")
    @GetMapping(value = "/list")
    public ResultResponse<List<AuditLog>> list(
            @ApiParam(required = false, value = "查询参数") @Valid AuditLogQuery param) {
        return new ResultResponse<List<AuditLog>>(auditLogService.listAll(param));
    }

}
