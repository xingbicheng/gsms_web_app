package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.gs.basic.WebPlugin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.basic.WebPluginService;
import com.myqm.vo.gs.basic.WebPluginQuery;

@Api(tags = "插件")
@RestController
@Validated
@RequestMapping("/api/webPlugin")
public class WebPluginController extends BaseController {

	@Autowired
	public WebPluginService webPluginService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加WebPlugin") @Valid @RequestBody WebPlugin record) {
		
		return super.dbOperateResult(webPluginService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改WebPlugin") @Valid @RequestBody WebPlugin record) {
		return super.dbOperateResult(webPluginService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(webPluginService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<WebPlugin> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		WebPlugin record = webPluginService.getById(id);
		return new ResultResponse<WebPlugin>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<WebPlugin>> list(
			@ApiParam(required = false, value = "查询参数") @Valid WebPluginQuery param) {
		return new ResultResponse<PageTableData<WebPlugin>>(
				webPluginService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<WebPlugin>> all(
			@ApiParam(required = false, value = "查询参数") @Valid WebPluginQuery param) {
		return new ResultResponse<List<WebPlugin>>(webPluginService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid WebPluginQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<WebPlugin> listData = webPluginService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("WebPlugin查询数据", "WebPlugin查询数据"),
				WebPlugin.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=WebPlugin查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid WebPluginQuery param,
			HttpServletResponse response) throws Exception {
		List<WebPlugin> listData = webPluginService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("WebPlugin查询数据", "WebPlugin查询数据"),
				WebPlugin.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=WebPlugin查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
