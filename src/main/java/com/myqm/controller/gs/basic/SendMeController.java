package com.myqm.controller.gs.basic;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.pojo.ResultResponse;
import com.myqm.service.gs.basic.SendMeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
@Api(tags = "发短信")
@RestController
@Validated
@RequestMapping("/api/sendMessage")
public class SendMeController {
	
		@Autowired
		public SendMeService sendMeService;
		
		@ApiOperation(value = "电话号码")
		@GetMapping(value = "/get")
		public  void sendMe(
				@ApiParam(required = true, value = "电话号码") @RequestParam("number") @NotNull(message = "number不能为空") String number,
				@ApiParam(required = true, value = "短信内容") @RequestParam("message") @NotNull(message = "message不能为空") String message)
		{
		 sendMeService.sendMe(number,message,"");
		 System.out.println("已发送短信");
	}	
}
	  
	  


