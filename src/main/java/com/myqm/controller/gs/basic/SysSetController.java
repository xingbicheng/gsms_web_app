package com.myqm.controller.gs.basic;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.service.gs.basic.SysSetService;
import com.myqm.vo.gs.basic.SysSetQuery;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "系统设置")
@RestController
@Validated
@RequestMapping("/api/sysset")
public class SysSetController extends BaseController{
	@Autowired
	public SysSetService sysSetService;
	
	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改sysset") @Valid @RequestBody SysSet record) {
		return super.dbOperateResult(sysSetService.editById(record));

	}
	
	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<SysSet> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		SysSet record = sysSetService.getById(id);
		return new ResultResponse<SysSet>(record);
	}
	
	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<SysSet>> list(
			@ApiParam(required = false, value = "查询参数") @Valid SysSetQuery param) {
		return new ResultResponse<PageTableData<SysSet>>(
				sysSetService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<SysSet>> all(
			@ApiParam(required = false, value = "查询参数") @Valid SysSetQuery param) {
		return new ResultResponse<List<SysSet>>(sysSetService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid SysSetQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<SysSet> listData = sysSetService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("SysSet查询数据", "SysSet查询数据"),
				SysSet.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=SysSet查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid SysSetQuery param,
			HttpServletResponse response) throws Exception {
		List<SysSet> listData = sysSetService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("SysSet查询数据", "SysSet查询数据"),
				SysSet.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=SysSet查询数据.xls");
		workbook.write(response.getOutputStream());
	}

}
