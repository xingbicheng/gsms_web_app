package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;

import com.myqm.pojo.gs.basic.GoodsConvert;
import com.myqm.service.gs.basic.GoodsConvertService;
import com.myqm.vo.gs.basic.GoodsConvertQuery;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;

@Api(tags = "货物转换")
@RestController
@Validated
@RequestMapping("/api/goodsconvert")
public class GoodsConvertController extends BaseController {

	@Autowired
	public GoodsConvertService goodsConvertService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加goodsconvert") @Valid @RequestBody GoodsConvert record) {
		String re=goodsConvertService.save(record);
		if(re=="新增失败")
            return new ResultResponse<>(1,re);
        else return new ResultResponse<>(0,re);
	}

	@ApiOperation(value = "新增多条记录")
	@PostMapping(value = "/update")
	public ResultResponse<String> saveall(
			@ApiParam(required = true, value = "添加goodsconvert") @Valid @RequestBody List<GoodsConvert> record) {
		return super.dbOperateResult(goodsConvertService.saveall(record));

	}
	
	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改goodsconvert") @Valid @RequestBody GoodsConvert record) {
		return super.dbOperateResult(goodsConvertService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(goodsConvertService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<GoodsConvert> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		GoodsConvert record = goodsConvertService.getById(id);
		return new ResultResponse<GoodsConvert>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<GoodsConvert>> list(
			@ApiParam(required = false, value = "查询参数") @Valid GoodsConvertQuery param) {
		return new ResultResponse<PageTableData<GoodsConvert>>(
				goodsConvertService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<GoodsConvert>> all(
			@ApiParam(required = false, value = "查询参数") @Valid GoodsConvertQuery param) {
		return new ResultResponse<List<GoodsConvert>>(goodsConvertService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid GoodsConvertQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<GoodsConvert> listData = goodsConvertService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("GoodsConvert查询数据", "GoodsConvert查询数据"),
				GoodsConvert.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=GoodsConvert查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid GoodsConvertQuery param,
			HttpServletResponse response) throws Exception {
		List<GoodsConvert> listData = goodsConvertService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("GoodsConvert查询数据", "GoodsConvert查询数据"),
				GoodsConvert.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=GoodsConvert查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
