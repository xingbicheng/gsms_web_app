package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsTypeQuery;
import com.myqm.service.gs.basic.GoodsTypeService;

@Api(tags = "商品类型")
@RestController
@Validated
@RequestMapping("/api/goodstype")
public class GoodsTypeController extends BaseController {

	@Autowired
	public GoodsTypeService goodsTypeService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加goodstype") @Valid @RequestBody GoodsType record) {
		
		return super.dbOperateResult(goodsTypeService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改goodstype") @Valid @RequestBody GoodsType record) {
		return super.dbOperateResult(goodsTypeService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(goodsTypeService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<GoodsType> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		GoodsType record = goodsTypeService.getById(id);
		return new ResultResponse<GoodsType>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<GoodsType>> list(
			@ApiParam(required = false, value = "查询参数") @Valid GoodsTypeQuery param) {
		return new ResultResponse<PageTableData<GoodsType>>(
				goodsTypeService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<GoodsType>> all(
			@ApiParam(required = false, value = "查询参数") @Valid GoodsTypeQuery param) {
		return new ResultResponse<List<GoodsType>>(goodsTypeService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid GoodsTypeQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<GoodsType> listData = goodsTypeService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("GoodsType查询数据", "GoodsType查询数据"),
				GoodsType.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=GoodsType查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid GoodsTypeQuery param,
			HttpServletResponse response) throws Exception {
		List<GoodsType> listData = goodsTypeService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("GoodsType查询数据", "GoodsType查询数据"),
				GoodsType.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=GoodsType查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
