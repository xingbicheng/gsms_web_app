package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.gs.basic.Customer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.basic.CustomerService;
import com.myqm.vo.gs.basic.CustomerQuery;

@Api(tags = "客户")
@RestController
@Validated
@RequestMapping("/api/customer")
public class CustomerController extends BaseController {

	@Autowired
	public CustomerService customerService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加customer") @Valid @RequestBody Customer record) {
		
		return super.dbOperateResult(customerService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改customer") @Valid @RequestBody Customer record) {
		return super.dbOperateResult(customerService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(customerService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Customer> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Customer record = customerService.getById(id);
		return new ResultResponse<Customer>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Customer>> list(
			@ApiParam(required = false, value = "查询参数") @Valid CustomerQuery param) {
		return new ResultResponse<PageTableData<Customer>>(
				customerService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Customer>> all(@ApiParam(required = false, value = "查询参数") @Valid CustomerQuery param) {
		return new ResultResponse<List<Customer>>(customerService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid CustomerQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<Customer> listData = customerService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Customer查询数据", "Customer查询数据"),
				Customer.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Customer查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid CustomerQuery param,
			HttpServletResponse response) throws Exception {
		List<Customer> listData = customerService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Customer查询数据", "Customer查询数据"),
				Customer.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Customer查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
