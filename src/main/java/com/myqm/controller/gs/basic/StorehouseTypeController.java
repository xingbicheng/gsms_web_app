package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;

import com.myqm.pojo.gs.basic.StorehouseType;
import com.myqm.service.gs.basic.StorehouseTypeService;
import com.myqm.vo.gs.basic.StorehouseTypeQuery;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;

@Api(tags = "仓库类型")
@RestController
@Validated
@RequestMapping("/api/storehousetype")
public class StorehouseTypeController extends BaseController {

	@Autowired
	public StorehouseTypeService storehouseTypeService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加storehousetype") @Valid @RequestBody StorehouseType record) {
		
		return super.dbOperateResult(storehouseTypeService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改storehousetype") @Valid @RequestBody StorehouseType record) {
		return super.dbOperateResult(storehouseTypeService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(storehouseTypeService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<StorehouseType> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		StorehouseType record = storehouseTypeService.getById(id);
		return new ResultResponse<StorehouseType>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<StorehouseType>> list(
			@ApiParam(required = false, value = "查询参数") @Valid StorehouseTypeQuery param) {
		return new ResultResponse<PageTableData<StorehouseType>>(
				storehouseTypeService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<StorehouseType>> all(
			@ApiParam(required = false, value = "查询参数") @Valid StorehouseTypeQuery param) {
		return new ResultResponse<List<StorehouseType>>(storehouseTypeService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid StorehouseTypeQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<StorehouseType> listData = storehouseTypeService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("StorehouseType查询数据", "StorehouseType查询数据"),
				StorehouseType.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=StorehouseType查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid StorehouseTypeQuery param,
			HttpServletResponse response) throws Exception {
		List<StorehouseType> listData = storehouseTypeService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("StorehouseType查询数据", "StorehouseType查询数据"),
				StorehouseType.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=StorehouseType查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
