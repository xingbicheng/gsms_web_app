package com.myqm.controller.gs.basic;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.gs.basic.CustomerType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.basic.CustomerTypeService;
import com.myqm.vo.gs.basic.CustomerTypeQuery;

@Api(tags = "客户类型")
@RestController
@Validated
@RequestMapping("/api/customertype")
public class CustomerTypeController extends BaseController {

	@Autowired
	public CustomerTypeService customerTypeService;

	@ApiOperation(value = "新增记录")
	@PostMapping(value = "/add")
	public ResultResponse<String> save(
			@ApiParam(required = true, value = "添加customertype") @Valid @RequestBody CustomerType record) {
		
		return super.dbOperateResult(customerTypeService.save(record));

	}

	@ApiOperation(value = "修改内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editById(
			@ApiParam(required = true, value = "修改CustomerType") @Valid @RequestBody CustomerType record) {
		return super.dbOperateResult(customerTypeService.editById(record));

	}

	@ApiOperation(value = "删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(customerTypeService.delById(id));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<CustomerType> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		CustomerType record = customerTypeService.getById(id);
		return new ResultResponse<CustomerType>(record);
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<CustomerType>> list(
			@ApiParam(required = false, value = "查询参数") @Valid CustomerTypeQuery param) {
		return new ResultResponse<PageTableData<CustomerType>>(
				customerTypeService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<CustomerType>> all(
			@ApiParam(required = false, value = "查询参数") @Valid CustomerTypeQuery param) {
		return new ResultResponse<List<CustomerType>>(customerTypeService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid CustomerTypeQuery param,
			HttpServletResponse response) throws Exception {
		PageTableData<CustomerType> listData = customerTypeService.list(param.getCurrentPage(), param.getPageSize(),
				param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("CustomerType查询数据", "CustomerType查询数据"),
				CustomerType.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=CustomerType查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid CustomerTypeQuery param,
			HttpServletResponse response) throws Exception {
		List<CustomerType> listData = customerTypeService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("CustomerType查询数据", "CustomerType查询数据"),
				CustomerType.class, listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=CustomerType查询数据.xls");
		workbook.write(response.getOutputStream());
	}
}
