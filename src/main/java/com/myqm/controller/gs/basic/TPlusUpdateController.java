package com.myqm.controller.gs.basic;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;  
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation; 
import com.myqm.controller.base.BaseController;  
import com.myqm.service.gs.basic.TPlusService;  

@Api(tags = "TPlus同步")
@RestController
@Validated
@RequestMapping("/api/Tplus")
public class TPlusUpdateController extends BaseController {

	@Autowired
	public TPlusService tplusService;

	@ApiOperation(value = "同步客户信息")
	@GetMapping(value = "/customer")
	public ResultResponse<String> updateCustomer() { 
		return super.newResult(tplusService.updateCustomer());

	}

	@ApiOperation(value = "同步供应商信息")
	@GetMapping(value = "/provider")
	public ResultResponse<String> updateProvider() {
		return super.newResult(tplusService.updateProvider());

	}

	@ApiOperation(value = "同步仓库信息")
	@GetMapping(value = "/storehouse")
	public ResultResponse<String> updateStorehouse() {
		return super.newResult(tplusService.updateStorehouse());
	}

	@ApiOperation(value = "同步商品信息")
	@GetMapping(value = "/goods")
	public ResultResponse<String> updateGoods() {
		return super.newResult(tplusService.updateGoods());
	}
 
}
