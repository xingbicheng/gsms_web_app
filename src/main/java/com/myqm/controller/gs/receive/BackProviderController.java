package com.myqm.controller.gs.receive;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.gs.receive.BackProvider;
import com.myqm.service.gs.receive.BackService;
import com.myqm.vo.gs.receive.BackProviderQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


@Api(tags = "BackProvider")
@RestController
@Validated
@RequestMapping("/api/backProvider")
public class BackProviderController extends BaseController {

	@Autowired
	public BackService backProviderService;

//    @ApiOperation(value="新增记录")
//    @PostMapping(value="/add")
//    public ResultResponse<String> save(
//    	@ApiParam(required = true, value = "添加BackProvider")
//    	@Valid @RequestBody BackProvider record) {
//        return super.dbOperateResult(backProviderService.save(record));
//
//    }
//
//    @ApiOperation(value = "修改内容")
//    @PostMapping(value = "/edit")
//    public ResultResponse<String> editById(
//    	@ApiParam(required=true,value="修改BackProvider")
//		@Valid @RequestBody BackProvider record) {
//        return super.dbOperateResult(backProviderService.editById(record));
//
//    }
//
//	@ApiOperation(value="删除记录")
//	@GetMapping(value="/del")
//	public ResultResponse<String> delById(
//		@ApiParam(required=true,value="查询编号")
//        @RequestParam("id") @NotNull(message = "id不能为空") String id) {
//        return super.dbOperateResult(backProviderService.delById(id));
//    }

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<BackProvider> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        BackProvider record=backProviderService.getById(id);
        return new ResultResponse<BackProvider>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<BackProvider>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid BackProviderQuery param) {
        return new ResultResponse<PageTableData<BackProvider>>(
            backProviderService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<BackProvider>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  BackProviderQuery param) {
        return new ResultResponse<List<BackProvider>>(
            backProviderService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid BackProviderQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <BackProvider> listData= backProviderService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "BackProvider查询数据", 
        	"BackProvider查询数据"), BackProvider.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=BackProvider查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid BackProviderQuery param, HttpServletResponse response) 
    	throws Exception {
        List<BackProvider> listData= backProviderService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "BackProvider查询数据", "BackProvider查询数据"), BackProvider.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=BackProvider查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
