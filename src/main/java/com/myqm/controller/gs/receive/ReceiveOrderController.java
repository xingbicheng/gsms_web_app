package com.myqm.controller.gs.receive;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BackInfo;
import com.myqm.pojo.base.DetailInfo;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.Order.OrderState;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.gs.receive.BackService;
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.vo.gs.order.OrderQuery.SysState;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "结算单")
@RestController
@Validated
@RequestMapping("/api/receive")
public class ReceiveOrderController extends BaseController {
	@Autowired
	public OrderService orderService; 
	@Autowired 
	private BackService backService;
	
	@ApiOperation(value = "根据订单明细id查询相应的供货商信息")
	@GetMapping(value = "/getProviders")
	public ResultResponse<List<StroehouseIn>> getProviders(
			@ApiParam(required = true, value = "订单明细id") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		List<StroehouseIn>providers=backService.getProviders(id);		
		return new ResultResponse<List<StroehouseIn>>(providers);
	}
	
	@ApiOperation(value = "订单明细退货")
	@PostMapping(value = "/backGood")
	public ResultResponse<String> backGood(  
			@ApiParam(required = true, value = "退货信息") @Valid @RequestBody BackInfo backInfo) {
		String result=backService.backGood(backInfo); 
		return new ResultResponse<String>(0,result,backInfo.getDetailId());
 
	}
	

	@ApiOperation(value = "一键订单批量收货")
	@GetMapping(value = "/receiveOrder")
	public ResultResponse<String>receiveOrders(
		@ApiParam(required = true, value = "查询编号") 
		@RequestParam("ids") @NotNull(message = "ids不能为空")  List<String> ids){
		String re=backService.receiveOrders(ids);
		return super.newResult(re);
		
	}
	
	
	@ApiOperation(value = "一键收货")
	@PostMapping(value = "/receiveGoods")
	public ResultResponse<String>receiveGoods(
		@ApiParam(required = true, value = "查询编号") @RequestBody DetailInfo record){
		String re=backService.receiveGoods(record);
		return new ResultResponse<String>(0,re);
		
	}
	
	@ApiOperation(value = "取消收货")
	@GetMapping(value = "/cancelReceive")
	public ResultResponse<String>cancelReceive(
			@ApiParam(required = true, value = "订单明细id") @RequestParam("id") @NotNull(message = "id不能为空") String id){
		String s=backService.cancelReceive(id);
		return new ResultResponse<String>(0,s);
		
	}


 

	@ApiOperation(value = "下一个order信息")
	@GetMapping(value = "next")
	public ResultResponse<Order> getNext(
			@Valid OrderQuery param) {
		param.setSysState(SysState.receive);
		//param.setState(OrderState.out); 
		Order record = orderService.getNext(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有下一条");
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "上一个order信息")
	@GetMapping(value = "pre")
	public ResultResponse<Order> getPre(
			@Valid OrderQuery param) { 
		param.setSysState(SysState.receive);
		Order record = orderService.getPre(param);
		if(record==null) return new ResultResponse<Order>(-1,"没有上一条");
		return new ResultResponse<Order>(record);
	}
 

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Order> getById(
			@ApiParam(required = true, value = "查询编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Order record = orderService.getById(id);
		return new ResultResponse<Order>(record);
	}

	@ApiOperation(value = "详细列表分页查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Order>> list(
			@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		param.setSysState(SysState.receive);
		return new ResultResponse<PageTableData<Order>>(
				orderService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/all")
	public ResultResponse<List<Order>> all(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param) {
		param.setState(OrderState.out);
		return new ResultResponse<List<Order>>(orderService.listAll(param));
	}

	@ApiOperation(value = "excel 导出")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param, HttpServletResponse response)
			throws Exception {
		param.setSysState(SysState.receive);
		PageTableData<Order> listData = orderService.list(param.getCurrentPage(), param.getPageSize(), param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	}

	@ApiOperation(value = "excelAll 导出(不分页)")
	@GetMapping(value = "/excelAll")
	public void excelAll(@ApiParam(required = false, value = "查询参数") @Valid OrderQuery param,
			HttpServletResponse response) throws Exception {
		param.setSysState(SysState.receive);
		List<Order> listData = orderService.listAll(param);
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("Order查询数据", "Order查询数据"), Order.class,
				listData);
		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=Order查询数据.xls");
		workbook.write(response.getOutputStream());
	} 
	
}
