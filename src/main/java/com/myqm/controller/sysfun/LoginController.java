package com.myqm.controller.sysfun;

import java.awt.Color;
import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.myqm.pojo.sysfun.SysWxUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.config.data.sysfun.CaptchaCfg;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.security.authorization.Token;
import com.myqm.service.security.authorization.CaptchaService;
import com.myqm.service.security.authorization.TokenService;
import com.myqm.utils.ResponseUtil;
import com.myqm.utils.UserUtil;

import cn.apiclub.captcha.Captcha;
import cn.apiclub.captcha.backgrounds.GradiatedBackgroundProducer;
import cn.apiclub.captcha.text.renderer.DefaultWordRenderer;
import cn.apiclub.captcha.text.renderer.WordRenderer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "登录")
@RestController
@Validated
@RequestMapping("/")
public class LoginController {

	@Autowired
	private TokenService tokenService;

	@Autowired
	private CaptchaService captchaService;

	@Autowired
	private CaptchaCfg captchaCfg;
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenService tokenservice;

	static class LoginInfo {
		@NotNull
		@ApiModelProperty(value = "用户名")
		String username;
		@NotNull
		@ApiModelProperty(value = "密码")
		String password;
		@ApiModelProperty(value = "验证码")
		String captcha;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getCaptcha() {
			return captcha;
		}

		public void setCaptcha(String captcha) {
			this.captcha = captcha;
		}

	}


	@GetMapping(value = "/login")
	@ApiParam(required = true, value = "登录信息")
	@ApiOperation(value = "登录")
	public void login(@Valid LoginInfo loginInfo, HttpServletRequest request, HttpServletResponse response) {

		String username = loginInfo.getUsername().trim();
		String password = loginInfo.getPassword().trim();
		//String captcha = loginInfo.getCaptcha().trim();

//		// 验证码校验
//		if (!this.checkCaptcha(captcha, request)) {
//			ResultResponse<Object> result = new ResultResponse<Object>(1, "catcha code error!");
//			ResponseUtil.responseJson(response, HttpStatus.OK.value(), result);
//			return;
//		}
		try {
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username,
					password);
			Authentication authentication = authenticationManager.authenticate(authRequest);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			HttpSession session = request.getSession();
			session.setAttribute("SPRING_SECURITY_CONTEXT", SecurityContextHolder.getContext());

			LoginUser loginUser = (LoginUser) authentication.getPrincipal();
			Token token = tokenService.saveToken(loginUser);
			ResultResponse<Token> info = new ResultResponse<Token>(token);
			ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
		} catch (AuthenticationException ex) {
			ResultResponse<String> info = new ResultResponse<String>(-1, "username or password error!");
			ResponseUtil.responseJson(response, HttpStatus.OK.value(), info);
		}
	}

	@ApiOperation(value = "获取验证码")
	@RequestMapping(value = "getcaptcha", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getCaptcha(HttpServletResponse response) {

		GradiatedBackgroundProducer gbp = new GradiatedBackgroundProducer();
		gbp.setFromColor(Color.BLUE);
		gbp.setToColor(Color.WHITE);
		// 生成验证码
		// Captcha captcha = new Captcha.Builder(captchaCfg.getCaptchaWidth(),
		// captchaCfg.getCaptchaHeight()).addText()
		// .addBackground(gbp).gimp(new DropShadowGimpyRenderer()).addText(new
		// ColoredEdgesWordRenderer())
		// // .gimp(new FishEyeGimpyRenderer())
		// .addNoise().addNoise().addNoise().addBorder().build();

		List<Font> fonts = new ArrayList<>();
		List<Color> colors = new ArrayList<>();
		fonts.add(new Font("cmex10", Font.BOLD, 42));
		colors.add(Color.BLUE);
		WordRenderer wr = new DefaultWordRenderer(colors, fonts);
		Captcha captcha = new Captcha.Builder(captchaCfg.getCaptchaWidth(), captchaCfg.getCaptchaHeight()).addText(wr)
				.addNoise().addNoise()
				// .gimp(new FishEyeGimpyRenderer())
				.build();
		String uuid = captchaService.saveCaptcha(captcha.getAnswer());

		// 将验证码key，及验证码的图片返回
		Cookie cookie = new Cookie(captchaCfg.getCaptchaCookieName(), uuid);
		response.addCookie(cookie);
		cookie.setHttpOnly(true);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		try {
			ImageIO.write(captcha.getImage(), "png", bao);
			return bao.toByteArray();
		} catch (IOException e) {
			return null;
		}
	}

	// 检验验证码
	protected boolean checkCaptcha(String captcha, HttpServletRequest request) {
		String captchaId = com.myqm.utils.CookieUtils.getCookie(request, captchaCfg.getCaptchaCookieName());
		if (captchaId == null) {
			return false;
		}
		return captchaService.checkCaptcha(captchaId, captcha);
	}
	@RequestMapping("/logout")
	    @ApiParam(required = false, value = "")
	    @ApiOperation(value = "登出")
	    public ResultResponse<Object> loginOut(HttpServletRequest request, HttpServletResponse response) {
	        String id = UserUtil.getLoginUser().getId();
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        HttpSession session = request.getSession();
	        if (session == null) {
	            return new ResultResponse<Object>(0,"现在处于未登陆状态",null);
	        }
	        session.removeAttribute("SPRING_SECURITY_CONTEXT");
	        tokenService.deleteToken(UserUtil.getLoginUser().getToken());
	        if (auth != null){
	            new SecurityContextLogoutHandler().logout(request, response, auth);
	        }
	        return new ResultResponse<Object>(0, "退出登录成功", null);
	    }

}
