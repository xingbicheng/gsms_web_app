package com.myqm.controller.sysfun;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.dao.sysfun.RoleDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.sysfun.Role;
import com.myqm.service.sysfun.RoleService;
import com.myqm.vo.sysfun.QueryRole;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 角色相关接口
 * 
 */
@Api(tags = "角色")
@Validated
@RestController
@RequestMapping("/api/roles")
public class RoleController extends BaseController {
	@Autowired
	private RoleService roleService;

	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping(value = "/add")
	public ResultResponse<String> saveRole(@Valid @RequestBody Role record) {
		return super.dbOperateResult(roleService.save(record));
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editRole(@Valid @RequestBody Role record) {
		return super.dbOperateResult(roleService.edit(record));
	}

	@ApiOperation(value = "批量修改角色状态")
	@ApiParam(required = true, value = "批量角色id")
	@PostMapping(value = "/allactivation")
	public ResultResponse<String> editActivation(@Valid @RequestBody List<Role> record) {
	    int re=roleService.editActivation(record);
		return new ResultResponse<>(re,re==0?"状态修改成功":"状态修改失败");
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Role> getRoleById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Role Role = roleService.getById(id);
		return new ResultResponse<Role>(Role);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delRoleById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(roleService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@DeleteMapping(value = "/delall")
	public ResultResponse<String> delAllRole(@RequestBody @Valid DeleteList list) {
		return super.dbDelOperateResult(roleService.delAll(list.getIds()));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Role>> listRole(
			@ApiParam(required = false, value = "查询参数") @Valid QueryRole param) {
		return new ResultResponse<PageTableData<Role>>(
				roleService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/listbrif")
	public ResultResponse<PageTableData<Role>> listRoleBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryRole param) {
		return new ResultResponse<PageTableData<Role>>(
				roleService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/allbrif")
	public ResultResponse<List<Role>> listAllRoleBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryRole param) {
		return new ResultResponse<List<Role>>(roleService.listAllBrif(param));
	}

	@GetMapping("/allrole")
	@ApiOperation(value = "所有角色")
	// @PreAuthorize("hasAnyAuthority('sys:user:query','sys:role:query')")
	public ResultResponse<List<Role>> roles(

            @ApiParam(required = false, value = "查询参数") @Valid QueryRole param
    ) {
		return new ResultResponse<List<Role>>(roleService.listAllBrif(param));
	}

	@GetMapping("/user/get")
	@ApiOperation(value = "根据用户id获取拥有的角色")
	@ApiParam(required = true, value = "用户id")
	// @PreAuthorize("hasAnyAuthority('sys:user:query','sys:role:query')")
	public ResultResponse<List<Role>> roles(@Valid @NotNull String userId) {
		return new ResultResponse<List<Role>>(roleService.listByUserId(userId));
	}

	@ApiOperation(value = "设置功能权限")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping(value = "/set")
	public ResultResponse<Integer> setRole(@Valid @RequestBody RolePermissionSet permission) {
		int count = roleService.saveRolePermission(permission.getRoleId(), permission.getPermissionIds());
		return new ResultResponse<Integer>(count);

	}

	@ApiOperation(value = "excel 导出")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid QueryRole param, HttpServletResponse response)
			throws Exception {
		PageTableData<Role> listData = roleService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), Role.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");

		workbook.write(response.getOutputStream());

	}

	public static class RolePermissionSet {
		@NotNull
		@ApiModelProperty(value = "角色Id")
		String roleId;

		@NotNull
		@Size(min = 1)
		@ApiModelProperty(value = "许可Ids数组")
		List<String> permissionIds;

		public String getRoleId() {
			return roleId;
		}

		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}

		public List<String> getPermissionIds() {
			return permissionIds;
		}

		public void setPermissionIds(List<String> permissionIds) {
			this.permissionIds = permissionIds;
		}

	}

}
