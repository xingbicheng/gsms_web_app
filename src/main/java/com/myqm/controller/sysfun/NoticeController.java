package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myqm.controller.base.BaseController;
import com.myqm.dao.sysfun.NoticeDao;
import com.myqm.dto.NoticeVO;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.pojo.sysfun.Notice;
import com.myqm.pojo.sysfun.Notice.Status;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.sysfun.NoticeService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QueryDict;
import com.myqm.vo.sysfun.QueryNotice;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "公告")
@RestController
@RequestMapping("/api/notices")
public class NoticeController extends BaseController {
	@Autowired
	private NoticeService noticeService;

	@PostMapping(value = "/add")
	@ApiOperation(value = "保存公告")
	//@PreAuthorize("hasAuthority('notice:add')")
	public ResultResponse<String> saveNotice(@RequestBody @Valid Notice notice) {
		return super.dbOperateResult(noticeService.save(notice));
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@GetMapping(value = "/edit")
	public ResultResponse<String> editNotice(@Valid @RequestBody Notice record) {
		return super.dbOperateResult(noticeService.edit(record));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Notice> getNoticeById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Notice Notice = noticeService.getById(id);
		return new ResultResponse<Notice>(Notice);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delNoticeById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(noticeService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@PostMapping(value = "/delall")
	public ResultResponse<String> delAllNotice(@RequestBody @Valid DeleteList list) {
		return super.dbDelOperateResult(noticeService.delAll(list.getIds()));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "管理员列表查询")
	@GetMapping(value = "/admin/listbrif")
	public ResultResponse<PageTableData<Notice>> listNoticeBrifByAdmin(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "管理员详细列表查询")
	@GetMapping(value = "/admin/list")
	public ResultResponse<PageTableData<Notice>> listNoticeByAdmin(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Notice>> listNotice(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		param.setIsPrivated("0");
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "列表查询")
	@GetMapping(value = "/listbrif")
	public ResultResponse<PageTableData<Notice>> listNoticeBrifByPrivate(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	@ApiOperation(value = "私有详细列表查询")
	@GetMapping(value = "/private/list")
	public ResultResponse<PageTableData<Notice>> listNoticeByPrivate(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		param.setUserId(UserUtil.getLoginUser().getId());
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.Privatelist(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "私有列表查询")
	@GetMapping(value = "/private/listbrif")
	public ResultResponse<PageTableData<Notice>> listNoticeBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param) {
		param.setUserId(UserUtil.getLoginUser().getId());
		return new ResultResponse<PageTableData<Notice>>(
				noticeService.PrivatelistBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	// /**
	// * 查询所有简要记录
	// * @param param
	// * 查询条件
	// * @return 记录
	// */
	// @ApiOperation(value = "详细列表查询")
	// @PostMapping(value = "allbrif")
	// public ResultResponse<List<Notice>> listAllNoticeBrif(
	// @ApiParam(required = false, value = "查询参数")
	// @RequestBody @Valid QueryNotice param) {
	// return new ResultResponse<List<Notice>>(super.listAllBrif(param));
	// }

	@ApiOperation(value = "读公告的所有用户")
	@GetMapping(params = "/allreaders")
	public ResultResponse<NoticeVO> readNotice(
			@ApiParam(required = false, value = "通告id") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Notice notice = noticeService.getBrifById(id);
		if (notice == null || notice.getStatus() == Status.DRAFT) {
			return new ResultResponse<NoticeVO>(1, "notice is draft.");
		}
		NoticeVO vo = new NoticeVO();
		vo.setNotice(notice);
		List<SysUser> users = noticeService.listReadUsers(id);
		vo.setUsers(users);
		return new ResultResponse<NoticeVO>(vo);
	}

	@ApiOperation(value = "未读公告数")
	@GetMapping("/unreads")
	public ResultResponse<Integer> countUnread() {
		SysUser user = UserUtil.getLoginUser();
		return new ResultResponse<Integer>(noticeService.countUnread(user.getId()));
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid QueryNotice param,
			HttpServletResponse response) throws IOException {
		// 私有访问 不同
		PageTableData<Notice> listData;
		if (param.getIsPrivated() == "1") {
			listData = noticeService.Privatelist(param.getCurrentPage(), param.getPageSize(), param);
		} else {
			listData = noticeService.list(param.getCurrentPage(), param.getPageSize(), param);
		}

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), Notice.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}

}
