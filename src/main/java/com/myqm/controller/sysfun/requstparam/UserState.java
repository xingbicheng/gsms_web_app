package com.myqm.controller.sysfun.requstparam;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class UserState {
	@NotNull
	@Min(1)
	@ApiModelProperty(value = "用户Id")
	String id;

	@NotNull
	@Min(0)
	@Max(2)
	@ApiModelProperty(value = "状态 DISABLED 0;VALID 1;LOCKED 2;")
	int state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}