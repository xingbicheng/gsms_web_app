package com.myqm.controller.sysfun.requstparam;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class ChangePsw {
	@ApiModelProperty(value = "旧密码")
	@NotNull
	//(min = 6, max = 18)
	String oldPassword;
	@ApiModelProperty(value = "新密码")
	//@Pattern(regexp = "^[a-zA-Z]\\w{5,17}$")
	@NotNull
	//@Size(min = 6, max = 18)
	String newPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
