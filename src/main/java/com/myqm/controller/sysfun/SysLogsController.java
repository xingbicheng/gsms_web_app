package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.vo.sysfun.QueryDict;
import com.myqm.vo.sysfun.QuerySysLogs;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "日志")
@RestController
@RequestMapping("/api/logs")
public class SysLogsController extends BaseController {
	@Autowired
	SysLogsService logService;

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<SysLogs> getSysLogsById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		SysLogs SysLogs = logService.getById(id);
		return new ResultResponse<SysLogs>(SysLogs);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delSysLogsById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(logService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@PostMapping(value = "/delall")
	public ResultResponse<String> delAllSysLogs(@RequestBody @Valid DeleteList list) {
		return super.dbDelOperateResult(logService.delAll(list.getIds()));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<SysLogs>> listSysLogs(
			@ApiParam(required = false, value = "查询参数") @Valid QuerySysLogs param) {
		return new ResultResponse<PageTableData<SysLogs>>(
				logService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/listbrif")
	public ResultResponse<PageTableData<SysLogs>> listSysLogsBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QuerySysLogs param) {
		return new ResultResponse<PageTableData<SysLogs>>(
				logService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	// /**
	// * 查询所有简要记录
	// * @param param
	// * 查询条件
	// * @return 记录
	// */
	// @ApiOperation(value = "详细列表查询")
	// @ApiParam(required = true, value = "查询分页参数")
	// @PostMapping(value = "/allbrif")
	// public ResultResponse<List<SysLogs>> listAllSysLogsBrif(@RequestBody
	// @Valid QuerySysLogs param) {
	// return new ResultResponse<List<SysLogs>>(super.listAllBrif(param));
	// }

	@ApiOperation(value = "删除选选年份，几月到几月的日志")
	@GetMapping(value = "/delmonth")
	public ResultResponse<Integer> delMonthSysLogs(@ApiParam(required = true, value = "年") @Valid @Min(2018) int year,
			@ApiParam(required = true, value = "开始的月份") @Min(1) @Max(12) int start,
			@ApiParam(required = true, value = "开始的月份") @Min(1) @Max(12) int end) {
		int count = logService.delMonthLogs(year, start, end);
		return new ResultResponse<Integer>(count);
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid @RequestBody QuerySysLogs param,
			HttpServletResponse response) throws IOException {
		PageTableData<SysLogs> listData = logService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), SysLogs.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}

	//
	// @ApiOperation(value="导出excel")
	// @ApiParam(required = true, value="查询参数")
	// @PostMapping(value="/listbrifexcel")
	// public void listbrifexcel(
	// @ApiParam(required = false,value = "查询参数")
	// @Valid @RequestBody QuerySysLogs param,HttpServletResponse response)
	// throws IOException {
	// PageTableData<SysLogs> listData =
	// logService.listBrif(param.getPage(), param.getPageSize(), param);
	//
	// Workbook workbook =
	// ExcelExportUtil.exportExcel(new ExportParams(),
	// SysLogs.class,listData.getData());
	//
	// // 告诉浏览器用什么软件可以打开此文件
	// response.setHeader("content-Type", "application/vnd.ms-excel");
	// // 下载文件的默认名称
	// response.setHeader("Content-Disposition",
	// "attachment;filename=iftSystemCode.xls");
	// workbook.write(response.getOutputStream());
	// }

}
