package com.myqm.controller.sysfun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.myqm.controller.base.BaseController;
import com.myqm.dao.sysfun.PermissionDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.service.sysfun.PermissionService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QueryPermission;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;

import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 权限相关接口
 * 
 */
@Api(tags = "权限")
@Validated
@RestController
@RequestMapping("/api/permissions")
public class PermissionController extends BaseController {
	@Autowired
	private PermissionService permissionService;

	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping(value = "/add")
	public ResultResponse<String> savePermission(@Valid @RequestBody Permission record) {
		String re=(permissionService.save(record));
		return new ResultResponse<>("新增成功".equals(re)?0:1,re);
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editPermission(@Valid @RequestBody Permission record) {
		return super.dbOperateResult(permissionService.edit(record));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<Permission> getPermissionById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Permission Permission = permissionService.getById(id);
		return new ResultResponse<Permission>(Permission);
	}
  

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delPermissionById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(permissionService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@DeleteMapping(value = "/delall")
	public ResultResponse<String> delAllPermission(@RequestBody @Valid DeleteList list) {
		return super.dbDelOperateResult(permissionService.delAll(list.getIds()));
	}

	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<Permission>> listPermission(
			@ApiParam(required = false, value = "查询参数") @Valid QueryPermission param) {
		return new ResultResponse<PageTableData<Permission>>(
				permissionService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

    @ApiOperation(value = "获取子菜单")
    @ApiParam(required = true, value = "查询分页参数")
    @GetMapping(value = "/getchild")
    public ResultResponse<List<Permission>> getChildMenu(@ApiParam(required = true, value = "当前菜单id") @RequestParam("id") @NotNull(message = "id不能为空")  String id) {
        return new ResultResponse<List<Permission>>(permissionService.childMenu(id));
    }

	@ApiOperation(value = "查询所有")
	@GetMapping(value = "/listall")
	public ResultResponse<List<Permission>> listAll(
			@ApiParam(required = false, value = "查询参数") @Valid QueryPermission param
			) {
		return new ResultResponse<List<Permission>>(permissionService.listAll(param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/listbrif")
	public ResultResponse<PageTableData<Permission>> listPermissionBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryPermission param) {
		return new ResultResponse<PageTableData<Permission>>(
				permissionService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/allbrif")
	public ResultResponse<List<Permission>> listAllPermissionBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryPermission param) {
		return new ResultResponse<List<Permission>>(permissionService.listAllBrif(param));
	}

	/**
	 * 当前用户树形菜单
	 * 
	 * @return
	 */
	@ApiOperation(value = "当前登录用户拥有的权限, 树形结构权限")
	@GetMapping("/current/tree")
	public ResultResponse<List<Permission>> permissionsCurrentTree() {
		LoginUser loginUser = UserUtil.getLoginUser();
		List<Permission> list = loginUser.getPermissions();
		final List<Permission> permissions = list.stream().filter(l -> l.getType().equals(1))
				.collect(Collectors.toList());

		setChild(permissions);
		return new ResultResponse<List<Permission>>(
				permissions.stream().filter(p -> p.getParentId().equals(0L)).collect(Collectors.toList()));
	}

	/**
	 * 当前用户菜单列表
	 * 
	 * @return
	 */
	@ApiOperation(value = "当前登录用户拥有的权限, 表结构")
	@GetMapping("/current/list")
	public ResultResponse<List<Permission>> permissionsCurrent() {
		LoginUser loginUser = UserUtil.getLoginUser();
		List<Permission> list = loginUser.getPermissions();
		return new ResultResponse<List<Permission>>(list);
	}

	private void setChild(List<Permission> permissions) {
		permissions.parallelStream().forEach(per -> {
			List<Permission> child = permissions.stream().filter(p -> p.getParentId().equals(per.getId()))
					.collect(Collectors.toList());
			per.setChild(child);
		});
	}

	/**
	 * 菜单列表
	 * 
	 * @param pId
	 * @param permissionsAll
	 * @param list
	 */
	private void setPermissionsList(String pId, List<Permission> permissionsAll, List<Permission> list) {
		for (Permission per : permissionsAll) {
			if (per.getParentId().equals(pId)) {
				list.add(per);
				if (permissionsAll.stream().filter(p -> p.getParentId().equals(per.getId())).findAny() != null) {
					setPermissionsList(per.getId(), permissionsAll, list);
				}
			}
		}
	}

	/**
	 * 所有菜单树列表
	 * 
	 * @return
	 */
	@GetMapping("/menu/tree")
	@ApiOperation(value = "所有菜单树列表")
	// @PreAuthorize("hasAuthority('sys:menu:query')")
	public ResultResponse<List<Permission>> permissionsTree() {
		List<Permission> list = permissionService.listAllBrif(null); 
		List<Permission> parentList = new ArrayList<Permission>();
		for (Permission per : list) {
			if (per.getParentId().equals("0")) { 
					parentList.add(per);
					setPermissionsTree(per, list);
				}
		}
			 
		return new ResultResponse<List<Permission>>(parentList);
	}

	/**
	 * 所有一级菜单
	 * 
	 * @return
	 */
	@GetMapping("/parents")
	@ApiOperation(value = "一级菜单")
	// @PreAuthorize("hasAuthority('sys:menu:query')")
	public ResultResponse<List<Permission>> parentMenu() {
		List<Permission> parents = permissionService.listParents();
		return new ResultResponse<List<Permission>>(parents);
	}

	/**
	 * 菜单树
	 * 
	 * @param pId
	 * @param permissionsAll
	 * @param array
	 */
	private void setPermissionsTree(Permission parent, List<Permission> permissionsAll) {
		for (Permission per : permissionsAll) {
			if (per.getParentId().equals(parent.getId())) { 
					if (parent.getChild() == null)
						parent.setChild(new ArrayList<Permission>());
				 	parent.getChild().add(per);
					setPermissionsTree(per, permissionsAll);
				}
			 
		}
	}

	/**
	 * 获取角色权限
	 * 
	 * @param roleId
	 * @return
	 */
	@GetMapping("/role")
	@ApiOperation(value = "根据角色id获取权限")
	// @PreAuthorize("hasAnyAuthority('sys:menu:query','sys:role:query')")
	public ResultResponse<List<Permission>> listByRoleId(@Valid @NotNull String roleId) {
		return new ResultResponse<List<Permission>>(permissionService.listByRoleId(roleId));
	}

	/**
	 * 校验权限
	 * 
	 * @return
	 */
	@GetMapping("/owns")
	@ApiOperation(value = "校验当前用户的权限")
	public ResultResponse<Set<String>> ownsPermission() {
		List<Permission> permissions = UserUtil.getLoginUser().getPermissions();
		if (CollectionUtils.isEmpty(permissions)) {
			return new ResultResponse<Set<String>>(Collections.emptySet());
		}
		return new ResultResponse<Set<String>>(
				permissions.parallelStream().filter(p -> !StringUtils.isEmpty(p.getPermission()))
						.map(Permission::getPermission).collect(Collectors.toSet()));
	}

	@ApiOperation(value = "excel 导出")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/excel")
	public void excel(@ApiParam(required = false, value = "查询参数") @Valid QueryPermission param,
			HttpServletResponse response) throws Exception {
		PageTableData<Permission> listData = permissionService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), Permission.class, listData.getRows());

		response.setHeader("content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");

		workbook.write(response.getOutputStream());

	}
}
