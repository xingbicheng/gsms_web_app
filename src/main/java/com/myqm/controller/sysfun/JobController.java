package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.pojo.sysfun.JobModel;
import com.myqm.service.sysfun.JobService;
import com.myqm.vo.sysfun.QueryDict;
import com.myqm.vo.sysfun.QueryJobModel;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "任务")
// @RestController
@Validated
@RequestMapping("/api/JobModels")
public class JobController extends BaseController {
	@Autowired
	private JobService jobService;

	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping(value = "/add")
	public ResultResponse<String> saveJobModel(@Valid @RequestBody JobModel record) {
		return super.dbOperateResult(jobService.save(record));
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping(value = "/edit")
	public ResultResponse<String> editJobModel(@Valid @RequestBody JobModel record) {

		return super.dbOperateResult(jobService.edit(record));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping(value = "/get")
	public ResultResponse<JobModel> getJobModelById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		JobModel JobModel = jobService.getById(id);
		return new ResultResponse<JobModel>(JobModel);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping(value = "/del")
	public ResultResponse<String> delJobModelById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(jobService.delById(id));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/list")
	public ResultResponse<PageTableData<JobModel>> listJobModel(
			@ApiParam(required = false, value = "查询参数") @Valid @RequestBody QueryJobModel param) {
		return new ResultResponse<PageTableData<JobModel>>(
				jobService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@PostMapping(value = "/listbrif")
	public ResultResponse<PageTableData<JobModel>> listJobModelBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryJobModel param) {
		return new ResultResponse<PageTableData<JobModel>>(
				jobService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping(value = "/allbrif")
	public ResultResponse<List<JobModel>> listAllJobModelBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryJobModel param) {
		return new ResultResponse<List<JobModel>>(jobService.listAllBrif(param));
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid QueryJobModel param,
			HttpServletResponse response) throws IOException {
		PageTableData<JobModel> listData = jobService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), JobModel.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}
}
