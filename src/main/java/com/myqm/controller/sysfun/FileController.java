package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.sysfun.FileInfo;
import com.myqm.service.sysfun.FileService;
import com.myqm.vo.sysfun.QueryFileInfo;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "文件")
@RestController
@Validated
@RequestMapping("/api/files")
public class FileController extends BaseController {
	@Autowired
	private FileService fileService;

	@PostMapping("/upload")
	@ApiOperation(value = "文件上传")
	public ResultResponse<FileInfo> uploadFile(@Valid MultipartFile file) throws IOException {
		FileInfo fileinfo = fileService.save(file);
		return new ResultResponse<FileInfo>(fileinfo);
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping("/get")
	public ResultResponse<FileInfo> getFileInfoById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		FileInfo FileInfo = fileService.getById(id);
		return new ResultResponse<FileInfo>(FileInfo);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping("/del")
	public ResultResponse<String> delFileInfoById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(fileService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@PostMapping("/delall")
	public ResultResponse<String> delAllFileInfo(@RequestBody @Valid DeleteList list) {
		return super.dbDelOperateResult(fileService.delAll(list.getIds()));
	}

	@ApiOperation(value = "详细列表查询")
	@GetMapping("/list")
	public ResultResponse<PageTableData<FileInfo>> listFileInfo(
			@ApiParam(required = false, value = "查询参数") @Valid QueryFileInfo param) {
		return new ResultResponse<PageTableData<FileInfo>>(
				fileService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@PostMapping("/listbrif")
	public ResultResponse<PageTableData<FileInfo>> listFileInfoBrif(

			@ApiParam(required = false, value = "查询参数") @Valid @RequestBody QueryFileInfo param) {
		return new ResultResponse<PageTableData<FileInfo>>(
				fileService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@PostMapping("/allbrif")
	public ResultResponse<List<FileInfo>> listAllFileInfoBrif(
			@ApiParam(required = false, value = "查询参数") @RequestBody @Valid QueryFileInfo param) {
		return new ResultResponse<List<FileInfo>>(fileService.listAllBrif(param));
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@PostMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid @RequestBody QueryFileInfo param,
			HttpServletResponse response) throws IOException {
		PageTableData<FileInfo> listData = fileService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), FileInfo.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}

}