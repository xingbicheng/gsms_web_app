package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.myqm.dao.sysfun.PermissionDao;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.service.sysfun.PermissionService;
import io.swagger.annotations.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.myqm.controller.base.BaseController;
import com.myqm.controller.sysfun.requstparam.ChangePsw;
import com.myqm.controller.sysfun.requstparam.UserState;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.sysfun.SysUserService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QuerySysUser;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;

/**
 * 用户相关接口
 */
@Api(tags = "用户")
@RestController
@Validated
@RequestMapping("/api/users")
public class SysUserController extends BaseController {
	@Autowired
	private SysUserService userService;

	@Autowired
    PermissionService permissionService;

	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping("/add")
	public ResultResponse<String> saveSysUserInfo(@Valid @RequestBody SysUser record) {
		String password = "05b530ad0fb56286fe051d5f8be5b8453f1cd93f";
		record.setPassword(password);
		return super.dbOperateResult(userService.save(record));
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping("/edit")
	public ResultResponse<String> editSysUserInfo(@Valid @RequestBody SysUser record) {
		return super.dbOperateResult(userService.edit(record));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping("/get")
	public ResultResponse<SysUser> getSysUserInfoById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		SysUser SysUserInfo = userService.getById(id);
		return new ResultResponse<SysUser>(SysUserInfo);
	}

	@ApiOperation(value = "根据ID设置状态")
	@GetMapping("/state")
	public ResultResponse<String> delSysUserInfoById(
			@ApiParam(required = true, value = "更改信息") @Valid UserState state) {
		return super.dbOperateResult(userService.setState(state.getState(), state.getId()));
	}
//
//	 @ApiOperation(value = "用户删除")
//	 @ApiImplicitParams({
//	 @ApiImplicitParam(name = "id", value = "记录编号，唯一id",
//	 required = true, dataType = "string", paramType = "path") })
//	 @DeleteMapping(value = "/del")
//	 public ResultResponse<Long> delSysUserInfoById( long id) {
//	 Long count = super.delById(id);
//	 return new ResultResponse<Long>(count);
//	 }
	//
	// @ApiOperation(value = "批量删除")
	// @ApiParam(required = true, value = "记录编号")
	// @DeleteMapping(value = "/delall")
	// public ResultResponse<List<Long>> delAllSysUserInfo(@RequestBody
	// List<Long> list) {
	// List<Long> dels = super.delAll(list);
	// return new ResultResponse<List<Long>>(dels);
	// }

	@ApiOperation(value = "详细列表查询")
	@GetMapping("/list")
	public ResultResponse<PageTableData<SysUser>> listSysUserInfo(
			@ApiParam(required = false, value = "查询参数") @Valid QuerySysUser param) {
		return new ResultResponse<PageTableData<SysUser>>(
				userService.newList(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param param
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@GetMapping("/listbrif")
	public ResultResponse<PageTableData<SysUser>> listSysUserInfoBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QuerySysUser param) {
		return new ResultResponse<PageTableData<SysUser>>(
				userService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@ApiParam(required = true, value = "查询分页参数")
	@GetMapping("/allbrif")
	public ResultResponse<List<SysUser>> listAllSysUserInfoBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QuerySysUser param) {
		return new ResultResponse<List<SysUser>>(userService.listAllBrif(param));
	}

	@GetMapping("/updatepsw")
	@ApiOperation(value = "修改密码")
	public ResultResponse<Long> changePassword(@Valid ChangePsw psw) {
		SysUser user = this.getCurrentUser();
		userService.changePassword(user.getId(), psw.getOldPassword(), psw.getNewPassword());
		return new ResultResponse<Long>(user.getId());
	}

	@ApiOperation(value = "重置密码")
	@GetMapping("/reset")
	public ResultResponse<String> resetPsw(@RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(userService.resetPassword(id));
	}

	@ApiOperation(value = "当前登录用户")
	@GetMapping("/current")
	public ResultResponse<SysUser> currentUser() {
		return new ResultResponse<SysUser>(permissionService.getCurrent());
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid QuerySysUser param,
			HttpServletResponse response) throws IOException {
		PageTableData<SysUser> listData = userService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), SysUser.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}

	// @ApiOperation(value="导出excel")
	// @ApiParam(required = true, value="查询参数")
	// @PostMapping(value="/listbrifexcel")
	// public void listbrifexcel(
	// @ApiParam(required = false,value = "查询参数")
	// @Valid @RequestBody QuerySysUserInfo param,HttpServletResponse response)
	// throws IOException {
	// PageTableData<SysUserInfo> listData =
	// userService.listBrif(param.getPage(), param.getPageSize(), param);
	//
	// Workbook workbook =
	// ExcelExportUtil.exportExcel(new ExportParams(),
	// SysUserInfo.class,listData.getData());
	//
	// // 告诉浏览器用什么软件可以打开此文件
	// response.setHeader("content-Type", "application/vnd.ms-excel");
	// // 下载文件的默认名称
	// response.setHeader("Content-Disposition",
	// "attachment;filename=iftSystemCode.xls");
	// workbook.write(response.getOutputStream());
	// }
	//
	// @ApiOperation(value="导出excel")
	// @ApiParam(required = true, value="查询参数")
	// @PostMapping(value="/allbrifexcel")
	// public void allbrifexcel(
	// @ApiParam(required = false,value = "查询参数")
	// @Valid @RequestBody QuerySysUserInfo param,HttpServletResponse response)
	// throws IOException {
	// List<SysUserInfo> listData =
	// userService.listAllBrif(param);
	//
	// Workbook workbook =
	// ExcelExportUtil.exportExcel(new ExportParams(),
	// SysUserInfo.class,listData);
	//
	// // 告诉浏览器用什么软件可以打开此文件
	// response.setHeader("content-Type", "application/vnd.ms-excel");
	// // 下载文件的默认名称
	// response.setHeader("Content-Disposition",
	// "attachment;filename=iftSystemCode.xls");
	// workbook.write(response.getOutputStream());
	// }
}
