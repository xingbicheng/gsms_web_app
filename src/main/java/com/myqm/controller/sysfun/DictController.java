package com.myqm.controller.sysfun;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.annotation.LogAnnotation;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.pojo.base.DeleteList;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.service.sysfun.DictService;
import com.myqm.vo.sysfun.QueryDict;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "字典")
@RestController
@Validated
@RequestMapping("/api/dicts")
public class DictController extends BaseController {
	@Autowired
	private DictService dictService;

	@ApiOperation(value = "新增记录")
	@ApiParam(required = true, value = "记录值")
	@PostMapping("/add")
	public ResultResponse<String> saveDict(@Valid @RequestBody Dict record) {
		return super.dbOperateResult(dictService.save(record));
	}

	@ApiOperation(value = "修改记录")
	@ApiParam(required = true, value = "记录内容")
	@PostMapping("/edit")
	public ResultResponse<String> editDict(@Valid @RequestBody Dict record) {
		return super.dbOperateResult(dictService.edit(record));
	}

	@ApiOperation(value = "根据ID查询记录")
	@GetMapping("/get")
	public ResultResponse<Dict> getDictById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		Dict dict = dictService.getById(id);
		return new ResultResponse<Dict>(dict);
	}

	@ApiOperation(value = "根据ID删除记录")
	@GetMapping("/del")
	public ResultResponse<String> delDictById(
			@ApiParam(required = true, value = "查询记录编号") @RequestParam("id") @NotNull(message = "id不能为空") String id) {
		return super.dbOperateResult(dictService.delById(id));
	}

	@ApiOperation(value = "批量删除")
	@ApiParam(required = true, value = "记录编号")
	@DeleteMapping("/delall")
	public ResultResponse<String> delAllDict(@RequestBody @Valid DeleteList list) {
		List<String> dels = dictService.delAll(list.getIds());
		return dbDelOperateResult(dels);
	}

	@LogAnnotation(module = "list dict")
	@ApiOperation(value = "详细列表查询")
	@GetMapping("/list")
	public ResultResponse<PageTableData<Dict>> listDict(
			@ApiParam(required = false, value = "查询参数") @Valid QueryDict param) {
		return new ResultResponse<PageTableData<Dict>>(
				dictService.list(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@PostMapping("/listbrif")
	public ResultResponse<PageTableData<Dict>> listDictBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryDict param) {
		return new ResultResponse<PageTableData<Dict>>(
				dictService.listBrif(param.getCurrentPage(), param.getPageSize(), param));
	}

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	@ApiOperation(value = "详细列表查询")
	@GetMapping(value = "/allbrif")
	public ResultResponse<List<Dict>> listAllDictBrif(
			@ApiParam(required = false, value = "查询参数") @Valid QueryDict param) {
		return new ResultResponse<List<Dict>>(dictService.listAllBrif(param));
	}

	@ApiOperation(value = "导出excel")
	@ApiParam(required = true, value = "查询参数")
	@GetMapping(value = "/excel")
	public void listexcel(@ApiParam(required = false, value = "查询参数") @Valid QueryDict param,
			HttpServletResponse response) throws IOException {
		PageTableData<Dict> listData = dictService.list(param.getCurrentPage(), param.getPageSize(), param);

		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), Dict.class, listData.getRows());

		// 告诉浏览器用什么软件可以打开此文件
		response.setHeader("content-Type", "application/vnd.ms-excel");
		// 下载文件的默认名称
		response.setHeader("Content-Disposition", "attachment;filename=iftSystemCode.xls");
		workbook.write(response.getOutputStream());
	}
}