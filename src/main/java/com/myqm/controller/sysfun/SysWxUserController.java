package com.myqm.controller.sysfun;


import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.myqm.pojo.ResultResponse;
import com.myqm.pojo.base.BaseRecord; 
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.myqm.controller.base.BaseController;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.sysfun.SysWxUser;
import com.myqm.pojo.sysfun.SysUser.Status;
import com.myqm.vo.sysfun.SysWxUserQuery;
import com.myqm.service.sysfun.SysWxUserService;


@Api(tags = "SysWxUser")
@RestController
@Validated
@RequestMapping("/api/sysWxUser")
public class SysWxUserController extends BaseController {

	@Autowired
	public SysWxUserService sysWxUserService;

    @ApiOperation(value="申请微信小程序账号")
    @PostMapping(value="/add")
    public ResultResponse<String> save(
    	@ApiParam(required = true, value = "添加SysWxUser")
    	@Valid @RequestBody SysWxUser record) {
    	record.setStatus("0");
        return super.dbOperateResult(sysWxUserService.save(record));
 
    }


    @GetMapping(value="/status")
    @ApiParam(required = true, value = "添加状态")
    @ApiOperation(value="status")
    public ResultResponse<SysWxUser> status(@ApiParam(required = true, value = "添加状态")
                                                @RequestParam("id") @NotNull(message = "id不能为空") String id){
        SysWxUser record=sysWxUserService.getById(id);
        System.out.println("测试"+record.toString());
        SysWxUser sysWxUser=new SysWxUser();
        sysWxUser.setStatus(record.getStatus());
        sysWxUser.setType(record.getType());
        return new ResultResponse<SysWxUser>(sysWxUser);
    }
    
    @GetMapping(value="/get/byopenid")
    @ApiParam(required = true, value = "根据openID获取用户信息")
    @ApiOperation(value="根据openID获取用户信息")
    public ResultResponse<SysWxUser> getByOpenid(@ApiParam(required = true, value = "添加状态")
                                                @RequestParam("openId") @NotNull(message = "openId不能为空") String openId){
        SysWxUser record=sysWxUserService.getByOpenId(openId);
        return new ResultResponse<SysWxUser>(record);
    }
    
    @ApiOperation(value="审核微信小程序的申请")
    @PostMapping(value="/audited")
    public ResultResponse<String> audited(
    		@ApiParam(required = true, value = "添加SysWxUser")
        	@Valid @RequestBody SysWxUser record
    	) {
        return super.dbOperateResult(sysWxUserService.auited(record.getOpenId(),record.getUserId()));
 
    }
    
    
    @ApiOperation(value="获得当前微信账号的状态")
    @GetMapping(value="/getopenid")
    public ResultResponse<String> getOpenId(
        	@ApiParam(required = true, value = "查询编号")
        	@RequestParam("id") @NotNull(message = "id不能为空") String openId
    	) {
    	String openId2 = sysWxUserService.getOpenId(openId);
    	if(openId2==null)
    		return new ResultResponse<String>(0,"用户为空",null);
    	if(openId2.equals("0"))
    		return new ResultResponse<String>(0,"用户没有被审核","0");
    	if(openId2.equals("2"))
    		return new ResultResponse<String>(0,"用户被锁定","2");
    	
    	return new ResultResponse<String>(0,"用户可用","1");
    }
    
    @ApiOperation(value="审核不通过")
    @GetMapping(value="/unaudited")
    public ResultResponse<String> unaudited(
    		@ApiParam(required = true, value = "添加状态")
            @RequestParam("openId") @NotNull(message = "openId不能为空") String openId
    	) {
        return super.dbOperateResult(sysWxUserService.unaudited(openId));
 
    }
    
    @ApiOperation(value = "修改内容")
    @PostMapping(value = "/edit")
    public ResultResponse<String> editById(
    	@ApiParam(required=true,value="修改SysWxUser")
		@Valid @RequestBody SysWxUser record) { 
        return super.dbOperateResult(sysWxUserService.editById(record));
 
    } 
    
	@ApiOperation(value="删除记录")
	@GetMapping(value="/del")
	public ResultResponse<String> delById(
		@ApiParam(required=true,value="查询编号")
        @RequestParam("id") @NotNull(message = "id不能为空") String id) { 
        return super.dbOperateResult(sysWxUserService.delById(id));
    } 

    @ApiOperation(value = "根据ID查询记录")
    @GetMapping(value = "/get")
    public ResultResponse<SysWxUser> getById(
    	@ApiParam(required = true, value = "查询编号")
    	@RequestParam("id") @NotNull(message = "id不能为空") String id) {
        SysWxUser record=sysWxUserService.getById(id);
        return new ResultResponse<SysWxUser>(record);
    } 

    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/list")
    public ResultResponse<PageTableData<SysWxUser>> list(
    	@ApiParam(required = false, value = "查询参数")
        @Valid SysWxUserQuery param) {
        return new ResultResponse<PageTableData<SysWxUser>>(
            sysWxUserService.list(param.getCurrentPage(), 
            param.getPageSize(), param));
    } 
    
    @ApiOperation(value = "详细列表查询")
    @GetMapping(value = "/all")
    public ResultResponse<List<SysWxUser>> all(
    	@ApiParam(required = false, value = "查询参数")
        @Valid  SysWxUserQuery param) {
        return new ResultResponse<List<SysWxUser>>(
            sysWxUserService.listAll(param));
    } 

    @ApiOperation(value = "excel 导出")
    @GetMapping(value = "/excel")
    public void excel(
	    @ApiParam(required = false, value = "查询参数")
	    @Valid SysWxUserQuery param, HttpServletResponse response) 
	    throws Exception {
        PageTableData <SysWxUser> listData= sysWxUserService.list(
        	param.getCurrentPage(), param.getPageSize(),param);
        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams( "SysWxUser查询数据", 
        	"SysWxUser查询数据"), SysWxUser.class, listData.getRows());
        
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=SysWxUser查询数据.xls");
        workbook.write(response.getOutputStream());
    }

    @ApiOperation(value = "excelAll 导出(不分页)")
    @GetMapping(value = "/excelAll")
    public void excelAll(
    	@ApiParam(required = false, value = "查询参数")
    	@Valid SysWxUserQuery param, HttpServletResponse response) 
    	throws Exception {
        List<SysWxUser> listData= sysWxUserService.listAll(param);
        Workbook workbook = ExcelExportUtil.exportExcel(
        new ExportParams( "SysWxUser查询数据", "SysWxUser查询数据"), SysWxUser.class,listData);
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=SysWxUser查询数据.xls");
        workbook.write(response.getOutputStream());
    }
}
