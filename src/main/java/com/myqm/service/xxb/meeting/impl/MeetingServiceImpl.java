package com.myqm.service.xxb.meeting.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.meeting.MeetingDao;
import com.myqm.pojo.xxb.meeting.Meeting;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import com.myqm.service.xxb.meeting.MeetingService;


@Service
public class MeetingServiceImpl implements MeetingService{

	@Autowired
	public MeetingDao meetingDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (meetingDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(Meeting record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingDao.save(record) > 0)
		{
			return record.getId() + "";
		}
		else
			return null;
	}

	@Override
	public Meeting getById(String id) {
		return meetingDao.getById(id);
	}


	@Override
	public String editById(Meeting record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingDao.editById(record) > 0)
			return record.getId() + "";
		else
			return null;
	}

	@Override
	public PageTableData<Meeting> list(int pagenum, int pagesize,MeetingQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Meeting> datalist = meetingDao.list(param);
		PageTableData<Meeting> resultPage = new PageTableData<Meeting>(datalist);
		PageInfo<Meeting> p = new PageInfo<Meeting>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<Meeting> listAll(MeetingQuery param) {
		List<Meeting> datalist = meetingDao.list(param);
		return datalist;
	}

}
