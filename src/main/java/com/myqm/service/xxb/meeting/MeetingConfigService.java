package com.myqm.service.xxb.meeting;

import com.myqm.pojo.xxb.meeting.MeetingConfig;
import com.myqm.vo.xxb.meeting.MeetingConfigQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface MeetingConfigService {
    /**
     *根据id删除MeetingConfig
     */
    String delById(String id);

    /**
     *根据新增MeetingConfig，id自增
     */
    String save(MeetingConfig record);

    /**
     *根据id查询MeetingConfig
     */
    MeetingConfig getById(String id);

    /**
     *根据id更新MeetingConfig
     */
    String editById(MeetingConfig record);

    /**
     *分页查询MeetingConfig
     */
    PageTableData<MeetingConfig> list(int pagenum, int pagesize,MeetingConfigQuery param);

    /**
     *查询所有MeetingConfig
     */
    List<MeetingConfig> listAll(MeetingConfigQuery param);
}