package com.myqm.service.xxb.meeting.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.meeting.MeetingConfigDao;
import com.myqm.pojo.xxb.meeting.MeetingConfig;
import com.myqm.vo.xxb.meeting.MeetingConfigQuery;
import com.myqm.service.xxb.meeting.MeetingConfigService;


@Service
public class MeetingConfigServiceImpl implements MeetingConfigService{

	@Autowired
	public MeetingConfigDao meetingConfigDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (meetingConfigDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(MeetingConfig record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingConfigDao.save(record) > 0)
		{
			return record.getId() + "";
		}
		else
			return null;
	}

	@Override
	public MeetingConfig getById(String id) {
		return meetingConfigDao.getById(id);
	}


	@Override
	public String editById(MeetingConfig record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingConfigDao.editById(record) > 0)
			return record.getId() + "";
		else
			return null;
	}

	@Override
	public PageTableData<MeetingConfig> list(int pagenum, int pagesize,MeetingConfigQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<MeetingConfig> datalist = meetingConfigDao.list(param);
		PageTableData<MeetingConfig> resultPage = new PageTableData<MeetingConfig>(datalist);
		PageInfo<MeetingConfig> p = new PageInfo<MeetingConfig>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<MeetingConfig> listAll(MeetingConfigQuery param) {
		List<MeetingConfig> datalist = meetingConfigDao.list(param);
		return datalist;
	}

}
