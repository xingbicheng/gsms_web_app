package com.myqm.service.xxb.meeting;

import com.myqm.pojo.xxb.meeting.MeetingUser;
import com.myqm.vo.xxb.meeting.MeetingUserQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface MeetingUserService {
    /**
     *根据id删除MeetingUser
     */
    String delById(String id);

    /**
     *根据新增MeetingUser，id自增
     */
    String save(MeetingUser record);

    /**
     *根据id查询MeetingUser
     */
    MeetingUser getById(String id);

    /**
     *根据id更新MeetingUser
     */
    String editById(MeetingUser record);

    /**
     *分页查询MeetingUser
     */
    PageTableData<MeetingUser> list(int pagenum, int pagesize,MeetingUserQuery param);

    /**
     *查询所有MeetingUser
     */
    List<MeetingUser> listAll(MeetingUserQuery param);
}