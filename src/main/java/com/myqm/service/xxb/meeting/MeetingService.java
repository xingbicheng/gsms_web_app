package com.myqm.service.xxb.meeting;

import com.myqm.pojo.xxb.meeting.Meeting;
import com.myqm.vo.xxb.meeting.MeetingQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface MeetingService {
    /**
     *根据id删除Meeting
     */
    String delById(String id);

    /**
     *根据新增Meeting，id自增
     */
    String save(Meeting record);

    /**
     *根据id查询Meeting
     */
    Meeting getById(String id);

    /**
     *根据id更新Meeting
     */
    String editById(Meeting record);

    /**
     *分页查询Meeting
     */
    PageTableData<Meeting> list(int pagenum, int pagesize,MeetingQuery param);

    /**
     *查询所有Meeting
     */
    List<Meeting> listAll(MeetingQuery param);
}