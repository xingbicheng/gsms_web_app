package com.myqm.service.xxb.meeting.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.meeting.MeetingUserDao;
import com.myqm.pojo.xxb.meeting.MeetingUser;
import com.myqm.vo.xxb.meeting.MeetingUserQuery;
import com.myqm.service.xxb.meeting.MeetingUserService;


@Service
public class MeetingUserServiceImpl implements MeetingUserService{

	@Autowired
	public MeetingUserDao meetingUserDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (meetingUserDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(MeetingUser record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingUserDao.save(record) > 0)
		{
			return record.getId() + "";
		}
		else
			return null;
	}

	@Override
	public MeetingUser getById(String id) {
		return meetingUserDao.getById(id);
	}


	@Override
	public String editById(MeetingUser record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (meetingUserDao.editById(record) > 0)
			return record.getId() + "";
		else
			return null;
	}

	@Override
	public PageTableData<MeetingUser> list(int pagenum, int pagesize,MeetingUserQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<MeetingUser> datalist = meetingUserDao.list(param);
		PageTableData<MeetingUser> resultPage = new PageTableData<MeetingUser>(datalist);
		PageInfo<MeetingUser> p = new PageInfo<MeetingUser>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<MeetingUser> listAll(MeetingUserQuery param) {
		List<MeetingUser> datalist = meetingUserDao.list(param);
		return datalist;
	}

}
