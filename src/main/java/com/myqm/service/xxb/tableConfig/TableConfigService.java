package com.myqm.service.xxb.tableConfig;

import com.myqm.pojo.xxb.tableConfig.TableConfig;
import com.myqm.vo.xxb.tableConfig.TableConfigQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface TableConfigService {
    /**
     *根据id删除TableConfig
     */
    String delById(String id);

    /**
     *根据新增TableConfig，id自增
     */
    String save(TableConfig record);

    /**
     *根据id查询TableConfig
     */
    TableConfig getById(String id);

    /**
     *根据id更新TableConfig
     */
    String editById(TableConfig record);

    /**
     *分页查询TableConfig
     */
    PageTableData<TableConfig> list(int pagenum, int pagesize,TableConfigQuery param);

    /**
     *查询所有TableConfig
     */
    List<TableConfig> listAll(TableConfigQuery param);
}