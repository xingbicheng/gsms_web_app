package com.myqm.service.xxb.tableConfig.Impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.tableConfig.TableConfigDao;
import com.myqm.pojo.xxb.tableConfig.TableConfig;
import com.myqm.vo.xxb.tableConfig.TableConfigQuery;
import com.myqm.service.xxb.tableConfig.TableConfigService;


@Service
public class TableConfigServiceImpl implements TableConfigService{

	@Autowired
	public TableConfigDao tableConfigDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (tableConfigDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(TableConfig record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (tableConfigDao.save(record) > 0)
		{
			return record.getId() + "";
		}
		else
			return null;
	}

	@Override
	public TableConfig getById(String id) {
		return tableConfigDao.getById(id);
	}


	@Override
	public String editById(TableConfig record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));

		if (tableConfigDao.editById(record) > 0)
			return record.getId() + "";
		else
			return null;
	}

	@Override
	public PageTableData<TableConfig> list(int pagenum, int pagesize,TableConfigQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<TableConfig> datalist = tableConfigDao.list(param);
		PageTableData<TableConfig> resultPage = new PageTableData<TableConfig>(datalist);
		PageInfo<TableConfig> p = new PageInfo<TableConfig>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<TableConfig> listAll(TableConfigQuery param) {
		List<TableConfig> datalist = tableConfigDao.list(param);
		return datalist;
	}

}
