package com.myqm.service.xxb.article.Impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.xxb.article.ArticleDao;
import com.myqm.pojo.xxb.article.Article;
import com.myqm.vo.xxb.article.ArticleQuery;
import com.myqm.service.xxb.article.ArticleService;


@Service
public class ArticleServiceImpl implements ArticleService{

	@Autowired
	public ArticleDao articleDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if (articleDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(Article record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (articleDao.save(record) > 0) 
		{			 
			return record.getId() + "";
		}
		else
			return null; 
	}

	@Override
	public Article getById(String id) {
		return articleDao.getById(id);
	}


	@Override
	public String editById(Article record) {
		//请修改更新的拼音
		// record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getName()));
	
		if (articleDao.editById(record) > 0)
			return record.getId() + "";
		else
			return null; 
	}

	@Override
	public PageTableData<Article> list(int pagenum, int pagesize,ArticleQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Article> datalist = articleDao.list(param);
		PageTableData<Article> resultPage = new PageTableData<Article>(datalist);
		PageInfo<Article> p = new PageInfo<Article>(datalist);
		// resultPage.setCurrentPage(p.getPageNum());
		// resultPage.setShowRows(p.getPageSize());
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<Article> listAll(ArticleQuery param) {
		List<Article> datalist = articleDao.list(param);
		return datalist;
	}

}
