package com.myqm.service.xxb.article;

import com.myqm.pojo.xxb.article.Article;
import com.myqm.vo.xxb.article.ArticleQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface ArticleService {
   /**
    *根据id删除Article
    */
	String delById(String id);

   /**
    *根据新增Article，id自增
    */
    String save(Article record);

   /**
    *根据id查询Article
    */
    Article getById(String id);
    
   /**
    *根据id更新Article
    */ 
    String editById(Article record);

   /**
    *分页查询Article
    */ 
    PageTableData<Article> list(int pagenum, int pagesize, ArticleQuery param);

	/**
    *查询所有Article
    */ 
    List<Article> listAll(ArticleQuery param);
}