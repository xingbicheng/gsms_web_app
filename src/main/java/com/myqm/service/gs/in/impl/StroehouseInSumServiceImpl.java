package com.myqm.service.gs.in.impl;


import com.myqm.dao.gs.in.StroehouseInSumDao;
import com.myqm.pojo.gs.in.StroehouseInSum;
import com.myqm.pojo.gs.in.StroehouseInTotal;
import com.myqm.service.gs.in.StroehouseInSumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StroehouseInSumServiceImpl implements StroehouseInSumService {
    @Autowired
    StroehouseInSumDao stroehouseInSumDao;
    @Override
    public List<StroehouseInSum> sum(StroehouseInTotal stroehouseInTotal) {
        List<StroehouseInSum> stroehouseInSums=stroehouseInSumDao.sum(stroehouseInTotal);
        return stroehouseInSums;
    }
}
