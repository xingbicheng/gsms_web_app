package com.myqm.service.gs.in.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.dao.gs.order.OrderDao;
import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.dao.gs.purchase.OrderDetailPurchaseDao;
import com.myqm.dao.gs.purchase.PurchaseOrderDao;
import com.myqm.dao.gs.purchase.PurchaseOrderDetailDao;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.in.FromStroehouseInfo;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.*;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderDetail.*;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.pojo.gs.purchase.PurchaseOrder.PurchaseOrderState;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail.*;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.in.StroehouseInService;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.in.StroehouseInQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class StroehouseInServiceImpl implements StroehouseInService {

    @Autowired
    public StroehouseInDao stroehouseInDao;

    @Autowired
    public SysLogsService sysLogsService;

    @Autowired
    PurchaseOrderDetailDao purchaseOrderDetailDao;
    
	@Autowired
	private PurchaseOrderDao purchaseOrderDao;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderDetailDao orderDetailDao;

    @Autowired
    OrderDetailPurchaseDao orderDetailPurchaseDao;

    @Override
    public StroehouseIn getById(String id) {

        StroehouseIn response=stroehouseInDao.getById(id);


        List<StroehouseIn>  toStroehouse=stroehouseInDao.getByFromId(id);
        if(toStroehouse!=null) {
            List<FromStroehouseInfo> result=new ArrayList<FromStroehouseInfo>();
            for (StroehouseIn s : toStroehouse) {
                FromStroehouseInfo f=new FromStroehouseInfo();
                f.setAmount(s.getAmount());
                f.setGoodsName(s.getGoodsName());
                f.setGoodsTypeId(s.getGoodsTypeId());
                f.setLossAmount(s.getLossAmount());
                f.setStoreName(s.getStoreName());
                result.add(f);
            }
            response.setToStroehouseInfos(result);
        }
        return response;
    }

    @Override
    public PageTableData<StroehouseIn> list(int pagenum, int pagesize, StroehouseInQuery param) {
        PageHelper.startPage(pagenum, pagesize);
        List<StroehouseIn> datalist = stroehouseInDao.list(param);
        PageTableData<StroehouseIn> resultPage = new PageTableData<StroehouseIn>(datalist);
        PageInfo<StroehouseIn> p = new PageInfo<StroehouseIn>(datalist);
        resultPage.setPageNumber(p.getPageNum());
        resultPage.setPageSize(p.getPageSize());
        resultPage.setTotalPage(p.getPages());
        resultPage.setTotal(p.getTotal());
        return resultPage;
    }

    @Override
    public List<StroehouseIn> listAll(StroehouseInQuery param) {

        List<StroehouseIn> datalist = stroehouseInDao.list(param);
        return datalist;
    }
 @Override
    public List<StroehouseIn> printlistAll(StroehouseInQuery param) {


        List<StroehouseIn> datalist = stroehouseInDao.list(param);
        List<StroehouseIn> re=new ArrayList<StroehouseIn>();
        Date inTime=param.getInTimeBegin();
        if(inTime!=null){
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            for(StroehouseIn stroehouseIn:datalist){
                if(simpleDateFormat.format(inTime)==simpleDateFormat.format(stroehouseIn.getInTime()))
                    re.add(stroehouseIn);
            }
            return re;
        }
        return datalist;
    }

    /**
     * 一键入库
     * @param ids
     * @return
     */
    @Transactional
    @Override
    public String inStorehouse(List<String> ids) {
        int result=0;
        for(int i=0;i<ids.size();i++){
            String id =ids.get(i);
            StroehouseIn record=stroehouseInDao.getById(id);
            if (!StroehouseInState.audited.equals(record.getState()))
                continue;
            if (PurchaseType.transaction.equals(record.getPurchaseType())) {
                //处理from_in_id 剩余量
                String fromInId = record.getFromStorehouseInId();
                StroehouseIn fromStroeIn=stroehouseInDao.getById(fromInId);
                fromStroeIn.setSurplus(fromStroeIn.getAvailableAmount());
                stroehouseInDao.editById(fromStroeIn);
            }
            stroehouseInDao.inStorehouse(id);

            /*计算
            1 查询id 对应 purchaseOrderDetail ==》pd_id
            2.根据pd_id 查询 purchaseOrderDetail下面所以的stroehouseInId
            3.如果这些InId的状态 都是in  ==  all
             */

            //更改订单细节状态为入库
            orderDetailDao.setStateByStroehouseInId(id, Order.OrderState.in);
            //刷新订单状态
            //获取订单细节
            List<OrderDetail> orderDetails=
                    orderDetailPurchaseDao.getOrderDetailByPDId(record.getPurchaseDetailId());
            //对每一个细节刷新订单头
            for(OrderDetail orderDetail:orderDetails)
                orderService.updateOrderStateByDetailId(orderDetail.getId());
 
            //判断和更改采购单细节状态
            if(stroehouseInDao.countAuditedIn(record.getPurchaseDetailId())==0) //没有审核状态的明细
                purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                        , PurchaseOrderDetailState.allIn);
            else 
            	if(stroehouseInDao.countIn(record.getPurchaseDetailId())==0) //没有审核状态的明细
            		 purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                             , PurchaseOrderDetailState.audited);
            	else
            		purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                        ,PurchaseOrderDetailState.in);
            //更新采购单状态
            PurchaseOrderDetail  pd = purchaseOrderDetailDao.getById(record.getPurchaseDetailId());
            PurchaseOrder order = purchaseOrderDao.getById(pd.getOrderId()); 
			
			if (order.getState().equals(PurchaseOrderState.audited)){ 
				 purchaseOrderDao.updateStateById(order.getId(), PurchaseOrderState.in);
			}


            sysLogsService.save("采购单",ids.get(i),true,
                    "操作员:"+ UserUtil.getLoginUser().getUsername()+",预采购单号为:"+id
                            +"商品名:"+record.getGoodsName()+"采购数量:"+record.getAmount()+"的入库单已入库");
            result++;
        }
        int tmp = ids.size() - result;
        if(tmp == 0)
            return "共入库"+result+"条记录";
        else
            return "共入库"+result+"条记录,还有"+tmp+"条记录未入库，请检查其状态是否为审核通过状态。";
    }

    /**
     * 取消入库
     * @param id
     * @return
     */
    @Transactional
    @Override
    public String reInStorehouse(String id) {

        //退到审核状态
        StroehouseIn record=stroehouseInDao.getById(id);
        if(record == null)
            return "入库信息不存在";
        if(!record.getState().equals(StroehouseInState.in))
            return "状态不符合";

        if(PurchaseType.transaction.equals(record.getPurchaseType())) {
            String fromInId = record.getFromStorehouseInId();
            StroehouseIn fromStroeIn = stroehouseInDao.getById(fromInId);
            //恢复剩余量
            fromStroeIn.setSurplus(record.getAvailableAmount() + fromStroeIn.getSurplus());
        }
        stroehouseInDao.reInStorehouse(id);

        //更改订单细节状态为入库
        orderDetailDao.setStateByStroehouseInId(id, Order.OrderState.Purchase);
        //刷新订单状态
        //获取订单细节
        List<OrderDetail> orderDetails=
                orderDetailPurchaseDao.getOrderDetailByPDId(record.getPurchaseDetailId());
        //对每一个细节刷新订单头
        for(OrderDetail orderDetail:orderDetails)
            orderService.updateOrderStateByDetailId(orderDetail.getId());

        //判断和更改采购单细节状态
        if(stroehouseInDao.countAuditedIn(record.getPurchaseDetailId())==0) //没有审核状态的明细
            purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                    , PurchaseOrderDetailState.allIn);
        else 
        	if(stroehouseInDao.countIn(record.getPurchaseDetailId())==0) //没有审核状态的明细
        		 purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                         , PurchaseOrderDetailState.audited);
        	else
        		purchaseOrderDetailDao.changeState(record.getPurchaseDetailId()
                    ,PurchaseOrderDetailState.in);

        PurchaseOrderDetail detail = purchaseOrderDetailDao.getById(record.getPurchaseDetailId()); 
        PurchaseOrder order = purchaseOrderDao.getById(detail.getOrderId()); 
		
		if (order.getState().equals(PurchaseOrderState.in)){
			if (purchaseOrderDetailDao.countByState(order.getId(), PurchaseOrderDetailState.in)
					+ purchaseOrderDetailDao.countByState(order.getId(), PurchaseOrderDetailState.allIn)==0)
				 purchaseOrderDao.updateStateById(order.getId(), PurchaseOrderState.audited);
		}
        
        sysLogsService.save("入库单",id,true,
                "操作员:"+UserUtil.getLoginUser().getUsername()+
                        ",取消入库单号为:"+id +"商品名:"+record.getGoodsName()+"采购数量:"+record.getAmount()+"的入库单");

        return "取消入库成功";
    }

    @Override
    public String cleanStorehouseIn(String id) {

        StroehouseIn stroehouseIn=stroehouseInDao.getById(id);
        stroehouseIn.setSurplus(0.0);
        stroehouseIn.setAvailableAmount(0.0);
        stroehouseIn.setState(StroehouseInState.clean);
        stroehouseInDao.editById(stroehouseIn);
        return "清空成功";
    }

    @Override
    public List<StroehouseIn> uses(String id) {
        List<StroehouseIn> stroehouseIn=stroehouseInDao.uses(id);
        return stroehouseIn;
    }

	@Override
	public int addPrint(List<String> ids) {
		 
		return stroehouseInDao.addPrint(ids);
	}

	@Override
	public List<StroehouseIn> listByIds(List<String> ids) {
		 
		return stroehouseInDao.listByIds(ids);
	}
}
