package com.myqm.service.gs.in.impl;

import java.util.List;

import com.myqm.utils.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.in.StoreLossTypeDao;
import com.myqm.pojo.gs.in.StoreLossType;
import com.myqm.vo.gs.in.StoreLossTypeQuery;
import com.myqm.service.gs.in.StoreLossTypeService;


@Service
public class StoreLossTypeServiceImpl implements StoreLossTypeService{

	@Autowired
	public StoreLossTypeDao storeLossTypeDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if(id.equals("1"))
			return "此记录不能被删除";
		if (storeLossTypeDao.delById(id) > 0)
			return id;
		return null;
	}

	@Override
	public String save(StoreLossType record) {
		//请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getPinyin()));
		record.setUpdaterName(UserUtil.getLoginUser().getUsername());

		if (storeLossTypeDao.save(record) > 0) 
		{			 
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public StoreLossType getById(String id) {
		return storeLossTypeDao.getById(id);
	}


	@Override
	public String editById(StoreLossType record) {
		//请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getPinyin()));
	
		if (storeLossTypeDao.editById(record) > 0)
			return record.getId();
		else
			return null; 
	}

	@Override
	public PageTableData<StoreLossType> list(int pagenum, int pagesize,StoreLossTypeQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<StoreLossType> datalist = storeLossTypeDao.list(param);
		PageTableData<StoreLossType> resultPage = new PageTableData<StoreLossType>(datalist);
		PageInfo<StoreLossType> p = new PageInfo<StoreLossType>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<StoreLossType> listAll(StoreLossTypeQuery param) {
		List<StoreLossType> datalist = storeLossTypeDao.list(param);
		return datalist;
	}

}
