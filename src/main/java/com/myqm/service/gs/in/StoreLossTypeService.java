package com.myqm.service.gs.in;

import com.myqm.pojo.gs.in.StoreLossType;
import com.myqm.vo.gs.in.StoreLossTypeQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface StoreLossTypeService {
   /**
    *根据id删除StoreLossType
    */
	String delById(String id);

   /**
    *根据新增StoreLossType，id自增
    */
    String save(StoreLossType record);

   /**
    *根据id查询StoreLossType
    */
    StoreLossType getById(String id);
    
   /**
    *根据id更新StoreLossType
    */ 
    String editById(StoreLossType record);

   /**
    *分页查询StoreLossType
    */ 
    PageTableData<StoreLossType> list(int pagenum, int pagesize,StoreLossTypeQuery param);

	/**
    *查询所有StoreLossType
    */ 
    List<StoreLossType> listAll(StoreLossTypeQuery param);
}