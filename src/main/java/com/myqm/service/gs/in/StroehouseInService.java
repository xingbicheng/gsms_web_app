package com.myqm.service.gs.in;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.vo.gs.in.StroehouseInQuery;

import java.util.List;
import java.util.Map;

public interface StroehouseInService {
	/**
	 * 根据id查询StroehouseIn
	 */
	StroehouseIn getById(String id);

	/**
	 * 分页查询StroehouseIn
	 */
	PageTableData<StroehouseIn> list(int pagenum, int pagesize, StroehouseInQuery param);

	/**
	 * 查询所有StroehouseIn
	 */
	List<StroehouseIn> listAll(StroehouseInQuery param);
	
	
	String inStorehouse(List<String> ids);
    String reInStorehouse(String id);
    String cleanStorehouseIn(String id);


	List<StroehouseIn> uses(String id);
	
	int addPrint(List<String> ids);
	List<StroehouseIn> listByIds(List<String> ids);


    List<StroehouseIn> printlistAll(StroehouseInQuery param);
}