package com.myqm.service.gs.in;

import com.myqm.pojo.gs.in.StroehouseInSum;
import com.myqm.pojo.gs.in.StroehouseInTotal;

import java.util.List;

public interface StroehouseInSumService {
    List<StroehouseInSum> sum(StroehouseInTotal stroehouseInTotal);
}
