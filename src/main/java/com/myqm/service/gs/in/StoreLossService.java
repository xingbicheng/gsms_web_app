package com.myqm.service.gs.in;

import com.myqm.dto.gs.in.SetLoss;
import com.myqm.pojo.gs.in.StoreLoss;
import com.myqm.vo.gs.in.StoreLossQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface StoreLossService {
   /**
    *根据id删除StoreLoss
    */
	String delById(String id);

   /**
    *根据新增StoreLoss，id自增
    */
    String save(StoreLoss record);

   /**
    *根据id查询StoreLoss
    */
    StoreLoss getById(String id);
    
   /**
    *根据id更新StoreLoss
    */ 
    String editById(StoreLoss record);

   /**
    *分页查询StoreLoss
    */ 
    PageTableData<StoreLoss> list(int pagenum, int pagesize,StoreLossQuery param);

	/**
    *查询所有StoreLoss
    */ 
    List<StoreLoss> listAll(StoreLossQuery param);
    
    String setLoss(List<String> inIds);

    /**
     * 自定义商品损耗
     * @param inIds
     * @return
     */
    String setLoses(List<SetLoss> records);
    String cancelLoss(List<String> ids);

}