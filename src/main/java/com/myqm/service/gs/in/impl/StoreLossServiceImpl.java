package com.myqm.service.gs.in.impl;

import java.util.List;

import com.myqm.dao.gs.basic.GoodsDao;
import com.myqm.dto.gs.in.SetLoss;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.in.StoreLossDao;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.pojo.gs.in.StoreLoss;
import com.myqm.pojo.gs.in.StoreLoss.StoreLossType;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.vo.gs.in.StoreLossQuery;
import com.myqm.service.gs.in.StoreLossService;
import org.springframework.transaction.annotation.Transactional;


@Service
public class StoreLossServiceImpl implements StoreLossService{

	@Autowired
	private StoreLossDao storeLossDao;
	
	@Autowired
	private SysLogsDao logsDao;
	
	@Autowired
	private StroehouseInDao stroehouseInDao;

	@Autowired
    private GoodsDao goodsDao;
	@Override
	public String delById(String id) {
		if (storeLossDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(StoreLoss record) {
	
		if (storeLossDao.save(record) > 0){	
			Double amount = record.getAmount();
			StroehouseIn stroehouseIn = stroehouseInDao.getById(record.getInId());
			Double surplus = stroehouseIn.getSurplus();
			Double available = stroehouseIn.getAvailableAmount();
			if(record.getType().equals(StoreLossType.negative)) {
				stroehouseIn.setSurplus(surplus - amount);
				stroehouseIn.setAvailableAmount(available - amount);
			}else {
				stroehouseIn.setSurplus(surplus + amount);
				stroehouseIn.setAvailableAmount(available + amount);
			}
			stroehouseInDao.calculation(stroehouseIn.getSurplus(), stroehouseIn.getAvailableAmount(), stroehouseIn.getId());
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public StoreLoss getById(String id) {
		return storeLossDao.getById(id);
	}


	@Override
	public String editById(StoreLoss record) {
		StoreLoss old = storeLossDao.getById(record.getId());
		Double oldAmount = old.getAmount();
		String oldType = old.getType();
		StroehouseIn stroehouseIn = stroehouseInDao.getById(record.getInId());
		Double surplus = 0.00, available =0.00;
		surplus = stroehouseIn.getSurplus();
		available = stroehouseIn.getAvailableAmount();
		if (storeLossDao.editById(record) > 0) {
			Double newAmount = record.getAmount();
			String newType = record.getType();
			//将剩余数量与可用量还原
			if(oldType.equals(StoreLossType.negative)) {
				surplus += oldAmount;
				available += oldAmount;
			}else {
				surplus -= oldAmount;
				available -= oldAmount;
			}
			//修改剩余数量与可用量
			if(newType.equals(StoreLossType.negative)) {
				surplus -= newAmount;
				available -= newAmount;
			}else {
				surplus += newAmount;
				available += newAmount;
			}
			stroehouseIn.setSurplus(surplus);
			stroehouseIn.setAvailableAmount(available);
			stroehouseInDao.calculation(surplus, available, stroehouseIn.getId());
			
			return record.getId();
		}else
			return null; 
	}

	@Override
	public PageTableData<StoreLoss> list(int pagenum, int pagesize,StoreLossQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<StoreLoss> datalist = storeLossDao.list(param);
		PageTableData<StoreLoss> resultPage = new PageTableData<StoreLoss>(datalist);
		PageInfo<StoreLoss> p = new PageInfo<StoreLoss>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<StoreLoss> listAll(StoreLossQuery param) {
		List<StoreLoss> datalist = storeLossDao.list(param);
		return datalist;
	}

	@Transactional
	@Override
	public String setLoss(List<String> inIds) {
	    int count=0;
		for(String id:inIds) { 
                StroehouseIn stroehouseIn = stroehouseInDao.getById(id);
                if (!stroehouseIn.getState().equals(StroehouseInState.in))
                {
                	continue;
                }
                if(stroehouseIn.getLossAmount()>0){
	                StoreLoss storeLoss = new StoreLoss();
	                storeLoss.setInId(stroehouseIn.getId());
	                storeLoss.setSum(stroehouseIn.getSum());
	                storeLoss.setPrice(stroehouseIn.getPrice());
	                storeLoss.setGoodsId(stroehouseIn.getGoodsId());
	                storeLoss.setAmount(stroehouseIn.getLossAmount());
	                storeLoss.setProviderId(stroehouseIn.getProviderId());
	                storeLoss.setPackingMemo(stroehouseIn.getPackingMemo());
	                storeLoss.setStorehouseId(stroehouseIn.getStorehouseId());
	                storeLoss.setType(StoreLossType.negative);
	                //一键损耗 设为 一键损耗 1
	                storeLoss.setLossType("1");
	                storeLossDao.save(storeLoss);
                }
                //修改
                stroehouseIn.setLossAmount(stroehouseIn.getLossAmount());
                stroehouseIn.setAvailableAmount(stroehouseIn.getAmount()-stroehouseIn.getLossAmount());
            
                stroehouseIn.setState(StroehouseInState.loss);
                stroehouseInDao.editById(stroehouseIn);
                count++;
            } 
		 
		return "传入"+inIds.size()+"条记录，共一键损耗" + count +"条记录";

	}

	@Transactional
    @Override
    public String setLoses(List<SetLoss> records) {
        try {
            for (SetLoss s : records) {
                StroehouseIn stroehouseIn = stroehouseInDao.getById(s.getInId());
                StoreLoss storeLoss = new StoreLoss();
                storeLoss.setInId(s.getInId());
                storeLoss.setAmount(s.getAmount());
                storeLoss.setType(s.getType());
                storeLoss.setLossType(s.getLossType());
                storeLoss.setGoodsId(stroehouseIn.getGoodsId());
                storeLossDao.save(storeLoss);

                stroehouseIn.setLossAmount(s.getAmount() + stroehouseIn.getLossAmount());
                stroehouseInDao.editById(stroehouseIn);
            }
            return "自定义损耗成功" + records.size() + "条记录";
        } catch (Exception e) {
            return "自定义损耗失败";
        }
    }

    /**
     * 取消损耗
     * @param records
     * @return
     */
    @Transactional
    @Override
    public String cancelLoss(List<String> ids) {
        String remark="";
        try {
            for (String id : ids) {
                StoreLoss storeLoss = storeLossDao.getById(id);
                if (storeLoss == null) {
                    remark += "没有id为" + id + "的损耗记录";
                    continue;
                }
                else {
                    String inId = storeLoss.getInId();
                    Double cancelLossAmount = storeLoss.getAmount();
                    String type = storeLoss.getType();
                    StroehouseIn stroehouseIn = stroehouseInDao.getById(inId);

                    //判断一键损耗
                    if(StroehouseInState.loss.equals(stroehouseIn.getState())){
                        stroehouseIn.setState(StroehouseInState.in);
	                    if (type.equals(StoreLossType.negative)) {
	                        stroehouseIn.setLossAmount(stroehouseIn.getLossAmount() - cancelLossAmount);
	                        stroehouseIn.setAvailableAmount(stroehouseIn.getAvailableAmount() + cancelLossAmount);
	                    } else {
	                        stroehouseIn.setLossAmount(stroehouseIn.getLossAmount() + cancelLossAmount);
	                        stroehouseIn.setAvailableAmount(stroehouseIn.getAvailableAmount() - cancelLossAmount);
	                    }
	                    stroehouseInDao.editById(stroehouseIn);
	                    storeLossDao.delById(id);
	                    remark+="取消了id为" + id + "的损耗记录";
                    }else
                    	continue;
                    
                   
                }

            }
        }catch(Exception e){
            remark="取消损耗错误";
        }finally {
            return remark;
        }
    }
}
