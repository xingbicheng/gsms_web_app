package com.myqm.service.gs.receive;

import java.util.List;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.base.BackInfo;
import com.myqm.pojo.base.DetailInfo;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.receive.BackProvider;
import com.myqm.vo.gs.receive.BackProviderQuery;
import com.myqm.vo.gs.receive.StroehouseBackQuery;

public interface BackService {
	/*
	 * 显示对应商品的供应商列表
	 */
	List<StroehouseIn>getProviders(String detailId);
	
	/*
	 * 退货
	 */
	String backGood(BackInfo backInfo);
	
	
	
	/*
	 * 一键收货
	 */
	String receiveGoods(DetailInfo record);
	
	
	String receiveOrders(List<String> ids);
	
	String cancelReceive(String detailId);

//	退货到供应商分页查询
	PageTableData<BackProvider> list(int pagenum, int pagesize, BackProviderQuery param);


    /**
     *根据id删除BackProvider
     */
    String delById(String id);

    /**
     *根据新增BackProvider，id自增
     */
    String save(BackProvider record);

    /**
     *根据id查询BackProvider
     */
    BackProvider getById(String id);

    /**
     *根据id更新BackProvider
     */
    String editById(BackProvider record);
//
//    /**
//     *分页查询BackProvider
//     */
//    PageTableData<BackProvider> list(int pagenum, int pagesize,BackProviderQuery param);

    /**
     *查询所有BackProvider
     */
    List<BackProvider> listAll(BackProviderQuery param);
}
