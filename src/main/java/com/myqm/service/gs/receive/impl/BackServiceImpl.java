package com.myqm.service.gs.receive.impl;

import java.math.BigDecimal;
import java.util.List;

import com.myqm.dao.sysfun.SysLogsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.in.StoreLossDao;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.dao.gs.order.OrderDao;
import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.dao.gs.purchase.OrderDetailPurchaseDao;
import com.myqm.dao.gs.receive.BackProviderDao;
import com.myqm.dao.gs.receive.StroehouseBackDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.base.BackInfo;
import com.myqm.pojo.base.DetailInfo;
import com.myqm.pojo.gs.in.StoreLoss;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.Order.OrderState;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailState;
import com.myqm.pojo.gs.receive.BackProvider;
import com.myqm.pojo.gs.receive.StroehouseBack;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.gs.receive.BackService;
import com.myqm.vo.gs.receive.BackProviderQuery;

@Service
public class BackServiceImpl implements BackService {
	@Autowired
	private OrderDetailPurchaseDao orderDetailPurchaseDao;

	@Autowired
	private StroehouseInDao storeHouseInDao;

	@Autowired
	private BackProviderDao backProviderDao;

	@Autowired
	private StroehouseBackDao storeHouseBackDao;

	@Autowired
	private StoreLossDao storeLossDao;

	@Autowired
	private OrderDetailDao orderDetailDao;

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private OrderService orderService;
	

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public List<StroehouseIn> getProviders(String detailId) {
		String purchaseDetailId = orderDetailPurchaseDao.getPDByODId(detailId);
		String storehouseId = orderDetailPurchaseDao.getStorehouseIdByPdId(detailId);
		List<StroehouseIn> providers = storeHouseInDao.getProviderByPDId(purchaseDetailId, storehouseId);
		return providers;
	}

	@Override
	public String backGood(BackInfo backInfo) {
		// 修改订单明细实际收货量
		OrderDetail detail = orderDetailDao.getById(backInfo.getDetailId());
		if(detail.getState().equals(OrderDetailState.tplusExport)||detail.getState().equals(OrderDetailState.tplusLock))
			return "该条订单明细已被导出或锁定，不可修改！";
		Double receiveAmount = detail.getAmount() - backInfo.getAmount();
		if (receiveAmount < 0)
			return "退货量大于订货量，请检查后，再修改！";
		orderDetailDao.editReceiveAmount(backInfo.getDetailId(), receiveAmount, backInfo.getType());
		
		StroehouseIn inRecord = storeHouseInDao.getById(backInfo.getInId());
		
		switch (backInfo.getType()) {
		case OrderDetailState.backToProvider:
			BackProvider(detail.getId(),inRecord, backInfo.getAmount());// 退回供应商
			break;
		case OrderDetailState.backGoodsToCenter:
			if (inRecord.getFromStorehouseInId() == null)
				return "不是从中央库调拨，不能退货到中央库！";
			backStorehouse(detail.getId(),inRecord, backInfo.getStorehouseId(), backInfo.getAmount(), backInfo.getType());// 退回中央库
			break;
		case OrderDetailState.backToCenter:
			if (inRecord.getFromStorehouseInId() == null)
				return "不是从中央库调拨，不能退货到中央库！";
			backStorehouse(detail.getId(),inRecord, backInfo.getStorehouseId(), backInfo.getAmount(), backInfo.getType());// 退回中央库（货品留在分库）
			break;
		case OrderDetailState.recordLoss:
			recordLoss(detail.getId(),inRecord, backInfo.getAmount());// 记入损耗
			break;
		default:
			break;
		}
		Order  order = orderDao.getById(detail.getOrderId()); 
		orderService.updateOrderStateById(order.getId());
			
		return "退货成功";
	}

	@Override
	@Transactional
	public String cancelReceive(String detailId) {
		OrderDetail detail = orderDetailDao.getById(detailId);
		if(detail.getState().equals(OrderDetailState.tplusExport)||detail.getState().equals(OrderDetailState.tplusLock))
			return "该条订单明细已被导出或锁定，不可修改";
	
		if (detail.getState().equals(OrderDetailState.received))
		{
			// 设置收货数量，单价，总价为0
			orderDetailDao.resetReceiveInfo(detailId);
		}
		else if (detail.getState().equals(OrderDetailState.backGoodsToCenter) ||
				detail.getState().equals(OrderDetailState.backToCenter))
		{  
			 StroehouseIn in = storeHouseInDao.getById(detail.getNewInId());
			 if ((in != null) && in.getAmount() == in.getAvailableAmount()){
				 return "退货到中央库的货物已经被使用，不能进行取消退货。";
			 }
			 else if  (in != null) 
			 {
				 storeHouseBackDao.delById(detail.getStorehouseBackId());
				 storeHouseInDao.delById(detail.getNewInId());
				 orderDetailDao.resetReceiveInfo(detailId); 
				 storeHouseInDao.resetReturnAmount(in.getFromStorehouseInId(), in.getAmount());
			 }else
				 return "退货入库信息有误，请核对后操作"; 
				
		}
		else if (detail.getState().equals(OrderDetailState.backToProvider))
		{
			Double reAmount = backProviderDao.getById(detail.getBackProviderId()).getAmount();
			
//			Double returnableAmount = storeHouseInDao.getById(detail.getInId()).getReturnableAmount() 
//					+ backProviderDao.getById(detail.getBackProviderId()).getAmount();
			storeHouseInDao.resetReturnAmount(detail.getInId(), reAmount);
			
			backProviderDao.delById(detail.getBackProviderId());
			orderDetailDao.resetReceiveInfo(detailId);
		}
		else if (detail.getState().equals(OrderDetailState.recordLoss))
		{
			storeLossDao.delById(detail.getLossId());
			orderDetailDao.resetReceiveInfo(detailId);
		} else
		{
			return "商品未进入到收货环节，不能进行取消收货";
		} 
		 Order order = orderDao.getById(detail.getOrderId());
		// 订单状态为收货则改为部分收货，若为部分收货并且只收货一条则改为出库
		if (order.getState().equals(OrderState.received))
			orderDao.editStatedById(OrderState.partReceived, detail.getOrderId());

		else if (order.getState().equals(OrderState.partReceived)
				&& orderDetailDao.getPartReceivedAmount(detail.getOrderId()) == 1)
			orderDao.editStatedById(OrderDetailState.out, detail.getOrderId());

		return "取消成功";
	}

	@Override
	@Transactional
	public String receiveGoods(DetailInfo record) {
		int size = record.getDetailIds().size();

		for (String id : record.getDetailIds()) {
			OrderDetail detail=orderDetailDao.getById(id);
			if (!detail.getState().equals(OrderDetailState.out)||detail.getState().equals(OrderDetailState.tplusExport)
				||detail.getState().equals(OrderDetailState.tplusLock))
				record.getDetailIds().remove(id);
		}
		// 批量更新订单明细的状态(实收单价,数量,state)
		orderDetailDao.setReceiveInfo(record.getDetailIds());
		
		orderService.updateOrderStateById(record.getOrderId());
		if (record.getDetailIds().size() == size)
			return "收货成功";
		else
			return "存在不是出库状态或者订单明细已被导出或锁定的商品，该商品不能收货";

	}

	private void BackProvider(String detailId,StroehouseIn inRecord, Double amount) {
		// 修改入库单可退货量
		//Double returnableAmount = storeHouseInDao.getById(inRecord.getId()).getReturnableAmount() - amount;
		storeHouseInDao.setReturnAmount(inRecord.getId(), amount );
		
		BackProvider backGood = new BackProvider();
		backGood.setAmount(amount);
		backGood.setGoodsId(inRecord.getGoodsId());
		backGood.setInId(inRecord.getId());
		backGood.setPrice(inRecord.getPrice());
		backGood.setProviderId(inRecord.getProviderId());
		backGood.setStorehouseId(inRecord.getStorehouseId());
		BigDecimal amountCopy = new BigDecimal(amount);
		BigDecimal sum = inRecord.getPrice().multiply(amountCopy);
		backGood.setSum(sum);
		backProviderDao.save(backGood);
		
		//为订单明细设置inId,backProviderId
		orderDetailDao.setProviderBackInfo(detailId,inRecord.getId(),backGood.getId());

	}

	private void backStorehouse(String detailId,StroehouseIn inRecord, String storehouseId, Double amount, String type) {
		// storehouse_in插入记录
		String inId = inRecord.getId();
		inRecord.setAmount(amount);
		BigDecimal amountCopy = new BigDecimal(amount);
		BigDecimal sum = inRecord.getPrice().multiply(amountCopy);
		inRecord.setPurchaseDetailId(null);
		inRecord.setSum(sum);
		inRecord.setPurchaseType("4");// 分库退货入库
		inRecord.setState("2");// 入库完成
		inRecord.setSurplus(amount);
		inRecord.setLossAmount(amount * inRecord.getLossAmount() / inRecord.getAmount());
		inRecord.setRealAmount(amount);
		inRecord.setAvailableAmount(amount);
		inRecord.setFromStorehouseId(inRecord.getStorehouseId());
		inRecord.setStorehouseId(storehouseId);
		if (type.equals(OrderDetailState.backGoodsToCenter))
			inRecord.setMemo("退货到中央库");
		else
			inRecord.setMemo("退货到中央库,货品留在分库");
		storeHouseInDao.save(inRecord);

		// storehouse_back插入记录
		StroehouseBack backRecord = new StroehouseBack();
		backRecord.setGoodsId(inRecord.getGoodsId());
		backRecord.setAmount(amount);
		backRecord.setProviderId(inRecord.getProviderId());
		backRecord.setFromStorehouseId(inRecord.getStorehouseId());
		backRecord.setToStorehouseId(storehouseId);
		backRecord.setToInId(inRecord.getId());
		backRecord.setFormInId(inId);
		if (type.equals(OrderDetailState.backGoodsToCenter)) {
			backRecord.setType("0");// 0 退货到中央库
		} else
			backRecord.setType("2");// 2 退货到中央库，但货品不退回
		storeHouseBackDao.save(backRecord);
		
		storeHouseInDao.setReturnAmount(inId, amount);
		
		//为订单明细设置inId,newInId,storehouseBackId
		orderDetailDao.setStoreBackInfo(detailId, inId, inRecord.getId(), backRecord.getId());
	}

	private void recordLoss(String detailId,StroehouseIn inRecord, Double amount) {
		StoreLoss loss = new StoreLoss();
		loss.setAmount(amount);
		loss.setGoodsId(inRecord.getGoodsId());
		loss.setInId(inRecord.getId());
		loss.setLossType("2");// 商品退货损耗
		loss.setPrice(inRecord.getPrice());
		loss.setProviderId(inRecord.getProviderId());
		loss.setStorehouseId(inRecord.getStorehouseId());
		BigDecimal amountCopy = new BigDecimal(amount);
		BigDecimal sum = inRecord.getPrice().multiply(amountCopy);
		loss.setSum(sum);
		loss.setType("1");// 冲负
		storeLossDao.save(loss); 
		orderDetailDao.setLossInfo(detailId, inRecord.getId(), loss.getId() );  
	}

	@Override
	public PageTableData<BackProvider> list(int pagenum, int pagesize, BackProviderQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<BackProvider> datalist = backProviderDao.list(param);
		PageTableData<BackProvider> resultPage = new PageTableData<BackProvider>(datalist);
		PageInfo<BackProvider> p = new PageInfo<BackProvider>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public String delById(String id) {
		if (backProviderDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(BackProvider record) {

		if (backProviderDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public BackProvider getById(String id) {
		return backProviderDao.getById(id);
	}

	@Override
	public String editById(BackProvider record) {

		if (backProviderDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	// @Override
	// public PageTableData<BackProvider> list(int pagenum, int
	// pagesize,BackProviderQuery param) {
	// PageHelper.startPage(pagenum, pagesize);
	// List<BackProvider> datalist = backProviderDao.list(param);
	// PageTableData<BackProvider> resultPage = new
	// PageTableData<BackProvider>(datalist);
	// PageInfo<BackProvider> p = new PageInfo<BackProvider>(datalist);
	// resultPage.setCurrentPage(p.getPageNum());
	// resultPage.setShowRows(p.getPageSize());
	// resultPage.setTotalPage(p.getPages());
	// resultPage.setTotal(p.getTotal());
	// return resultPage;
	// }

	@Override
	public List<BackProvider> listAll(BackProviderQuery param) {
		List<BackProvider> datalist = backProviderDao.list(param);
		return datalist;
	}
	@Transactional
	@Override
	public String receiveOrders(List<String> ids) {
		if (ids.size() == 0)
			return "没有订单编号"; 
		orderDetailDao.setReceiveByOrderId(ids);
		for(String id: ids) {
			orderService.updateOrderStateById(id);
		}
		  return "收货成功"; 
	}

}