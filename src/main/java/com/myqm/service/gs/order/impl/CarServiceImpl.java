package com.myqm.service.gs.order.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.order.CarDao;
import com.myqm.dao.gs.order.OrderDao;
import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.pojo.gs.basic.GoodsInfo;
import com.myqm.pojo.gs.order.Car;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.service.gs.order.CarService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.order.QueryCar;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarDao carDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private OrderDetailDao orderDetailDao;
	
	@Override
	public int delById(String customerId) {
		return carDao.delById(customerId);
	}

	@Override
	public int add(Car record) {
		Car count = carDao.get(record.getCustomerId(), record.getGoodsId());
		if(count == null) {
			return carDao.add(record);
		}else {
			Double counts = count.getCount() + record.getCount();
			record.setCount(counts);
			return carDao.editById(record);
		}
		
	}

	@Override
	public int editById(Car record) {
		return carDao.editById(record);
	}

	@Override
	public PageInfo<Car> listByCustomerId(int pagenum, int pagesize,QueryCar param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Car> datalist = carDao.listByCustomerId(param.getCustomerId()); 
		PageInfo<Car> p = new PageInfo<Car>(datalist);
		return p;
	}
	
	@Override 
	public String copyToCar(String id)
	{
		Order order = orderDao.getById(id);
		if (order == null) return "没有该订单,添加错误";
		List<OrderDetail>details=orderDetailDao.listByOrderId(id);
		String customerId = order.getCustomerId();
		String userId = UserUtil.getLoginUser().getUserId();
		if(!customerId.equals(userId)) {
			return "该订单不是你的订单";
		}
		Car good= new Car();
		for(int i=0;i<details.size();i++)
		{
			good.setCustomerId(customerId);
			good.setGoodsId(details.get(i).getInGoodsId());
			Car count = carDao.get(good.getCustomerId(), good.getGoodsId());
			if(count == null) {
				Double counts = details.get(i).getAmount();
				good.setCount(counts);
				carDao.add(good);
			}else {
				Double counts = details.get(i).getAmount()+count.getCount();
				good.setCount(counts);
				carDao.editById(good);
			} 
		}
		return customerId;
	}

	@Override
	public int del(GoodsInfo goodsInfo) {
		return carDao.del(goodsInfo.getCustomerId(), goodsInfo.getGoodsId());
	}

	@Override
	public List<Car> listAll(QueryCar param) {
		List<Car> datalist = carDao.listByCustomerId(param.getCustomerId()); 
		return datalist;
	}

}
