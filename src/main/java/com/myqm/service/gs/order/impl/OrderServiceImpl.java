package com.myqm.service.gs.order.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.base.SequenceDao;
import com.myqm.dao.gs.basic.CustomerDao;
import com.myqm.dao.gs.basic.CustomerTypeDao;
import com.myqm.dao.gs.basic.GoodsDao;
import com.myqm.dao.gs.basic.StoreCustomerDao;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.dao.gs.order.*;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.AuditLog;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.pojo.gs.in.StoreAndOrderDet;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.Order.OrderError;
import com.myqm.pojo.gs.order.Order.OrderOrigin;
import com.myqm.pojo.gs.order.Order.OrderState;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailError;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailSource;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailState;
import com.myqm.pojo.gs.order.OrderDetail.OrderEditFlag;
import com.myqm.pojo.gs.order.OrderDetailIn;
import com.myqm.pojo.gs.order.OrderDetailStatistic;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.Verified;
import com.myqm.service.gs.basic.AuditLogService;
import com.myqm.service.gs.order.OrderCheckService;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.gs.order.WebOrderService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.DateUtil;
import com.myqm.utils.ExcelUtil;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.basic.StoreCustomerQuery;
import com.myqm.vo.gs.order.Ids;
import com.myqm.vo.gs.order.OrderDetailQuery;
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.vo.gs.order.QueryAudit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private WebOrderDao webOrderDao;

    @Autowired
    private SysLogsDao logsDao;

    @Autowired
    private OrderDetailInDao orderDetailInDao;

    @Autowired
    private OrderDetailDao orderDetailDao;

    @Autowired
    private WebOrderDetailDao webOrderDetailDao;

    @Autowired
    private SysUserDao userDao;

    @Autowired
    private OrderCheckService checkService;

    @Autowired
    private CustomerTypeDao customerTypeDao;

    @Autowired
    private SequenceDao sequenceDao;

    @Autowired
    private WebOrderService webService;

    @Autowired
    private StroehouseInDao stroehouseInDao;

    @Autowired
    private AuditLogService auditLogService;
    // @Autowired
    // SysLogs syslogs;
    @Autowired
    SysLogsService logService;
    @Autowired
    SysLogsDao sysLogsDao;
    @Autowired
    CustomerDao customerDao;
    @Autowired
    GoodsDao goodsDao;
    
    @Autowired
    private StoreCustomerDao scDao;

    @Override
    @Transactional
    public String delById(String id) {
        SysLogs syslogs = new SysLogs();
        Order order = orderDao.getById(id);
        if (!order.getState().equals(OrderState.del))
            return "订单未被删除标记过,不能删除请核实过再删除";
        orderDetailDao.delByOrderId(id);
        orderDao.delById(id);
        syslogs.setUserId(UserUtil.getLoginUser().getId());
        syslogs.setFlag(true);
        syslogs.setModule("订单删除操作");
        syslogs.setModuleId(id);
        syslogs.setRemark("操作人昵称是：" + UserUtil.getLoginUser().getNickname());
        logService.save(syslogs);
        return "订单删除成功";
    }

    @Override
    @Transactional
    public String delFlagById(String id) {
        SysLogs syslogs = new SysLogs();
        Order byId = orderDao.getById(id);

        if (!byId.getState().equals(OrderState.place)) {
            return "-1";
        }
        orderDao.editStatedById(OrderState.del, id);
        orderDetailDao.delFlagByOrderId(id);
        syslogs.setUserId(UserUtil.getLoginUser().getId());
        syslogs.setFlag(true);
        syslogs.setModule("订单");
        syslogs.setModuleId(id);
        syslogs.setRemark("操作人：" + UserUtil.getLoginUser().getNickname() + "删除了订单:" + byId.getOrderNo());
        logService.save(syslogs);
        return "删除成功";
    }

    @Override
    @Transactional
    public String save(Order record) { // server imp 层
        return saveOrder(record);
    }

    @Transactional // 若对数据库执行未成功 则回滚
    public String saveOrder(Order record) {
        SysLogs syslogs = new SysLogs();
        record.setCreaterId(UserUtil.getLoginUser().getId());
        record.setUpdaterId(UserUtil.getLoginUser().getId());
        if (record.getRealDistributionDate() == null)
            record.setRealDistributionDate(record.getDistributionDate());
        record.setState(OrderState.place);
        record.setVerified(0L);
        if (!record.getOrigin().equals(OrderOrigin.webOrder))
            record.setFlag(OrderError.noError);
        if (record.getCreaterId() == null)
            record.setCreaterId(UserUtil.getLoginUser().getId());
        checkService.checkOrder(record);

        if (record.getOrigin().equals(OrderOrigin.webOrder)) {
            webOrderDetailDao.delByOrderId(record.getId());
            webOrderDao.delById(record.getId());
        }
        orderDao.save(record);
            List<OrderDetail> orderDetailList = record.getOrderDetailList();
        for (OrderDetail detail : record.getOrderDetailList()) {
            detail.setOrderId(record.getId());
            detail.setState(OrderDetailState.place);
            if (!record.getOrigin().equals(OrderOrigin.webOrder))
                detail.setFlag(OrderDetailError.noError);
            if (detail.getReceiveAmount() == null) {
                detail.setReceiveAmount(detail.getAmount());
            }
            if (detail.getOutAmount() == null) {
                detail.setOutAmount(detail.getAmount());
            }
            orderDetailDao.save(detail);
        }
        syslogs.setUserId("1");
        syslogs.setModule("订单");
        syslogs.setFlag(true);
        syslogs.setModuleId(record.getId());
        //syslogs.setRemark("操作人：" + UserUtil.getLoginUser().getNickname() + "新增了订单:" + record.getId());
        syslogs.setRemark("操作人： 新增了订单:" + record.getOrderNo());
        logService.save(syslogs);

        return record.getId();
    }

    @Override
    @Transactional
    public String saveWeb(Ids ids) {
        String result = "";
        List<String> list = ids.getIds();
        for (String id : list) {
            Order order = webService.getById(id);
            order.setOrigin(OrderOrigin.webOrder);
            order.setOrderNo(this.getNum());
            saveOrder(order);
            result += order.getOrderNo() + ";";
        }
        return result;
    }

    @Override
    public Order getById(String id) {
        Order order = orderDao.getById(id);

        if (order == null)
            return null;

        OrderDetailQuery dq = new OrderDetailQuery();
        dq.setOrderId(id);
        List<OrderDetail> orderDetailList = orderDetailDao.list(dq);
        for (OrderDetail orderDetail : orderDetailList) {

        }
        String createrName;
        if (order.getOrigin().equals(OrderOrigin.weixin)) {
            createrName = order.getCustomerName();
        } else

            createrName = userDao.getUserNameById(order.getCreaterId());
        String updaterName = userDao.getUserNameById(order.getUpdaterId());
        if (order.getOuterId() != null) {
            String outerName = userDao.getUserNameById(order.getOuterId());
            order.setOuterName(outerName);
        }
        if (order.getAuditorId() != null) {
            String auditorName = userDao.getUserNameById(order.getAuditorId());
            order.setAuditorName(auditorName);
        }
        if (order.getCustomerTypeId() != null) {
            String customerType = customerTypeDao.getById(order.getCustomerTypeId()).getCustomerType();
        }
        order.setCreaterName(createrName);
        order.setUpdaterName(updaterName);
        order.setOrderDetailList(orderDetailList);
        return order;
    }

    /**
     * 比较订单明细差异 记录每一条明细
     */
    public String compareOrderDetailData(OrderDetail detail, String orderId) {
        // 先获得order中的Remark,再对订单明细进行处理
        OrderDetail originOrder = orderDetailDao.getById(detail.getId());
        // 对比现在订单和数据库订单
        if (originOrder != null) {
            BigDecimal originUnitPrice = originOrder.getUnitPrice();// 单价
            BigDecimal currUnitPrice = detail.getUnitPrice();
            Double originAmount = originOrder.getAmount();// 总数量
            Double currAmount = detail.getAmount();
            String originUnitName = originOrder.getUnitName();// 单位名称
            String currUnitName = detail.getUnitName();
            BigDecimal originSum = originOrder.getSum();// 总价
            BigDecimal currSum = detail.getSum();
            Double originSelfAmount = originOrder.getSelfAmount();// 自费数量
            Double currSelfAmount = detail.getSelfAmount();
            Double originFreeAmount = originOrder.getFreeAmount();// 免费数量
            Double currFreeAmount = detail.getFreeAmount();
            BigDecimal originSelfSum = originOrder.getSelfSum();// 自费钱
            BigDecimal currSelfSum = detail.getSelfSum();
            BigDecimal originFreeSum = originOrder.getFreeSum();// 免费金额
            BigDecimal currFreeSum = detail.getFreeSum();
            String originSource = originOrder.getSource();// 状态
            String currSource = detail.getSource();
            Double originOutAmount = originOrder.getOutAmount();// 出库数量
            Double currOutAmount = detail.getOutAmount();
            Double originReceiveAmount = originOrder.getReceiveAmount();// 接收数量
            Double currReceiveAmount = detail.getReceiveAmount();
            String sys = "操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + orderId + "订单明细单号为" + detail.getId()
                    + "的订单，";
            if (originUnitPrice != null) {
                sys = sys + (originUnitPrice.compareTo(currUnitPrice) == 0 ? ""
                        : "商品单价由" + originUnitPrice + "变为" + currUnitPrice);
            }

            if (originAmount != null) {
                sys = sys + (Double.doubleToLongBits(originAmount) == Double.doubleToLongBits(currAmount) ? ""
                        : "商品总数量由" + originAmount + "变为" + currUnitPrice);
            }

            if (originUnitName != null) {
                sys = sys + (originUnitName.equals(currUnitName) ? "" : "商品单位由" + originUnitName + "变为" + currUnitName);
            }

            if (originSum != null) {
                sys = sys + (originSum.compareTo(currSum) == 0 ? "" : "商品总价由" + originSum + "变为" + currSum);
            }
            if (originSelfAmount != null) {
                sys = sys + (Double.doubleToLongBits(originSelfAmount) == Double.doubleToLongBits(currSelfAmount) ? ""
                        : "商品自费数量由" + originSelfAmount + "变为" + currSelfAmount);
            }
            if (originFreeAmount != null) {
                sys = sys + (Double.doubleToLongBits(originFreeAmount) == Double.doubleToLongBits(currFreeAmount) ? ""
                        : "商品免费数量由" + originFreeAmount + "变为" + currFreeAmount);
            }
            if (originSelfSum != null) {
                sys = sys + (originSelfSum.compareTo(currSelfSum) == 0 ? "" : "商品自费金额由" + originSelfSum + "变为" + currSelfSum);
            }
            if (originFreeSum != null) {
                sys = sys + (originFreeSum.compareTo(currFreeSum) == 0 ? "" : "商品免费金额由" + originFreeSum + "变为" + currFreeSum);
            }
            if (originSource != null) {
                sys = sys + (originSource.equals(currSource) ? "" : "商品状态由" + originSource + "变为" + currSource);
            }
            if (originOutAmount != null) {
                sys = sys + (Double.doubleToLongBits(originOutAmount) == Double.doubleToLongBits(currOutAmount) ? ""
                        : "商品出库数量由" + originOutAmount + "变为" + currOutAmount);
            }
            if (originReceiveAmount != null) {
                sys = sys + (Double.doubleToLongBits(originReceiveAmount) == Double.doubleToLongBits(currReceiveAmount) ? ""
                        : "商品接收数量由" + originReceiveAmount + "变为" + currReceiveAmount);
            }
            return sys;
        }
        return null;
    }

    /**
     * 比较订单差异
     *
     * @param record
     */
    public void compareOrderData(Order record) {
        SysLogs syslogs = new SysLogs();
        // 获取对象id
        syslogs.setId(UserUtil.getLoginUser().getId());
        syslogs.setModule("网络订单和明细删除操作");
        Order o = orderDao.getById(record.getId());
        // 比较order对应字段
        String orginCustomerId = o.getCustomerId();
        String currCustomerId = record.getCustomerId();
        String originState = o.getState();
        String currState = record.getState();
        String originFlag = o.getFlag();
        String currFlag = record.getFlag();
        String originOuterName = o.getOuterName();
        String currOuterName = record.getOuterName();
        String sys = "操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "操作order编号是:" + record.getOrderNo();
        if (orginCustomerId != null) {
            sys = sys
                    + (orginCustomerId.equals(currCustomerId) ? "" : "配送单位由" + orginCustomerId + "变为" + currCustomerId);
        }
        if (originState != null)
            sys = sys + (originState.equals(currState) ? "" : "订单状态由" + originState + "变为" + currState);
        if (currFlag != null)
            sys = sys + (currFlag.equals(originFlag) ? "" : "订单flag由" + originFlag + "变化为" + currFlag);
        if (originOuterName != null)
            sys = sys
                    + (originOuterName.equals(currOuterName) ? "" : "出货人姓名由" + originOuterName + "变为" + currOuterName);

        List<OrderDetail> orderDetailList = record.getOrderDetailList();
        for (OrderDetail orderDetail : orderDetailList) {
            String compareOrderDetailData = compareOrderDetailData(orderDetail, record.getId());
            sys = sys + compareOrderDetailData;
        }
        syslogs.setUserId(UserUtil.getLoginUser().getId());
        syslogs.setModule("订单");
        syslogs.setModuleId(record.getId());
        syslogs.setFlag(true);
        syslogs.setRemark(sys);
        logService.save(syslogs);
    }

    @Transactional
    @Override
    public String editById(Order record) {
        Order o = orderDao.getById(record.getId());
        // 缺少对比数据日志
        if (!o.getState().equals(OrderState.place))
            return "订单非可编辑状态,不能编辑,请检查后再修改！";
        record.setUpdaterId(UserUtil.getLoginUser().getId());
        // record.setUpdaterId("1");
        if (o.getOrigin().equals(OrderOrigin.webOrder) || o.getOrigin().equals(OrderOrigin.weixin)) {
            o.setCustomerId(record.getCustomerId());
            o.setRealDistributionDate(record.getRealDistributionDate());
            o.setOrderDetailList(record.getOrderDetailList());
            checkService.checkOrder(o);
        } else
            checkService.checkOrder(record);

        // 比对订单数据的不同、然后记录record编辑了哪些数据
        compareOrderData(record);
        record.setUpdaterId(UserUtil.getLoginUser().getId());
        orderDao.editById(record);
        for (OrderDetail detail : record.getOrderDetailList()) {
            if (detail.getEditFlag() == null) {
                detail.setEditFlag(OrderEditFlag.none);
            }
            if (detail.getEditFlag().equals(OrderEditFlag.none))
                continue;
            detail.setOrderId(record.getId());
            if (o.getOrigin().equals(OrderOrigin.webOrder) || o.getOrigin().equals(OrderOrigin.weixin)) {
                if (detail.getSource().equals(OrderDetailSource.original) || detail.getSource().equals(OrderDetailSource.abandon))
                    editWebWeiXinDetail(detail, record.getId());
                else
                    editDetail(detail);
            } else
                editDetail(detail);
        }
        return "订单修改成功！";
    }

    public void editWebWeiXinDetail(OrderDetail detail, String orderId) {
        SysLogs syslogs = new SysLogs();
        OrderDetail originOrder = orderDetailDao.getById(detail.getId());
        if (detail.getEditFlag().equals(OrderEditFlag.add)) {
       
            detail.setSource(OrderDetailSource.newDetail);
            detail.setState(OrderDetailState.place);
            detail.setOrderId(orderId);
            orderDetailDao.save(detail);
        }
        if (detail.getEditFlag().equals(OrderEditFlag.edit)) {
            compareOrderDetailData(detail, orderId);
            orderDetailDao.editStateById(OrderDetailState.del, detail.getId());
            orderDetailDao.editSourceById(OrderDetailSource.abandon, detail.getId());
            detail.setSource(OrderDetailSource.eidtDetail);
            detail.setState(OrderDetailState.place);
            orderDetailDao.save(detail);
        }
        if (detail.getEditFlag().equals(OrderEditFlag.del)) {
            orderDetailDao.editSourceById(OrderDetailSource.abandon, detail.getId());
            orderDetailDao.editStateById(OrderDetailState.del, detail.getId());
        }
    }

    public void editDetail(OrderDetail detail) {
        if (detail.getEditFlag().equals(OrderEditFlag.add)) {
            orderDetailDao.save(detail);
        }
        if (detail.getEditFlag().equals(OrderEditFlag.edit)) {
            orderDetailDao.editById(detail);
        }
        if (detail.getEditFlag().equals(OrderEditFlag.del)) {
            orderDetailDao.delById(detail.getId());
        }
    }

    @Override
    public PageTableData<Order> list(int pagenum, int pagesize, OrderQuery param) {
        PageHelper.startPage(pagenum, pagesize);
        List<Order> datalist = orderDao.list(param);
        for (Order order : datalist) {
            String createTime = order.getCreateTime();
            createTime = createTime.split(" ")[0];
            order.setCreateTime(createTime);
        }
        PageTableData<Order> resultPage = new PageTableData<Order>(datalist);
        PageInfo<Order> p = new PageInfo<Order>(datalist);
        resultPage.setPageNumber(p.getPageNum());
        resultPage.setPageSize(p.getPageSize());
        resultPage.setTotalPage(p.getPages());
        resultPage.setTotal(p.getTotal());
        return resultPage;
    }

    @Override
    @Transactional
    public List<Order> listAll(OrderQuery param) {
        List<Order> datalist = orderDao.list(param);
        return datalist;
    }

    @Override
    @Transactional
    public Order getNext(OrderQuery param) {
        String id = orderDao.getNext(param);
        if (id != null)
            return this.getById(id);
        else
            return null;
    }

    @Override
    @Transactional
    public Order getPre(OrderQuery param) {
        String id = orderDao.getPre(param);
        if (id != null)
            return this.getById(id);
        else
            return null;
    }

    @Override
    public String audit(String id, boolean audited, String auditMemo) {
        SysLogs syslogs = new SysLogs();
        Order order = this.getById(id);
        String userId = UserUtil.getLoginUser().getId();

        if (order == null)
            return "该订单不存在！";
        StoreCustomerQuery q = new StoreCustomerQuery();
        Customer c = customerDao.getById(order.getCustomerId());
        q.setCustomerTypeId(c.getCustomerTypeId() );
        List<StoreCustomer> listSc = scDao.list(q);
        if (listSc == null)
        	 return "该订单客户类型没有设置仓库！";
     
        if (listSc.size() == 0)
       	 	return "该订单客户类型没有设置仓库！";
        
        String state = order.getState();
        if (!state.equals(OrderState.place)) {
            syslogs.setUserId(UserUtil.getLoginUser().getId());
            syslogs.setModule("订单明细添加操作");
            syslogs.setFlag(false);
            syslogs.setModuleId(id);
            syslogs.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + order.getOrderNo()
                    + "该订单的状态不是下单状态,不能进行审核,请确认后在审核！");
            logService.save(syslogs);
            return "该订单的状态不是下单状态,不能进行审核,请确认后在审核！";
        } else {
            if (order.getFlag().equals(OrderError.goodsIdError)) {
                syslogs.setUserId(UserUtil.getLoginUser().getId());
                syslogs.setModule("订单明细添加操作");
                syslogs.setFlag(false);
                syslogs.setModuleId(id);
                syslogs.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + order.getOrderNo()
                        + "该订单中商品存在没有内部商品名称的商品,请确认后审核！");
                logService.save(syslogs);
                return "该订单中商品存在没有内部商品名称的商品,请确认后审核！";
            }
            if (order.getFlag().equals(OrderError.customerIdError)) {
                syslogs.setId(UserUtil.getLoginUser().getId());
                syslogs.setModule("订单明细添加操作");
                syslogs.setFlag(false);
                syslogs.setModuleId(id);
                syslogs.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + order.getOrderNo()
                        + "该订单客户没有明确，请确认后在审核!");
                logService.save(syslogs);
                return "该订单客户没有明确，请确认后在审核！";
            }

            if (audited) {
                orderDao.auditById(userId, id, auditMemo, Verified.pass);
                orderDetailDao.auditByOrderId(id);
                syslogs.setUserId(UserUtil.getLoginUser().getId());
                syslogs.setModule("订单明细添加操作");
                syslogs.setFlag(true);
                syslogs.setModuleId(id);
                syslogs.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + order.getOrderNo() + "审核成功");
                logService.save(syslogs);
                return "订单审核成功！";
            } else {
                orderDao.auditById(userId, id, auditMemo, Verified.fail);
                syslogs.setUserId(UserUtil.getLoginUser().getId());
                syslogs.setModule("订单明细添加操作");
                syslogs.setFlag(true);
                syslogs.setModuleId(id);
                syslogs.setRemark(
                        "操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + order.getOrderNo() + "审核不通过");
                logService.save(syslogs);
                return "订单审核为不通过！";
            }
        }
    }

    @Override
    public String audits(List<QueryAudit> queryAudits) {
        String result = "";
        List<SysLogs> syslogs = new ArrayList<>();
        for (QueryAudit queryAudits1 : queryAudits) {
            Order order = this.getById(queryAudits1.getId());
            String audit = this.audit(queryAudits1.getId(), queryAudits1.isAudited(), queryAudits1.getAuditMemo());
            result = result + order.getOrderNo() + audit + "\n";
        }

        AuditLog auditLog = new AuditLog();
        auditLog.setUserName(UserUtil.getLoginUser().getUsername());
        auditLog.setType(AuditLog.AuditLogType.ORDER);
        auditLog.setCount((long) queryAudits.size());
        auditLogService.save(auditLog);
        return result;
    }

    @Override
    public String unaudit(String id) {
        try {
            Order order = this.getById(id);
            if (order == null)
                return "该订单不存在！";
            String state = order.getState();
            if (!state.equals(OrderState.audited))
                return "该订单的状态不是审核通过状态，请确认后弃审！";
            else {
                orderDao.unauditById(id);
                orderDetailDao.unauditByOrderId(id);
                return "订单弃审成功!";
            }
        } catch (Exception e) {
        }
        return "订单弃审失败!";
    }

    @Override
    public String getNum() {
        String preFix = "O";
        preFix += DateUtil.getDays();
        int id = sequenceDao.getNextValue("Order");
        String no = String.format("%04d", id);
        System.out.println(no);
        return preFix + no;
    }

    /**
     * 一键出库
     *
     * @param orderIds
     * @return
     */
    @Transactional
    @Override
    public String setOuter(List<String> orderIds) {
        // 修改出库数量等于总数量，未判定状态，修改order,orderdetali状态,修改order时间
        List<Order> orderList = new ArrayList<Order>();
        for (String orderId : orderIds) {
            List<OrderDetail> orderDetails = orderDetailDao.getDetailByOrderId(orderId);
            for (OrderDetail orderDetail : orderDetails)
                if (!orderDetail.getState().equals(OrderDetailState.in)) {
                    return "存在状态不为入库状态的订单细节";
                }

            //细节存list
            Order order = new Order();
            order.setId(orderId);
            order.setOrderDetailList(orderDetails);
            orderList.add(order);

        }
        //复制一份，从中删除不满足条件的orderId
        List<String> successIds = new ArrayList<String>(orderIds);

        //遍历订单
        for (Order order : orderList) {
            //获取仓库id
            String storeId = orderDao.getStoreIdByOrderId(order.getId());

            boolean satisfy = true;
            List<OrderDetail> orderDetailList = order.getOrderDetailList();
            //遍历当前订单细节,确认剩余量是否满足条件
            for (int i = 0; i < orderDetailList.size(); i++) {
                OrderDetail orderDetail = orderDetailList.get(i);

                //设置入库单查询条件 仓库id 和 订单细节id
                StoreAndOrderDet storeAndOrderDet = new StoreAndOrderDet();
                storeAndOrderDet.setStoreId(storeId);
                storeAndOrderDet.setOrderDetailId(orderDetail.getId());

                //剩余量降序排列的入库单
                List<StroehouseIn> inList = stroehouseInDao.getByStoreIdAndDetailId(storeAndOrderDet);
                orderDetailList.get(i).setStroehouseInList(inList);

                //计算仓库可用量
                double countSurplus = 0;
                for (StroehouseIn stroehouseIn1 : inList)
                    countSurplus += stroehouseIn1.getSurplus();

                //仓库量小于出库量，出货失败
                if (countSurplus < orderDetail.getAmount()) {
                    successIds.remove(order.getId());
                    satisfy = false;
                    break;
                }
            }
            if (satisfy) {
                for (int i = 0; i < orderDetailList.size(); i++) {
                    OrderDetail orderDetail = orderDetailList.get(i);
                    //减去仓库中 可用量 和 剩余量 ，增添order_detail_in信息

                    //需要出库的量
                    Double outAmount = orderDetail.getAmount();

                    for (StroehouseIn stroehouseIn1 : orderDetail.getStroehouseInList()) {
                        //当前订单剩余量
                        double inSurplus = stroehouseIn1.getSurplus();
                        if (inSurplus >= outAmount) {
                            stroehouseIn1.setSurplus(inSurplus - outAmount);
                            stroehouseIn1.setAvailableAmount(inSurplus - outAmount);
                            stroehouseInDao.editById(stroehouseIn1);

                            OrderDetailIn orderDetailIn = new OrderDetailIn();
                            orderDetailIn.setDetailId(orderDetail.getId());
                            orderDetailIn.setInId(stroehouseIn1.getId());
                            orderDetailIn.setAmount(outAmount);
                            orderDetailInDao.save(orderDetailIn);
                            break;
                        } else {
                            stroehouseIn1.setSurplus((double) 0);
                            stroehouseIn1.setAvailableAmount((double) 0);
                            stroehouseInDao.editById(stroehouseIn1);

                            OrderDetailIn orderDetailIn = new OrderDetailIn();
                            orderDetailIn.setDetailId(orderDetail.getId());
                            orderDetailIn.setInId(stroehouseIn1.getId());
                            orderDetailIn.setAmount(inSurplus);
                            orderDetailInDao.save(orderDetailIn);
                            outAmount -= inSurplus;
                        }
                    }
                }
            }

        }
        //更改订单状态
        int counts = 0;
        if (successIds.size() > 0) {
            orderDetailDao.setOuter(successIds);
            counts = orderDao.setOuter(successIds);
        }

        List<SysLogs> syslogs = new ArrayList<>();
        for (String id : successIds) {
            SysLogs sys = new SysLogs();
            Order byId = this.getById(id);
            sys.setUserId(UserUtil.getLoginUser().getId());
            sys.setModule("订单");
            sys.setFlag(true);
            sys.setModuleId(id);
            sys.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + byId.getOrderNo() + "修改出库数量等于总数量,修改状态为出库");
            syslogs.add(sys);
        }
        sysLogsDao.insertMany(syslogs);
        int tmp = orderIds.size() - counts;
        if (tmp == 0)
            return "共出库" + counts + "条记录";
        else {
            String res = "";
            orderIds.removeAll(successIds);
            for (String id : orderIds)
                res += " " + id;

            return "共出库" + counts + "条记录,还有" + tmp + "条记录未入库，请检查其状态。id为" + res + "的订单，库存量不足。";
        }

    }

    //取消出库
    @Transactional
    @Override
    public String reOutOuter(List<String> orderIds) {
        // 修改出库数量等于总数量，未判定状态，修改order,orderdetali状态,修改order时间


        orderDetailDao.cancelOuter(orderIds);
        int counts = orderDao.cancelOuter(orderIds);

        // ===========2018-11-02把数量加回去==========================
        //加回库房中的可用量和剩余量
        for (String orderId : orderIds) {
            List<OrderDetailIn> orderDetailIns = orderDetailInDao.getByOrderId(orderId);
            for (OrderDetailIn orderDetailIn : orderDetailIns)
                stroehouseInDao.reOuterByInId(orderDetailIn.getInId(), orderDetailIn.getAmount());
            orderDetailInDao.delByOrderId(orderId);
        }


        List<SysLogs> syslogs = new ArrayList<>();
        for (String id : orderIds) {
            SysLogs sys = new SysLogs();
            Order byId = this.getById(id);
            sys.setUserId(UserUtil.getLoginUser().getId());
            sys.setModule("订单");
            sys.setModuleId(id);
            sys.setFlag(true);
            sys.setRemark("操作人的昵称是：" + UserUtil.getLoginUser().getNickname() + "订单号为" + byId.getOrderNo() + "修改出库为0,修改状态为入库");
            syslogs.add(sys);
        }
        sysLogsDao.insertMany(syslogs);
        int tmp = orderIds.size() - counts;
        if (tmp == 0)
            return "共取消出库" + counts + "条记录";
        else
            return "共取消出库" + counts + "条记录,还有" + tmp + "条记录未取消出库，请检查其状态。";
    }

    @Override
    public void updateOrderStateByDetailId(String detailId) {
        String orderId = orderDetailDao.getOrderId(detailId);
        updateOrderStateById(orderId);
    }

    @Override
    public void updateOrderStateById(String orderId) {
        List<OrderDetailStatistic> orderDetailStatistic = orderDetailDao.getOrderDetailStatistic(orderId);
        int total = 0;
        for (OrderDetailStatistic orderDetailStatistic1 : orderDetailStatistic)
            total += orderDetailStatistic1.getTotal();

        int tplusExport=0;
        int tInclock = 0; //导入锁定
        int received = 0;
        int out = 0;
        int in = 0;
        int purchase = 0;
        int prePurchase = 0;
        int audited = 0;
        int place = 0;
        int del = 0;

        for (OrderDetailStatistic orderDetailStatis : orderDetailStatistic) {
            switch (orderDetailStatis.getState()) {
                case OrderDetailState.tplusExport:
                    tplusExport+= orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.tplusLock:
                    tInclock += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.backToCenter:
                case OrderDetailState.backToProvider:
                case OrderDetailState.recordLoss:
                case OrderDetailState.received:
                    received += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.out:
                    out += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.in:
                    in += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.Purchase:
                    purchase += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.prePurchase:
                    prePurchase += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.audited:
                    audited += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.place:
                    place += orderDetailStatis.getTotal();
                    break;
                case OrderDetailState.del:
                    del += orderDetailStatis.getTotal();
                    break;
            }
        }
        //排除删除细节单
        int totalExportDel = total - del;
        if(tplusExport>0)
            orderDao.changeState(orderId, OrderState.tplusExport);
        else if (tInclock >0)
            orderDao.changeState(orderId, OrderState.tplusLock);
        else if (received > 0) {
            if (received == totalExportDel)
                orderDao.changeState(orderId, OrderState.received);
            else orderDao.changeState(orderId, OrderState.partReceived);
        } else if (out > 0)
            orderDao.changeState(orderId, OrderState.out);
        else if (in > 0)
            orderDao.changeState(orderId, OrderState.in);
        else if (purchase > 0)
            orderDao.changeState(orderId, OrderState.Purchase);
        else if (prePurchase > 0)
            orderDao.changeState(orderId, OrderState.prePurchase);
        else if (audited > 0)
            orderDao.changeState(orderId, OrderState.audited);
        else if (place > 0)
            orderDao.changeState(orderId, OrderState.place);
        else
            orderDao.changeState(orderId, OrderState.del);
    }


    @Override
    public Order printOrginById(String id, String flag) {
        Order byId = this.getById(id);
        List<OrderDetail> orderDetailList = byId.getOrderDetailList();
        List<OrderDetail> orderDetailList0 = new ArrayList<>();
        List<OrderDetail> orderDetailList1 = new ArrayList<>();
        for (OrderDetail orderDetail : orderDetailList) {
            if (orderDetail.getSource().equals("0") || orderDetail.equals("1")) {
                orderDetailList0.add(orderDetail);
            } else {
                orderDetailList1.add(orderDetail);
            }
        }
        if (flag.equals("0")) {
            byId.setOrderDetailList(orderDetailList0);
            return byId;
        }
        byId.setOrderDetailList(orderDetailList1);
        return byId;

    }

    @Override
    @Transactional
    public String importExcel(MultipartFile file) throws IOException {
        Workbook batchImport = ExcelUtil.batchImport(file);
        if (batchImport == null) return "上传文件格式不正确";
        Sheet sheet = batchImport.getSheetAt(0);
        Sheet sheet1 = batchImport.getSheetAt(1);
        if (sheet == null || sheet1 == null) {
            return "文件为空";
        }
        int count = sheet.getLastRowNum();
        int rows = 0;
        for (int r = 1; r <= count; r++) {
            rows = r + 1;
            Row row = sheet.getRow(r);
            if (row == null) {
                continue;
            }
            Order order = new Order();
            order.setOrderNo(this.getNum());
            if ( row.getCell(1) == null) {
            	 continue;
            }
            String customerId = ExcelUtil.getcellvalue(row.getCell(1));
            Customer byId = customerDao.getById(customerId);
            if (byId == null) {
                return "订单表:第" + rows + "行第2列 不存在这个ID";
            }
            order.setCustomerId(ExcelUtil.getcellvalue(row.getCell(1)));
            
            order.setCustomerName(byId.getCustomerName());
            
            if (row.getCell(3) == null) {
                return "订单表:第" + rows + "行第4列 格式有问题";
            }
            Date dateCellValue = row.getCell(3).getDateCellValue();
            java.sql.Date date = new java.sql.Date(dateCellValue.getTime());
            order.setDistributionDate(date);
            if (row.getCell(4).getCellTypeEnum() != CellType.STRING || row.getCell(4) == null) {
                rows = r + 1;
                return "订单表:第" + rows + "行第5列 格式有问题";
            }
            order.setMemo(ExcelUtil.getcellvalue(row.getCell(4)));
            ExcelUtil.getcellvalue(row.getCell(7));
            if (row.getCell(5) == null) {
                rows = r + 1;
                return "订单表:第" + rows + "行第6列 格式有问题";
            }
            order.setOrigin(ExcelUtil.getcellvalue(row.getCell(5)));
            order.setCreaterId(UserUtil.getLoginUser().getId());
            String load = this.load(ExcelUtil.getcellvalue(row.getCell(0)), order, sheet1);
            if (!load.equals("1")) {
                return load;
            }
            this.save(order);
        }
        return "Excle数据导入成功";
    }

    public String load(String id, Order order, Sheet sheet1) {

        int count = sheet1.getLastRowNum();
        BigDecimal counts = new BigDecimal(0);
        BigDecimal FreeSum = new BigDecimal(0);
        BigDecimal SelfSum = new BigDecimal(0);
        List<OrderDetail> list = new ArrayList<>();
        int row1 = 0;
        for (int r = 1; r <= count; r++) {
            Row row = sheet1.getRow(r);
            row1 = r + 1;
            if (row == null) {
                continue;
            }
            if (ExcelUtil.getcellvalue(row.getCell(0)).equals(id)) {
                OrderDetail orderDetail = new OrderDetail();
                if ( row.getCell(2) == null) {
                    return "订单明细表:第" + row1 + "行第3列 格式有问题";
                }
                String inGoodsId = ExcelUtil.getcellvalue(row.getCell(2));
                Goods byId = goodsDao.getById(inGoodsId);
                if (byId == null) {
                    return "订单明细表:第" + row1 + "行第3列 没有这个id";
                }
                orderDetail.setInGoodsId(ExcelUtil.getcellvalue(row.getCell(2)));
                
                orderDetail.setUnitName(byId.getUnitName());
                if (row.getCell(4).getCellTypeEnum() != CellType.NUMERIC || row.getCell(4) == null) {
                    return "订单明细表:第" + row1 + "行第5列 格式有问题";
                }
                orderDetail.setFreeAmount(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(5))));
                if (row.getCell(5).getCellTypeEnum() != CellType.NUMERIC || row.getCell(5) == null) {
                    return "订单明细表:第" + row1 + "行第6列 格式有问题";
                }
                orderDetail.setSelfAmount(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(6))));
                if (row.getCell(6).getCellTypeEnum() != CellType.NUMERIC || row.getCell(6) == null) {
                    return "订单明细表:第" + row1 + "行第7列 格式有问题";
                }
                orderDetail.setAmount(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(7))));
                if (row.getCell(7).getCellTypeEnum() != CellType.NUMERIC || row.getCell(7) == null) {
                    return "订单明细表:第" + row1 + "行第8列 格式有问题";
                }
                orderDetail.setUnitPrice(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(8)))));
                if (row.getCell(8).getCellTypeEnum() != CellType.NUMERIC || row.getCell(8) == null) {
                    return "订单明细表:第" + row1 + "行第9列 格式有问题";
                }
                orderDetail.setSum(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(8)))));
                orderDetail.setFreeSum(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(7)))).multiply(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(4))))));
                orderDetail.setSelfSum(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(7)))).multiply(BigDecimal.valueOf(Double.parseDouble(ExcelUtil.getcellvalue(row.getCell(5))))));
                counts = counts.add(orderDetail.getSum());
                FreeSum = FreeSum.add(orderDetail.getFreeSum());
                SelfSum = SelfSum.add(orderDetail.getSelfSum());
                list.add(orderDetail);
            }
        }
        order.setLumpSum(counts);
        order.setFreeSum(FreeSum);
        order.setSelfSum(SelfSum);
        order.setOrderDetailList(list);
        return "1";
    }

	@Override
	public int addPrint(List<String> ids) {
		return orderDao.addPrint(ids);
	}

	@Override
	public List<Order> printInfoByIds(List<String> ids) {
		List<Order> newList = new ArrayList<Order>();
		for (String id : ids) {
			Order o = orderDao.getById(id);
			String companyName=orderDao.getCompanyNameByOrderId(id);
			if (o != null) {
				if (!(o.getState().equals(OrderState.del )|| o.getState().equals(OrderState.place ))) {
					List<OrderDetail> details = orderDetailDao.getTypeDetailByOrderId(id);
					String pretype = "";
					Order preOrder;
					List<OrderDetail> preDetails = null;
					if (details.size() > 0) {
						for (OrderDetail detail : details) {
						    detail.setCompanyName(companyName);
							if (!pretype.equals(detail.getInGoodsTypeId())) {
								preOrder = (Order) o.clone();
								preDetails = new ArrayList<OrderDetail>();
								preOrder.setOrderDetailList(preDetails);
								newList.add(preOrder);
								pretype =detail.getInGoodsTypeId();
							}
							preDetails.add(detail);
						}
					}
				}
			}
		}
		return newList;
	}

}
