package com.myqm.service.gs.order;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderSum;
import com.myqm.pojo.gs.order.OrderTotal;

import java.util.List;

public interface OrderSumService {

    List<OrderSum> sum(OrderTotal orderTotal);
}
