package com.myqm.service.gs.order.impl;

import com.myqm.dao.gs.basic.CustomerDao;
import com.myqm.dao.gs.basic.CustomerTypeDao;
import com.myqm.dao.gs.order.OrderDao;
import com.myqm.dao.gs.order.OrderSumDao;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderSum;
import com.myqm.pojo.gs.order.OrderTotal;
import com.myqm.service.gs.order.OrderSumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderSumServiceImpl implements OrderSumService {
    @Autowired
    OrderSumDao orderSumDao;


    @Override
    public List<OrderSum> sum(OrderTotal orderTotal) {
       List<OrderSum> list= orderSumDao.sum(orderTotal);
       return list;
    }
}
