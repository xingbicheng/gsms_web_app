package com.myqm.service.gs.order;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.myqm.pojo.gs.basic.GoodsInfo;
import com.myqm.pojo.gs.order.Car;
import com.myqm.vo.gs.order.QueryCar;

public interface CarService {

	/**
	 * 根据客户Id删除
	 * @param customerId 客户ID
	 * @return 删除记录数量
	 */
	int delById(String customerId);

	/**
	 * 新增记录
	 * @param record
	 * @return 新增记录数量
	 */
	int add(Car record);

	/**
     * 编辑记录
     * @param record 
     * @return 编辑记录数量
     */
    int editById(Car record);
    
    /**
     * 通过用户Id获取购物车列表
     * @param customerId 用户Id
     * @return 记录数量列表
     */
    PageInfo<Car> listByCustomerId(int pagenum, int pagesize,QueryCar parm);
    
    String copyToCar(String id);
    
    
    List<Car> listAll(QueryCar parm);
    
    /**
     * 删除货物
     * @param goodsInfo 货物信息
     * @return
     */
    int del(GoodsInfo goodsInfo);
}
