package com.myqm.service.gs.order;

import java.util.List;

import com.myqm.pojo.gs.order.Order;

public interface OrderCheckService {

	/**
	 * 检查订单，数量和单位价格，web订单需先检查客户单位和商品是否已经匹配
	 * 
	 * @param order
	 */
	void checkOrder(Order order);

	/**
	 * 检查订单，数量和单位价格，web订单需先检查客户单位和商品是否已经匹配
	 * 
	 * @param orderList
	 */
	void checkOrder(List<Order> orderList);

}
