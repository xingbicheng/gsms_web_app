package com.myqm.service.gs.order;

import com.myqm.pojo.gs.order.Order;
import com.myqm.vo.gs.order.Ids;
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.vo.gs.order.QueryAudit;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.myqm.pojo.PageTableData;

public interface OrderService {
	/**
	 * 根据id删除Order
	 */
	String delById(String id);

	/**
	 * 根据新增Order，id自增
	 */
	String save(Order record);

	/**
	 * 将Web订单批量保存成订单
	 */
	String saveWeb(Ids ids);
	 
	/**
	 * 审核订单
	 */
	String audit(String id, boolean audited, String auditMemo);

	/**
	 * 弃审订单
	 */
	String unaudit(String id);

	/**
	 * 根据id查询Order
	 */
	Order getById(String id);

	/**
	 * 根据id更新Order
	 */
	String editById(Order record);

	/**
	 * 分页查询Order
	 */
	PageTableData<Order> list(int pagenum, int pagesize, OrderQuery param);

	/**
	 * 查询所有Order
	 */
	List<Order> listAll(OrderQuery param);

	/*
	 * 删除改状态
	 */
	String delFlagById(String id);

	/**
	 * 获取上一张
	 * @param orderNumber
	 * @return
	 */
	Order getNext(OrderQuery orderNumber);

	/**
	 * 获取下一张
	 * @param orderNumber
	 * @return
	 */
	Order getPre(OrderQuery orderNumber);

	/**
	 * 批量审核
	 * @param param
	 * @return
	 */
	String audits(List<QueryAudit> queryAudits);

	/**
	 * 获取编号
	 * @return
	 */
	String getNum();

	/**
	 * 设置一键出库
	 * @param orderIds
	 * @return
	 */
	String setOuter(List<String> orderIds);
    /**
     * 取消出库
     * @param orderIds
     * @return
     */
    String reOutOuter(List<String> orderIds);

    /**
     * 升级订单状态
     * @param detailId
     */
    void updateOrderStateByDetailId(String detailId);
    
    /**
     * 升级订单状态
     * @param id
     */
    void updateOrderStateById(String id);
    
    

	Order printOrginById(String id,String flag);
	
	 /**
	  * 导入excel数据到数据库
     * @param id
     */
	String  importExcel(MultipartFile file)throws IOException;
	
	int addPrint(List<String> ids);
	
	List<Order> printInfoByIds(List<String> ids);
}