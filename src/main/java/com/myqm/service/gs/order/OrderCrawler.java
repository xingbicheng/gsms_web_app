package com.myqm.service.gs.order;

import java.sql.Date;
import java.util.List;
import com.myqm.pojo.gs.order.Order;

public interface OrderCrawler {

	String getOrder(List<Order> orders, String url, String username, String password);

	String getOldOrder(List<Order> orders, String url, String username, String password, Date startTime, Date endTime,
			String customerName);

}
