package com.myqm.service.gs.order.impl;

import java.util.*;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.support.json.JSONParser;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonParser;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.base.SequenceDao;
import com.myqm.dao.gs.basic.CustomerDao;
import com.myqm.dao.gs.basic.CustomerTypeDao;
import com.myqm.dao.gs.basic.GoodsConvertDao;
import com.myqm.dao.gs.basic.WebPluginDao; 
import com.myqm.dao.gs.order.WebOrderDao;
import com.myqm.dao.gs.order.WebOrderDetailDao;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.basic.GoodsConvert;
import com.myqm.pojo.gs.basic.WebPlugin;
import com.myqm.pojo.gs.order.Order; 
import com.myqm.pojo.gs.order.Order.OrderOrigin;
import com.myqm.pojo.gs.order.OrderDetail; 
import com.myqm.service.gs.order.OrderCheckService;
import com.myqm.service.gs.order.OrderCrawler; 
import com.myqm.utils.DateUtil;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.order.OrderDetailQuery;
import com.myqm.vo.gs.order.OrderQuery;
import com.myqm.service.gs.order.WebOrderService;

@Service
public class WebOrderServiceImpl implements WebOrderService {
	@Autowired
	CustomerDao customerdao;

	@Autowired
	GoodsConvertDao goodsConvertdao;

	@Autowired
	WebPluginDao plugindao;

	@Autowired
	private CustomerTypeDao customerTypeDao;

	@Autowired
	private WebOrderDao webOrderDao;

	@Autowired
	private SysLogsDao logsDao;

	@Autowired
	private WebOrderDetailDao webOrderDetailDao;

	@Autowired
	private SysUserDao userDao;

	@Autowired
	private OrderCheckService checkService;
	 
	@Autowired
	private SequenceDao sequenceDao;

	
	@Override
	public String saveOrder(String pluginId) throws Exception {
		WebPlugin plugin = plugindao.getById(pluginId);
		OrderCrawler crawler = PluginUtil.execute(plugin.getPath(), plugin.getClassName());
		List<Order> orderList = new ArrayList<Order>();
		String result = crawler.getOrder(orderList, plugin.getUrl(), plugin.getUsername(), plugin.getPassword());
		if (!result.equals("ok")) {
			return result;
		}
		save(orderList);
		return plugin.getName() + "从" + plugin.getUrl() + "共爬取" + orderList.size() + "条数据。";
	}
	
	@Override
	public String saveOldOrder(String pluginId, String customerName, Date beginDate, Date endDate) throws Exception {
		WebPlugin plugin = plugindao.getById(pluginId);
		OrderCrawler crawler = PluginUtil.execute(plugin.getPath(), plugin.getClassName());
		List<Order> orderList = new ArrayList<Order>();
		String result = crawler.getOldOrder(orderList, plugin.getUrl(), plugin.getUsername(), plugin.getPassword(),
				beginDate, endDate, customerName);
		if (!result.equals("ok")) {
			return result;
		}
		save(orderList);
		return plugin.getName() + "从" + plugin.getUrl() + "共爬取" + orderList.size() + "条数据。";
	}

	private void save(List<Order> orderList) {
		corresponding(orderList);
		checkService.checkOrder(orderList);
		String out = JSONArray.toJSONString(orderList);
		System.out.println(out);
		List<Order> orderList1=new ArrayList<Order>();
		for (Order order : orderList){
			if (order.getRealDistributionDate() == null)
				order.setRealDistributionDate(order.getDistributionDate());

			order.setCreaterId(UserUtil.getLoginUser().getId());
			order.setUpdaterId(UserUtil.getLoginUser().getId());
			order.setId(UUID.randomUUID().toString().replaceAll("-", ""));
			order.setOrderNo(this.getNum());
			orderList1.add(order);
			if(orderList1.size()>=100){ 
				webOrderDao.saveAll(orderList1);
				orderList1.clear();
			}
		}
 
		if(orderList1.size()>0)
             webOrderDao.saveAll(orderList1);
			//webOrderDao.saveAll(orderList);

		List<OrderDetail> orderDetailSaveList = new ArrayList<OrderDetail>();
		for (Order order : orderList) { 
			List<OrderDetail> orderDetailList = order.getOrderDetailList();
			for (OrderDetail detail : orderDetailList) {
				detail.setOrderId(order.getId());
				orderDetailSaveList.add(detail);
			}
			if (orderDetailSaveList.size()> 100) { 
				 webOrderDetailDao.saveAll(orderDetailSaveList);
				orderDetailSaveList.clear();
			}
		}
		if(orderDetailSaveList.size()>0)
			webOrderDetailDao.saveAll(orderDetailSaveList);
	}

	/**
	 * 订单 订货单位，内部商品对应
	 * 
	 * @param order
	 */
	private void corresponding(List<Order> orderlist) {
		if (orderlist == null)
			return;
		if (orderlist.size() == 0)
			return;
		Map<String, String> customerIdNameMap = getCustomerIdName();
		Map<String, String> allGoods = getGoodsConvertData();
		for (Order order : orderlist) {
			order.setOrigin(OrderOrigin.webOrder);// "1"
			String cIdTypeyId = customerIdNameMap.get(order.getCustomerName());
			if (cIdTypeyId == null) {
				order.setCustomerId(null); 
				continue;
			}
			String[] strarray = cIdTypeyId.split("_");
			order.setCustomerId(strarray[0]);
			String customerTypId = strarray[1];
			for (OrderDetail detail : order.getOrderDetailList()) {
				String goodsId = allGoods.get(customerTypId + detail.getGoodsOriginalName() + detail.getUnitName());
				if (goodsId == null) {
					detail.setInGoodsId(null);
				}else {
					detail.setInGoodsId(goodsId);
				}
			}
		}
	}

	private Map<String, String> getCustomerIdName() {
		Map<String, String> allCustomer = new HashMap<String, String>();
		List<Customer> customerlist = customerdao.list(null);
		for (Customer customer : customerlist) {
			allCustomer.put(customer.getCustomerName(), customer.getId() + "_" + customer.getCustomerTypeId());
		}
		return allCustomer;
	}

	private Map<String, String> getGoodsConvertData() {
		Map<String, String> allGoods = new HashMap<String, String>();
		List<GoodsConvert> goodslist = goodsConvertdao.list(null);
		for (GoodsConvert goods : goodslist) {
			allGoods.put(goods.getCustomerTypeId() + goods.getOriginalName() + goods.getOriginalUnit(), goods.getInGoodsId());
		}
		return allGoods;
	}

	@Override
	public Order getById(String id) {
		Order order = webOrderDao.getById(id);
		if (order == null)
			return null;

		OrderDetailQuery dq = new OrderDetailQuery();
		dq.setOrderId(id);
		List<OrderDetail> orderDetailList = webOrderDetailDao.list(dq);

		//网络订单特有的外部单位设置
		for(OrderDetail od:orderDetailList)
		    od.setOriginalUnit(od.getUnitName());

		String createrName = userDao.getUserNameById(order.getCreaterId());
		String updaterName = userDao.getUserNameById(order.getUpdaterId());
		if (order.getOuterId() != null) {
			String outerName = userDao.getUserNameById(order.getOuterId());
			order.setOuterName(outerName);
		}
		if (order.getAuditorId() != null) {
			String auditorName = userDao.getUserNameById(order.getAuditorId());
			order.setAuditorName(auditorName);
		}
		if (order.getCustomerTypeId() != null) {
			String customerType = customerTypeDao.getById(order.getCustomerTypeId()).getCustomerType();
		}
		order.setCreaterName(createrName);
		order.setUpdaterName(updaterName);
		order.setOrderDetailList(orderDetailList);
		return order;
	}

	@Override
	public Order getNext(OrderQuery param) {
		String id = webOrderDao.getNext(param);
		if (id != null)
			return this.getById(id);
		else
			return null;
	}

	@Override
	public Order getPre(OrderQuery param) {
		String id = webOrderDao.getPre(param);
		if (id != null)
			return this.getById(id);
		else
			return null;
	}

	@Override
	public PageTableData<Order> list(int pagenum, int pagesize, OrderQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Order> datalist = webOrderDao.list(param);
//		for (Order order : datalist) {
//			String createTime = order.getCreateTime();
//			order.setCreateTime(createTime.split(" ")[0]);
//		}
		PageTableData<Order> resultPage = new PageTableData<Order>(datalist);
		PageInfo<Order> p = new PageInfo<Order>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<Order> listAll(OrderQuery param) {
		List<Order> datalist = webOrderDao.list(param);
		return datalist;
	}

	@Override
	public String getNum() {
		String preFix = "W";
		preFix += DateUtil.getDays();
		int id = sequenceDao.getNextValue("WebOrder");
		
		String no = String.format("%04d", id);
		System.out.println(no);
		return preFix + no;
	}

	@Override
	public String delete( List<String> ids) {
		for (String id : ids) {
			webOrderDetailDao.delByOrderId(id);
			int delById = webOrderDao.delById(id);
		}
		return "修改成功";
	}
}
