package com.myqm.service.gs.order.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.CookieStore;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.service.gs.order.OrderCrawler;

@SuppressWarnings("deprecation")
public class FCWebOrderServiceImpl implements OrderCrawler {

	private String BaseUrl;
	private String UserName;
	private String PSW;
	int DataTag; // 1为回顾 0为打印

	private String LoginUrl = "/E_Admin/adm_Login.aspx";
	private String MenuUrl = "E_Admin/adm_MainFrame.aspx";
	private String MerchOrderUrl;
	private String OrderHeader;
	private String ReviewOrderUrl;
	private String ReviewOrderHeader;

	private HttpClientContext context = null;
	private CookieStore cookieStore = null;
	private DefaultHttpClient httpClient = null;

	private String __EVENTTARGET = "";
	private String __EVENTARGUMENT = "";
	private String __LASTFOCUS = "";
	private String __VIEWSTATE = "";
	private String __EVENTVALIDATION = "";
	private String UCRegion_school1$ddl_UCRegion_school = "-1";
	private String ddl_dwmc = "-1";
	private String UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = "-1";
	private String HF_Islockval = "";
	private String ScriptManager1 = "";
	private boolean IsFirstLoad = true;
	private boolean IsQuery = false;
	private boolean IsPage = false;

	private List<String> Regions;
	private List<String> Schools;
	private List<String> SchoolsDate;

	public void InitUrl(String baseurl, String username, String psw) {
		try {
			BaseUrl = baseurl;
			UserName = username;
			PSW = psw;
		} catch (Exception e) {
		}
	}

	public void Init() {
		context = HttpClientContext.create();
		httpClient = new DefaultHttpClient(new PoolingClientConnectionManager());
		Regions = new ArrayList<String>();
		Schools = new ArrayList<String>();
		SchoolsDate = new ArrayList<String>();
	}

	/*
	 * 关于重定向。 http多种状态已经在HttpStatus这个类中定义好，其中重定向 根据RFC2616中对自动转向的定义，主要有两种：301和302。
	 * 301表示永久的移走（Moved
	 * Permanently），当返回的是301，则表示请求的资源已经被移到一个固定的新地方，任何向该地址发起请求都会被转到新的地址上。
	 * 
	 * 302表示暂时的转向，比如在服务器端的servlet程序调用了sendRedirect方法，则在客户端就会得到一个302的代码，
	 * 这时服务器返回的头信息中location的值就是sendRedirect转向的目标地址。
	 * 
	 * get方式请求，httpclient是自动帮你重定向并拿到响应信息，也可以通过设置method.setFollowRedirects(false)
	 * 不自动转。 post的请求是不能自动跳转的，需要从头部信息中拿到Location（可能是相对路径）
	 * 因此需要对location返回的值做一些处理才可以发起向新地址的请求。
	 */

	// 登录
	public boolean login() {
		try {

			MerchOrderUrl = BaseUrl + "/E_Admin/sos/MerchandiseOrderDownloadSupplier.aspx?" // 订单打印登陆URL
					+ "MenuId=fd526d04-6860-429d-adee-27a95e80822d&RoleID=35089585-f526-4cf0-b59b-40565d70d922&"
					+ "PageName=订单管理++-++订单打印&" + "prodName=绵阳市涪城区益生教育投资有限公司统购系统";
			OrderHeader = BaseUrl + "/E_Admin/sos/MerchandiseOrderDownloadSupplierReview.aspx?" // 订单打印获取数据URL
					+ "MenuId=CCCC&RoleID=35089585-f526-4cf0-b59b-40565d70d922&"
					+ "PageName=%u8ba2%u5355%u7ba1%u7406++-++%u8ba2%u5355%u56de%u987e&"
					+ "prodName=%u7ef5%u9633%u5e02%u6daa%u57ce%u533a%u76ca%u751f%u6559%u80b2%u6295%u8d44%u6709%u9650%u516c%u53f8%u7edf%u8d2d%u7cfb%u7edf";
			ReviewOrderUrl = BaseUrl + "/E_Admin/sos/MerchandiseOrderDownloadSupplierReview.aspx?" // 订单回顾登陆URL
					+ "MenuId=CCCC&RoleID=35089585-f526-4cf0-b59b-40565d70d922&"
					+ "PageName=订单管理++-++订单回顾&prodName=绵阳市涪城区益生教育投资有限公司统购系统";

			ReviewOrderHeader = BaseUrl + "/E_Admin/sos/MerchandiseOrderDownloadSupplierReview.aspx?"
					+ "MenuId=CCCC&RoleID=35089585-f526-4cf0-b59b-40565d70d922&"
					+ "PageName=%e8%ae%a2%e5%8d%95%e7%ae%a1%e7%90%86++-++%e8%ae%a2%e5%8d%95%e5%9b%9e%e9%a1%be&" // 订单回顾获取数据URL
					+ "prodName=%e7%bb%b5%e9%98%b3%e5%b8%82%e6%b6%aa%e5%9f%8e%e5%8c%ba%e7%9b%8a%e7%94%9f%e6%95%"
					+ "99%e8%82%b2%e6%8a%95%e8%b5%84%e6%9c%89%e9%99%90%e5%85%ac%e5%8f%b8%e7%bb%9f%e8%b4%ad%e7%b3%bb%e7%bb%9f";
			HttpGet httpGet = new HttpGet(BaseUrl);
			HttpResponse responcewelcom;
			responcewelcom = httpClient.execute(httpGet, context);
			int status = responcewelcom.getStatusLine().getStatusCode();
			if (status != HttpStatus.SC_OK)
				return false;
			// 服务器的相应内容
			InputStream entity = responcewelcom.getEntity().getContent();
			if (entity == null)
				return false;
			Document doc = Jsoup.parse(entity, "UTF-8", BaseUrl);
			Element element = doc.getElementById("__VIEWSTATE");
			// 获取表单元素的值（输入，文本区域等）。
			String viewstate = element.val();
			element = doc.getElementById("__EVENTVALIDATION");
			String event = element.val();
			element = doc.getElementById("ImageButtonLogin");

			HttpPost httpPost = new HttpPost(BaseUrl + LoginUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:47.0) Gecko/20100101 Firefox/47.0");
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

			// 装配post请求参数
			List<BasicNameValuePair> params = new ArrayList<>();
			params.add(new BasicNameValuePair("__VIEWSTATE", viewstate));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", event));
			params.add(new BasicNameValuePair("TxtName", UserName));
			params.add(new BasicNameValuePair("TxtPassword", PSW));
			params.add(new BasicNameValuePair("ImageButtonLogin.x", "41"));
			params.add(new BasicNameValuePair("ImageButtonLogin.y", "15"));

			// 设置post请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			HttpResponse response = httpClient.execute(httpPost, context);
			status = response.getStatusLine().getStatusCode();
			if (status != 302)
				return false;

			entity = response.getEntity().getContent();
			if (entity == null)
				return false;

			doc = Jsoup.parse(entity, "UTF-8", BaseUrl);
			Element link = doc.select("a").first();
			String absHref = link.attr("abs:href");

			HttpGet mainFrame = new HttpGet(absHref);
			response = httpClient.execute(mainFrame, context);

			entity = response.getEntity().getContent();
			if (entity == null)
				return false;

			return true;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	// 订单打印HTML
	public Document getOrderFrame() {
		DataTag = 0;
		Document doc = new Document("");
		HttpPost httpPost = new HttpPost(MerchOrderUrl);
		httpPost.setHeader("Referer", OrderHeader);
		HttpResponse response;
		try {
			if (IsFirstLoad) {
				IsFirstLoad = false;
			} else {
				List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("__EVENTTARGET", __EVENTTARGET));
				params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
				params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));
				params.add(new BasicNameValuePair("__EVENTARGUMENT", __EVENTARGUMENT));
				params.add(new BasicNameValuePair("__LASTFOCUS", __LASTFOCUS));
				params.add(new BasicNameValuePair("UCRegion_school1$ddl_UCRegion_school",
						UCRegion_school1$ddl_UCRegion_school));
				params.add(new BasicNameValuePair("ddl_dwmc", ddl_dwmc));
				params.add(new BasicNameValuePair("UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime",
						UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime));
				params.add(new BasicNameValuePair("HF_Islockval", HF_Islockval));
				if (IsQuery)
					params.add(new BasicNameValuePair("btn_Query", "查 询"));
				if(IsPage){
					params.add(new BasicNameValuePair("ScriptManager1", ScriptManager1));
					//params.add(new BasicNameValuePair("__ASYNCPOST", "true"));
				}
				httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));
			}
			response = httpClient.execute(httpPost, context);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					doc = Jsoup.parse(entity, "UTF-8", BaseUrl);
					return doc;
				} else
					return doc;
			} else
				return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public Document getReviewOrderFrame() {
		DataTag = 1;
		Document doc = new Document("");
		HttpPost httpPost = new HttpPost(ReviewOrderUrl);
		httpPost.setHeader("Referer", ReviewOrderHeader);
		HttpResponse response;
		try {
			if (IsFirstLoad) {
				IsFirstLoad = false;
			} else {
				List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("__EVENTTARGET", __EVENTTARGET));
				params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
				params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));
				params.add(new BasicNameValuePair("__EVENTARGUMENT", __EVENTARGUMENT));
				params.add(new BasicNameValuePair("__LASTFOCUS", __LASTFOCUS));
				params.add(new BasicNameValuePair("UCRegion_school1$ddl_UCRegion_school",
						UCRegion_school1$ddl_UCRegion_school));
				params.add(new BasicNameValuePair("ddl_dwmc", ddl_dwmc));
				params.add(new BasicNameValuePair("UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime",
						UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime));
				params.add(new BasicNameValuePair("HF_Islockval", HF_Islockval));
				if (IsQuery)
					params.add(new BasicNameValuePair("btn_Query", "查 询"));
				if (IsPage) {
					params.add(new BasicNameValuePair("ScriptManager1", ScriptManager1));
					// params.add(new BasicNameValuePair("__ASYNCPOST", "true"));
				}
				httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			}
			response = httpClient.execute(httpPost, context);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					doc = Jsoup.parse(entity, "UTF-8", BaseUrl);
					return doc;
				} else
					return doc;
			} else
				return doc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// 获取区域
	public void JsoupRegion(Document doc) {
		Element element = doc.getElementById("UCRegion_school1_ddl_UCRegion_school");
		Elements elements = element.children();
		for (Element e : elements) {
			if (!e.val().equals("-1"))
				Regions.add(e.val());
		}
	}

	public void JsoupRegionSchools(Document doc) {
		Element element = doc.getElementById("ddl_dwmc");
		// 清除之前区域所含的学校
		Schools.clear();
		Elements elements = element.children();
		for (Element e : elements) {
			if (!e.val().equals("-1")) {
				if (e.text().contains("(0)"))
					continue;
				Schools.add(e.val());
			}
		}
	}

	public void JsoupRegionSchoolsChoose(Document doc, String customerName) {
		Element element = doc.getElementById("ddl_dwmc");
		// 清除之前区域所含的学校
		Schools.clear();
		Elements elements = element.children();
		for (Element e : elements) {
			if (!e.val().equals("-1")) {
				if (e.text().contains("(0)"))
					continue;
				if (customerName != null) {
					if (e.text().substring(0, e.text().indexOf("(")).equals(customerName)) {
						Schools.add(e.val());
						break;
					} else
						continue;
				} else {
				   // System.out.println("学校val:"+e.text().substring(0, e.text().indexOf("(")));
					Schools.add(e.val());
				}
			}
		}
	}

	public void JsoupOrderDate(Document doc) {
		Element element = doc.getElementById("UCSchoolDeliveryTime1_ddl_TSchoolDeliveryTime");
		SchoolsDate.clear();
		Elements elements = element.children();
		for (int i = elements.size() - 1; i >= 0; i--) {
			Element selected = elements.get(i).getElementsByAttribute("selected").first();

			if (selected != null) {
				UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = elements.get(i).val();
			}
			this.SchoolsDate.add(elements.get(i).val());

		}

	}

	public void JsoupOrderDateChoose(Document doc, Date startTime, Date endTime) {
		Element element = doc.getElementById("UCSchoolDeliveryTime1_ddl_TSchoolDeliveryTime");
		SchoolsDate.clear();
		Elements elements = element.children();
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = elements.size() - 1; i >= 0; i--) {
			Element selected = elements.get(i).getElementsByAttribute("selected").first();
			Date time;
			time  = Date.valueOf(elements.get(i).val()); 
			if (selected != null) {
				UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = elements.get(i).val();
			}
			// 输入日期,按条件爬取
			if (!time.before(startTime) && !time.after(endTime))
				this.SchoolsDate.add(elements.get(i).val());
			// 不输入日期,全部爬取
			else if (startTime == null && endTime == null)
				this.SchoolsDate.add(elements.get(i).val());

		}
	}

	// 给各类参数赋值
	public void JsoupOrderData(Document doc) {
		Element element = doc.getElementById("__EVENTTARGET");
		if (element != null)
			__EVENTTARGET = element.val();
		else
			__EVENTTARGET = "";
		element = doc.getElementById("__EVENTARGUMENT");
		if (element != null)
			__EVENTARGUMENT = element.val();
		else
			__EVENTARGUMENT = "";
		element = doc.getElementById("__LASTFOCUS");
		if (element != null)
			__LASTFOCUS = element.val();
		else
			__LASTFOCUS = "";
		element = doc.getElementById("__VIEWSTATE");
		__VIEWSTATE = element.val();
		element = doc.getElementById("__EVENTVALIDATION");
		__EVENTVALIDATION = element.val();
	}

	public void getOrderBaseInfo(Document orderDoc, Order order) {
		Elements elements = orderDoc.getElementsByAttributeValue("selected", "selected");
		int sTag = 0;
		for (Element e : elements) {
			if (sTag == 1)
				order.setCustomerName(e.text().substring(0, e.text().indexOf("(")));
			if (sTag == 2) {
			order.setDistributionDate(Date.valueOf(e.text()));
				order.setRealDistributionDate(Date.valueOf(e.text()));
			}
			sTag++;
		}
	}

	public void JsoupFcqOrderDetail(Document doc, Order originalOrder, List<OrderDetail> detailList, Integer page) {
		Element orderElement = doc.getElementById("UpdatePanel1");
		Elements orderElements = orderElement.getElementsByTag("tr");
		int trTag = 0;
		for (Element element : orderElements) {
			if (trTag == 0) {
				trTag++;
				continue;
			} else if (trTag >= orderElements.size() - 1)
				break;
			else if (trTag == orderElements.size() - 2) {
				Elements tdElements = element.getElementsByTag("td");
				for (Element tdElement : tdElements) {
					String sumStr = tdElement.text();
					if(page>1){
						String smallSum=sumStr.substring(sumStr.indexOf(":")+1);
						sumStr=smallSum.substring(smallSum.indexOf(":")+1);
					}else{
						sumStr=sumStr.substring(sumStr.indexOf(":")+1);
					}
					double  lumpSum=Double.parseDouble(sumStr); 
					originalOrder.setLumpSum(new BigDecimal(lumpSum));
					//System.out.println(sumStr);
					break;
				}
			} else {
				Elements tdElements = element.getElementsByTag("td");
			   for(int tdTag=0;tdTag<tdElements.size();tdTag+=5){
					if (!tdElements.get(tdTag).text().equals(" ")) {
						OrderDetail orderDetail= new OrderDetail();
						orderDetail.setFreeAmount((double) 0);
						orderDetail.setFreeSum(new BigDecimal(0));
						String name = tdElements.get(tdTag).text();
						orderDetail.setGoodsOriginalName(name);
						double sum = Double.parseDouble((tdElements.get(tdTag + 3).text()));
						double price = Double.parseDouble((tdElements.get(tdTag + 2).text()));
						String amoutString=tdElements.get(tdTag + 1).text();
						String amout="";
						for(int i=0;i<amoutString.length();i++){
							if((amoutString.charAt(i)>=48 && amoutString.charAt(i)<=57)||(amoutString.charAt(i)==46))
								amout+=amoutString.charAt(i);
						}
						orderDetail.setUnitPrice(new BigDecimal(price));
						orderDetail.setSum(new BigDecimal(sum));
						orderDetail.setAmount(Double.parseDouble(amout));
						orderDetail.setSelfAmount(Double.parseDouble(amout));
						orderDetail.setSelfSum(new BigDecimal(sum));
						String unitName=amoutString.replace(amout.toString(), "");
						orderDetail.setUnitName(unitName);
						//System.out.println(unitName);
						detailList.add(orderDetail);
					}	
			   }
			  // System.out.println(detailList.size());
			}
			trTag++;
		}
	}

	public String getPageAttribute(Document orderDoc) {
		Elements pageEles = orderDoc.getElementsByTag("a");
		Element pageEle = pageEles.get(pageEles.size() - 1);
		String strHref = pageEle.attr("href");
		if (strHref == null || strHref.equals(""))
			return null;
		String pageAttribute = strHref.substring(strHref.indexOf("(") + 2, strHref.indexOf(",") - 1);
		return pageAttribute;
	}

	public List<Order> jsoupOrder(List<Order> orderList) {
		Init();
		if (!login())
			return null;
		Document regionDoc = getOrderFrame();// 订单打印的那个
		JsoupRegion(regionDoc);// 得到地域列表
		for (String region : Regions) {
			JsoupOrderData(regionDoc);// 各类参数赋值
			UCRegion_school1$ddl_UCRegion_school = region;

			Document schoolDoc = getOrderFrame();
			JsoupRegionSchools(schoolDoc);// 根据element:ddl_dwmc得到学校列表
			for (String school : Schools) {
				JsoupOrderData(schoolDoc);// 各类参数赋值
				ddl_dwmc = school;

				Document schoolDateDoc = getOrderFrame();
				JsoupOrderDate(schoolDateDoc);

				for (String schoolDate : SchoolsDate) {
					JsoupOrderData(schoolDateDoc);
					IsQuery = true;
					UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = schoolDate;
					// 页数
					Document orderDoc = getOrderFrame();
					Order order = new Order();
					Elements pageElements = orderDoc.getElementsByAttributeValue("id", "LblPageCount");
					Integer page = Integer.valueOf(pageElements.text());
					if (page == null || page.equals(""))
						break;
					// 设置客户，配送时间
					getOrderBaseInfo(orderDoc, order);
					String pageAttribute = new String();
					List<OrderDetail> detailList = new ArrayList<>();
					for (int i = 0; i < page; i++) {
						if (i > 0) {
							JsoupOrderData(orderDoc);
							IsPage = true;
							IsQuery = false;
							__EVENTTARGET = pageAttribute;
							ScriptManager1 = "UpdatePanel1|" + pageAttribute;
							orderDoc = getOrderFrame();
						}
						JsoupFcqOrderDetail(orderDoc, order, detailList, page);// 设置detail中各类参数
						 if (detailList != null)
						 order.setOrderDetailList(detailList);
						pageAttribute = getPageAttribute(orderDoc);
						if (pageAttribute == null || pageAttribute.equals(""))
							break;
					}
					orderList.add(order);
					IsPage = false;
				}
				IsQuery = false;
				UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = "-1";
				ddl_dwmc = "-1";
			}
		}
		return orderList;

	}

	public List<Order> jsoupReviewOrder(List<Order> orderList, Date startTime, Date endTime, String customerName) {
		Init();
		if (!login())
			return null;
		Document regionDoc = getReviewOrderFrame();
		JsoupRegion(regionDoc);
		for (String region : Regions) {
			//System.out.println(region);
			JsoupOrderData(regionDoc);
			UCRegion_school1$ddl_UCRegion_school = region;
			Document schoolDoc = getReviewOrderFrame();
			JsoupRegionSchoolsChoose(schoolDoc, customerName);
			for (String school : Schools) {
				//System.out.println(school);
				JsoupOrderData(schoolDoc);
				ddl_dwmc = school;
				Document schoolDateDoc = getReviewOrderFrame();
				JsoupOrderDateChoose(schoolDateDoc, startTime, endTime);
				int datecount = 0;
				for (String schoolDate : SchoolsDate) {
					//System.out.println(schoolDate);
//					if (DataTag == 1)// 历史数据只采集10天，防止数据过多
//					{
//						if (datecount++ >= 10)
//							break;
//					}

					JsoupOrderData(schoolDateDoc);
					IsQuery = true;
					UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = schoolDate;
					Document orderDoc = getReviewOrderFrame();
					Order originalOrder = new Order();
					Elements pageElement = orderDoc.getElementsByAttributeValue("id", "LblPageCount");
					Integer page = Integer.valueOf(pageElement.text());
					if (page == null || page.equals(""))
						break;
					getOrderBaseInfo(orderDoc, originalOrder);
					String pageAttribute = new String();
					List<OrderDetail> originalOrderDetailList = new ArrayList<OrderDetail>();
					for (int i = 0; i < page; i++) {
						if (i > 0) {
							IsPage = true;
							IsQuery = false;
							JsoupOrderData(orderDoc);
							__EVENTTARGET = pageAttribute;
							ScriptManager1 = "UpdatePanel1|" + pageAttribute;
							orderDoc = getReviewOrderFrame();
						}
						JsoupFcqOrderDetail(orderDoc, originalOrder, originalOrderDetailList, page);
						 if (originalOrderDetailList != null)
						 originalOrder.setOrderDetailList(originalOrderDetailList);;
						pageAttribute = getPageAttribute(orderDoc);
						if (pageAttribute == null || pageAttribute.equals(""))
							break;
					}
					orderList.add(originalOrder);
					IsPage = false;
				}
				IsQuery = false;
				UCSchoolDeliveryTime1$ddl_TSchoolDeliveryTime = "-1";
				ddl_dwmc = "-1";
				if (Schools.size() == 1)
					break;
			}
		}
		// System.out.println(originalOrderList.size());
		return orderList;
	}

	public void release() {
		httpClient.getConnectionManager().shutdown();
	}

	@Override
	public String getOrder(List<Order> orders, String url, String username, String password) {
		InitUrl(url, username, password);
		jsoupOrder(orders); // 爬取数据
		//System.out.println("查看" + orders.size());
		if (orders.size() == 0)
			return "No data";
		return "ok";
	}

	@Override
	public String getOldOrder(List<Order> orders, String url, String username, String password, Date startTime,
			Date endTime, String customerName) {
		InitUrl(url, username, password); // 初始化URL和请求参数信息
		jsoupReviewOrder(orders, startTime, endTime, customerName); // 爬取数据
		//System.out.println("查看" + orders.size());
		release();
		if (orders.size() == 0)
			return "No data";
		return "ok";
	}

}
