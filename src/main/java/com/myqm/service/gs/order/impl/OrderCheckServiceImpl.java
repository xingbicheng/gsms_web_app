package com.myqm.service.gs.order.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myqm.dao.gs.basic.GoodsDao;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.Order.OrderError;
import com.myqm.pojo.gs.order.Order.OrderOrigin;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailError;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.order.OrderCheckService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.UserUtil;

@Service
public class OrderCheckServiceImpl implements OrderCheckService {

	@Autowired
	GoodsDao goodsDao; 
	@Autowired
	SysLogsService logService;
	@Override
	public void checkOrder(Order order) {
		Map<String, Goods> allGoods = getGoods();
		this.checkOneOrder(order, allGoods);
	}

	private Map<String, Goods> getGoods() {
		Map<String, Goods> allGoods = new HashMap<String, Goods>();
		List<Goods> goodslist = goodsDao.list(null);
		for (Goods goods : goodslist) {
			allGoods.put(goods.getId(), goods);
		}
		return allGoods;
	}

	@Override
	public void checkOrder(List<Order> orderList) {
		Map<String, Goods> allGoods = getGoods();
		for (Order order : orderList) {
			checkOneOrder(order, allGoods);
		}
	}

	public void checkOneOrder(Order order, Map<String, Goods> allGoods) {  
			if (order.getCustomerId()!= null)
				order.setFlag(OrderError.noError);
			else{
				order.setFlag(OrderError.customerIdError);
				List<OrderDetail> orderDetailList = order.getOrderDetailList();
				for (OrderDetail orderDetail : orderDetailList) {
					orderDetail.setFlag(OrderDetailError.customerIdError);
				}
				return;
			} 
			
			List<OrderDetail> orderDetailList = order.getOrderDetailList();
			for (OrderDetail orderDetail : orderDetailList) {
				if (orderDetail.getInGoodsId() == null){
					orderDetail.setFlag(OrderDetailError.goodsIdError);
					order.setFlag(OrderError.goodsIdError);
					continue;
				} 
				Goods goods = allGoods.get(orderDetail.getInGoodsId());
				if (goods == null ){
					orderDetail.setFlag(OrderDetailError.goodsIdError);
					order.setFlag(OrderError.goodsIdError);
					continue;
				}
				// 数量预警
				if (goods.getWarningAmount() != null){
					if (orderDetail.getAmount() >= goods.getWarningAmount()) {
						orderDetail.setFlag(OrderDetailError.goodsAmountError);
						if (order.getFlag().endsWith(OrderError.noError))
							order.setFlag(OrderError.detailError);
						continue;
					}
				}
				// 单价预警
				if (goods.getWarningPrice() != null){
					if (orderDetail.getUnitPrice().compareTo(goods.getWarningPrice()) > 0) {
						orderDetail.setFlag(OrderDetailError.goodsPricesError);
						if (order.getFlag().endsWith(OrderError.noError))
							order.setFlag(OrderError.detailError);
					}
				} 
			}  
	}
		 
}
