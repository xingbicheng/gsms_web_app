package com.myqm.service.gs.order;

import java.sql.Date;
import java.util.List;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.order.Order;
import com.myqm.vo.gs.order.OrderQuery;

public interface WebOrderService {
	String saveOrder(String pluginId) throws Exception;

	String saveOldOrder(String pluginId, String customerName, Date beginDate, Date endDate) throws Exception;

	Order getById(String id);

	PageTableData<Order> list(int pagenum, int pagesize, OrderQuery param);

	List<Order> listAll(OrderQuery param);

	Order getNext(OrderQuery orderNumber);

	Order getPre(OrderQuery orderNumber);
	/**
	 * 获取编号
	 * @return
	 */
	String getNum();
	String delete( List<String> ids);
}
