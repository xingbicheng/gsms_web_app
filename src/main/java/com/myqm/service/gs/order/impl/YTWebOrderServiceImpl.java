package com.myqm.service.gs.order.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.Order.OrderOrigin;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.service.gs.order.OrderCrawler;


public class YTWebOrderServiceImpl implements OrderCrawler {

	HttpClientContext context = null;
	DefaultHttpClient httpClient = null;
	List<String> Regions;
	List<String> Schools;
	List<String> SchoolsDate;
	String MerchOrderUrl;
	String MerchHeader;
	String __EVENTTARGET;
	String __EVENTARGUMENT;
	String __LASTFOCUS;
	String __VIEWSTATE;
	String __VIEWSTATEGENERATOR;
	String __EVENTVALIDATION;
	String HiddenFSupplier;
	String HiddenFSupplierName;
	String HiddenFMerchSchoolGroupID;
	String HiddenFSchool;
	String HiddenFSchoolName;
	String DdlDeliveryDate = "-1";
	boolean IsFirstLoad = true;
	
	int DataTag; //1为回顾  0为打印
	private final String LoginUrl = "/Admin/Eniac_Login.aspx";
	String Uid;
	String OrderHeader;
	
	@Override
	public String getOrder(List<Order> orders, String url, String username, String password) {
		this.Init();
		this.login(0, url, username, password);
		this.getOrderFrame(url);
		this.ProcessOrder(orders, url);
		return "ok";
	}

	@Override
	public String getOldOrder(List<Order> orders, String url, String username, String password, Date startTime, Date endTime,
			String customerName) {
		this.Init();
		this.login(1, url, username, password);
		this.getOrderFrame(url);
		this.ProcessOrder(orders, url, startTime, endTime, customerName);
		return "ok";
	}
	
	public void Init() {
		context = HttpClientContext.create();
		httpClient = new DefaultHttpClient(new PoolingClientConnectionManager());
		Regions = new ArrayList<String>();
		Schools = new ArrayList<String>();
		SchoolsDate = new ArrayList<String>();
		__EVENTTARGET="";
		__EVENTARGUMENT="";
		__LASTFOCUS="";
		__VIEWSTATE="";

		__VIEWSTATEGENERATOR="";
		__EVENTVALIDATION="";

		HiddenFSupplier="";
		HiddenFSupplierName="";

		HiddenFMerchSchoolGroupID="";
		HiddenFSchool="";
		HiddenFSchoolName="";
		DdlDeliveryDate = "-1";
		IsFirstLoad = true;
	}

	public boolean login(int tag, String url, String username, String password) {
		try {
			DataTag = tag;
			HttpGet httpGet = new HttpGet(url);
			HttpResponse responceWelcom = httpClient.execute(httpGet, context);
			int status = responceWelcom.getStatusLine().getStatusCode();
			if (status != HttpStatus.SC_OK){
				return false;
			}
			
			InputStream entity = responceWelcom.getEntity().getContent();
			if(entity == null)
				return false;
			
			Document doc = Jsoup.parse(entity, "UTF-8", url);
			Element element = doc.getElementById("__VIEWSTATE");
			String viewstate = element.val();
			element = doc.getElementById("__VIEWSTATEGENERATOR");
			String generator = element.val();
			element = doc.getElementById("__EVENTVALIDATION");
			String event = element.val();
			
			HttpPost httpPost = new HttpPost(url + LoginUrl);
			// 禁止重定向，由于刚刚Post的状态值是重定向，所以我们要去禁止它，不然网页会乱飞。
			// httpPost.getParams().setParameter(ClientPNames.HANDLE_REDIRECTS,
			// false);
			// 设置头部信息（头部信息在刚刚的Httpwatch下面Headers标签会有，不过我感觉写多跟写少没多大区别，只是多写没有坏处吧。）
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:47.0) Gecko/20100101 Firefox/47.0");
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
			
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", generator));
			params.add(new BasicNameValuePair("__VIEWSTATE", viewstate));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", event));
			params.add(new BasicNameValuePair("TxtName", username));
			params.add(new BasicNameValuePair("TxtPassword", password));
			params.add(new BasicNameValuePair("BtnSubmit", ""));
			params.add(new BasicNameValuePair("HidIAID", ""));
			params.add(new BasicNameValuePair("HidDigest", ""));
			
			httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));
			HttpResponse response = httpClient.execute(httpPost,context);
			status = response.getStatusLine().getStatusCode();
			if (status !=302)
				return false;

			entity = response.getEntity().getContent();
			if (entity == null)
				return false;
			
			doc = Jsoup.parse(entity, "UTF-8", url);
			Element link = doc.select("a").first();
			String absHref = link.attr("abs:href");
			
			HttpGet mainframe = new HttpGet(absHref);
			response = httpClient.execute(mainframe, context);

			entity = response.getEntity().getContent();
			if (entity == null)
				return false;

			doc = Jsoup.parse(entity, "UTF-8", url);
			link = doc.select("frame ").first();
			absHref = link.attr("src");
			Uid = absHref.substring(23, absHref.length());
			// =====================预览历史记录===========================
			if(tag==1){
				MerchOrderUrl = url +
				 "/Admin/Adm_MerchOrderDownloadReview.aspx?"
				 + "Mid=7ae03e54-47da-4214-84dd-1c0d0800ca49&" + "Uid=" + Uid +
				 "&"
				 + "Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&" +
				 "PageName=A320566930BBDF568A7907C44C7B4E7D&"
				 + "PPageName=28B0EB59994D08C4C1BD10492E9E76A6&" + "Mindex=1";
				
				 MerchHeader = url +
				 "/Admin/Eniac_Menu.aspx?Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&"
				 + "Uid=" + Uid;
				 OrderHeader = url +
				 "/Admin/Adm_MerchOrderDownloadReview.aspx?"
				 + "Mid=7ae03e54-47da-4214-84dd-1c0d0800ca49&" + "Uid=" + Uid +
				 "&"
				 + "Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&" +
				 "PageName=A320566930BBDF568A7907C44C7B4E7D&"
				 + "PPageName=28B0EB59994D08C4C1BD10492E9E76A6&Mindex=1";
				}else{
					// =====================打印记录===================================
					MerchOrderUrl = url + "/Admin/Adm_MerchOrderDownload.aspx?"
							+ "Mid=7712daac-6057-4cbb-9b01-d57c4a7c25ed&" + "Uid=" + Uid + "&"
							+ "Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&" + "PageName=1273111864CEC9D834D169929411D897&"
							+ "PPageName=28B0EB59994D08C4C1BD10492E9E76A6&" + "Mindex=0";
	
					MerchHeader = url + "/Admin/Eniac_Menu.aspx?Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&" + "Uid=" + Uid;
	
					OrderHeader = url + "/Admin/Adm_MerchOrderDownload.aspx?" + "Mid=7712daac-6057-4cbb-9b01-d57c4a7c25ed&"
							+ "Uid=" + Uid + "&" + "Did=3fbc2818-b63d-43d5-893e-a588b7ebace7&"
							+ "PageName=1273111864CEC9D834D169929411D897&"
							+ "PPageName=28B0EB59994D08C4C1BD10492E9E76A6&Mindex=0";
				}
			return true;
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean getOrderFrame(String url) {
		HttpGet httpget = new HttpGet(MerchOrderUrl);
		httpget.setHeader("Referer", MerchHeader);
		HttpResponse response;
		try {
			response = httpClient.execute(httpget, context);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					Document doc = Jsoup.parse(entity, "UTF-8", url);
					JsoupRegionSchools(doc);
					return true;
				} else
					return false;
			} else
				return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void JsoupRegionSchools(Document doc) {
		this.JsoupOrderData(doc);
		this.JsoupOrderDate(doc);
		if (IsFirstLoad) {
			Elements elems = doc.getElementsByAttributeValueMatching("id", "LnkBtnMerchSchoolGroup");
			for (Element e : elems) {
				String temp = e.attr("href");
				temp = temp.substring(25, temp.length() - 5);
				Regions.add(temp);
			}
			IsFirstLoad = false;
		}

		Elements elems = doc.getElementsByAttributeValueMatching("id", "LnkBtnSchool");
		Schools.clear();
		for (Element e : elems) {
			String temp = e.attr("href");
			temp = temp.substring(25, temp.length() - 5);
			Schools.add(temp);
		}
	}
	
	public void JsoupOrderData(Document doc) {
		Element element = doc.getElementById("__EVENTTARGET");
		if (element != null)
			__EVENTTARGET = element.val();
		else
			__EVENTTARGET = "";
		element = doc.getElementById("__EVENTARGUMENT");
		if (element != null)
			__EVENTARGUMENT = element.val();
		else
			__EVENTARGUMENT = "";
		element = doc.getElementById("__LASTFOCUS");
		if (element != null)
			__LASTFOCUS = element.val();
		else
			__LASTFOCUS = "";

		element = doc.getElementById("__VIEWSTATE");
		__VIEWSTATE = element.val();
		element = doc.getElementById("__VIEWSTATEGENERATOR");
		__VIEWSTATEGENERATOR = element.val();
		element = doc.getElementById("__EVENTVALIDATION");
		__EVENTVALIDATION = element.val();

		element = doc.getElementById("HiddenFSupplier");
		HiddenFSupplier = element.val();

		element = doc.getElementById("HiddenFSupplierName");
		HiddenFSupplierName = element.val();

		element = doc.getElementById("HiddenFMerchSchoolGroupID");
		HiddenFMerchSchoolGroupID = element.val();

		element = doc.getElementById("HiddenFSchool");
		HiddenFSchool = element.val();

		element = doc.getElementById("HiddenFSchoolName");
		HiddenFSchoolName = element.val();
	}

	public void JsoupOrderDate(Document doc) {
		Element element = doc.getElementById("DdlDeliveryDate");
		this.SchoolsDate.clear();
		Elements elems = element.children();
		for (Element e : elems) {
			Element selected = e.getElementsByAttribute("selected").first();
			if (selected != null) {
				DdlDeliveryDate = e.val();
			}
			if (!e.val().equals("-1"))
				this.SchoolsDate.add(e.val());
		}
	}

	public void ProcessOrder(List<Order> list, String url) {
		for (int i= 0; i < Regions.size(); i++) {
			String regionstr = Regions.get(i);
			if (!this.getRegionFrame(regionstr, url))
				continue;
			for (int j= 0; j < Schools.size(); j++) {
				String schoolstr = Schools.get(j);
				if (!this.getSchoolFrame(schoolstr, url)) 
					continue;
			
				int datecount = 0;
				for (int k= 0; k < SchoolsDate.size(); k++) {
					String datestr = SchoolsDate.get(k);
					Date date  = java.sql.Date.valueOf(datestr); 
					Order order = new Order();
					order.setOrigin(OrderOrigin.webOrder);
					List<OrderDetail> detail = new ArrayList<OrderDetail>();
					order.setOrderDetailList(detail);
					order.setCustomerName(this.HiddenFSchoolName);
					//System.out.println(HiddenFSchoolName);
					//String teststr = datestr.replaceAll("-", "/");
					order.setDistributionDate(date);
					order.setRealDistributionDate(date);
					if (!this.getDateFrame(datestr, order, url))
						continue;
					if (order.getOrderDetailList().size() != 0)
						list.add(order);
					
				}
			}
		}
	}
	
	public void ProcessOrder(List<Order> list, String url, Date startTime,Date endTime, String customerName) {
		for (int i= 0; i < Regions.size(); i++) {
			String regionstr = Regions.get(i);
			if (!this.getRegionFrame(regionstr, url))
				continue;
			for (int j= 0; j < Schools.size(); j++) {
				String schoolstr = Schools.get(j);
				if (!this.getSchoolFrame(schoolstr, url)) 
					continue;
				
				if(customerName != null && !customerName.equals(this.HiddenFSchoolName)) {
					continue;
				}
				int datecount = 0;
				List<String> schoolsDate = new ArrayList<String>();
				for(int l= 0; l <  SchoolsDate.size(); l++) {
					String stringDate = SchoolsDate.get(l);
					Date date = java.sql.Date.valueOf(stringDate);
					if(date.before(startTime) || endTime.before(date)) {
						continue;
					}
					schoolsDate.add(stringDate);
				}
				for (int k= 0; k < schoolsDate.size(); k++) {
					
					if (DataTag == 1)//历史数据只采集10天，防止数据过多
					{						
						if (datecount++ >= 20)  {
							//System.out.println("break;");
							break; 
						}
					}
					String datestr = schoolsDate.get(k);
					Date date = java.sql.Date.valueOf(datestr);
					Order order = new Order();
					order.setOrigin(OrderOrigin.webOrder);
					List<OrderDetail> detail = new ArrayList<OrderDetail>();
					order.setOrderDetailList(detail);
					order.setCustomerName(this.HiddenFSchoolName);
					//System.out.println(HiddenFSchoolName);
					//String teststr = datestr.replaceAll("-", "/");
					order.setDistributionDate(date);
					order.setRealDistributionDate(date);
					if (!this.getDateFrame(datestr, order, url))
						continue;
					if (order.getOrderDetailList().size() != 0)
						list.add(order);
				}
			}
		}
	}
	
	public boolean getRegionFrame(String regiontargeturl, String url) {

		HttpPost httpPost = new HttpPost(MerchOrderUrl);
		httpPost.setHeader("Referer", OrderHeader);
		HttpResponse response;
		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("__EVENTTARGET", regiontargeturl));
			params.add(new BasicNameValuePair("__EVENTTARGETMENU", ""));
			params.add(new BasicNameValuePair("__LASTFOCUS", ""));
			params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
			params.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));

			params.add(new BasicNameValuePair("HiddenFSupplier", HiddenFSupplier));
			params.add(new BasicNameValuePair("HiddenFSupplierName", HiddenFSupplierName));
			params.add(new BasicNameValuePair("HiddenFMerchSchoolGroupID", HiddenFMerchSchoolGroupID));
			params.add(new BasicNameValuePair("HiddenFSchool", HiddenFSchool));
			params.add(new BasicNameValuePair("HiddenFSchoolName", HiddenFSchoolName));
			params.add(new BasicNameValuePair("DdlDeliveryDate", DdlDeliveryDate));
			params.add(new BasicNameValuePair("Eniac_PageNavigator2$txtNewPageIndex", "1"));
			params.add(new BasicNameValuePair("Eniac_PageNavigator1$txtNewPageIndex", "1"));

			httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));

			response = httpClient.execute(httpPost, context);
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					Document doc = Jsoup.parse(entity, "UTF-8", url);
					JsoupRegionSchools(doc);
					return true;
				} else
					return false;
			} else
			{
				System.out.println("HttpStatus：" + String.valueOf(status));
				return false;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean getSchoolFrame(String schooltargeturl, String url) {

		HttpPost httpPost = new HttpPost(MerchOrderUrl);
		httpPost.setHeader("Referer", OrderHeader);
		HttpResponse response;
		try { 

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("__EVENTTARGET", schooltargeturl));
			params.add(new BasicNameValuePair("__EVENTTARGETMENU", ""));
			params.add(new BasicNameValuePair("__LASTFOCUS", ""));
			params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
			params.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));

			params.add(new BasicNameValuePair("HiddenFSupplier", HiddenFSupplier));
			params.add(new BasicNameValuePair("HiddenFSupplierName", HiddenFSupplierName));
			params.add(new BasicNameValuePair("HiddenFMerchSchoolGroupID", HiddenFMerchSchoolGroupID));
			params.add(new BasicNameValuePair("HiddenFSchool", HiddenFSchool));
			params.add(new BasicNameValuePair("HiddenFSchoolName", HiddenFSchoolName));
			params.add(new BasicNameValuePair("DdlDeliveryDate", DdlDeliveryDate));
			params.add(new BasicNameValuePair("Eniac_PageNavigator2$txtNewPageIndex", "1"));
			params.add(new BasicNameValuePair("Eniac_PageNavigator1$txtNewPageIndex", "1"));

			httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));

			response = httpClient.execute(httpPost, context);

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					Document doc = Jsoup.parse(entity, "UTF-8", url);
					JsoupOrderData(doc);
					JsoupOrderDate(doc);

					return true;
				} else
					return false;
			} else
			{
				//System.out.println("HttpStatus：" + String.valueOf(status));
				return false;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean getDateFrame(String dateStr, Order order, String url) {

		boolean haveDate = false;
		for (int i = 0; i < this.SchoolsDate.size(); i++) {
			if (dateStr.equals(this.SchoolsDate.get(i))) {
				haveDate = true;
				break;
			}
		}
		if (!haveDate)
			return false;

		HttpPost httpPost = new HttpPost(MerchOrderUrl);
		httpPost.setHeader("Referer", OrderHeader);
		HttpResponse response;
		try {

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("__EVENTTARGET", "DdlDeliveryDate"));
			params.add(new BasicNameValuePair("__EVENTTARGETMENU", ""));
			params.add(new BasicNameValuePair("__LASTFOCUS", ""));
			params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
			params.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));

			params.add(new BasicNameValuePair("HiddenFSupplier", HiddenFSupplier));
			params.add(new BasicNameValuePair("HiddenFSupplierName", HiddenFSupplierName));
			params.add(new BasicNameValuePair("HiddenFMerchSchoolGroupID", HiddenFMerchSchoolGroupID));
			params.add(new BasicNameValuePair("HiddenFSchool", HiddenFSchool));
			params.add(new BasicNameValuePair("HiddenFSchoolName", HiddenFSchoolName));
			params.add(new BasicNameValuePair("DdlDeliveryDate", dateStr));
			params.add(new BasicNameValuePair("Eniac_PageNavigator2$txtNewPageIndex", "1"));
			params.add(new BasicNameValuePair("Eniac_PageNavigator1$txtNewPageIndex", "1"));

			httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));

			response = httpClient.execute(httpPost, context);

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					Document doc = Jsoup.parse(entity, "UTF-8", url);
					 
					JsoupOrderDetail(doc, dateStr, order, url);
					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	public void JsoupOrderDetail(Document doc, String datestr, Order order, String url) throws IOException {
		this.JsoupOrderData(doc);
		this.JsoupOrderDate(doc); 
		BigDecimal amount, lumpSum;
		JsoupStxOrderPage(doc, order); 
  		Element e = doc.getElementsMatchingOwnText("小计").first();
		if (e != null) {
				String temp = e.text();
				int index = temp.indexOf("：");
				temp = temp.substring(index + 1, temp.length());
				amount = BigDecimal.valueOf(Double.parseDouble(temp));
				order.setLumpSum(amount);
		} 
	}

	public boolean getOrderPageFrame(String pageid, String dateStr, Order order, String url) {
		HttpPost httpPost = new HttpPost(MerchOrderUrl);
		httpPost.setHeader("Referer", OrderHeader);
		HttpResponse response;
		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("__EVENTTARGET", "Eniac_PageNavigator2$LnkBtnGoto"));
			params.add(new BasicNameValuePair("__EVENTTARGETMENU", ""));
			params.add(new BasicNameValuePair("__LASTFOCUS", ""));
			params.add(new BasicNameValuePair("__VIEWSTATE", __VIEWSTATE));
			params.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR));
			params.add(new BasicNameValuePair("__EVENTVALIDATION", __EVENTVALIDATION));

			params.add(new BasicNameValuePair("HiddenFSupplier", HiddenFSupplier));
			params.add(new BasicNameValuePair("HiddenFSupplierName", HiddenFSupplierName));
			params.add(new BasicNameValuePair("HiddenFMerchSchoolGroupID", HiddenFMerchSchoolGroupID));
			params.add(new BasicNameValuePair("HiddenFSchool", HiddenFSchool));
			params.add(new BasicNameValuePair("HiddenFSchoolName", HiddenFSchoolName));
			params.add(new BasicNameValuePair("DdlDeliveryDate", dateStr));
			params.add(new BasicNameValuePair("Eniac_PageNavigator2$txtNewPageIndex", pageid));
			params.add(new BasicNameValuePair("Eniac_PageNavigator1$txtNewPageIndex", "1"));

			httpPost.setEntity((HttpEntity) new UrlEncodedFormEntity(params, "UTF-8"));
			response = httpClient.execute(httpPost, context);

			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				InputStream entity = response.getEntity().getContent();
				if (entity != null) {
					Document doc = Jsoup.parse(entity, "UTF-8", url);

					JsoupStxOrderPage(doc, order);

					return true;
				} else
					return false;
			} else
				return false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	
	public void JsoupStxOrderPage(Document doc, Order order) {
		Elements elements = doc.getElementsByAttribute("onmouseover");
		OrderDetail d = null;
		String valuestr, unitname;
		BigDecimal unit;
		boolean over = false;
		double selfcount = 0, freecount = 0;
		double selfsum = 0, freesum = 0;
		double orderFreeSum = 0, orderSelfSum = 0;
		for (Element row : elements){// 一行
			Elements cols = row.children();
			int count = 0;
			for (Element col : cols) {
				switch (count) {
				case 0:
					if (col.text().equals("")) {
						over = true;
					} else {
						d = new OrderDetail();
						d.setGoodsOriginalName(col.text());
						//System.out.println(col.text());
						order.getOrderDetailList().add(d);
					}
					break;
				case 1:
					valuestr = col.text();
					//System.out.println(valuestr);
					int index1 = valuestr.indexOf('[');
					int index2 = valuestr.indexOf(']');

					unitname = valuestr.substring(index1 + 1, index2);
					//unitname=valuestr.substring(valuestr.indexOf('/')+1, valuestr.indexOf('/')+2);
					String temp2 = valuestr.substring(0, index1);
					unit = BigDecimal.valueOf(Double.parseDouble(temp2));
					d.setUnitPrice(unit);
					d.setUnitName(unitname);
					//System.out.println(unitname);
					break;
				case 2:
					valuestr = col.text();
					//System.out.println(valuestr);
					selfcount = Double.parseDouble(valuestr);
					d.setSelfAmount(selfcount);
					d.setAmount(selfcount);
					break;
				case 3:
					valuestr = col.text();
					//System.out.println(valuestr);
					freecount = Double.parseDouble(valuestr);
					d.setFreeAmount(freecount);
					d.setAmount(selfcount + freecount);
					break;
				case 4:
					valuestr = col.text();
					//System.out.println(valuestr);
					selfsum = Double.parseDouble(valuestr);
					d.setSelfSum(BigDecimal.valueOf(selfsum));
					orderSelfSum += selfsum;
					break;
				case 5:
					valuestr = col.text();
					//System.out.println(valuestr);
					freesum = Double.parseDouble(valuestr);
					d.setFreeSum(BigDecimal.valueOf(freesum));
					d.setSum(BigDecimal.valueOf(selfsum + freesum));
					orderFreeSum += freesum;
					break;
				}
				count++;
				if (count >= 6)
					count = 0;

				if (over)
					break;
			}
			if (over)
				break;
		}
		order.setFreeSum(BigDecimal.valueOf(orderFreeSum));
		order.setSelfSum(BigDecimal.valueOf(orderSelfSum));
	}

	 public static void main(String[] args) throws ParseException {
		 YTWebOrderServiceImpl yt = new YTWebOrderServiceImpl();
		 List<Order> orders = new ArrayList<Order>();
		 String url = "http://ytx.tgxt.net/";
		 String username = "蔬菜";
		 String password = "520666"; 
	     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		 String result = yt.getOldOrder(orders, url, username, password, 
				 new java.sql.Date(sdf.parse("2018-12-01").getTime()),
				 new java.sql.Date(sdf.parse("2018-12-15").getTime()),
				"盐亭县大兴回族乡义务教育学校"); 
		 for(Order o : orders){
			 System.out.println(o.getDistributionDate());
			for(OrderDetail od :o.getOrderDetailList())
			{
				 System.out.println(od.getGoodsOriginalName());
			}
		 }
		 System.out.println(result);
		 
		 
	}

}
