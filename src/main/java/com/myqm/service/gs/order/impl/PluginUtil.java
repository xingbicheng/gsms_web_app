package com.myqm.service.gs.order.impl;

import java.net.URL;
import java.net.URLClassLoader;
import com.myqm.service.gs.order.OrderCrawler;

public class PluginUtil {

	/**
	 * 执行插件
	 * 
	 * @param filePath
	 *            插件路径
	 * @param className
	 *            类名
	 * @return
	 * @throws Exception
	 */
	public static OrderCrawler execute(String filePath, String className) throws Exception {
		filePath = "file:" + filePath;
		URLClassLoader classLoader = new URLClassLoader(new URL[] { new URL(filePath) },
				Thread.currentThread().getContextClassLoader());
		Class clazz = classLoader.loadClass(className);
		Object object = clazz.newInstance();
		return (OrderCrawler) object;
	}
}
