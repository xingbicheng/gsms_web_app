package com.myqm.service.gs.tplus.impl;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional; 
import com.myqm.utils.DateUtil;
import com.myqm.utils.UrlSendUtil;
import com.myqm.vo.gs.in.StroehouseInQuery;
import com.myqm.dao.base.SequenceDao;
import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.dao.gs.basic.StorehouseDao;
import com.myqm.dao.gs.basic.SysSetDao;
import com.myqm.dao.gs.in.StroehouseInDao; 
import com.myqm.dao.gs.receive.BackProviderDao;
import com.myqm.dao.gs.receive.StroehouseBackDao;
import com.myqm.dao.gs.tplus.TPlusInDao; 
import com.myqm.pojo.gs.basic.Provider;
import com.myqm.pojo.gs.basic.ResponseRoot;
import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.pojo.gs.in.CountOrderDetailStateByIn;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.pojo.gs.in.TPlusEntryOrder;
import com.myqm.pojo.gs.in.TPlusEntryOrderList;
import com.myqm.pojo.gs.in.TPlusGoodsLine;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetail.OrderDetailState;
import com.myqm.pojo.gs.tplus.TPlusLockInfo;
import com.myqm.pojo.gs.tplus.TPlusOrderCustomer;
import com.myqm.pojo.gs.tplus.TPlusProvider;
import com.myqm.service.gs.order.OrderService; 
import com.myqm.service.gs.tplus.TplusInService;

@Service
public class TplusInServiceImpl implements TplusInService {
	@Autowired
	private SequenceDao sequenceDao;

	@Autowired
	private StroehouseInDao stroehouseInDao;

	@Autowired
	private TPlusInDao tplusInDao;

	@Autowired
	private StorehouseDao storehousDao;

	@Autowired
	private ProviderDao providerDao;

	 
	@Autowired
	private OrderService orderService;


	@Autowired
	private BackProviderDao backProviderDao;

	@Autowired
	private StroehouseBackDao backStorebDao;
	@Autowired
	private SysSetDao sysSetDao;
	@Override
	public List<StroehouseIn> listNotBack(String storehouseId, Date inDate, String state) {
		StroehouseInQuery q = new StroehouseInQuery();
		q.setStorehouseId(storehouseId);
		q.setInTimeBegin(inDate);
		q.setInTimeEnd(inDate);
		q.setCanBack("0");
		q.setState(state);
		List<StroehouseIn> allIn = stroehouseInDao.list(q);
		return allIn;
	}

	@Override
	public String notBackExport(List<String> ids, String storehouseId) {
		String result = "";
		int count = 0;
		String firstNum = "";
		List<StroehouseIn> allIn = tplusInDao.listByIds(ids);
		if (!this.CheckListByStoreId(allIn, storehouseId))
			return "导入数据不是一个仓库的或者入库数据状态不正确";

		Storehouse store = storehousDao.getById(storehouseId);
		String toStoreCode = store.getToStoreCode(); 
		String tplus = store.getTplusAccount();
		if (toStoreCode == null ||toStoreCode.equals(""))
			return "仓库数据未设置导入到T+的套账，请设置后导入";

		if (tplus == null ||tplus.equals(""))
			return "仓库数据未设置导入到T+的仓库，请设置后导入";
		
		String providerId = "";
		String tplusNum = "";
		Provider provider = null;
		String providerCode = "";
		String goodsTypeId = "";
		List<StroehouseIn> dayIn = new ArrayList<StroehouseIn>();
		List<String> dayInIds = new ArrayList<String>();
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (StroehouseIn in : allIn) {
			if (providerId.equals(in.getProviderId()) && goodsTypeId.endsWith(in.getGoodsTypeId())) {
				dayIn.add(in);
				dayInIds.add(in.getId());
				continue;
			}
			if (dayIn.size() != 0) {
				String ot = sdf.format(dayIn.get(0).getInTime());
				ResponseRoot root = null;
				try {
					tplusNum = this.getNum();
					root = exportIn(store.getTplusAccount(), dayIn, tplusNum, ot, providerCode, toStoreCode, false);
					if (root.getCode().equals("00")) {
						tplusInDao.setTplusState(dayInIds, tplusNum);
						count++;
						if (count == 1)
							firstNum = tplusNum;
					}

				} catch (JAXBException e) {
					result = provider.getContactName() + "采购入库导入+失败，请检查T+服务器是否运行正常！\"";
					e.printStackTrace();
				}

				dayIn.clear();
				dayInIds.clear();

			}

			if (!providerId.equals(in.getProviderId())) {
                provider = providerDao.getById(in.getProviderId());
                if(provider!=null) {
                    providerCode = provider.getCode();
                    providerId = provider.getId();
                }

			}
			goodsTypeId = in.getGoodsTypeId();
			dayIn.add(in);
			dayInIds.add(in.getId());
		}

        if (dayIn.size() != 0) {
            String ot = sdf.format(dayIn.get(0).getInTime());
            ResponseRoot root = null;
            try {
                tplusNum = this.getNum();
                root = exportIn(store.getTplusAccount(), dayIn, tplusNum, ot, providerCode, toStoreCode, false);
                if (root.getCode().equals("00")) {
                    tplusInDao.setTplusState(dayInIds, tplusNum);
                    count++;
                    if (count == 1)
                        firstNum = tplusNum;
                }

            } catch (JAXBException e) {
                result = provider.getContactName() + "采购入库导入+失败，请检查T+服务器是否运行正常！\"";
                e.printStackTrace();
            }

            dayIn.clear();
            dayInIds.clear();
        }


		if (count > 0)
			return "共导入" + count + "条。采购单号到T+ ,单据号为" + firstNum + "-" + tplusNum;
		else 
			return "共导入" + count + "条";
	}

	private boolean checkNotBackIn(StroehouseIn in, String storeId) {
		if (in.getState().equals(StroehouseInState.loss) && (in.getStorehouseId().equals(storeId)) && !in.getCanBack())
			return true;
		else
			return false;
	}

	private boolean CheckListByStoreId(List<StroehouseIn> listStoreIn, String storeId) {
		boolean result = true;
		for (StroehouseIn in : listStoreIn) {
			if (!checkNotBackIn(in, storeId)) {
				return false;
			}
		}
		return result;
	}

	private String getNum() {
		String preFix = "IN";
		preFix += DateUtil.getDays();
		int id = sequenceDao.getNextValue("TPlusInOrder");
		String no = String.format("%04d", id);
		return preFix + no;
	}

	private ResponseRoot exportIn(String tPlusId, List<StroehouseIn> ins, String orderCode, String ot, String providerCode,
			String storeCode, boolean canback) throws JAXBException {

		//String tplus = "http://220.166.61.4:8090/API/T?app_key=20181029001&method=entryorder.create&format=xml";
		//String tplus = "http://220.166.61.4:8090/API/T?id=1&app_key=20181029001&method=entryorder.create&format=xml";
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1();
		 
		String tplus = url+"&id="+tPlusId+"&method=entryorder.create&format=xml";

		
		
		TPlusEntryOrderList exportList = new TPlusEntryOrderList();

		TPlusEntryOrder order = new TPlusEntryOrder(orderCode, storeCode, providerCode, ot);
		order.setOrderLines(new ArrayList<TPlusGoodsLine>());
		for (StroehouseIn in : ins) {
			double sum = in.getAmount();
			if (canback) {
				// 查询退货问题
				Double sum1 = backProviderDao.getBackSum(in.getId());
				Double sum2 = backStorebDao.getBackSum(in.getId());
				if (sum1 != null)
					sum -= sum1;
				if (sum2 != null)
					sum -= sum2;
			}
			TPlusGoodsLine line = new TPlusGoodsLine(in.getGoodsCode(), sum, in.getPrice());
			order.getOrderLines().add(line);
		}

		exportList.getList().add(order);
		JAXBContext context = JAXBContext.newInstance(TPlusEntryOrderList.class, TPlusEntryOrder.class,
				TPlusGoodsLine.class);
		final Marshaller marshaller = context.createMarshaller();
		StringWriter sw = new StringWriter();
		marshaller.marshal(exportList, sw);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);// 是否格式化
		// 保存数据成xml
		System.out.println(sw.toString());
		String out = sw.toString();
		out = out.replace("\"", "\\\"");
		System.out.println(out);

		out = "\"" + out + "\"";
		System.out.println(out);
		String result = UrlSendUtil.sendPost(tplus, out);
		System.out.println(result);

		result = result.substring(1, result.length() - 1);
		result = result.replaceAll("\\\\\"", "\"");
		JAXBContext context1 = JAXBContext.newInstance(ResponseRoot.class);
		final Unmarshaller unmarshaller = context1.createUnmarshaller();
		ResponseRoot response = (ResponseRoot) unmarshaller.unmarshal(new StringReader(result));
		return response;
	}
 

	// 检查入库单对应单销售单是否都进入了结算阶段。
	private boolean checkOrderState(String stroehouoseInId) {
		List<CountOrderDetailStateByIn> states = this.tplusInDao.countStateByInId(stroehouoseInId);

		for (CountOrderDetailStateByIn ods : states) {
			String state = ods.getState();
			if (state.equals(OrderDetailState.place))
				return false;
			if (state.equals(OrderDetailState.audited))
				return false;
			if (state.equals(OrderDetailState.prePurchase))
				return false;
			if (state.equals(OrderDetailState.Purchase))
				return false;
			if (state.equals(OrderDetailState.in))
				return false;
			if (state.equals(OrderDetailState.out))
				return false;
			if (state.equals(OrderDetailState.back))
				return false;
//			if (state.equals(OrderDetailState.tplusLock))
//				return false;
//			if (state.equals(OrderDetailState.tplusExport))
//				return false; 
		}
		return true;
	}

	public static void main(String[] args) throws JAXBException {
		TplusInServiceImpl ser = new TplusInServiceImpl();
		List<StroehouseIn> ins = new ArrayList<StroehouseIn>();
		String orderCode = "3377";
		String ot = "2018-11-22";
		String providerCode = "test";
		String storeCode = "WARE01";
		StroehouseIn in1 = new StroehouseIn();
		in1.setGoodsCode("In001");
		in1.setAmount(88.0);
		in1.setPrice(new BigDecimal("23.5"));
		ins.add(in1);
		StroehouseIn in2 = new StroehouseIn();
		in2.setGoodsCode("In001");
		in2.setAmount(99.0);
		in2.setPrice(new BigDecimal("33.5"));
		ins.add(in2);
		ResponseRoot response = ser.exportIn("1", ins, orderCode, ot, providerCode, storeCode, false);
		System.out.println(response.getMessage());

	}

	@Transactional
	@Override
	public String exportByProvider(String storehouseId, String providerId, Date beginDate, Date endDate) {
		int count = 0;
		String result = "";
		String firstNum = "";
		StroehouseInQuery q = new StroehouseInQuery();
		q.setProviderId(providerId);
		q.setInTimeBegin(beginDate);
		q.setInTimeEnd(endDate);
		q.setState(StroehouseInState.tplusLock);
		List<StroehouseIn> listin = tplusInDao.listStoreIn(q);

		Storehouse store = storehousDao.getById(storehouseId);
		String toStoreCode = store.getToStoreCode();//.getCode();
		Provider provider = providerDao.getById(providerId);
		String providerCode = provider.getCode();
		String preDate = "";
		String goodsTypeId = "";
		String tplusNum = "";

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		List<StroehouseIn> allIn = new ArrayList<StroehouseIn>();
		List<String> allInIds = new ArrayList<String>();
		for (StroehouseIn in : listin) {
			String indate = sdf.format(in.getInTime());

			if (indate.equals(preDate) || goodsTypeId.equals(in.getGoodsTypeId())) {
				allIn.add(in);
				allInIds.add(in.getId()); 
				continue;
			}else{ 
				if (allIn.size() != 0) {
					ResponseRoot root = null;
					try {
						tplusNum = this.getNum();
						root = exportIn(store.getTplusAccount(), allIn, tplusNum, preDate, providerCode, toStoreCode, true);
						if (root.getCode().equals("00")) {
							tplusInDao.setTplusState(allInIds, tplusNum);
							count++;
							if (count == 1)
								firstNum = tplusNum;
						}
	
					} catch (JAXBException e) {
						return provider.getContactName() + "采购入库T+导入错误,请检测T+服务器！";
	
					} finally {
						allIn.clear();
						allInIds.clear();
					}
				}
				allIn.add(in);
				allInIds.add(in.getId());
				preDate = indate;
				goodsTypeId = in.getGoodsTypeId();
			}
		}

        if (allIn.size() != 0) {
            ResponseRoot root = null;
            try {
                tplusNum = this.getNum();
                root = exportIn(store.getTplusAccount(), allIn, tplusNum, preDate, providerCode, toStoreCode, true);
                if (root.getCode().equals("00")) {
                    tplusInDao.setTplusState(allInIds, tplusNum);
                    count++;
                    if (count == 1)
                        firstNum = tplusNum;
                }

            } catch (JAXBException e) {
                return provider.getContactName() + "采购入库T+导入错误,请检测T+服务器！";

            } finally {
                allIn.clear();
                allInIds.clear();
            }
        }

		if (count > 0)
			return "共导入" + count + "条。采购单号到T+ ,单据号为" + firstNum + "-" + tplusNum;
		else 
			return "共导入" + count + "条";
	}

	private boolean checkIn(StroehouseIn in, String storeId) {
		// 已经做完损耗
		if (in.getState().equals(StroehouseInState.loss) && (in.getStorehouseId().equals(storeId)))
			return true;
		else if (in.getState().equals(StroehouseInState.tplusImport) && (in.getStorehouseId().equals(storeId)))
			return true;// T+已经导入数据
		else if (in.getState().equals(StroehouseInState.tplusLock) && (in.getStorehouseId().equals(storeId)))
			return true;
		else
			return false;
	}

	@Override
	public boolean checkOrder(String storehouseId, Date beginDate, Date endDate, StringBuffer  msg) {
		Storehouse store = storehousDao.getById(storehouseId);
		String toStoreCode = store.getToStoreCode(); 
		String tplus = store.getTplusAccount();
		if (toStoreCode == null ||toStoreCode.equals("")){
			msg.append("仓库数据未设置导入到T+的套账，请设置后导入");
			return false;
		}

		if (tplus == null ||tplus.equals("")){
			msg.append("仓库数据未设置导入到T+的仓库，请设置后导入");
			return false;
		} 
		
		StroehouseInQuery q = new StroehouseInQuery();
		q.setInTimeBegin(beginDate);
		q.setInTimeEnd(endDate);
		List<StroehouseIn> list = stroehouseInDao.list(q);
		for (StroehouseIn in : list) {
			if (!this.checkIn(in, storehouseId)) {
				msg.append(in.getProviderName() + "-" + in.getGoodsName() + "入库单状态不正确，请检查。");
			}
			if (!checkOrderState(in.getId())) {
				msg.append(in.getProviderName() + "-" + in.getGoodsName() + "入库单所涉及到到销售订单没有完全结算");
			}
		}
		if (msg.toString().equals(""))
			return true;
		else
			return false;
	}

	@Override
	public String getLock(String storehouseId, Date beginDate, Date endDate,
			TPlusLockInfo lockInfo){
		
		List<TPlusProvider> proverders = tplusInDao.listLockProvider(storehouseId, beginDate, endDate);
		lockInfo.setProviders(proverders);
		
		List<TPlusOrderCustomer> listcustomer = lockInfo.getOrders();
		 
		StroehouseInQuery q = new StroehouseInQuery();
		q.setInTimeBegin(beginDate);
		q.setInTimeEnd(endDate);
		List<StroehouseIn> list = stroehouseInDao.list(q); 
		if (list.size() ==0)
			return "无入库数据，请重新设置锁定参数。"; 
		
		List<String> orderIds = new ArrayList<String>();
		for (StroehouseIn in : list) {
			List<String> tempOrderids = tplusInDao.getOrderIdByInId(in.getId()); 
			orderIds.addAll(tempOrderids); 
		} 
		removeDuplicate(orderIds);
		//查询客户订单信息
		this.getCustomerOrderInfo(orderIds, storehouseId,listcustomer);
		
		//已经锁定订单信息  
		return "共获取" + proverders.size() + "个供应商采购入库数据及" + listcustomer.size() + "个客户销售单数据。";
	 
	}
	
	private static List removeDuplicate(List<String> list) {   
	    Set h = new HashSet<String>(list);   
	    list.clear();   
	    list.addAll(h);   
	    return list;   
	}   
	private  List<TPlusOrderCustomer> getCustomerOrderInfo(List<String> orderIds, String storeId, List<TPlusOrderCustomer> listcustomer) {

		List<Order> orders = tplusInDao.listOrderByIds(orderIds);
		String customerId = "";
		TPlusOrderCustomer oc;
		List<String> tempOrderNos = null;
		List<String> tempOrderIds = null;
		
		for(Order o : orders)
		{
			if (!customerId.equals(o.getCustomerId()))
			{
				oc = new TPlusOrderCustomer();
				oc.setCustomerId(o.getCustomerId());
				oc.setCustomerName(o.getCustomerName());
				oc.setStoreId(storeId);
				tempOrderNos = new ArrayList<String>();
				tempOrderIds = new ArrayList<String>();
				oc.setOrderIds(tempOrderIds);
				oc.setOrderNos(tempOrderNos);
                listcustomer.add(oc);
                customerId = o.getCustomerId();
			}
			tempOrderNos.add(o.getOrderNo());
			tempOrderIds.add(o.getId());
		}
		return listcustomer;
	}

	@Override
	public String lock(String storehouseId, Date beginDate, Date endDate, TPlusLockInfo lockInfo) {
		
		List<TPlusProvider> proverders = tplusInDao.listProvider(storehouseId, beginDate, endDate);
		lockInfo.setProviders(proverders);
		
		List<TPlusOrderCustomer> listcustomer = lockInfo.getOrders();
		 
		StroehouseInQuery q = new StroehouseInQuery();
		q.setInTimeBegin(beginDate);
		q.setInTimeEnd(endDate);
		List<StroehouseIn> list = stroehouseInDao.list(q);
		if (list.size() ==0)
			return "无入库数据，请重新设置参数。";
		List<String> inIds = new ArrayList<String>();
		List<String> orderIds = new ArrayList<String>();
		int countDetail = 0;
		int countIn = 0;
		for (StroehouseIn in : list) {
			countDetail += tplusInDao.setOrderDetailLockByInId(in.getId());
			List<String> tempOrderids = tplusInDao.getOrderIdByInId(in.getId());
			inIds.add(in.getId());
			orderIds.addAll(tempOrderids); 
		}
		countIn = tplusInDao.setInLockByInIds(inIds);
		removeDuplicate(orderIds);
		for(String id: orderIds) {
			orderService.updateOrderStateById(id);
		}
		//查询客户订单信息
		 this.getCustomerOrderInfo(orderIds, storehouseId,listcustomer);
		
		//锁定订单信息  
		return "共锁定" + countIn + "条采购入库数据及" + listcustomer.size() + "个客户销售单数据。";

	}    
}
