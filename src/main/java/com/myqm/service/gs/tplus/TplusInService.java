package com.myqm.service.gs.tplus;

import java.sql.Date;
import java.util.List;

import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.tplus.TPlusLockInfo;
import com.myqm.pojo.gs.tplus.TPlusOrderCustomer; 

public interface TplusInService {
	/**
	*列出所有不能退货的订单
	*/
	List<StroehouseIn> listNotBack(String storehouseId, Date inDate,String state);
	/**
	*批量导入销售入库单
	*/
	String notBackExport(List<String> ids,String storehouseId);
	 
	/**
	 *检查所有订单是否结算完
	*/
	public boolean checkOrder(String storehouseId, 
			Date beginDate, Date endDate,StringBuffer  msg);
	
	/*
	 * 锁定采购入库及订单
	 */ 
	String lock(String storehouseId, Date beginDate, Date endDate,TPlusLockInfo lockInfo);
	/*
	 * 获取锁定采购入库及订单
	 */  
	String getLock(String storehouseId, Date beginDate, Date endDate,TPlusLockInfo lockInfo);

	/**
	 *按供应商导入
	 */ 
	String exportByProvider(String storehouseId,  String providerId, Date beginDate,Date endDate); 
	 
}