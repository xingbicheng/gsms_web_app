package com.myqm.service.gs.tplus.impl;

import com.myqm.dao.gs.basic.CustomerDao; 
import com.myqm.dao.gs.basic.StorehouseDao;
import com.myqm.dao.gs.basic.SysSetDao;
import com.myqm.dao.gs.order.OrderDao; 
import com.myqm.dao.gs.tplus.TPlusInDao;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.pojo.gs.basic.ResponseRoot;
import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.pojo.gs.in.TPlusGoodsLine;
import com.myqm.pojo.gs.order.Order; 
import com.myqm.pojo.gs.order.TPlusDeliveryOrder;
import com.myqm.pojo.gs.order.TPlusDeliveryOrderList;
import com.myqm.pojo.gs.tplus.TPlusOrderCustomer;
import com.myqm.service.gs.tplus.TplusOrderService;
import com.myqm.utils.UrlSendUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;  
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class TplusOrderServiceImpl implements TplusOrderService {

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private TPlusInDao tplusInDao;

	@Autowired
	private StorehouseDao storehousDao;

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private SysSetDao sysSetDao;
	@Transactional
	@Override
	public String exportOrder(TPlusOrderCustomer customerInfo) {
		// 获取基础信息
		Storehouse store = storehousDao.getById(customerInfo.getStoreId());
		String toStoreCode = store.getToStoreCode();//.getCode();
		Customer customer = customerDao.getById(customerInfo.getCustomerId());
		String customerCode = customer.getCode();

		List<String> errorStatusNo = new ArrayList<String>();
		// 需要导入单据
		List<Order> orderList = tplusInDao.listOrderByIds(customerInfo.getOrderIds());

		TPlusDeliveryOrderList tPlusDeliveryOrderList = new TPlusDeliveryOrderList();
		List<TPlusDeliveryOrder> tPlusDeliveryOrders = new ArrayList<TPlusDeliveryOrder>();
		tPlusDeliveryOrderList.setList(tPlusDeliveryOrders);
		int count = 0;
		int errorcount = 0;

		for (Order order : orderList) {
			if (!Order.OrderState.tplusLock.equals(order.getState())) {
				errorStatusNo.add(order.getOrderNo());
				errorcount++;
				continue;
			}
			if (!order.getCustomerId().equals(customerInfo.getCustomerId())) {
				errorStatusNo.add(order.getOrderNo());
				errorcount++;
				continue;
			}

			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String ot = sdf.format(order.getDistributionDate());

			TPlusDeliveryOrder tPlusDeliveryOrder = new TPlusDeliveryOrder(order.getOrderNo(), 
					toStoreCode, customerCode, ot);

			List<TPlusGoodsLine> tPlusGoodsLines = tplusInDao.listByOrderId(order.getId());
			String goodsType = "";
			List<TPlusGoodsLine> tempLines = null;
			TPlusDeliveryOrder temporder = null;
			int no = 0;
			for (TPlusGoodsLine orderDetail1 : tPlusGoodsLines) {
				if (!goodsType.equals(orderDetail1.getGoodsTypeId())) { 
					no++;
					try {
						temporder = (TPlusDeliveryOrder) tPlusDeliveryOrder.clone();
					} catch (Exception e) {
						temporder = tPlusDeliveryOrder;
						e.printStackTrace();
					}  
					temporder.setDeliveryOrderCode(order.getOrderNo() +"-"+ no);
					tempLines = new ArrayList<TPlusGoodsLine>();
					temporder.setOrderLines(tempLines);
					tPlusDeliveryOrders.add(temporder);
					count++;
					goodsType = orderDetail1.getGoodsTypeId(); 
					
				}
				tempLines.add(orderDetail1);
			}
			String msg = "";
			boolean result = this.sendOrder(store.getTplusAccount(), tPlusDeliveryOrderList, order.getId(), msg);
			if (!result)
				return msg;
			else{
				tPlusDeliveryOrderList = new TPlusDeliveryOrderList();
			}
		}
 
		int ordercount = orderList.size() - errorcount;
		String re = "共导出" + ordercount + "张订单。分解后导入到T+订单数为" + count + "张，共有" + errorcount + "张错误。";
		if (errorStatusNo.size() != 0) {
				re += ",以下订单号状态不满足条件：";
				for (String id : errorStatusNo)
					re += " id";
		}
		return re; 
	}
	
	private boolean sendOrder(String tPlusId, TPlusDeliveryOrderList tPlusDeliveryOrderList, String orderId,String msg){
	 	ResponseRoot response;
		for (TPlusDeliveryOrder order : tPlusDeliveryOrderList.getList())	{ 
			TPlusDeliveryOrderList sendlist = new TPlusDeliveryOrderList(); 
			sendlist.getList().add(order);  
			try {
				response = send(tPlusId, sendlist);   
			} catch (Exception e) {
				msg +=  "TPlus导出失败，请检查T+服务器是否运行正常！";
				return false;
			}   
			if ((response != null) && (response.getCode().equals("00"))) {
				 continue;
			} else {
				msg +=  "TPlus导出失败，请检查T+服务器是否运行正常！";
				return false;
			}  
		 }  
		tplusInDao.changeOrderTEById(orderId);
		tplusInDao.changeOrderDetalTEByOrderId(orderId); 
		return true;
	}

	public ResponseRoot send( String tPlusId, TPlusDeliveryOrderList tPlusDeliveryOrderList) throws JAXBException {
		//String tplus = "http://220.166.61.4:8090/API/T?app_key=20181029001&method=stockout.create&format=xml";
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1(); 
		String tplus = url+"&id="+tPlusId+"&method=stockout.create&format=xml";
		
		JAXBContext context = JAXBContext.newInstance(TPlusDeliveryOrderList.class, TPlusDeliveryOrder.class,
				TPlusGoodsLine.class);
		final Marshaller marshaller = context.createMarshaller();
		StringWriter sw = new StringWriter();
		marshaller.marshal(tPlusDeliveryOrderList, sw);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);// 是否格式化
		// 保存数据成xml

		System.out.println(sw.toString());
		String out = sw.toString();
		out = out.replace("\"", "\\\"");
		System.out.println(out);

		out = "\"" + out + "\"";
		System.out.println(out);
		String result = UrlSendUtil.sendPost(tplus, out);
		System.out.println(result);

		result = result.substring(1, result.length() - 1);
		result = result.replaceAll("\\\\\"", "\"");
		JAXBContext context1 = JAXBContext.newInstance(ResponseRoot.class);
		final Unmarshaller unmarshaller = context1.createUnmarshaller();

		ResponseRoot response = (ResponseRoot) unmarshaller.unmarshal(new StringReader(result));
    	return response;

	}

	public static void main(String[] args) throws JAXBException {
		TplusOrderServiceImpl test = new TplusOrderServiceImpl();
		List<String> ids = new ArrayList<String>();
		Collections.addAll(ids, "c248d842ec9011e8b63000163e0a39a2");
		// test.exportOrder(ids);
	}
}
