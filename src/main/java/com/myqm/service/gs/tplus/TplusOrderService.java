package com.myqm.service.gs.tplus; 
import com.myqm.pojo.gs.tplus.TPlusOrderCustomer;
 
public interface TplusOrderService  {

    String exportOrder(TPlusOrderCustomer customerInfo) ;

}
