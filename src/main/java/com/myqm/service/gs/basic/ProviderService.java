package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.Provider;
import com.myqm.vo.gs.basic.ProviderQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;

public interface ProviderService {
	/**
	 * 根据id删除Provider
	 */
	String delById(String id);

	/**
	 * 根据新增Provider，id自增
	 */
	String save(Provider record);

	/**
	 * 根据id查询Provider
	 */
	Provider getById(String id);

	/**
	 * 根据id更新Provider
	 */
	String editById(Provider record);

	/**
	 * 分页查询Provider
	 */
	PageTableData<Provider> list(int pagenum, int pagesize, ProviderQuery param);

	/**
	 * 查询所有Provider
	 */
	List<Provider> listAll(ProviderQuery param);
}