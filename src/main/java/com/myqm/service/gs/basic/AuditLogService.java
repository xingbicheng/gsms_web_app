package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.AuditLog;
import com.myqm.vo.gs.basic.AuditLogQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface AuditLogService {
   /**
    *根据id删除AuditLog
    */
	String delById(String id);

   /**
    *根据新增AuditLog，id自增
    */
    String save(AuditLog record);

   /**
    *根据id查询AuditLog
    */
    AuditLog getById(String id);
    
   /**
    *根据id更新AuditLog
    */ 
    String editById(AuditLog record);

   /**
    *分页查询AuditLog
    */ 
    PageTableData<AuditLog> list(int pagenum, int pagesize, AuditLogQuery param);

	/**
    *查询所有AuditLog
    */ 
    List<AuditLog> listAll(AuditLogQuery param);
}