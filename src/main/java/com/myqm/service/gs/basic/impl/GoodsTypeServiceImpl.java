package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.basic.GoodsTypeDao;
import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsTypeQuery;
import com.myqm.service.gs.basic.GoodsTypeService;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

	@Autowired
	public GoodsTypeDao goodsTypeDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (goodsTypeDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(GoodsType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getGoodsType()));

		if (goodsTypeDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public GoodsType getById(String id) {
		return goodsTypeDao.getById(id);
	}

	@Override
	public String editById(GoodsType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getGoodsType()));

		if (goodsTypeDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<GoodsType> list(int pagenum, int pagesize, GoodsTypeQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<GoodsType> datalist = goodsTypeDao.list(param);
		PageTableData<GoodsType> resultPage = new PageTableData<GoodsType>(datalist);
		PageInfo<GoodsType> p = new PageInfo<GoodsType>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<GoodsType> listAll(GoodsTypeQuery param) {
		List<GoodsType> datalist = goodsTypeDao.list(param);
		return datalist;
	}

}
