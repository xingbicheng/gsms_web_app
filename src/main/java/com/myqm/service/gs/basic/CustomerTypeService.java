package com.myqm.service.gs.basic;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.vo.gs.basic.CustomerTypeQuery;

public interface CustomerTypeService {
	/**
	 * 根据id删除CustomerType
	 */
	String delById(String id);

	/**
	 * 根据新增CustomerType，id自增
	 */
	String save(CustomerType record);

	/**
	 * 根据id查询CustomerType
	 */
	CustomerType getById(String id);

	/**
	 * 根据id更新CustomerType
	 */
	String editById(CustomerType record);

	/**
	 * 分页查询CustomerType
	 */
	PageTableData<CustomerType> list(int pagenum, int pagesize, CustomerTypeQuery param);

	/**
	 * 查询所有CustomerType
	 */
	List<CustomerType> listAll(CustomerTypeQuery param);
}