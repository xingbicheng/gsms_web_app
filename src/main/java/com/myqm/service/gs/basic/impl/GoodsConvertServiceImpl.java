package com.myqm.service.gs.basic.impl;

import java.util.List;

import com.myqm.dao.gs.basic.GoodsConvertDao;
import com.myqm.pojo.gs.basic.GoodsConvert;
import com.myqm.service.gs.basic.GoodsConvertService;
import com.myqm.vo.gs.basic.GoodsConvertQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class GoodsConvertServiceImpl implements GoodsConvertService {

	@Autowired
	public GoodsConvertDao goodsConvertDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (goodsConvertDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(GoodsConvert record) {
		// 请修改更新的拼音

		if (goodsConvertDao.save(record) > 0) {
			return record.getId();
		} else
			return "新增失败";
	}

	@Override
	public GoodsConvert getById(String id) {
		return goodsConvertDao.getById(id);
	}

	@Override
	public String editById(GoodsConvert record) {
		// 请修改更新的拼音

		if (goodsConvertDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<GoodsConvert> list(int pagenum, int pagesize, GoodsConvertQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<GoodsConvert> datalist = goodsConvertDao.list(param);
		PageTableData<GoodsConvert> resultPage = new PageTableData<GoodsConvert>(datalist);
		PageInfo<GoodsConvert> p = new PageInfo<GoodsConvert>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<GoodsConvert> listAll(GoodsConvertQuery param) {
		List<GoodsConvert> datalist = goodsConvertDao.list(param);
		return datalist;
	}

	@Override
	public String saveall(List<GoodsConvert> record) { 
		int count = goodsConvertDao.saveall(record);
		return "修改成功"+count+"条记录";
	}

}
