package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.GoodsType;
import com.myqm.vo.gs.basic.GoodsTypeQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;

public interface GoodsTypeService {
	/**
	 * 根据id删除GoodsType
	 */
	String delById(String id);

	/**
	 * 根据新增GoodsType，id自增
	 */
	String save(GoodsType record);

	/**
	 * 根据id查询GoodsType
	 */
	GoodsType getById(String id);

	/**
	 * 根据id更新GoodsType
	 */
	String editById(GoodsType record);

	/**
	 * 分页查询GoodsType
	 */
	PageTableData<GoodsType> list(int pagenum, int pagesize, GoodsTypeQuery param);

	/**
	 * 查询所有GoodsType
	 */
	List<GoodsType> listAll(GoodsTypeQuery param);
}