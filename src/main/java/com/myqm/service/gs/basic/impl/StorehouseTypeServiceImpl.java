package com.myqm.service.gs.basic.impl;

import java.util.List;

import com.myqm.dao.gs.basic.StorehouseTypeDao;
import com.myqm.pojo.gs.basic.StorehouseType;
import com.myqm.service.gs.basic.StorehouseTypeService;
import com.myqm.vo.gs.basic.StorehouseTypeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class StorehouseTypeServiceImpl implements StorehouseTypeService {

	@Autowired
	public StorehouseTypeDao storehouseTypeDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (storehouseTypeDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(StorehouseType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getStoreType()));

		if (storehouseTypeDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public StorehouseType getById(String id) {
		return storehouseTypeDao.getById(id);
	}

	@Override
	public String editById(StorehouseType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getStoreType()));
		if (storehouseTypeDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<StorehouseType> list(int pagenum, int pagesize, StorehouseTypeQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<StorehouseType> datalist = storehouseTypeDao.list(param);
		PageTableData<StorehouseType> resultPage = new PageTableData<StorehouseType>(datalist);
		PageInfo<StorehouseType> p = new PageInfo<StorehouseType>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<StorehouseType> listAll(StorehouseTypeQuery param) {
		List<StorehouseType> datalist = storehouseTypeDao.list(param);
		return datalist;
	}

}
