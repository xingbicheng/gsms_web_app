package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.GoodsConvert;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.vo.gs.basic.GoodsConvertQuery;

public interface GoodsConvertService {
	/**
	 * 根据id删除GoodsConvert
	 */
	String delById(String id);

	/**
	 * 根据新增GoodsConvert，id自增
	 */
	String save(GoodsConvert record);

	/**
	 * 根据id查询GoodsConvert
	 */
	GoodsConvert getById(String id);

	/**
	 * 根据id更新GoodsConvert
	 */
	String editById(GoodsConvert record);

	/**
	 * 分页查询GoodsConvert
	 */
	PageTableData<GoodsConvert> list(int pagenum, int pagesize, GoodsConvertQuery param);

	/**
	 * 查询所有GoodsConvert
	 */
	List<GoodsConvert> listAll(GoodsConvertQuery param);

	String saveall(List<GoodsConvert> record);
}