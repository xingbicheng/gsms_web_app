package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.service.gs.basic.CustomerService;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.vo.gs.basic.CustomerQuery;
import com.myqm.dao.gs.basic.CustomerDao;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	public CustomerDao customerDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (customerDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(Customer record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getCustomerName()));

		if (customerDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public Customer getById(String id) {
		return customerDao.getById(id);
	}

	@Override
	public String editById(Customer record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getCustomerName()));

		if (customerDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<Customer> list(int pagenum, int pagesize, CustomerQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Customer> datalist = customerDao.list(param);
		PageTableData<Customer> resultPage = new PageTableData<Customer>(datalist);
		PageInfo<Customer> p = new PageInfo<Customer>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<Customer> listAll(CustomerQuery param) {
		List<Customer> datalist = customerDao.list(param);
		return datalist;
	}

}
