package com.myqm.service.gs.basic;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.WebPlugin;
import com.myqm.vo.gs.basic.WebPluginQuery;

public interface WebPluginService {
	/**
	 * 根据id删除WebPlugin
	 */
	String delById(String id);

	/**
	 * 根据新增WebPlugin，id自增
	 */
	String save(WebPlugin record);

	/**
	 * 根据id查询WebPlugin
	 */
	WebPlugin getById(String id);

	/**
	 * 根据id更新WebPlugin
	 */
	String editById(WebPlugin record);

	/**
	 * 分页查询WebPlugin
	 */
	PageTableData<WebPlugin> list(int pagenum, int pagesize, WebPluginQuery param);

	/**
	 * 查询所有WebPlugin
	 */
	List<WebPlugin> listAll(WebPluginQuery param);
}