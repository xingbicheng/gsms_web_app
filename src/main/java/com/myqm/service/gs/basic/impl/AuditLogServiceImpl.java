package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.basic.AuditLogDao;
import com.myqm.pojo.gs.basic.AuditLog;
import com.myqm.vo.gs.basic.AuditLogQuery;
import com.myqm.service.gs.basic.AuditLogService;


@Service
public class AuditLogServiceImpl implements AuditLogService{

	@Autowired
	public AuditLogDao auditLogDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Override
	public String delById(String id) {
		if (auditLogDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(AuditLog record) {

	
		if (auditLogDao.save(record) > 0) 
		{			 
			return record.getId();
		}
		else
			return null; 
	}

	@Override
	public AuditLog getById(String id) {
		return auditLogDao.getById(id);
	}


	@Override
	public String editById(AuditLog record) {

		if (auditLogDao.editById(record) > 0)
			return record.getId();
		else
			return null; 
	}

	@Override
	public PageTableData<AuditLog> list(int pagenum, int pagesize,AuditLogQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<AuditLog> datalist = auditLogDao.list(param);
		PageTableData<AuditLog> resultPage = new PageTableData<AuditLog>(datalist);
		PageInfo<AuditLog> p = new PageInfo<AuditLog>(datalist);
//		resultPage.setCurrentPage(p.getPageNum());
//		resultPage.setShowRows(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<AuditLog> listAll(AuditLogQuery param) {
		List<AuditLog> datalist = auditLogDao.list(param);
		return datalist;
	}

}
