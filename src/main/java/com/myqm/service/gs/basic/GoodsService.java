package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.Goods;
import com.myqm.vo.gs.basic.GoodsQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;

public interface GoodsService {
	/**
	 * 根据id删除Goods
	 */
	String delById(String id);

	/**
	 * 根据新增Goods，id自增
	 */
	String save(Goods record);

	/**
	 * 根据id查询Goods
	 */
	Goods getById(String id);

	/**
	 * 根据id更新Goods
	 */
	String editById(Goods record);

	/**
	 * 分页查询Goods
	 */
	PageTableData<Goods> list(int pagenum, int pagesize, GoodsQuery param);

	/**
	 * 查询所有Goods
	 */
	List<Goods> listAll(GoodsQuery param);
}