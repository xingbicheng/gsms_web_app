package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.basic.ProviderTypeDao;
import com.myqm.pojo.gs.basic.ProviderType;
import com.myqm.vo.gs.basic.ProviderTypeQuery;
import com.myqm.service.gs.basic.ProviderTypeService;

@Service
public class ProviderTypeServiceImpl implements ProviderTypeService {

	@Autowired
	public ProviderTypeDao providerTypeDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (providerTypeDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(ProviderType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getProviderType()));

		if (providerTypeDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public ProviderType getById(String id) {
		return providerTypeDao.getById(id);
	}

	@Override
	public String editById(ProviderType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getProviderType()));

		if (providerTypeDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<ProviderType> list(int pagenum, int pagesize, ProviderTypeQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<ProviderType> datalist = providerTypeDao.list(param);
		PageTableData<ProviderType> resultPage = new PageTableData<ProviderType>(datalist);
		PageInfo<ProviderType> p = new PageInfo<ProviderType>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<ProviderType> listAll(ProviderTypeQuery param) {
		List<ProviderType> datalist = providerTypeDao.list(param);
		return datalist;
	}

}
