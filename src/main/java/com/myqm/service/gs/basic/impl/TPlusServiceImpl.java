package com.myqm.service.gs.basic.impl;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myqm.dao.gs.basic.SysSetDao;
import com.myqm.dao.gs.basic.TPlusCustomerDao;
import com.myqm.dao.gs.basic.TPlusGoodsDao;
import com.myqm.dao.gs.basic.TPlusStorehouseDao;
import com.myqm.pojo.gs.basic.ResponseRoot;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.pojo.gs.basic.TPlusCustomer;
import com.myqm.pojo.gs.basic.TPlusGoods;
import com.myqm.pojo.gs.basic.TPlusStorehouse;
import com.myqm.service.gs.basic.TPlusService;
import com.myqm.utils.UrlSendUtil; 

@Service
public class TPlusServiceImpl implements TPlusService {
	
	@Autowired
	private TPlusGoodsDao tPlusGoodsDao;
	
	@Autowired
	private TPlusCustomerDao  tPlusCustomerDao;
	
	@Autowired
	private TPlusStorehouseDao tPlusStorehouseDao;
	
	@Autowired
	private SysSetDao sysSetDao;
	
	public static void main(String[] args) throws JAXBException {  
		TPlusServiceImpl ser = new TPlusServiceImpl();
		String str = ser.updateCustomer(); 
		System.out.println(str);
		ResponseRoot unXml =  ser.unXml(str);
		System.out.println(unXml.getItems());
	}  

 
	public ResponseRoot unXml(String content) throws JAXBException { 
		JAXBContext context = JAXBContext.newInstance(ResponseRoot.class);
		final Unmarshaller unmarshaller = context.createUnmarshaller();   
 
		content = content.replace("<item>", "<HouseItem>");
		content = content.replace("</item>", "</HouseItem>");
		// ResponseRoot<TPlusGoods>  chart = (ResponseRoot<TPlusGoods>) unmarshaller.unmarshal(new StringReader(content));  
		ResponseRoot<TPlusStorehouse>  chart = (ResponseRoot<TPlusStorehouse>) unmarshaller.unmarshal(new StringReader(content));  
 	       return chart;  
	}
//T+修改后需要进一步修改
	@Override
	public String updateProvider() {
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1();
		String id = byId.getSetvalue2();
		String strUrl = url+"&id="+id+"&method=partner&format=xml";
	  	String str = UrlSendUtil.sendPost(strUrl,null); 
		str=str.substring(1, str.length()-1);
		String content=str.replaceAll("\\\\\"", "\"");
		JAXBContext context;
		int count = 0;
		try {
			context = JAXBContext.newInstance(ResponseRoot.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();   
			 
			content = content.replace("<item>", "<CustomerItem>");
			content = content.replace("</item>", "</CustomerItem>");
			ResponseRoot<TPlusCustomer>  chart = (ResponseRoot<TPlusCustomer>) unmarshaller.unmarshal(new StringReader(content)); 
			if (!chart.getCode().equals("00"))
				return "获取数据失败，请检查配置或T+服务器运行是否正常。错误信息：" + chart.getMessage();
		 List<TPlusCustomer> items = chart.getItems();
			for (TPlusCustomer tPlusCustomer : items) {
				if(tPlusCustomer.getIsCustomer().equals("供应商")||tPlusCustomer.getIsCustomer().equals("客户/供应商")) {
					if(tPlusCustomer.getClassName().equals("不可换货")) {
						count++;
						tPlusCustomer.setClassCode("2");
						int editCById = tPlusCustomerDao.editPById(tPlusCustomer);
						if(editCById==0) {
							tPlusCustomerDao.saveProvider(tPlusCustomer);
						}
					}
					else if(tPlusCustomer.getClassName().equals("可换货")){
						count++;
						tPlusCustomer.setClassCode("1");
						int editCById = tPlusCustomerDao.editPById(tPlusCustomer);
						if(editCById==0) {
							tPlusCustomerDao.saveProvider(tPlusCustomer);
						}
					}
					
				}
			}
				
			return "共获取"+items.size()+"条往来往来客户数据，共更新"+count+"条供应商信息！";
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			return "更新供应商信息失败";
		}
	}

	@Override
	public String updateStorehouse() {
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1();
		String id = byId.getSetvalue2();
		String strUrl = url+"&id="+id+"&method=warehouse&format=xml";
	  	String str = UrlSendUtil.sendPost(strUrl,null); 
		str=str.substring(1, str.length()-1);
		String content=str.replaceAll("\\\\\"", "\"");
		JAXBContext context;
		int count = 0;
		try {
			context = JAXBContext.newInstance(ResponseRoot.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();   
			 
			content = content.replace("<item>", "<HouseItem>");
			content = content.replace("</item>", "</HouseItem>");
			ResponseRoot<TPlusStorehouse>  chart = (ResponseRoot<TPlusStorehouse>) unmarshaller.unmarshal(new StringReader(content)); 
			if (!chart.getCode().equals("00"))
				return "获取数据失败，请检查配置或T+服务器运行是否正常。错误信息" + chart.getMessage();
		List<TPlusStorehouse> items = chart.getItems();
			for (TPlusStorehouse tPlusCustomer : items) {
				count++;
				int editCById = tPlusStorehouseDao.editById(tPlusCustomer);
				if(editCById==0) {
					tPlusStorehouseDao.saveStorehouse(tPlusCustomer);
				}
			}
			return "共获取"+items.size()+"条往来仓库数据，共更新"+count+"条仓库信息！";
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			return "导入失败";
		}
	}

	@Override
	public String updateGoods()  {
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1();
		String id = byId.getSetvalue2();
		String strUrl = url+"&id="+id+"&method=inventory&format=xml";
		// TODO Auto-generated method stub
	  	String str = UrlSendUtil.sendPost(strUrl,null); 
		str=str.substring(1, str.length()-1);
		String content=str.replaceAll("\\\\\"", "\"");
		JAXBContext context;
		int count = 0;
		try {
			context = JAXBContext.newInstance(ResponseRoot.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();   
			 
			content = content.replace("<item>", "<GoodsItem>");
			content = content.replace("</item>", "</GoodsItem>");
			// ResponseRoot<TPlusGoods>  chart = (ResponseRoot<TPlusGoods>) unmarshaller.unmarshal(new StringReader(content));  
			ResponseRoot<TPlusGoods>  chart = (ResponseRoot<TPlusGoods>) unmarshaller.unmarshal(new StringReader(content)); 
			if (!chart.getCode().equals("00"))
				return "获取数据失败，请检查配置或T+服务器运行是否正常。错误信息："+ chart.getMessage();;
		    List<TPlusGoods> items = chart.getItems();
			for (TPlusGoods tPlusGoods : items) {
				count++;
				int editById = tPlusGoodsDao.editById(tPlusGoods);
				int editByIdType = tPlusGoodsDao.editByIdType(tPlusGoods);
				if(editByIdType==0) {
					tPlusGoodsDao.saveGoodsType(tPlusGoods);
				}
				if(editById==0) {
					tPlusGoodsDao.saveGoods(tPlusGoods);
				}
			}
			return "共获取"+items.size()+"条往来商品数据，共更新"+count+"条商品信息！";
			
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			return"导入商品失败";
		}
	}

 
	@Override
	public String updateCustomer() {
		SysSet byId = sysSetDao.getById("2");
		String url = byId.getSetvalue1();
		String id = byId.getSetvalue2();
		String strUrl = url+"&id="+id+"&method=partner&format=xml";
	  	String str = UrlSendUtil.sendPost(strUrl,null); 
		str=str.substring(1, str.length()-1);
		String content=str.replaceAll("\\\\\"", "\"");
		JAXBContext context;
		int count = 0;
		try {
			context = JAXBContext.newInstance(ResponseRoot.class);
			final Unmarshaller unmarshaller = context.createUnmarshaller();   
			 
			content = content.replace("<item>", "<CustomerItem>");
			content = content.replace("</item>", "</CustomerItem>");
			ResponseRoot<TPlusCustomer>  chart = (ResponseRoot<TPlusCustomer>) unmarshaller.unmarshal(new StringReader(content)); 
			if (!chart.getCode().equals("00"))
				return "获取数据失败，请检查配置或T+服务器运行是否正常。错误信息："+ chart.getMessage();;
			List<TPlusCustomer> items = chart.getItems();
			for (TPlusCustomer tPlusCustomer : items) {
			 
				if(tPlusCustomer.getIsCustomer().equals("客户")||tPlusCustomer.getIsCustomer().equals("客户/供应商")) {
					count++;
					int editCById = tPlusCustomerDao.editCById(tPlusCustomer);
					int editCByIdType = tPlusCustomerDao.editCByIdType(tPlusCustomer);
					if(editCByIdType==0) {
						tPlusCustomerDao.saveCustomerType(tPlusCustomer);
					}
					if(editCById==0) {
						tPlusCustomerDao.saveCustomer(tPlusCustomer);
					}
				}
			}
			return "共获取"+items.size()+"条往来单位数据，共更新"+count+"条客户信息！";
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			return "更新数据失败！";
		}
	} 
}
