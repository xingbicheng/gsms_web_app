package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.Storehouse;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.vo.gs.basic.StorehouseQuery;

public interface StorehouseService {
	/**
	 * 根据id删除Storehouse
	 */
	String delById(String id);

	/**
	 * 根据新增Storehouse，id自增
	 */
	String save(Storehouse record);

	/**
	 * 根据id查询Storehouse
	 */
	Storehouse getById(String id);

	/**
	 * 根据id更新Storehouse
	 */
	String editById(Storehouse record);

	/**
	 * 分页查询Storehouse
	 */
	PageTableData<Storehouse> list(int pagenum, int pagesize, StorehouseQuery param);

	/**
	 * 查询所有Storehouse
	 */
	List<Storehouse> listAll(StorehouseQuery param);
}