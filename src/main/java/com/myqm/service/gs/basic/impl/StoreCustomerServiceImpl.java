package com.myqm.service.gs.basic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.basic.StoreCustomerDao;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.ProviderType;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.service.gs.basic.StoreCustomerService;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.utils.UserUtil;
import com.myqm.vo.gs.basic.ProviderTypeQuery;
import com.myqm.vo.gs.basic.StoreCustomerQuery;
@Service
public class StoreCustomerServiceImpl implements StoreCustomerService{
	
	@Autowired
	public StoreCustomerDao storeCustomerDao;
	
	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if(storeCustomerDao.delById(id)>0)
			return id;
		else
		    return null;
	}

	@Override
	public String save(StoreCustomer record) {
		record.setCreaterId(UserUtil.getLoginUser().getId());
		if(storeCustomerDao.save(record)>0) {
			return record.getId();
		}else
		return null;
	}

	@Override
	public StoreCustomer getById(String id) {
		return storeCustomerDao.getById(id);
	}

	@Override
	public String editById(StoreCustomer record) {
		record.setUpdaterId(UserUtil.getLoginUser().getId());		
		if(storeCustomerDao.editById(record)>0) {
			return record.getId();
		}else
		return null;
	}

	@Override
	public PageTableData<StoreCustomer> list(int pagenum, int pagesize, StoreCustomerQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<StoreCustomer> datalist = storeCustomerDao.list(param);
		PageTableData<StoreCustomer> resultPage = new PageTableData<StoreCustomer>(datalist);
		PageInfo<StoreCustomer> p = new PageInfo<StoreCustomer>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<StoreCustomer> listAll(StoreCustomerQuery param) {
		List<StoreCustomer> datalist = storeCustomerDao.list(param);
		return datalist;
	}

}
