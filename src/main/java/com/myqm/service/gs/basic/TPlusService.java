package com.myqm.service.gs.basic;

import javax.xml.bind.JAXBException;

public interface TPlusService {
	 
	/**
	 * 同步客户信息
	 */
	String updateCustomer();

	/**
	 * 同步供应商信息
	 */
	String updateProvider();

	/**
	 * 同步仓库信息
	 */
	String updateStorehouse();

	/**
	 * 同步商品信息
	 */
	String updateGoods();
	 
}