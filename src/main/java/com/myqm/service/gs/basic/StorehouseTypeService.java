package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.StorehouseType;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.vo.gs.basic.StorehouseTypeQuery;

public interface StorehouseTypeService {
	/**
	 * 根据id删除StorehouseType
	 */
	String delById(String id);

	/**
	 * 根据新增StorehouseType，id自增
	 */
	String save(StorehouseType record);

	/**
	 * 根据id查询StorehouseType
	 */
	StorehouseType getById(String id);

	/**
	 * 根据id更新StorehouseType
	 */
	String editById(StorehouseType record);

	/**
	 * 分页查询StorehouseType
	 */
	PageTableData<StorehouseType> list(int pagenum, int pagesize, StorehouseTypeQuery param);

	/**
	 * 查询所有StorehouseType
	 */
	List<StorehouseType> listAll(StorehouseTypeQuery param);
}