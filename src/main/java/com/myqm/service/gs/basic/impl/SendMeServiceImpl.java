package com.myqm.service.gs.basic.impl;

import org.springframework.stereotype.Service;

import com.myqm.service.gs.basic.SendMeService;
import com.myqm.utils.SmsUtil;

@Service
public class SendMeServiceImpl implements SendMeService {
	
	@Override
	public void sendMe(String number, String code, String times)
	{
		SmsUtil.sendSms(number, code, times);
		System.out.println("已发短信");
	}
}
