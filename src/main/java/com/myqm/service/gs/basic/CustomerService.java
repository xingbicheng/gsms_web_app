package com.myqm.service.gs.basic;

import java.util.List;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.Customer;
import com.myqm.vo.gs.basic.CustomerQuery;

public interface CustomerService {
	/**
	 * 根据id删除Customer
	 */
	String delById(String id);

	/**
	 * 根据新增Customer，id自增
	 */
	String save(Customer record);

	/**
	 * 根据id查询Customer
	 */
	Customer getById(String id);

	/**
	 * 根据id更新Customer
	 */
	String editById(Customer record);

	/**
	 * 分页查询Customer
	 */
	PageTableData<Customer> list(int pagenum, int pagesize, CustomerQuery param);

	/**
	 * 查询所有Customer
	 */
	List<Customer> listAll(CustomerQuery param);
}