package com.myqm.service.gs.basic.impl;

import java.util.List;

import com.myqm.dao.gs.basic.StorehouseDao;
import com.myqm.pojo.gs.basic.Storehouse;
import com.myqm.service.gs.basic.StorehouseService;
import com.myqm.vo.gs.basic.StorehouseQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class StorehouseServiceImpl implements StorehouseService {

	@Autowired
	public StorehouseDao storehouseDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (storehouseDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(Storehouse record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getStoreName()));

		if (storehouseDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public Storehouse getById(String id) {
		return storehouseDao.getById(id);
	}

	@Override
	public String editById(Storehouse record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getStoreName()));

		if (storehouseDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<Storehouse> list(int pagenum, int pagesize, StorehouseQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Storehouse> datalist = storehouseDao.list(param);
		PageTableData<Storehouse> resultPage = new PageTableData<Storehouse>(datalist);
		PageInfo<Storehouse> p = new PageInfo<Storehouse>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<Storehouse> listAll(StorehouseQuery param) {
		List<Storehouse> datalist = storehouseDao.list(param);
		return datalist;
	}

}
