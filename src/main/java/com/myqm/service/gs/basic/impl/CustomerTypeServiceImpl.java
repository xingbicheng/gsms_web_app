package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.CustomerType;
import com.myqm.service.gs.basic.CustomerTypeService;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.vo.gs.basic.CustomerTypeQuery;
import com.myqm.dao.gs.basic.CustomerTypeDao;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class CustomerTypeServiceImpl implements CustomerTypeService {

	@Autowired
	public CustomerTypeDao customerTypeDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (customerTypeDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(CustomerType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getCustomerType()));

		if (customerTypeDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public CustomerType getById(String id) {
		return customerTypeDao.getById(id);
	}

	@Override
	public String editById(CustomerType record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getCustomerType()));

		if (customerTypeDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<CustomerType> list(int pagenum, int pagesize, CustomerTypeQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<CustomerType> datalist = customerTypeDao.list(param);
		PageTableData<CustomerType> resultPage = new PageTableData<CustomerType>(datalist);
		PageInfo<CustomerType> p = new PageInfo<CustomerType>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<CustomerType> listAll(CustomerTypeQuery param) {
		List<CustomerType> datalist = customerTypeDao.list(param);
		return datalist;
	}

}
