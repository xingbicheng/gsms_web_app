package com.myqm.service.gs.basic.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.basic.SysSetDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.service.gs.basic.SysSetService;
import com.myqm.vo.gs.basic.SysSetQuery;

@Service
public class SysSetServiceImpl implements SysSetService{
	
	@Autowired
	public SysSetDao sysSetDao;

	@Override
	public SysSet getById(String id) {
		return sysSetDao.getById(id);
	}

	@Override
	public String editById(SysSet record) {
		if (sysSetDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<SysSet> list(int pagenum, int pagesize, SysSetQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<SysSet> datalist = sysSetDao.list(param);
		PageTableData<SysSet> resultPage = new PageTableData<SysSet>(datalist);
		PageInfo<SysSet> p = new PageInfo<SysSet>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}
	
	@Override
	public List<SysSet> listAll(SysSetQuery param) {
		List<SysSet> datalist = sysSetDao.list(param);
		return datalist;
	}
}
