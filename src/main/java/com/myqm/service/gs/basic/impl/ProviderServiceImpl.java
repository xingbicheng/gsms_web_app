package com.myqm.service.gs.basic.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.pojo.gs.basic.Provider;
import com.myqm.vo.gs.basic.ProviderQuery;
import com.myqm.service.gs.basic.ProviderService;

@Service
public class ProviderServiceImpl implements ProviderService {

	@Autowired
	public ProviderDao providerDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (providerDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(Provider record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getProviderName()));

		if (providerDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public Provider getById(String id) {
		return providerDao.getById(id);
	}

	@Override
	public String editById(Provider record) {
		// 请修改更新的拼音
		record.setPinyin(GetPinyinUtil.getPinYinHeadChar(record.getProviderName()));

		if (providerDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<Provider> list(int pagenum, int pagesize, ProviderQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Provider> datalist = providerDao.list(param);
		PageTableData<Provider> resultPage = new PageTableData<Provider>(datalist);
		PageInfo<Provider> p = new PageInfo<Provider>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<Provider> listAll(ProviderQuery param) {
		List<Provider> datalist = providerDao.list(param);
		return datalist;
	}

}
