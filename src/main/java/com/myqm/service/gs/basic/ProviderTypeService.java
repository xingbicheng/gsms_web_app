package com.myqm.service.gs.basic;

import com.myqm.pojo.gs.basic.ProviderType;
import com.myqm.vo.gs.basic.ProviderTypeQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;

public interface ProviderTypeService {
	/**
	 * 根据id删除ProviderType
	 */
	String delById(String id);

	/**
	 * 根据新增ProviderType，id自增
	 */
	String save(ProviderType record);

	/**
	 * 根据id查询ProviderType
	 */
	ProviderType getById(String id);

	/**
	 * 根据id更新ProviderType
	 */
	String editById(ProviderType record);

	/**
	 * 分页查询ProviderType
	 */
	PageTableData<ProviderType> list(int pagenum, int pagesize, ProviderTypeQuery param);

	/**
	 * 查询所有ProviderType
	 */
	List<ProviderType> listAll(ProviderTypeQuery param);
}