package com.myqm.service.gs.basic;

import java.util.List;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.SysSet;
import com.myqm.vo.gs.basic.SysSetQuery;

public interface SysSetService {

	/**
	 * 根据id查询SysSet
	 */
	SysSet getById(String id);

	/**
	 * 根据id更新SysSet
	 */
     String editById(SysSet record);

	/**
	 * 分页查询SysSet
	 */
	PageTableData<SysSet> list(int pagenum, int pagesize, SysSetQuery param);
	
	/**
	 * 查询所有SysSet
	 */
	List<SysSet> listAll(SysSetQuery param);

}
