package com.myqm.service.gs.basic;

import java.util.List;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.StoreCustomer;
import com.myqm.vo.gs.basic.StoreCustomerQuery;

public interface StoreCustomerService {
	 /**
	  *根据id删除StoreCustomer
	  */
	  String delById(String id);

	  /**
	   *根据新增StoreCustomer，id自增
	   */
	   String save(StoreCustomer record);

	  /**
	   *根据id查询StoreCustomer
	   */
	   StoreCustomer getById(String id);
	    
	  /**
	   *根据id更新StoreCustomer
	   */ 
	   String editById(StoreCustomer record);

	  /**
	   *分页查询StoreCustomer
	   */ 
	   PageTableData<StoreCustomer> list(int pagenum, int pagesize,StoreCustomerQuery param);

	  /**
	   *查询所有StoreCustomer
	   */ 
	   List<StoreCustomer> listAll(StoreCustomerQuery param);
}

