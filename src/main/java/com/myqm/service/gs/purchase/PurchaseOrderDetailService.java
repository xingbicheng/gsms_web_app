package com.myqm.service.gs.purchase;

import java.util.List;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.vo.gs.purchase.PurchaseOrderDetailQuery;

public interface PurchaseOrderDetailService {
	/**
	 * 分页查询采购单明细
	 */
	PageTableData<PurchaseOrderDetail> list(int pagenum, int pagesize, PurchaseOrderDetailQuery param);

	/**
	 * 查询采购单明细
	 */
	List<PurchaseOrderDetail> listAll(PurchaseOrderDetailQuery param);
}
