package com.myqm.service.gs.purchase.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.myqm.dao.gs.basic.GoodsDao;
import com.myqm.dao.gs.basic.StoreCustomerDao;
import com.myqm.dao.gs.basic.StorehouseDao;
import com.myqm.dao.gs.order.OrderDao;
import com.myqm.dao.gs.order.OrderDetailDao;
import com.myqm.dao.gs.order.OrderDetailInDao;
import com.myqm.pojo.gs.basic.AuditLog;
import com.myqm.pojo.gs.basic.Goods;
import com.myqm.pojo.gs.order.Order;
import com.myqm.pojo.gs.order.OrderDetailIn;
import com.myqm.pojo.gs.purchase.*;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail.PurchaseOrderDetailState;
import com.myqm.pojo.gs.purchase.PurchaseOrder.*;

import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.gs.basic.AuditLogService;
import com.myqm.service.gs.order.OrderService;
import com.myqm.service.sysfun.SysLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.dao.base.SequenceDao;
import com.myqm.dao.gs.purchase.OrderDetailPurchaseDao;
import com.myqm.dao.gs.purchase.PlannedStorageDao;
import com.myqm.dao.gs.purchase.PurchaseOrderDao;
import com.myqm.dao.gs.purchase.PurchaseOrderDetailDao;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.purchase.PurchaseOrder.PurchaseOrderState;
import com.myqm.vo.gs.basic.StoreCustomerQuery;
import com.myqm.vo.gs.purchase.PurchaseOrderQuery;
import com.myqm.service.gs.purchase.PurchaseOrderService;
import com.myqm.utils.DateUtil;
import com.myqm.utils.UserUtil;

@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private static final String String = null;
    @Autowired
    private PurchaseOrderDao purchaseOrderDao;
    @Autowired
    private PurchaseOrderDetailDao purchaseOrderDetailDao;

    @Autowired
    private StorehouseDao storehouseDao;

    @Autowired
    private OrderDetailDao orderDetailDao;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderDetailPurchaseDao orderDetailPurchaseDao;

    @Autowired
    private SysLogsDao sysLogsDao;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    public PlannedStorageDao plannedStorageDao;

    @Autowired
    public StroehouseInDao stroehouseInDao;

    @Autowired
    private SequenceDao sequenceDao;

    @Autowired
    private SysLogsService sysLogsService;

    @Autowired
    private OrderDetailInDao orderDetailInDao;

    @Autowired
    private AuditLogService auditLogService;
     

    /**
     * 删除采购订单、删除采购明细
     */
    @Transactional
    @Override
    public String delById(String id) {
        PurchaseOrder order = purchaseOrderDao.getById(id);
        if (order == null) {
            return "采购订单不存在,不能删除请核实过再删除!";
        }
        if (!order.getState().equals(PurchaseOrderState.del)) {
            return "采购订单未被删除标记过,不能删除请核实过再删除!";
        }

        List<PurchaseOrderDetail> purchaseOrderDetails = purchaseOrderDetailDao.getByOrderId(id);
        for (PurchaseOrderDetail person : purchaseOrderDetails) {
            String deId = person.getId();

            // 查询stroehouseInDao的Id
            String inId = stroehouseInDao.getByPurchaseDetailId(deId);

            // 删除stroehouse_back
            stroehouseInDao.stroehouseBackdelByFromInId(inId);

            // 删除stroehouse_in
            stroehouseInDao.delByPurchaseId(deId);

            // 删除orderDetail_purchase
            int ff = orderDetailPurchaseDao.delByPDetailId(deId);

        }
        // 删除purchaseOrderDetail
        purchaseOrderDetailDao.delByOrderId(id);
        purchaseOrderDao.delById(id);
        sysLogsService.save("采购单", id, true, "操作员：" + UserUtil.getLoginUser().getUsername() + ",删除单号为" + order.getOrderNo() + "的采购单");
        return "采购订单删除成功！";
    }

    /**
     * 标记删除采购单
     *
     * @param id
     * @return
     */
    @Override
    public String delFlagById(String id) {
        PurchaseOrder order = purchaseOrderDao.getById(id);
        if (!order.getState().equals(PurchaseOrderState.place)) {
            return "采购订单状态不是下单状态,不能标记删除，请核实过再标记删除";
        }
        List<PurchaseOrderDetail> details = purchaseOrderDetailDao.getByOrderId(id);
        for (PurchaseOrderDetail d : details) {
            // 先修改订单明细的状态为审核通过
            orderDetailPurchaseDao.batchUnupdateOrderDetailPrePurchase(d.getId());
            if (order.getPurchaseType().equals(PurchaseType.byOrder)) {
                // 更新订单状态
                List<String> orderDetailIds = orderDetailPurchaseDao.getOrderDetailIdsByPdId(d.getId());
                // 清理旧的订单状态
                List<String> orderids = orderDao.getDistinctOrderId(orderDetailIds);
                for (String orderid : orderids) {
                    orderService.updateOrderStateById(orderid);
                }
            }
            // 删除采购计划
            plannedStorageDao.delStoreIn(d.getId());
            // 删除订单与采购明细的管理
            if (order.getPurchaseType().equals(PurchaseType.byOrder))
                orderDetailPurchaseDao.delByPDetailId(d.getId());

        }

        purchaseOrderDao.delFlagById(id);

        purchaseOrderDetailDao.delFlagByOrderId(id);
        sysLogsService.save("采购单", id, true,
                "操作员：" + UserUtil.getLoginUser().getUsername() + ",标记删除单号为" + order.getOrderNo() + "的采购单");
        return "标记删除成功";
    }

    /**
     * 新添订单采购单
     *
     * @param record
     * @return
     */
    @Transactional
    @Override
    public String save(PurchaseOrder record) {
    	record.setCreaterId(UserUtil.getLoginUser().getId());
    	StoreCustomerQuery q = new StoreCustomerQuery();
    	
    	if (purchaseOrderDao.save(record) > 0) {
            for (PurchaseOrderDetail detail : record.getOrderDetailList()) {
                detail.setPurchaseType(PurchaseType.byOrder);
                detail.setOrderId(record.getId());
                detail.setStatisticAmount(detail.getAmount() - detail.getLossAmount());
                purchaseOrderDetailDao.save(detail);
                List<OrderDetailPurchase> dplist = new ArrayList<OrderDetailPurchase>();
                // 采购单细节和订单细节关联
                if (detail.getOrderDetailIds() != null)
                    for (int i = 0; i < detail.getOrderDetailIds().size(); i++) {
                        String detailId = detail.getOrderDetailIds().get(i);
                        orderDetailDao.changeState(detailId, Order.OrderState.prePurchase);// 设为预采购
                        orderService.updateOrderStateByDetailId(detail.getOrderDetailIds().get(i)); // 更新订单状态
                        OrderDetailPurchase dp = new OrderDetailPurchase(detail.getOrderDetailIds().get(i),
                                detail.getId());
                        dplist.add(dp);
                    }
                if (dplist.size() > 0) {
                    orderDetailPurchaseDao.batchSave(dplist); // 批量存储
                    // 设置细节为预采购
                    orderDetailPurchaseDao.batchUpdateOrderDetailPrePurchase(detail.getOrderDetailIds());
                }
                // 设置头为预采购
                List<String> newOrderids = orderDao.getDistinctOrderId(detail.getOrderDetailIds());
                for (String orderid : newOrderids) {
                    orderService.updateOrderStateById(orderid);
                }

                createOrderPlannedIn(detail, false, record.getPurchaseDate());
            }
            sysLogsService.save("采购单", record.getId(), true,
                    "操作员：" + UserUtil.getLoginUser().getUsername() + ",新添采购单号为" + record.getOrderNo() + "的订单采购单成功");
            return record.getId();
        } else {
            sysLogsService.save("采购单", record.getId(), false,
                    "操作员：" + UserUtil.getLoginUser().getUsername() + ",新添采购单号为" + record.getOrderNo() + "的订单采购单失败");
            return null;
        }
    }

    /**
     * 计划采购
     *
     * @param record
     * @return
     */
    @Transactional
    @Override
    public String planPurchaseSave(PurchaseOrder record) {
        record.setCreaterId(UserUtil.getLoginUser().getId());
        boolean isCenter = true;
        for (PurchaseOrderDetail detail : record.getOrderDetailList())
            if (storehouseDao.getIsCenter(detail.getStoreId()) == "0") {
                isCenter = false;
                break;
            }
        if (isCenter) {
            record.setPurchaseType(PurchaseType.byPlan);
            record.setCreaterId(UserUtil.getLoginUser().getCreaterId());
            if (purchaseOrderDao.save(record) > 0) {
                for (PurchaseOrderDetail detail : record.getOrderDetailList()) {
                    detail.setOrderId(record.getId());
                    detail.setPurchaseType(PurchaseType.byPlan);
                    this.savePlanDetail(detail, record.getPurchaseDate());
                }
                sysLogsService.save("采购单", record.getId(), true,
                        "操作员：" + UserUtil.getLoginUser().getUsername() + ",新添采购单号为" + record.getOrderNo() + "的计划采购单成功");
                return record.getId();
            } else {
                sysLogsService.save("采购单", record.getId(), false,
                        "操作员：" + UserUtil.getLoginUser().getUsername() + ",新添采购单号为" + record.getOrderNo() + "的计划采购单失败");
                return null;
            }

        } else
            return "存在库房id为非中心库的采购单";

    }

    /**
     * 修改采购单细节
     *
     * @param record
     * @return
     */
    @Transactional
    @Override
    public String editDetailById(PurchaseOrderDetail record) {
        String detailId = record.getId();
        if (detailId == null)
            return "订单明细编号为空，请核实后再修改！";
        PurchaseOrderDetail old = purchaseOrderDetailDao.getById(detailId);
        if (old == null)
            return "订单明细不存在，请核实后再修改！";
        if (!old.getState().equals(PurchaseOrderDetailState.place))
            return "订单明细不在下单状态，不能修改！";

        PurchaseOrder po = purchaseOrderDao.getById(record.getOrderId());
        if ((record.getOrderDetailIds() != null) && (record.getOrderDetailIds().size() != 0)) {
            // 需要修改订单
            purchaseOrderDetailDao.editById(record);
            // 先修改订单的状态为审核通过
            orderDetailPurchaseDao.batchUnupdateOrderDetailPrePurchase(record.getId());

            // 删除原来购买的，改为现在购买的
            List<String> orderDetailIds = orderDetailPurchaseDao.getOrderDetailIdsByPdId(record.getId());
            // 清理旧的订单状态
            List<String> orderids = orderDao.getDistinctOrderId(orderDetailIds);
            for (String orderid : orderids) {
                orderService.updateOrderStateById(orderid);
            }
            // 删除原来的订单明细-采购单明细关系
            orderDetailPurchaseDao.delByPDetailId(record.getId());

            // 保存现在订单明细-采购单明细关系
            List<OrderDetailPurchase> dplist = new ArrayList<OrderDetailPurchase>();
            for (int i = 0; i < record.getOrderDetailIds().size(); i++) {
                OrderDetailPurchase dp = new OrderDetailPurchase(record.getId(), record.getOrderDetailIds().get(i));
                dplist.add(dp);
            }
            if (dplist.size() > 0)
                orderDetailPurchaseDao.batchSave(dplist);
            // 设置订单明细为预采购状态
            orderDetailPurchaseDao.batchUpdateOrderDetailPrePurchase(record.getOrderDetailIds());
            // 设置头为预采购
            List<String> newOrderids = orderDao.getDistinctOrderId(record.getOrderDetailIds());
            for (String orderid : newOrderids) {
                orderService.updateOrderStateById(orderid);
            }

            createOrderPlannedIn(record, true, po.getPurchaseDate());
        } else {
            purchaseOrderDetailDao.editById(record);
            createOrderPlannedIn(record, true, po.getPurchaseDate());
        }
        sysLogsService.save("采购单", old.getId(), true,
                "操作员：" + UserUtil.getLoginUser().getUsername() + ",采购单细节修改:" + old.difference(record));
        return "修改成功";
    }

    private void savePlanDetail(PurchaseOrderDetail detail, Date purchaseDate) {
        detail.setStatisticAmount(detail.getAmount() - detail.getLossAmount());
        Goods goods = goodsDao.getById(detail.getGoodsId());
        if (goods.getPrice() != null) {
            detail.setAveragePrice(goods.getPrice());
            detail.setSum(new BigDecimal(goods.getPrice().doubleValue() * detail.getAmount()));
        } else {
            detail.setAveragePrice(new BigDecimal("0.0"));
            detail.setSum(new BigDecimal("0.0"));
        }
        purchaseOrderDetailDao.save(detail);
        // 计划入库
        StroehouseIn in = new StroehouseIn();
        in.setPurchaseDetailId(detail.getId());
        in.setGoodsId(detail.getGoodsId());
        if (PurchaseType.byPlan.equals(detail.getPurchaseType()))
            in.setPurchaseType(StroehouseIn.PurchaseType.planPurchase);
        else
            in.setPurchaseType(StroehouseIn.PurchaseType.orderPurchase);
        // 设置初始价格和供应商
        in.setPrice(goods.getPrice());
        in.setProviderId(goods.getProviderId());
        // 计算数量
        double amount = detail.getAmount();
        double loss = 0;
        if (detail.getLossAmount() != null)
            loss = detail.getLossAmount() * amount / detail.getAmount();
        in.setAmount(amount);
        in.setLossAmount(loss);
        in.setStorehouseId(detail.getStoreId());
        in.setPurchaseDate(purchaseDate);
        stroehouseInDao.save(in);
        sysLogsService.save("入库", in.getId(), true, "操作员："
                + UserUtil.getLoginUser().getUsername() + "添加新建入库计划单");

    }

    /**
     * 修改计划采购单细节
     *
     * @param record
     * @return
     */
    @Transactional
    @Override
    public String editPlanById(PurchaseOrder order) {
        PurchaseOrder old = purchaseOrderDao.getById(order.getId());
        if (old == null)
            return "订单不存在，请核实后再修改！";
        if (!old.getState().equals(PurchaseOrderState.place))
            return "订单已经审核，请核实后再修改！";

        //n不修改   a 新增  e 修改  d 删除
        this.editById(order);
        sysLogsService.save("采购单", old.getId(), true,
                "操作员：" + UserUtil.getLoginUser().getUsername() + "修改:" + old.difference(order));

        for (PurchaseOrderDetail detail : order.getOrderDetailList()) {
            if (detail.getEditFlag().equals("n")) {
                stroehouseInDao.setPuchaseDate(detail.getId(), order.getPurchaseDate());
                continue;
            } else if (detail.getEditFlag().equals("a")) {
                detail.setOrderId(order.getId());
                this.savePlanDetail(detail, order.getPurchaseDate());
                continue;
            } else if (detail.getEditFlag().equals("e")) {
                String detailId = detail.getId();
                if (detailId == null)
                    continue;
                this.delDetailById(detailId);
                detail.setOrderId(order.getId());
                this.savePlanDetail(detail, order.getPurchaseDate());
                continue;
            } else if (detail.getEditFlag().equals("d")) {
                String detailId = detail.getId();
                if (detailId == null)
                    continue;
                this.delDetailById(detailId);
                continue;
            }
        }
        return "修改成功";
    }


    /**
     * 修改计划采购单细节
     *
     * @param record
     * @return
     */
    @Transactional
    @Override
    public String editPlanDetailById(PurchaseOrderDetail record) {
        String detailId = record.getId();
        if (detailId == null)
            return "订单明细编号为空，请核实后再修改！";
        PurchaseOrderDetail old = purchaseOrderDetailDao.getById(detailId);
        if (old == null)
            return "订单明细不存在，请核实后再修改！";
        if (!old.getState().equals(PurchaseOrderDetailState.place))
            return "订单明细不在下单状态，不能修改！";

        PurchaseOrder po = purchaseOrderDao.getById(record.getOrderId());

        purchaseOrderDetailDao.editById(record);
        StroehouseIn in = new StroehouseIn();
        in.setPurchaseDetailId(record.getId());
        in.setGoodsId(record.getGoodsId());
        // 计算数量
        double amount = record.getAmount();
        double loss = 0;
        if (record.getLossAmount() != null)
            loss = record.getLossAmount() * amount / record.getStatisticAmount();
        in.setAmount(amount + loss);
        in.setLossAmount(loss);
        in.setStorehouseId(record.getStoreId());
        in.setPurchaseDate(po.getPurchaseDate());
        plannedStorageDao.delStoreIn(record.getId());
        stroehouseInDao.save(in);

        sysLogsService.save("采购单", old.getId(), true,
                "操作员：" + UserUtil.getLoginUser().getUsername() + "修改:" + old.difference(record));
        return "修改成功";
    }

    @Override
    public PurchaseOrder getById(String id) {
        PurchaseOrder order = purchaseOrderDao.getById(id);
        String createrName = sysUserDao.getUserNameById(order.getCreaterId());
        String updaterName = sysUserDao.getUserNameById(order.getUpdaterId());
        String purchaserName = sysUserDao.getUserNameById(order.getPurchaserId());

        order.setPurchaserName(purchaserName);
        order.setCreaterName(createrName);
        order.setUpdaterName(updaterName);

        if (order != null) {
            if (order.getPurchaseType().equals(PurchaseType.byOrder))
                order.setOrderDetailList(purchaseOrderDetailDao.getByOrderId(id));
            else if (order.getPurchaseType().equals(PurchaseType.byPlan)) {
                order.setOrderDetailList(purchaseOrderDetailDao.getByPlanOrderId(id));
            }
        }
        return order;
    }

    /**
     * 取消订单的预采购状态
     *
     * @param pDetailId
     */
    // private void clearOrderPrePurchaseByOrderDetailId(String pDetailId) {
    // // 根据采购单明细id 获取涉及到的订单为预采购状态 的 所有订单id
    // List<String> orderids =
    // orderDetailPurchaseDao.getPrePurchaseOrderIdByPurchaseDetailIds(pDetailId);
    // // 统计订单下所有明细为预采用状态
    // List<PrePurchaseCount> countlist =
    // orderDetailPurchaseDao.countOrderIdPrePurchase(orderids);
    // List<String> orderList = new ArrayList<String>();
    // for (PrePurchaseCount counter : countlist) {
    // if (counter.getPrePurchaseCount() == 0)
    // orderList.add(counter.getOrderId());
    //
    // }
    // // 设置订单状态为审核通过
    // if (orderList.size() > 0)
    // {
    // orderDetailPurchaseDao.clearOrderPrePurchase(orderList);
    // sysLogsService.save("订单",null,true,
    // "改变原始订单预采购状态为审核通过，订单编号为："+orderList);
    // }
    // else
    // sysLogsService.save("订单",null,false,
    // "改变原始订单预采购状态为审核通过，订单编号为："+orderList);
    // }
    @Transactional
    @Override
    public String editDetailT(PurchaseOrderTs record) {
        PurchaseOrder purchaseOrder = purchaseOrderDao.getById(record.getOrderId());
        if (purchaseOrder == null)
            return "此采购单不存在";
        try {
            purchaseOrderDao.editPurchaserIdAndMore(record.getOrderId(), record.getPurchaserId(),
                    record.getPurchaseDate());
            List<DetailMini> detailMini = record.getDetailsEdit();
            for (DetailMini detailMini1 : detailMini) {
                PurchaseOrderDetail detail = purchaseOrderDetailDao.getById(detailMini1.getDetailId());
                //删除计划入库
                plannedStorageDao.delStoreIn(detail.getId());
                if (detailMini1.getLossAmount() != null) {
                    detail.setLossAmount(detailMini1.getLossAmount());
                    detail.setAmount(detail.getStatisticAmount() + detail.getLossAmount());
                }

                purchaseOrderDetailDao.editById(detail);

                // 重新计划入库
                StroehouseIn in = new StroehouseIn();
                in.setPurchaseDetailId(detail.getId());
                in.setGoodsId(detail.getGoodsId());
                // 设置初始价格和供应商
                Goods goods = goodsDao.getById(detail.getGoodsId());
                in.setPrice(goods.getPrice());
                in.setProviderId(goods.getProviderId());
                // 计算数量
                double amount = detail.getAmount();
                double loss = 0;
                if (detail.getLossAmount() != null)
                    loss = detail.getLossAmount() * amount / detail.getAmount();
                in.setAmount(amount);
                in.setLossAmount(loss);
                in.setStorehouseId(detailMini1.getStoreId());
                in.setPurchaseDate(record.getPurchaseDate());
                stroehouseInDao.save(in);

            }
        } catch (Exception e) {
            return "修改失败";
        }
        return "修改成功";

    }

    @Override
    public String editById(PurchaseOrder record) {
        PurchaseOrder old = purchaseOrderDao.getById(record.getId());
        if (old == null)
            return "采购单不存在，请检查后重新输入！";
        if (!old.getState().equals(PurchaseOrderState.place))
            return "订单已经审核，不能修改该采购单！";
        record.setUpdaterId(UserUtil.getLoginUser().getId());
        this.stroehouseInDao.setPuchaseDateByPOrderId(record.getId(), record.getPurchaseDate());
        if (purchaseOrderDao.editById(record) > 0) {
            sysLogsService.save("采购单", old.getId(), true, "操作员：" +
                    UserUtil.getLoginUser().getUsername() + "修改:" + old.difference(record));
            return record.getId();
        } else
            return null;
    }

    @Override
    public PageTableData<PurchaseOrder> list(int pagenum, int pagesize, PurchaseOrderQuery param) {
        PageHelper.startPage(pagenum, pagesize);
        if (param.getSort() == null) {
            param.setSort("createTime");
            param.setOrder("desc");
        }
        List<PurchaseOrder> datalist = purchaseOrderDao.list(param);
        PageTableData<PurchaseOrder> resultPage = new PageTableData<PurchaseOrder>(datalist);
        PageInfo<PurchaseOrder> p = new PageInfo<PurchaseOrder>(datalist);
        resultPage.setPageNumber(p.getPageNum());
        resultPage.setPageSize(p.getPageSize());
        resultPage.setTotalPage(p.getPages());
        resultPage.setTotal(p.getTotal());
        return resultPage;
    }

    @Override
    public PurchaseOrder pre(PurchaseOrderQuery param) {
        String orderId = purchaseOrderDao.pre(param);
        if (orderId == null)
            return null;
        else {
            return this.getById(orderId);
        }
    }

    @Override
    public PurchaseOrder next(PurchaseOrderQuery param) {
        String orderId = purchaseOrderDao.next(param);
        if (orderId == null)
            return null;
        else {
            return this.getById(orderId);
        }
    }

    @Override
    public List<PurchaseOrder> listAll(PurchaseOrderQuery param) {
        List<PurchaseOrder> datalist = purchaseOrderDao.list(param);
        return datalist;
    }

    @Override
    public List<OrderDetail> getOrederDetailByPDId(String detailId) {
        List<OrderDetail> details = orderDetailPurchaseDao.getOrderDetailByPDId(detailId);
        if (details == null)
            return null;
        for (OrderDetail d : details) {
            CustomerTypeStorehouse cts = orderDetailPurchaseDao.getCustomerTypeStorehouse(d.getCustomerId());
            if (cts != null) {
                d.setCustomerType(cts.getCustomerType());
                d.setStoreName(cts.getStoreName());
            }
        }
        return details;
    }

    /**
     * 审核
     *
     * @param ids
     * @param audited
     * @param memo
     * @return
     */
    @Override
    @Transactional
    public String auditDetailById(List<String> ids, boolean audited, String memo) {
        String resultStr = "";
        for (String id : ids) {
            PurchaseOrderDetail detail = purchaseOrderDetailDao.getById(id);
            if (!detail.getState().equals(PurchaseOrderDetailState.place)) {
                resultStr += detail.getGoodsName() + "明细不是下单状态，请核实后再审核！";
                continue;
            }
            int result;
            if (audited) {
                result = purchaseOrderDetailDao.auditedById(id, memo);
                purchaseOrderDetailDao.auditedInById(id);

                stroehouseInDao.changeStateByDetailId(id, StroehouseInState.audited);

                PurchaseOrder order = purchaseOrderDao.getById(detail.getOrderId());
                if (order.getState().equals(PurchaseOrderState.place))
                    purchaseOrderDao.updateStateById(detail.getOrderId(), PurchaseOrderState.audited);

            } else
                result = purchaseOrderDetailDao.notAuditedById(id, memo);

            // 处理采购单的状态
            if (result > 0) {
                sysLogsService.save("采购单", detail.getOrderId(), true,
                        "操作员：" + UserUtil.getLoginUser().getUsername() +",对"+ detail.getGoodsName() + "审核通过!");
                resultStr += detail.getGoodsName() + "审核通过!";
            } else {
                sysLogsService.save("采购单", detail.getOrderId(), false,
                        "操作员：" + UserUtil.getLoginUser().getUsername() +",对"+ detail.getGoodsName() + "审核失败");
                resultStr += detail.getGoodsName() + "审核通过!";
            }
        }
        AuditLog auditLog = new AuditLog();
        auditLog.setUserName(UserUtil.getLoginUser().getUsername());
        auditLog.setType(AuditLog.AuditLogType.COST);
        auditLog.setCount((long) ids.size());
        auditLogService.save(auditLog);
        return resultStr;
    }


    /**
     * 审核之后，放弃审核
     *
     * @param id
     * @return
     */
    @Override
    public String unauditDetailById(String id) {
        PurchaseOrderDetail detail = purchaseOrderDetailDao.getById(id);
        if (!detail.getState().endsWith(PurchaseOrderDetailState.audited))
            return "该明细不是审核状态，不能放弃审核！";

        int result = purchaseOrderDetailDao.unauditedById(id);
        // 处理采购单的状态
        if (result > 0) {
            purchaseOrderDetailDao.unauditedInById(id);
            PurchaseOrder order = purchaseOrderDao.getById(detail.getOrderId());
            if (order.getPurchaseType().equals(PurchaseType.byOrder)) {
                //修改detail的状态为预采购
                List<String> ids = orderDetailPurchaseDao.getOrderDetailIdsByPdId(id);
                orderDetailPurchaseDao.batchUpdateOrderDetailPrePurchase(ids);
                //获取所有相关的订单编号
                List<String> orderids = orderDao.getDistinctOrderId(ids);
                //修改订单状态
                for (String orderid : orderids) {
                    orderService.updateOrderStateById(orderid);
                }
                //删除分订单信息
                orderDetailInDao.delByPurchaseDetailId(id);
            }

            if (order.getState().equals(PurchaseOrderState.audited)) {
                if (purchaseOrderDetailDao.countByState(detail.getOrderId(), PurchaseOrderDetailState.audited) == 0)
                    purchaseOrderDao.updateStateById(detail.getOrderId(), PurchaseOrderState.place);
            }
            sysLogsService.save("采购单", detail.getOrderId(), true,
                    "id为" + detail.getOrderId() + "的采购单中id为" + detail.getId() + "的条目细节放弃审核");
            return "放弃审核成功";
        } else
            return "放弃审核失败";
    }

    @Override
    public String delDetailById(String detailId) {
        PurchaseOrderDetail old = purchaseOrderDetailDao.getById(detailId);
        if (old == null) {
            return "订单明细不存在，请核实后再修改！";
        }
        if (!old.getState().equals(PurchaseOrderDetailState.place)) {
            return "订单明细已经审核，不能修改，若需要修改请弃审后再修改！";
        }
        PurchaseOrder order = purchaseOrderDao.getById(old.getOrderId());
        if (order.getPurchaseType().equals(PurchaseType.byOrder)) {
            orderDetailPurchaseDao.batchUnupdateOrderDetailPrePurchase(detailId);
            List<String> orderDetailIds = orderDetailPurchaseDao.getOrderDetailIdsByPdId(detailId);
            // 清理旧的订单状态
            List<String> orderids = orderDao.getDistinctOrderId(orderDetailIds);
            for (String orderid : orderids) {
                orderService.updateOrderStateById(orderid);
            }
        }
        plannedStorageDao.delStoreIn(detailId);
        purchaseOrderDetailDao.delById(detailId);
        sysLogsService.save("采购单", old.getOrderId(), true, "操作员：" + UserUtil.getLoginUser().getUsername()+"," + ",删除"
                + old.getGoodsName() + old.getAmount() + "的采购单条目详细成功");
        return "删除成功";
    }

    // 生成分库数据,isdel 是否删除原有的分库数据

    private void createOrderPlannedIn(PurchaseOrderDetail detail, boolean isdel, Date purDate) {
        if (isdel) {
            PurchaseOrderDetail old = purchaseOrderDetailDao.getById(detail.getId());
            plannedStorageDao.delStoreIn(detail.getId());
        }

        // 根据采购单id取原始订单明细并分组统计
        List<PurchaseOderClassStatistics> statisticsList = plannedStorageDao.statisticsGroupByStore(detail.getId());

        for (PurchaseOderClassStatistics s : statisticsList) {
            StroehouseIn in = new StroehouseIn();
            in.setPurchaseDetailId(detail.getId());
            in.setGoodsId(detail.getGoodsId());
            Goods goods = goodsDao.getById(s.getGoodsId());
            in.setPrice(goods.getPrice());
            in.setProviderId(goods.getProviderId());
            // 计算数量
            double amount = s.getAmount();
            double loss = 0;
            if (detail.getLossAmount() != null)
                loss = detail.getLossAmount() * amount / detail.getStatisticAmount();

            in.setPurchaseType(StroehouseIn.PurchaseType.orderPurchase);
            in.setAmount(amount + loss);
            in.setLossAmount(loss);
            in.setStorehouseId(s.getStoreId());
            in.setPurchaseDate(purDate); 
            stroehouseInDao.save(in);
            sysLogsService.save("入库", in.getId(), true, "添加新建入库计划单");
        }

    }

    @Override
    public String getNum() {
        String preFix = "P";
        preFix += DateUtil.getDays();
        int id = sequenceDao.getNextValue("PurchaseOrder");
        String no = String.format("%04d", id);
        System.out.println(no);
        return preFix + no;
    }

	@Override 
	public PurchasePrint getPrintPurchase(String id) {
		PurchaseOrder order = this.getById(id);
		if (order == null)
			return null;
		
		PurchasePrint pp = new PurchasePrint(order.getOrderNo(),order.getPurchaserName(), order.getPurchaseDate());
		List<PurchaseLinePrint> list = purchaseOrderDao.getPrintDetail(id);
		pp.setList(list);
		purchaseOrderDao.addPrint(id);
		return pp; 
	}
}
