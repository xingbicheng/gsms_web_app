package com.myqm.service.gs.purchase;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;

public interface PurchasePriceService {

	/**
	 * 根据id删除PurchasePrice
	 */
	String delById(String id );

	/**
	 * 根据新增PurchasePrice，id自增
	 */
	String save(PurchasePrice record);

	/**
	 * 根据id查询PurchasePrice
	 */
	PurchasePrice getById(String id);

	/**
	 * 根据id更新PurchasePrice
	 */
	String editById(PurchasePrice record);

	/**
	 * 分页查询PurchasePrice
	 */
	PageTableData<PurchasePrice> list(int pagenum, int pagesize, PurchasePriceQuery param);

	/**
	 * 查询所有PurchasePrice
	 */
	List<PurchasePrice> listAll(PurchasePriceQuery param);
 

	String saveAll(List<PurchasePrice> record);
	
	String  priceById(String id, BigDecimal price);

	String delFlagById(String id);

	String editByIds(List<PurchasePrice> record);
	
}
