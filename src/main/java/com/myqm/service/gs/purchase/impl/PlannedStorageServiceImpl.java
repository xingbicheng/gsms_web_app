package com.myqm.service.gs.purchase.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.myqm.dao.gs.basic.GoodsDao;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.sysfun.SysLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.dao.gs.in.StroehouseInDao;
import com.myqm.dao.gs.purchase.PlannedStorageDao;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dto.gs.in.SetProviderInfo;
import com.myqm.dto.gs.in.SetTransferInfo;
import com.myqm.pojo.gs.basic.Provider;
import com.myqm.pojo.gs.in.StroehouseIn;
import com.myqm.pojo.gs.in.StroehouseIn.StroehouseInState;
import com.myqm.pojo.gs.purchase.ProviderInfo;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.service.gs.purchase.PlannedStorageService;
import com.myqm.vo.gs.in.StroehouseInQuery;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlannedStorageServiceImpl implements PlannedStorageService {
	@Autowired
	public StroehouseInDao stroehouseInDao;

	@Autowired
    GoodsDao goodsDao;

	@Autowired
	public PlannedStorageDao plannedDao;

	@Autowired
	public SysLogsDao logsDao;

	@Autowired
    private SysLogsService sysLogsService;
	
	@Autowired
    private ProviderDao providerDao;

	// 修改采购单明细价格
	private void updatePurchaseDetailPrice(String purchaseDetailId) {
        PurchaseOrderDetail pDetail = plannedDao.getAveragePriceByPurchaseDetail(purchaseDetailId);
        pDetail.setId(purchaseDetailId);
        BigDecimal temp;
        if (pDetail != null) {
            if ((pDetail.getRealSum() == null) || (pDetail.getRealAmount() == 0))
                temp = new BigDecimal("0");
            else
                temp = new BigDecimal(pDetail.getRealSum().doubleValue() / pDetail.getRealAmount());

            pDetail.setAveragePrice(temp);
            plannedDao.updatePurchaseDetailPrice(pDetail);
        }
    }

	@Override
	public List<StroehouseIn> getByPurchaseDetailId(String pdid) {
		StroehouseInQuery query = new StroehouseInQuery();
		query.setPurchaseDetailId(pdid);
        List<StroehouseIn> response=stroehouseInDao.list(query);
        for(StroehouseIn stroehouseIn:response){
            stroehouseIn.setStatisticAmount(stroehouseIn.getAmount()-stroehouseIn.getLossAmount());
        }
		return response;
	}

	@Override
	public String merge(String id1, String id2) {
		StroehouseIn in1 = stroehouseInDao.getById(id1);
		String mark="";

		if (in1 == null)
            mark= "计划入库明细不存在，请核实后修改！";

		if (in1.getState().equals(StroehouseInState.audited))
            mark= "计划入库单已经审核，不能修改，请弃审后修改！";

		StroehouseIn in2 = stroehouseInDao.getById(id2);
		if (in2 == null)
            mark= "计划入库明细不存在，请核实后修改！";

		if (in2.getState().equals(StroehouseInState.audited))
            mark= "计划入库单已经审核，不能修改，请弃审后修改！";

		if (!in1.getStorehouseId().equals(in2.getStorehouseId()))
            mark= "非同一库房数据，不能合并";

		if(mark!=""){
            //sysLogsService.save("入库单",in1.getId(),false,"合并失败");
            return mark;
        }

        Double oldAmount=in1.getAmount();
		Double oldLossAmount=in1.getLossAmount();
		stroehouseInDao.delById(id2);
		in1.setAmount(in1.getAmount() + in2.getAmount());
		in1.setLossAmount(in1.getLossAmount() + in2.getLossAmount());
		stroehouseInDao.editById(in1);
		sysLogsService.save("入库单",in1.getId(),true,
                in2.getId()+"合并到了"+in1.getId()+
                        ",采购量从"+oldAmount+"变到了"+in1.getAmount()+
                        "，损失量从"+oldLossAmount+"变到了"+in1.getLossAmount());
		return "合并成功";
	}

    /**
     * 拆分入库单
     * @param inId
     * @param amount
     * @return
     */
	@Override
	public String resolve(String inId, List<Double> amount) {

	    String mark="";
		if (amount == null)
            mark= "分解数据为空，请核实后修改！";
		if (amount.size() == 1)
            mark= "分解数据只有一个，不用分解，请核实后修改！";

		StroehouseIn in = stroehouseInDao.getById(inId);
		if (in == null){
            mark= "计划入库明细不存在，请核实后修改！";
            return mark;

		}
		if (!in.getState().equals(StroehouseInState.place)){
            mark= "计划入库单非下单状态，不能修改";		 
            return mark; 
		}


		double sum = 0;
		for (int i = 0; i < amount.size(); i++)
			sum += amount.get(i);
		if (Math.abs(sum - in.getAmount()) > 0.01d){
           return  "分解数据之和不等于原购买量，分解错误，请核实后修改！";
        }

        Double amountToal=in.getAmount();
		Double lossAmountToal=in.getLossAmount();
        String remark="";
		for( int i=0;i<amount.size();i++){
		    remark+=(" "+amount.get(i));
            in.setAmount(amount.get(i));
            in.setLossAmount(amount.get(i)/amountToal*lossAmountToal);
		    if(i==0)
                stroehouseInDao.editById(in);
            else
                stroehouseInDao.save(in);
        }

        sysLogsService.save("入库单",inId,true,"入库单分解成功。总量为"+in.getAmount()+
                "分解成了"+remark);
		return "分解成功";
	}

    /**
     * ？
     * 设置供货商
     * @param setInfo
     * @return
     */
    @Transactional
	@Override
	public String setProvider(SetProviderInfo setInfo) {
		List<StroehouseIn> inlist = new ArrayList<StroehouseIn>();
		for (String sinid : setInfo.getStorehouseInId()) {
			StroehouseIn in = stroehouseInDao.getById(sinid);
			if (in == null){			  
                return "计划入库明细不存在，请核实后修改！";
            }
			if (!in.getState().equals(StroehouseInState.place))
            {              
                return "计划入库单非下单状态，不能修改！";
            } 
			inlist.add(in); 
		}
		Provider provider = providerDao.getById(setInfo.getProviderId());
		ProviderInfo pinfo = new ProviderInfo();
		pinfo.setPrice(setInfo.getPrice()); 
		pinfo.setProviderId(setInfo.getProviderId());
		pinfo.setPackingMemo(setInfo.getPackingMemo());
		if (provider.getProviderTypeId().equals("1"))
			pinfo.setCanBack(true);
		else
			pinfo.setCanBack(false);
		pinfo.setInIds(new ArrayList<String>());


		Set<String> goodsIds=new HashSet<String>();

		for (StroehouseIn in : inlist) {
			pinfo.getInIds().add(in.getId());
            goodsIds.add(in.getGoodsId());
		}
		for(String id:goodsIds)
		    goodsDao.updatePrice(id,setInfo.getPrice().doubleValue());

		stroehouseInDao.setProviderInfo(pinfo);
		for (StroehouseIn sin : inlist) {
			updatePurchaseDetailPrice(sin.getPurchaseDetailId());
		} 
        sysLogsService.save("入库单",null,true,
                "设置供货商信息成功");
		return "设置成功";
	}

	@Override
    public String setTransferInfo(SetTransferInfo setInfo) {
	    //判断状态
        StroehouseIn in=stroehouseInDao.getById(setInfo.getToStorehouseInId());
        if(in==null)
        	return "目标入库单不存在，请检查数据";
        if(!in.getState().equals(StroehouseInState.place))
            return "目标入库单非下单状态，不能设置转库"; 

        StroehouseIn  fromin = stroehouseInDao.getById(setInfo.getFromStorehouseInId());

        if(fromin==null)
        	return "目标入库单不存在，请检查数据";
         
        if(!fromin.getGoodsId().endsWith(in.getGoodsId()) )
        	return "库存商品品种不一样，请检查数据"; 
        
        if(fromin.getSurplus() < in.getAmount() )
        	return "库存量不够，请检查数据"; 
        
        //中央库入库单
        String id = setInfo.getFromStorehouseInId();

        //减去中央库入库单可用量
        stroehouseInDao.setFromTransferInfo(id, in.getAmount());

        setInfo.setFromStorehouseId(fromin.getStorehouseId());
        setInfo.setToStorehouseId(in.getStorehouseId());
        setInfo.setPackingMemo(fromin.getPackingMemo());
        setInfo.setPrice(fromin.getPrice());
        setInfo.setProviderId(fromin.getProviderId());
        setInfo.setCanBack(fromin.getCanBack());

        //更改被入库 入库单 信息
        stroehouseInDao.setTransferInfo(setInfo); 
        updatePurchaseDetailPrice(in.getPurchaseDetailId()); 
        return "转库成功";
    }
}


