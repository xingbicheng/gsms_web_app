package com.myqm.service.gs.purchase.impl;

import com.myqm.dao.gs.purchase.PurchaseOrderSumDao;
import com.myqm.pojo.gs.purchase.PurchaseOrderSum;
import com.myqm.pojo.gs.purchase.PurchaseOrderTotal;
import com.myqm.service.gs.purchase.PurchaseOrderSumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseOrderSumServiceImpl implements PurchaseOrderSumService {
    @Autowired
    PurchaseOrderSumDao purchaseOrderSumDao;
    @Override
    public List<PurchaseOrderSum> sum(PurchaseOrderTotal purchaseOrderTotal) {
        return purchaseOrderSumDao.sum(purchaseOrderTotal);
    }
}
