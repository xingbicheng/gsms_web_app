package com.myqm.service.gs.purchase;

import com.myqm.pojo.gs.order.OrderDetail;
import com.myqm.pojo.gs.purchase.PurchaseOrder;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.pojo.gs.purchase.PurchaseOrderTs;
import com.myqm.pojo.gs.purchase.PurchasePrint;
import com.myqm.vo.gs.purchase.OrderAndDetailRes;
//import com.myqm.vo.gs.purchase.OrderInfoQuery;
import com.myqm.vo.gs.purchase.PurchaseOrderQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;

public interface PurchaseOrderService {
	/**
	 * 根据id删除PurchaseOrder
	 */
	String delById(String id);

	/**
	 * 根据id删除PurchaseOrder
	 */
	String delFlagById(String id);

	/**
	 * 根据新增PurchaseOrder，id自增
	 */
	String save(PurchaseOrder record);

    /**
     * 计划采购
     * @param record
     * @return
     */
    String planPurchaseSave(PurchaseOrder record);

	String editDetailT(PurchaseOrderTs record);

	/**
	 * 根据id查询PurchaseOrder
	 */
	PurchaseOrder getById(String id);

	/**
	 * 根据id更新采购头部信息
	 */
	String editById(PurchaseOrder record);

	/**
	 * 根据id更新采购单明细
	 */
	String editDetailById(PurchaseOrderDetail record);
    String editPlanDetailById(PurchaseOrderDetail record);
    
    String editPlanById(PurchaseOrder order);
	String delDetailById(String detailId);

	List<OrderDetail> getOrederDetailByPDId(String detailId);

	/**
	 * 分页查询PurchaseOrder
	 */
	PageTableData<PurchaseOrder> list(int pagenum, int pagesize, PurchaseOrderQuery param);

	/**
	 * 分页查询PurchaseOrder
	 */
	List<PurchaseOrder> listAll(PurchaseOrderQuery param);

	/**
	 * 获取上一条详细
	 * 
	 * @param currentNo
	 * @return
	 */
	PurchaseOrder pre(PurchaseOrderQuery param);

	/**
	 * 获取下一条详细
	 * 
	 * @param currentNo
	 * @return
	 */
	PurchaseOrder next(PurchaseOrderQuery param);

	String auditDetailById(List<String> id, boolean audited, String memo);

	String unauditDetailById(String id);

	String getNum(); 
	
	PurchasePrint getPrintPurchase(String id);
}