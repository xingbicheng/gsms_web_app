package com.myqm.service.gs.purchase.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.purchase.PurchaseOrderDetailDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.purchase.PurchaseOrderDetail;
import com.myqm.service.gs.purchase.PurchaseOrderDetailService;
import com.myqm.vo.gs.purchase.PurchaseOrderDetailQuery;

@Service
public class PurchaseOrderDetailServiceImpl implements PurchaseOrderDetailService {

	@Autowired
	PurchaseOrderDetailDao purchaseDetailDao;

	@Override
	public PageTableData<PurchaseOrderDetail> list(int pagenum, int pagesize, PurchaseOrderDetailQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<PurchaseOrderDetail> datalist = purchaseDetailDao.list(param);
		PageTableData<PurchaseOrderDetail> resultPage = new PageTableData<PurchaseOrderDetail>(datalist);
		PageInfo<PurchaseOrderDetail> p = new PageInfo<PurchaseOrderDetail>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<PurchaseOrderDetail> listAll(PurchaseOrderDetailQuery param) {
		return purchaseDetailDao.list(param);
	}

}
