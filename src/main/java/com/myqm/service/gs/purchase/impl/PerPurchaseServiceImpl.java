package com.myqm.service.gs.purchase.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myqm.dao.gs.purchase.PerPurchaseDao;
import com.myqm.dto.gs.purchase.PrePurchaseGoods;
import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.pojo.gs.purchase.PrePurchaseDetail;
import com.myqm.service.gs.purchase.PerPurchaseService;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;

@Service
public class PerPurchaseServiceImpl implements PerPurchaseService {
	@Autowired
	PerPurchaseDao perDao;

	@Override
	public List<PrePurchaseGoodsType> getPurchaseDetail(PerPurchaseQuery param) {
		List<PrePurchaseGoodsType> typeList = new ArrayList<PrePurchaseGoodsType>();

		List<PrePurchaseDetail> details = perDao.preList(param);
		String type = "";
		String goodsName = "";
		PrePurchaseGoodsType preType = null;
		PrePurchaseGoods preGoods = null;
		List<PrePurchaseGoods> preGoodsList = null;
		List<PrePurchaseDetail> prePurchaseList = null;
		double allAmount = 0;
		for (int i = 0; i < details.size(); i++) {
			PrePurchaseDetail detail = details.get(i);
			if (!type.equals(detail.getGoodsType())) {
				type = detail.getGoodsType();
				PrePurchaseGoodsType goodsType = new PrePurchaseGoodsType();
				goodsType.setGoodsType(detail.getGoodsType());
				goodsType.setGoodsTypeId(detail.getGoodsTypeId());
				preType = goodsType;
				preGoodsList = new ArrayList<PrePurchaseGoods>();
				preType.setGoodsGruoup(preGoodsList);
				typeList.add(preType);
			}

			if (!goodsName.equals(detail.getGoodsName())) {
				if (preGoods != null) {
					preGoods.setStatisticAmount(allAmount);
					preGoods.setLossAmount(allAmount * preGoods.getLossRate());
					preGoods.setAmount(allAmount + preGoods.getLossAmount());
					allAmount = 0;
				}

				goodsName = detail.getGoodsName();
				PrePurchaseGoods goods = new PrePurchaseGoods();
				goods.setGoodsId(detail.getGoodsId());
				goods.setGoodsName(detail.getGoodsName());
				goods.setGoodsType(detail.getGoodsType());
				goods.setGoodsTypeId(detail.getGoodsTypeId());
				goods.setLossRate(detail.getLossRate());
				goods.setRealDistributionDate(detail.getRealDistributionDate());
				goods.setDistributionDate(detail.getDistributionDate());
				preGoods = goods;
				prePurchaseList = new ArrayList<PrePurchaseDetail>();
				preGoods.setDetails(prePurchaseList); 
				preType.getGoodsGruoup().add(goods);
				 
			}
			if (prePurchaseList != null) {
				allAmount += detail.getAmount();
				prePurchaseList.add(detail);
			}
		}
		return typeList;
	}
}
