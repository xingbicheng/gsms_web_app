package com.myqm.service.gs.purchase;

import com.myqm.dto.gs.purchase.PrePurchaseGoodsType;
import com.myqm.vo.gs.purchase.PerPurchaseQuery;

import java.util.List;

public interface PerPurchaseService {

	List<PrePurchaseGoodsType> getPurchaseDetail(PerPurchaseQuery param);
}