package com.myqm.service.gs.purchase;

import java.util.List;

import com.myqm.dto.gs.in.SetProviderInfo;
import com.myqm.dto.gs.in.SetTransferInfo;
import com.myqm.pojo.gs.in.StroehouseIn;

public interface PlannedStorageService {

	/**
	 * 根据新增入库计划
	 */
	String merge(String id1, String id2);

	/**
	 * 设置供应商信息
	 * @param setInfo
	 * @return
	 */
	String setProvider(SetProviderInfo setInfo);
	
	/**
	 * 设置中央库调入信息
	 * @param setInfo
	 * @return
	 */
	String setTransferInfo(SetTransferInfo setInfo);
	

	/**
	 * 根据入库计划单id更新入库计划
	 */
	String resolve(String inId, List<Double> amount);

	/**
	 * 根据采购单明细id查询StroehouseIn
	 */
	List<StroehouseIn> getByPurchaseDetailId(String pdid);
	
	
}
