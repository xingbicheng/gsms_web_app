package com.myqm.service.gs.purchase.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.gs.purchase.PurchasePriceDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.purchase.PurchasePrice;
import com.myqm.pojo.gs.purchase.PurchasePrice.PurchasePriceState;
import com.myqm.service.gs.purchase.PurchasePriceService;
import com.myqm.vo.gs.purchase.PurchasePriceQuery;


@Service
public class PurchasePriceImpl implements PurchasePriceService{

	
	@Autowired
	public PurchasePriceDao purchasePriceDao;

	@Override
	public String delById(String id) {
		PurchasePrice purchasePrice = purchasePriceDao.getById(id);
		if(purchasePriceDao.delById(id) > 0) {
		if(!purchasePrice.getState().equals(PurchasePriceState.del))
			return "订单未被删除标记过,不能删除请核实过再删除";
		else 
			   return "删除成功";
		}
		else
			return null;
	}
	
	@Override
	public String delFlagById(String id){
		PurchasePrice purchasePrice = purchasePriceDao.getById(id);
		if(purchasePriceDao.delFlagById(id) > 0) {
		if(!purchasePrice.getState().equals(PurchasePriceState.draft))
			//标记
			return "删除失败";
		else
		    purchasePriceDao.editStatedById(PurchasePriceState.del, id);
		    return "删除成功";
		}
		else
			return null;
	}
	
	
	@Override
	public String save(PurchasePrice record) {
		if(purchasePriceDao.save(record)>0) {
			return record.getId();
		}else
		return null;
	}

	@Override
	public String saveAll(List<PurchasePrice> record) {
		int count = purchasePriceDao.saveAll(record);
		return "储存成功"+count+"条记录";
	}
	
	@Override
	public PurchasePrice getById(String id) {
		return purchasePriceDao.getById(id);
	}

	@Override
	public String editById(PurchasePrice record) {
		if (purchasePriceDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}
	
	@Override
	public String editByIds(List<PurchasePrice> record) {
		if (purchasePriceDao.editByIds(record) > 0) {
		    int  count = purchasePriceDao.editByIds(record);
		    return "修改成功"+count+"条记录";}
		else
			return null;
	}

	@Override
	public PageTableData<PurchasePrice> list(int pagenum, int pagesize, PurchasePriceQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<PurchasePrice> datalist = purchasePriceDao.list(param);
		PageTableData<PurchasePrice> resultPage = new PageTableData<PurchasePrice>(datalist);
		PageInfo<PurchasePrice> p = new PageInfo<PurchasePrice>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<PurchasePrice> listAll(PurchasePriceQuery param) {
		List<PurchasePrice> datalist = purchasePriceDao.list(param);
		return datalist;
	}

	@Override
	public String priceById(String id, BigDecimal price) {
		Timestamp scurrtest = new Timestamp(System.currentTimeMillis());
		long sqlLastTime = scurrtest.getTime();// 直接转换成long
		PurchasePrice  pp = purchasePriceDao.getById(id);
	 
		String limitDate = pp.getLimitDate();
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date parse;
		 boolean isRight = true;
			try {
				parse = sdf.parse(limitDate);
				long limit = parse.getTime();
				if(limit < new Date().getTime())
					isRight = false;
			} catch (ParseException e) { 
				isRight = true;
				e.printStackTrace();
			}
			 
			if(isRight) {
				if (purchasePriceDao.priceById(id, price) > 0)
				    return "价格修改成功！";
			   else
				   return "价格修改失败！";
			} 
			 return "时间过去了，不能设置价格。";
	}
}
