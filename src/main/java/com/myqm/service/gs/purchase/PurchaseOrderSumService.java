package com.myqm.service.gs.purchase;

import com.myqm.pojo.gs.purchase.PurchaseOrderSum;
import com.myqm.pojo.gs.purchase.PurchaseOrderTotal;

import java.util.List;

public interface PurchaseOrderSumService {
    List<PurchaseOrderSum> sum(PurchaseOrderTotal purchaseOrderTotal);
}
