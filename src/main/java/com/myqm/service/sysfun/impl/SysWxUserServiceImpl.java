package com.myqm.service.sysfun.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.order.Order;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.dao.gs.basic.CustomerDao;
import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.dao.sysfun.SysWxUserDao;
import com.myqm.pojo.sysfun.SysWxUser;
import com.myqm.pojo.sysfun.SysUser.Status;
import com.myqm.vo.sysfun.SysWxUserQuery;
import com.myqm.service.sysfun.SysWxUserService;


@Service
public class SysWxUserServiceImpl implements SysWxUserService{

	@Autowired
	public SysWxUserDao sysWxUserDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Autowired
	public CustomerDao customerDao;
	
	@Autowired
	public ProviderDao providerDao;
	
	@Autowired
	public SysUserDao sysUserDao;
	@Override
	public String delById(String id) {
		if (sysWxUserDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(SysWxUser record) {
	
		return sysWxUserDao.save(record)+"";
	}

	@Override
	public SysWxUser getById(String id) {
		SysWxUser sysWxUser=sysWxUserDao.getById(id);
		if(sysWxUser==null) {
			return null;
		}
		if(sysWxUser.getUserId()!=null) {
			if(sysWxUser.getType().equals("0")) {
				String username = sysUserDao.getById(sysWxUser.getUserId()).getUsername();
				sysWxUser.setUserName(username);
			}
			if(sysWxUser.getType().equals("1")) {
				String username = providerDao.getById(sysWxUser.getUserId()).getProviderName();
				sysWxUser.setUserName(username);
			}
			if(sysWxUser.getType().equals("2")) {
				String username = customerDao.getById(sysWxUser.getUserId()).getCustomerName();
				sysWxUser.setUserName(username);
			}
		}
		return sysWxUser;
	}


	@Override
	public String editById(SysWxUser record) {
		return sysWxUserDao.editById(record)+"";
	}

	@Override
	public PageTableData<SysWxUser> list(int pagenum, int pagesize,SysWxUserQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<SysWxUser> datalist = sysWxUserDao.list(param);
		for (SysWxUser sysWxUser : datalist) {
			if(sysWxUser.getUserId()!=null) {
				if(sysWxUser.getType().equals("0")) {
					String username = sysUserDao.getById(sysWxUser.getUserId()).getUsername();
					sysWxUser.setUserName(username);
				}
				if(sysWxUser.getType().equals("1")) {
					String username = providerDao.getById(sysWxUser.getUserId()).getProviderName();
					sysWxUser.setUserName(username);
				}
				if(sysWxUser.getType().equals("2")) {
					String username = customerDao.getById(sysWxUser.getUserId()).getCustomerName();
					sysWxUser.setUserName(username);
				}
			}
			
		}
		PageTableData<SysWxUser> resultPage = new PageTableData<SysWxUser>(datalist);
		PageInfo<SysWxUser> p = new PageInfo<SysWxUser>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<SysWxUser> listAll(SysWxUserQuery param) {
		List<SysWxUser> datalist = sysWxUserDao.list(param);
		return datalist;
	}

	@Override
	public String auited(String openId,String userId) {

		sysWxUserDao.updateState(openId,userId);
		return openId;
	}

	@Override
	public String unaudited( String openId) {
		// TODO Auto-generated method stub
		sysWxUserDao.unauditedUpdateState(openId);
		return openId;
	}

	@Override
	public String getOpenId(String openId) {
		SysWxUser byOpenId = sysWxUserDao.getByOpenId(openId);
		if(byOpenId==null) {
			return null;
		}
		else if(byOpenId.getStatus().equals(Status.LOCKED+"")){
			return "2";
		}
		else if(byOpenId.getStatus().equals(Status.DISABLED+"")){
			return "0";
		}
		else
			return "1";
	}

	@Override
	public SysWxUser getByOpenId(String openId) {
		return sysWxUserDao.getByOpenId(openId);
	}

}
