package com.myqm.service.sysfun.impl;

import java.util.*;

import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.utils.UserUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.sysfun.PermissionDao;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.service.base.BaseRecordService;
import com.myqm.service.sysfun.PermissionService;
import com.myqm.vo.sysfun.QueryPermission;

@Service
public class PermissionServiceImpl extends BaseRecordService<Permission, QueryPermission, PermissionDao>
		implements PermissionService {

	private PermissionDao permissionDao;

	public PermissionServiceImpl(PermissionDao mapper) {
		super(mapper);
		permissionDao = mapper;
	}

    @Override
    public String save(Permission record) {
        int id=permissionDao.save(record);
        return id>0?"新增成功":"新增失败";
    }

    @Override
    public List<Permission> childMenu(String id) {

	    QueryPermission queryPermission=new QueryPermission();
	    queryPermission.setParentId(id);
        List<Permission> permissions=permissionDao.list(queryPermission);

        return permissions;

    }

    @Override
	@Transactional
	public String delById(String id) {
		permissionDao.deleteRolePermission(id);
		permissionDao.delById(id);
		permissionDao.deleteByParentId(id);
		log.debug("删除菜单id:{}", id);
		return id;
	}

	@Override
	public int deleteByParentId(String id) {
		return permissionDao.deleteByParentId(id);
	}

	@Override
	public int deleteRolePermission(String permissionId) {
		return permissionDao.deleteRolePermission(permissionId);
	}

	@Override
	public Set<String> listUserIds(String permissionId) {
		return permissionDao.listUserIds(permissionId);
	}

	@Override
	public List<Permission> listParents() {
		return permissionDao.rootMenu();
	}

	@Override
	public List<Permission> listByUserId(String userId) {
		return permissionDao.listByUserId(userId);
	}

	@Override
	public List<Permission> listByRoleId(String roleId) {
		return permissionDao.listByRoleId(roleId);
	}

	@Override
	public List<Permission> listAll(QueryPermission param) {
		return permissionDao.list(param);
	}

    @Override
    public SysUser getCurrent() {
        SysUser sysUser = UserUtil.getLoginUser();
        List<Permission> permissions=permissionDao.getByUserId(sysUser.getId());
        ((LoginUser) sysUser).setPermissions(permissions);
        return sysUser;
    }
}
