package com.myqm.service.sysfun.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData; 
import com.myqm.utils.UserUtil;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.dao.sysfun.DepartmentDao;
import com.myqm.pojo.sysfun.Department;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.vo.sysfun.DepartmentQuery;
import com.myqm.service.sysfun.DepartmentService;


@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	public DepartmentDao departmentDao;
	
	@Autowired
	public SysLogsDao logsDao;
	
	@Autowired
	private SysUserDao userDao;
	
	@Override
	public String delById(String id) {
		if (departmentDao.delById(id) > 0) 
			return id;
		else
			return null;
	}

	@Override
	public String save(Department record) {
		SysUser user = UserUtil.getLoginUser();
		record.setCreaterId(user.getId());
		record.setUpdaterId(user.getId());
		int count =departmentDao.save(record);
		return record.getId();
 
	}

	@Override
	public Department getById(String id) {
		Department dep = departmentDao.getById(id);
		if (dep != null){
			String createrName = userDao.getUserNameById(dep.getCreaterId());
			String updaterName = userDao.getUserNameById(dep.getUpdaterId());
			dep.setCreaterName(createrName);
			dep.setUpdaterName(updaterName);
		}
		return dep;
	}


	@Override
	public String editById(Department record) {
		return departmentDao.editById(record)+"";
	}

	@Override
	public PageTableData<Department> list(int pagenum, int pagesize,DepartmentQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Department> datalist = departmentDao.list(param);
		PageTableData<Department> resultPage = new PageTableData<Department>(datalist);
		PageInfo<Department> p = new PageInfo<Department>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal()); 
		return resultPage;
	}

	@Override
	public List<Department> listAll(DepartmentQuery param) {
		List<Department> datalist = departmentDao.list(param);
		return datalist;
	}

}
