package com.myqm.service.sysfun.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.myqm.dao.sysfun.RoleDao;
import com.myqm.pojo.sysfun.Role;
import com.myqm.service.base.BaseInfoService;
import com.myqm.service.sysfun.RoleService;
import com.myqm.vo.sysfun.QueryRole;

@Service
public class RoleServiceImpl extends BaseInfoService<Role, QueryRole, RoleDao> implements RoleService {

	@Autowired
	public RoleServiceImpl(RoleDao mapper) {
		super(mapper);
		roleDao = mapper;
	}

	private RoleDao roleDao;

	@Override
	@Transactional
	public String delById(String id) {
		roleDao.deleteRolePermission(id);
		roleDao.deleteRoleUser(id);
		roleDao.delById(id);
		log.debug("删除角色id:{}", id);
		return id;
	}

	@Override
	public List<Role> listByUserId(String userId) {

		return roleDao.listByUserId(userId);
	}

	@Override
	public int deleteRolePermission(String roleId) {

		return roleDao.deleteRolePermission(roleId);
	}

	@Override
	public int deleteRoleUser(String roleId) {

		return roleDao.deleteRoleUser(roleId);
	}

	@Override

	@Transactional
	public int saveRolePermission(String roleId, List<String> permissionIds) {

		roleDao.deleteRolePermission(roleId);
		return roleDao.saveRolePermission(roleId, permissionIds);
	}

    @Override
    public int editActivation(List<Role> record) {

	    try{
            for(Role role:record ){
                roleDao.editByActivation(role);
            }
        }catch (Exception e){
	        return 1;
        }
        return 0;
    }

    @Override
    public List<Role> listAllBrif(QueryRole param) {
        return roleDao.listByParam(param);
    }
}
