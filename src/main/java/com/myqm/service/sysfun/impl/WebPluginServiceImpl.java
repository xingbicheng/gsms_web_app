package com.myqm.service.sysfun.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.gs.basic.WebPlugin;
import com.myqm.service.gs.basic.WebPluginService;
import com.myqm.utils.GetPinyinUtil;
import com.myqm.vo.gs.basic.WebPluginQuery;
import com.myqm.dao.gs.basic.WebPluginDao;
import com.myqm.dao.sysfun.SysLogsDao;

@Service
public class WebPluginServiceImpl implements WebPluginService {

	@Autowired
	public WebPluginDao webPluginDao;

	@Autowired
	public SysLogsDao logsDao;

	@Override
	public String delById(String id) {
		if (webPluginDao.delById(id) > 0)
			return id;
		else
			return null;
	}

	@Override
	public String save(WebPlugin record) {

		if (webPluginDao.save(record) > 0) {
			return record.getId();
		} else
			return null;
	}

	@Override
	public WebPlugin getById(String id) {
		return webPluginDao.getById(id);
	}

	@Override
	public String editById(WebPlugin record) {
		// 请修改更新的拼音

		if (webPluginDao.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Override
	public PageTableData<WebPlugin> list(int pagenum, int pagesize, WebPluginQuery param) {
		PageHelper.startPage(pagenum, pagesize);
		List<WebPlugin> datalist = webPluginDao.list(param);
		PageTableData<WebPlugin> resultPage = new PageTableData<WebPlugin>(datalist);
		PageInfo<WebPlugin> p = new PageInfo<WebPlugin>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	@Override
	public List<WebPlugin> listAll(WebPluginQuery param) {
		List<WebPlugin> datalist = webPluginDao.list(param);
		return datalist;
	}

}
