package com.myqm.service.sysfun.impl;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.BaseRecordService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QuerySysLogs;

@Primary
@Service
public class SysLogsServiceImpl extends BaseRecordService<SysLogs, QuerySysLogs, SysLogsDao> implements SysLogsService {

	private SysLogsDao sysLogsDao;

	public SysLogsServiceImpl(SysLogsDao mapper) {
		super(mapper);
		sysLogsDao = mapper;
	}

	@Override
	public String save(SysLogs sysLogs) {
		SysUser user = UserUtil.getLoginUser();
		if (user == null || user.getId() == null) {
			return "";
		}
		sysLogs.setUserId(user.getId());
		sysLogsDao.save(sysLogs);
		return sysLogs.getId();
	}

	
	@Override
	public int save(String module, String moduleId,Boolean flag, String remark,String userId) {
		SysLogs sysLogs = new SysLogs();
		sysLogs.setFlag(flag);
		sysLogs.setModule(module);
		sysLogs.setModuleId(moduleId);
		sysLogs.setRemark(remark);
        sysLogs.setUserId(userId);
		return sysLogsDao.save(sysLogs);
	}
	
	
	@Override
	public int save(String module, String moduleId,Boolean flag, String remark) {
		SysLogs sysLogs = new SysLogs();
		sysLogs.setFlag(flag);
		sysLogs.setModule(module);
		sysLogs.setModuleId(moduleId);
		sysLogs.setRemark(remark);
        sysLogs.setUserId(UserUtil.getLoginUser().getId());
		return sysLogsDao.save(sysLogs);
	}

	@Override
	public int delMonthLogs(int year, int start, int end) {
		int n = sysLogsDao.deleteLogs(year, start, end);
		log.info("删除{}之前日志{}条", new Date(), n);
		return n;
	}

	public int deleteLogs() {
		System.out.println("delete log");
		return 2;
	}

    @Override
    public List<SysLogs> getLogsByModuleId(String record) {
        return sysLogsDao.getLogsByModuleId(record);
    }
}
