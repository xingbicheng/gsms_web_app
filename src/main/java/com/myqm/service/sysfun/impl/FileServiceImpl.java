package com.myqm.service.sysfun.impl;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.myqm.dao.sysfun.FileInfoDao;
import com.myqm.pojo.sysfun.FileInfo;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.BaseRecordService;
import com.myqm.service.sysfun.FileService;
import com.myqm.utils.FileUtil;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QueryFileInfo;

@Service
public class FileServiceImpl extends BaseRecordService<FileInfo, QueryFileInfo, FileInfoDao> implements FileService {

	@Value("${files.path}")
	private String filesPath;

	public FileServiceImpl(FileInfoDao mapper) {
		super(mapper);
	}

	@Override
	public FileInfo save(MultipartFile file) throws IOException {
		FileInfoDao filedao = (FileInfoDao) baseDaoMapper;
		String fileOrigName = file.getOriginalFilename();
		if (!fileOrigName.contains(".")) {
			throw new IllegalArgumentException("Lack of suffix name.");
		}
		String md5 = FileUtil.fileMd5(file.getInputStream());
		FileInfo fileInfo = filedao.getByMd5(md5);
		if (fileInfo == null) {
			fileOrigName = fileOrigName.substring(fileOrigName.lastIndexOf("."));
			String prefix = fileOrigName.substring(fileOrigName.lastIndexOf(".") + 1);
			String pathname = FileUtil.getPath() + md5 + fileOrigName;
			String fullPath = filesPath + pathname;
			FileUtil.saveFile(file, fullPath);
			long size = file.getSize();
			fileInfo = new FileInfo();
			fileInfo.setFileMd5(md5);
			fileInfo.setFileName(fileOrigName);
			fileInfo.setSize(size);
			fileInfo.setPath(fullPath);
			fileInfo.setUrl(pathname);
			fileInfo.setType(prefix);
			SysUser user = UserUtil.getLoginUser();
			fileInfo.setCreaterId(user.getId());
			filedao.save(fileInfo);
			log.debug("upload file{}", fullPath);
		}
		return fileInfo;
	}

	@Override
	public FileInfo getByMd5(String md5) {
		FileInfoDao filedao = (FileInfoDao) baseDaoMapper;

		return filedao.getByMd5(md5);
	}

}
