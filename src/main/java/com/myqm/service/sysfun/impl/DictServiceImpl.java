package com.myqm.service.sysfun.impl;

import org.springframework.stereotype.Service;
import com.myqm.dao.sysfun.DictDao;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.service.base.BaseInfoService;
import com.myqm.service.sysfun.DictService;
import com.myqm.vo.sysfun.QueryDict;

@Service
public class DictServiceImpl extends BaseInfoService<Dict, QueryDict, DictDao>

		implements DictService {

	public DictServiceImpl(DictDao mapper) {
		super(mapper);
	}

}