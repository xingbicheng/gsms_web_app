package com.myqm.service.sysfun.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.pojo.PageTableData;
import com.myqm.utils.GetPinyinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.pojo.sysfun.SysUser.Status;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.BaseInfoService;
import com.myqm.service.sysfun.SysUserService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QuerySysUser;

@Service
public class SysUserServiceImpl extends BaseInfoService<SysUser, QuerySysUser, SysUserDao>

		implements SysUserService {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	SysUserDao userDao;

	public SysUserServiceImpl(SysUserDao mapper) {
		super(mapper);
		userDao = mapper;
	}

	@Override
	@Transactional
	public String save(SysUser user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setStatus(Status.VALID);
		SysUser u = UserUtil.getLoginUser();
		user.setCreaterId(u.getId());
		user.setUpdaterId(u.getId());
		userDao.save(user);
		if(user.getRoleId()!=null)
        userDao.saveUserRole(user.getId(),user.getRoleId());
		log.debug("新增用户{}", user.getUsername());
		return user.getId();
	}

    @Override
    public String edit(SysUser record) {
	    if(record.getId()!=null&&record.getRoleId()!=null)
	    userDao.editUserRole(record.getId(),record.getRoleId());
        return super.edit(record);
    }

    @Override
	@Transactional
	public void setUserRoles(String userId, List<String> roleIds) {
		if (roleIds != null) {
			userDao.deleteUserRole(userId);
			if (!CollectionUtils.isEmpty(roleIds)) {
				userDao.setUserRoles(userId, roleIds);
			}
		}
	}

	@Override
	public String setState(int s, String id) {
		if (userDao.setState(s, id) > 0)
			return id;
		else
			return "";
	}

    @Override
    public SysUser getById(String id) {
        return userDao.getById(id);
    }

    @Override
	public String changePassword(String id, String oldPassword, String newPassword) {
		SysUser u = userDao.getById(id);
		if (u == null) {
			throw new IllegalArgumentException("the user is not exit.");
		}

		if (!passwordEncoder.matches(oldPassword, u.getPassword())) {
			throw new IllegalArgumentException("the old password of user is error");
		}

		userDao.changePassword(u.getId(), passwordEncoder.encode(newPassword));

		log.debug("修改{}的密码", u.getUsername());

		return id;
	}

	@Override
	public SysUser getByName(String username) {
		return userDao.getByName(username);
	}

	@Override
	public String resetPassword(String userId) {

		String password = passwordEncoder.encode("05b530ad0fb56286fe051d5f8be5b8453f1cd93f");
		int i = userDao.changePassword(userId, password);

		log.debug("修改{}的密码", userId);
		if (i > 0)
			return userId;
		else
			return "";

	}

	@Override
	public SysUser getUser(String username) {
		return userDao.getUser(username);
	}


    @Override
    public PageTableData<SysUser> newList(int pagenum, int pagesize, QuerySysUser param) {
        PageHelper.startPage(pagenum, pagesize);
        List<SysUser> usersList=userDao.list(param);
        List<SysUser> re=new ArrayList<SysUser>();
        if(param.getCodePy()!=null){
            String pinYin=param.getCodePy();
            Pattern pattern=Pattern.compile(pinYin);
            for(SysUser user:usersList){
                String thisPinYin= GetPinyinUtil.getPinYinHeadChar(user.getNickname());
                Matcher matcher=pattern.matcher(thisPinYin);
                if(matcher.lookingAt()){
                    re.add(user);
                    continue;
                }
            }
        }
       else
           re = usersList;
        PageTableData<SysUser> resultPage = new PageTableData<SysUser>(re);
        PageInfo<SysUser> pageInfo = new PageInfo<SysUser>(re);
        resultPage.setPageNumber(pageInfo.getPageNum());
        resultPage.setPageSize(pageInfo.getPageSize());
        resultPage.setTotalPage(pageInfo.getPages());
        resultPage.setTotal(pageInfo.getTotal());
        return resultPage;
    }

    @Override
    public List<SysUser> listAllBrif(QuerySysUser param) {

        List<SysUser> usersList=super.listAllBrif(param);
        List<SysUser> re=new ArrayList<SysUser>();
        if(param.getCodePy()!=null){
            String pinYin=param.getCodePy();
            Pattern pattern=Pattern.compile(pinYin);
            for(SysUser user:usersList){
                String thisPinYin= GetPinyinUtil.getPinYinHeadChar(user.getNickname());
                Matcher matcher=pattern.matcher(thisPinYin);
                if(matcher.lookingAt()){
                    re.add(user);
                    continue;
                }
            }
            return re;
        }
        else
            return usersList;

    }

}