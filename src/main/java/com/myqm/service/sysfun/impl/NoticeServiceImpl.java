package com.myqm.service.sysfun.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.sysfun.NoticeDao;
import com.myqm.dao.sysfun.PrivateNoticeDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.Notice;
import com.myqm.pojo.sysfun.Notice.Status;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.BaseInfoService;
import com.myqm.service.sysfun.NoticeService;
import com.myqm.utils.UserUtil;
import com.myqm.vo.sysfun.QueryNotice;

@Service
public class NoticeServiceImpl extends BaseInfoService<Notice, QueryNotice, NoticeDao> implements NoticeService {
	private NoticeDao noticeDao;

	@Autowired
	private PrivateNoticeDao privateNoticeDao;

	@Override
	public PageTableData<Notice> listBrif(int pagenum, int pagesize, QueryNotice param) {
		PageTableData<Notice> list = super.listBrif(pagenum, pagesize, param);
		List<Notice> notices = list.getRows();
		for (int i = 0; i < notices.size(); i++) {
			Notice n = notices.get(i);
			if (noticeDao.countUserRead(n.getId(), UserUtil.getLoginUser().getId()) > 0)
				n.setReaded(true);
		}
		return list;
	}

	@Override
	public List<Notice> listAllBrif(QueryNotice param) {
		List<Notice> list = super.listAllBrif(param);

		for (int i = 0; i < list.size(); i++) {
			Notice n = list.get(i);
			if (noticeDao.countUserRead(n.getId(), UserUtil.getLoginUser().getId()) > 0)
				n.setReaded(true);
		}
		return list;
	}

	@Override
	public PageTableData<Notice> list(int pagenum, int pagesize, QueryNotice param) {
		PageTableData<Notice> list = super.listBrif(pagenum, pagesize, param);
		List<Notice> notices = list.getRows();
		for (int i = 0; i < notices.size(); i++) {
			Notice n = notices.get(i);
			if (noticeDao.countUserRead(n.getId(), UserUtil.getLoginUser().getId()) > 0)
				n.setReaded(true);
		}
		return list;
	}

	@Override
	public PageTableData<Notice> Privatelist(int pagenum, int pagesize, QueryNotice param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Notice> datalist = privateNoticeDao.list(param);
		PageTableData<Notice> resultPage = new PageTableData<Notice>(datalist);
		PageInfo<Notice> p = new PageInfo<Notice>(datalist);
		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		PageTableData<Notice> list = resultPage;
		List<Notice> notices = list.getRows();
		for (int i = 0; i < notices.size(); i++) {
			Notice n = notices.get(i);
			if (noticeDao.countUserRead(n.getId(), UserUtil.getLoginUser().getId()) > 0)
				n.setReaded(true);
		}
		return list;
	}

	@Override
	public PageTableData<Notice> PrivatelistBrif(int pagenum, int pagesize, QueryNotice param) {
		PageHelper.startPage(pagenum, pagesize);
		List<Notice> datalist = privateNoticeDao.listBrif(param);
		PageTableData<Notice> resultPage = new PageTableData<Notice>(datalist);
		PageInfo<Notice> p = new PageInfo<Notice>(datalist);

		resultPage.setPageNumber(p.getPageNum());
		resultPage.setPageSize(p.getPageSize());
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());

		PageTableData<Notice> list = resultPage;
		List<Notice> notices = list.getRows();
		for (int i = 0; i < notices.size(); i++) {
			Notice n = notices.get(i);
			if (noticeDao.countUserRead(n.getId(), UserUtil.getLoginUser().getId()) > 0)
				n.setReaded(true);
		}
		return list;
	}

	public NoticeServiceImpl(NoticeDao mapper) {
		super(mapper);
		noticeDao = mapper;
	}

	@Override
	public Notice getById(String id) {
		Notice notice = super.getById(id);
		SysUser user = UserUtil.getLoginUser();
		noticeDao.saveReadRecord(id, user.getId());
		return notice;
	}

	@Override
	@Transactional
	public String edit(Notice notice) {
		Integer status = noticeDao.getStatus(notice.getId());
		if (status == Status.DRAFT) {
			SysUser user = UserUtil.getLoginUser();
			notice.setUpdaterId(user.getId());
			noticeDao.editById(notice);
			if (notice.getIsPrivated().equals("1")) {
				privateNoticeDao.delByNoticeId(notice.getId());
				for (int i = 0; i < notice.getCanReadUserIds().size(); i++) {
					privateNoticeDao.save(notice.getId(), notice.getCanReadUserIds().get(i));
				}
			}
			return notice.getId();
		} else
			return "";
	}

	@Override
	public String delById(String id) {
		noticeDao.delReadRecord(id);
		Notice notice = noticeDao.getById(id);
		if (notice.getIsPrivated().equals("1")) {
			privateNoticeDao.delByNoticeId(id);
		}

		if (noticeDao.delById(id) > 0)
			return id;
		else
			return "";
	}

	@Override
	public int saveReadRecord(String noticeId, String userId) {
		return noticeDao.saveReadRecord(noticeId, userId);
	}

	@Override
	public List<SysUser> listReadUsers(String noticeId) {
		return noticeDao.listReadUsers(noticeId);
	}

	@Override
	public int countUnread(String userId) {
		return noticeDao.countUnread(userId);
	}

	@Override
	public Notice getBrifById(String id) {

		return noticeDao.getBrifById(id);
	}

}
