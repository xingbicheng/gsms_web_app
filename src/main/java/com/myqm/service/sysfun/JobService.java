package com.myqm.service.sysfun;

import org.quartz.JobDataMap;
import com.myqm.dao.sysfun.JobDao;
import com.myqm.pojo.sysfun.JobModel;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryJobModel;

public interface JobService extends IBaseRecordService<JobModel, QueryJobModel, JobDao> {
	void doJob(JobDataMap jobDataMap);

	void stopById(String id);
}
