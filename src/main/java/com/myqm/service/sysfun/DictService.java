package com.myqm.service.sysfun;

import com.myqm.dao.sysfun.DictDao;
import com.myqm.pojo.sysfun.Dict;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryDict;

public interface DictService extends IBaseRecordService<Dict, QueryDict, DictDao> {

}
