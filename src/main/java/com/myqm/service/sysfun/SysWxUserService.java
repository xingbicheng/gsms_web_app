package com.myqm.service.sysfun;

import com.myqm.pojo.sysfun.SysWxUser;
import com.myqm.vo.sysfun.SysWxUserQuery;
import java.util.List;
import com.myqm.pojo.PageTableData;


public interface SysWxUserService {
   /**
    *根据id删除SysWxUser
    */
	String delById(String id);

   /**
    *根据新增SysWxUser，id自增
    */
    String save(SysWxUser record);

   /**
    *根据id查询SysWxUser
    */
    SysWxUser getById(String id);
    
   /**
    *根据id更新SysWxUser
    */ 
    String editById(SysWxUser record);

   /**
    *分页查询SysWxUser
    */ 
    PageTableData<SysWxUser> list(int pagenum, int pagesize,SysWxUserQuery param);

	/**
    *查询所有SysWxUser
    */ 
    List<SysWxUser> listAll(SysWxUserQuery param);
	/**
     *审核微信公众号
     */ 
	String  auited(String openId,String userId);

	String unaudited( String openId);

	String getOpenId(String openId);

	SysWxUser getByOpenId(String openId);
}