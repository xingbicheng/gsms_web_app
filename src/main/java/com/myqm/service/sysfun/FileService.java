package com.myqm.service.sysfun;

import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import com.myqm.dao.sysfun.FileInfoDao;
import com.myqm.pojo.sysfun.FileInfo;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryFileInfo;

public interface FileService extends IBaseRecordService<FileInfo, QueryFileInfo, FileInfoDao> {

	FileInfo save(MultipartFile file) throws IOException;

	FileInfo getByMd5(String md5);
}