package com.myqm.service.sysfun;

import java.util.List;

import com.myqm.pojo.PageTableData;
import org.apache.ibatis.annotations.Param;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QuerySysUser;

public interface SysUserService extends IBaseRecordService<SysUser, QuerySysUser, SysUserDao> {
	String setState(int s, String userid);

	String changePassword(String userid, String oldPassword, String password);

	SysUser getByName(String username);
    SysUser getById(String id);

	/**
	 * 根据userId和rileIds在sys_role_user增加信息
	 * 
	 * @param userId
	 * @param roleIds
	 * @return
	 */
	void setUserRoles(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);

	String resetPassword(String userId);

	/**
	 * 根据用户姓名查找用户信息
	 * 
	 * @param username
	 * @return
	 */
	SysUser getUser(String username);

    PageTableData<SysUser> newList(int pagenum, int pagesize, QuerySysUser param);

}
