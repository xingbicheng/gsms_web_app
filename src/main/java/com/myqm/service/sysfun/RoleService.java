package com.myqm.service.sysfun;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.myqm.dao.sysfun.RoleDao;
import com.myqm.pojo.sysfun.Role;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryRole;

public interface RoleService extends IBaseRecordService<Role, QueryRole, RoleDao> {

	/**
	 * 根据userId查询列表
	 * 
	 * @param userId
	 *            用户id
	 * @return 查询出来的列表
	 */
	List<Role> listByUserId(String userId);

	/**
	 * 根据roleId查询列表
	 * 
	 * @param roleId
	 *            角色id
	 * @return 查询出来的列表
	 */
	int deleteRolePermission(String roleId);

	/**
	 * 删除用户
	 * 
	 * @param roleId
	 *            角色id
	 */
	int deleteRoleUser(String roleId);

	/**
	 * 保存
	 */
	int saveRolePermission(@Param("roleId") String roleId, @Param("permissionIds") List<String> permissionIds);

    /**
     * 批量修改激活状态
     * @param record
     * @return
     */
    int editActivation(List<Role> record);
}
