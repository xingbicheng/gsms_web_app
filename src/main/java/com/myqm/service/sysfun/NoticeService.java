package com.myqm.service.sysfun;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.myqm.dao.sysfun.NoticeDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.sysfun.Notice;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryNotice;

public interface NoticeService extends IBaseRecordService<Notice, QueryNotice, NoticeDao> {

	public Notice getBrifById(String id);

	/**
	 * 保存已读公告
	 * 
	 * @param noticeId
	 *            公告id
	 * @param userId
	 *            用户id
	 * @return 公告数量
	 */
	public int saveReadRecord(String noticeId, String userId);

	/**
	 * 已读用户
	 * 
	 * @param noticeId
	 *            公告id
	 * @return 用户数量
	 */
	public List<SysUser> listReadUsers(String noticeId);

	/**
	 * 未读数量
	 * 
	 * @param userId
	 *            用户id
	 * @return 未读数量
	 */
	public int countUnread(String userId);

	/**
	 * 分页查询较详细记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	PageTableData<Notice> Privatelist(int pagenum, int pagesize, QueryNotice param);

	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	PageTableData<Notice> PrivatelistBrif(int pagenum, int pagesize, QueryNotice param);

}