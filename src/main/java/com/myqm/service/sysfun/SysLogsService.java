package com.myqm.service.sysfun;

import java.util.List;

import com.myqm.dao.sysfun.SysLogsDao;
import com.myqm.pojo.sysfun.SysLogs;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QuerySysLogs;

public interface SysLogsService extends IBaseRecordService<SysLogs, QuerySysLogs, SysLogsDao> {
	int save(String module, String moduleId,Boolean flag, String remark);

	int delMonthLogs(int year, int start, int end);

	List<SysLogs> getLogsByModuleId(String record);
	int save(String module, String moduleId,Boolean flag, String remark,String userId);

}
