package com.myqm.service.sysfun;

import java.util.List;
import java.util.Set;
import com.myqm.dao.sysfun.PermissionDao;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.service.base.IBaseRecordService;
import com.myqm.vo.sysfun.QueryPermission;

public interface PermissionService extends IBaseRecordService<Permission, QueryPermission, PermissionDao> {

    List<Permission> childMenu(String id);


	/**
	 * 根据ParentId删除记录
	 * 
	 * @param id
	 *            主键id
	 */
	int deleteByParentId(String id);

	/**
	 * 根据权限id删除记录
	 * 
	 * @param permissionId
	 *            权限id
	 */
	int deleteRolePermission(String permissionId);

	/**
	 * 根据权限permissionId查询列表
	 * 
	 * @param permissionId
	 *            权限id
	 * @return 查询出的列表
	 */
	Set<String> listUserIds(String permissionId);

	/**
	 * 查询parents列表
	 * 
	 * @return 查询出的parents列表
	 */
	List<Permission> listParents();

	/**
	 * 根据userId查询列表
	 * 
	 * @param userId
	 *            用户id
	 * @return 查询出来的列表
	 */
	List<Permission> listByUserId(String userId);

	/**
	 * 根据角色id查询公告
	 * 
	 * @param roleId
	 *            角色的id
	 * @return 查询出的公告
	 */
	List<Permission> listByRoleId(String roleId);

	/**
	 * 根据sort查询权限
	 * 
	 * @return 查询出的权限列表
	 */
	List<Permission> listAll(QueryPermission param);

	SysUser getCurrent();
}
