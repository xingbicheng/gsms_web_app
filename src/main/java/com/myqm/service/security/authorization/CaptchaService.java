package com.myqm.service.security.authorization;

public interface CaptchaService {
	String saveCaptcha(String captcha);

	boolean checkCaptcha(String id, String captcha);

}
