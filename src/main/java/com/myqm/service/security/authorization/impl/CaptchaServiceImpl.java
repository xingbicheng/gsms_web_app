package com.myqm.service.security.authorization.impl;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.myqm.config.data.sysfun.CaptchaCfg;
import com.myqm.service.security.authorization.CaptchaService;

@Service
public class CaptchaServiceImpl implements CaptchaService {
	@Autowired
	private CaptchaCfg captchaCfg;
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public String saveCaptcha(String captcha) {
		String uuid = UUID.randomUUID().toString();
		// 将验证码以<key,value>形式缓存到redis
		redisTemplate.opsForValue().set(uuid, captcha, captchaCfg.getCaptchaExpries(), TimeUnit.SECONDS);
		return uuid;
	}

	@Override
	public boolean checkCaptcha(String id, String captcha) {
		// redis中查询验证码
		String captchaValue = redisTemplate.opsForValue().get(id);
		// 验证码不存在
		if (captchaValue == null) {
			return false;
		}
		// 匹配验证码
		if (captchaValue.compareToIgnoreCase(captcha) != 0) {
			return false;
		}
		// 验证码匹配成功，redis则删除对应的验证码
		redisTemplate.delete(id);
		return true;

	}

}
