package com.myqm.service.security.authorization.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.myqm.dao.security.authorization.TokenDao;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.security.authorization.Token;
import com.myqm.pojo.sysfun.TokenModel;
import com.myqm.service.security.authorization.TokenService;
import com.myqm.service.sysfun.SysLogsService;

/**
 * token存到数据库的实现类
 *
 * 
 */
@Primary
@Service
public class TokenServiceDbImpl implements TokenService {

	/**
	 * token过期秒数
	 */
	@Value("${token.expire.seconds}")
	private Long expireSeconds;
	@Autowired
	private TokenDao tokenDao;
	@Autowired
	private SysLogsService logService;

	// @Autowired
	// private JwtInfo jwtInfo;

	@Override
	public Token saveToken(LoginUser loginUser) {
		String token = UUID.randomUUID().toString();

		// 拼装accessToken
		// String token = JwtHelper.createJWT(loginUser.getUsername(),
		// jwtInfo.getClientId(),
		// jwtInfo.getAudience(),
		// jwtInfo.getExpiresSecond() * 1000,
		// jwtInfo.getBase64Secret());

		loginUser.setToken(token);
		loginUser.setLoginTime(System.currentTimeMillis());
		loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);

		TokenModel model = new TokenModel();
		model.setToken(token);
		model.setCreateTime(new Timestamp(System.currentTimeMillis()));
		model.setUpdateTime(model.getCreateTime());
		model.setExpireTime(new Date(loginUser.getExpireTime()));
		model.setVal(JSONObject.toJSONString(loginUser));

		tokenDao.save(model);
		// 登陆日志
		logService.save(loginUser.getId(), "登陆", true, null);

		return new Token(token, loginUser.getLoginTime());
	}

	@Override
	public void refresh(LoginUser loginUser) {
		loginUser.setLoginTime(System.currentTimeMillis());
		loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);

		TokenModel model = tokenDao.getByToken(loginUser.getToken());
		model.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		model.setExpireTime(new Date(loginUser.getExpireTime()));
		model.setVal(JSONObject.toJSONString(loginUser));
		tokenDao.update(model);
	}

	@Override
	public LoginUser getLoginUser(String token) {
		TokenModel model = tokenDao.getByToken(token);
		return toLoginUser(model);
	}

	@Override
	public boolean deleteToken(String token) {
		TokenModel model = tokenDao.getByToken(token);
		LoginUser loginUser = toLoginUser(model);
		if (loginUser != null) {
			logService.save(loginUser.getId(), "logout", true, null, loginUser.getId());
			tokenDao.delete(token);
			return true;
		}
		return false;
	}

	private LoginUser toLoginUser(TokenModel model) {
		if (model == null) {
			return null;
		}
		return JSONObject.parseObject(model.getVal(), LoginUser.class);
	}

}
