package com.myqm.service.security.authorization.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.myqm.dao.sysfun.PermissionDao;
import com.myqm.dao.sysfun.SysWxUserDao;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.sysfun.Permission;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.pojo.sysfun.SysUser.Status;
import com.myqm.pojo.sysfun.SysUser.Type;
import com.myqm.pojo.sysfun.SysWxUser;
import com.myqm.service.sysfun.SysUserService;


/**
 * spring security登陆处理
 * 
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private SysUserService userService;
	@Autowired
	private PermissionDao permissionDao;
	@Autowired
	private SysWxUserDao sysWxUserDao;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SysUser sysUser = userService.getUser(username);
		SysWxUser sysWxUser = sysWxUserDao.getByOpenId(username);
		if (sysUser == null&&sysWxUser==null) {
			throw new AuthenticationCredentialsNotFoundException("the user is not exit.");
		}
		if(sysUser!=null) {
			if (sysUser.getStatus() == Status.LOCKED) {
				throw new LockedException("the ");
			} else if (sysUser.getStatus() == Status.DISABLED) {
				throw new DisabledException("用户已作废");
			}
			LoginUser loginUser = new LoginUser();
			BeanUtils.copyProperties(sysUser, loginUser);

			List<Permission> permissions = permissionDao.listByUserId(sysUser.getId());
			loginUser.setPermissions(permissions);
			return loginUser;
		}
		else {
			if(sysWxUser.getStatus().equals(Status.LOCKED+"")||sysWxUser.getStatus().equals(Status.DISABLED+"")) {
				throw new DisabledException("用户没有通过审核");
			}
			LoginUser loginUser = new LoginUser();
			BeanUtils.copyProperties(sysWxUser, loginUser);
			loginUser.setId(sysWxUser.getId().toString());
			loginUser.setUsername(username);
			loginUser.setUserId(sysWxUser.getUserId());
			loginUser.setType(sysWxUser.getType());
			loginUser.setPassword("$2a$10$iYM/H7TrSaLs7XyIWQdGwe1xf4cdmt3nwMja6RT0wxG5YY1RjN0EK");
			loginUser.setStatus(Integer.parseInt(sysWxUser.getStatus()));
			String type = loginUser.getType();
			List<Permission> permissions = new ArrayList<>();
			//审核员
			if(type.equals(Type.Auditing)) {
				//添加权限的设置
				Permission byId = permissionDao.getById("1");
				permissions.add(byId);
				loginUser.setPermissions(permissions);
			}
			//提供者
			else if(type.equals(Type.Provider)) {
				Permission byId = permissionDao.getById("2");
				permissions.add(byId);
				loginUser.setPermissions(permissions);
			}
			//消费者
			else {
				Permission byId = permissionDao.getById("3");
				permissions.add(byId);
				loginUser.setPermissions(permissions);
			}
			return loginUser;
		}
	}

}
