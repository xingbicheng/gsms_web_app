package com.myqm.service.security.authorization.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.myqm.config.data.security.authorization.JwtInfo;
import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.security.authorization.Token;
import com.myqm.service.security.authorization.TokenService;
import com.myqm.service.sysfun.SysLogsService;
import com.myqm.utils.JwtHelper;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import io.jsonwebtoken.Claims;

/**
 * token存到catch的实现类
 */
// @Primary
@Service
public class TokenServiceCatchImpl implements TokenService {

	// Create a cache manager
	final CacheManager cacheManager = CacheManager.create();

	// create the cache called
	final Cache cache = cacheManager.getCache("token");
	final static String KEY_TOKEN = "TOKEN";

	/**
	 * token过期秒数
	 */
	@Value("${token.expire.seconds}")
	private int expireSeconds;

	@Autowired
	private SysLogsService logService;

	@Autowired
	private JwtInfo jwtInfo;

	@Override
	public Token saveToken(LoginUser loginUser) {
		// 拼装accessToken
		String accessToken = JwtHelper.createJWT(loginUser.getUsername(), null, jwtInfo.getAudience(),
				jwtInfo.getIssuer(), jwtInfo.getExpiresSecond() * 1000, jwtInfo.getBase64Secret());
		loginUser.setToken(accessToken);
		cacheLoginUser(loginUser);
		// 登陆日志
		logService.save(loginUser.getId(), "登陆", true, null);
		return new Token(accessToken, loginUser.getLoginTime());
	}

	private void cacheLoginUser(LoginUser loginUser) {
		loginUser.setLoginTime(System.currentTimeMillis());
		loginUser.setExpireTime(loginUser.getLoginTime() + expireSeconds * 1000);
		cache.acquireWriteLockOnKey(KEY_TOKEN);
		Element element = new Element(KEY_TOKEN + loginUser.getUsername(), loginUser);
		element.setTimeToIdle(expireSeconds);
		element.setTimeToLive(expireSeconds);
		cache.put(element);
		cache.releaseWriteLockOnKey(KEY_TOKEN);
	}

	/**
	 * 更新缓存的用户信息
	 */
	@Override
	public void refresh(LoginUser loginUser) {
		cacheLoginUser(loginUser);
	}

	@Override
	public LoginUser getLoginUser(String token) {

		if (!JwtHelper.validate(token, jwtInfo))
			return null;
		Claims cliams = JwtHelper.parseJWT(token, jwtInfo.getBase64Secret());
		if (cliams == null)
			return null;
		if (cliams.getId() == null)
			return null;

		cache.acquireWriteLockOnKey(KEY_TOKEN);
		Element element = cache.get(KEY_TOKEN + cliams.getId());
		LoginUser value = null;
		if (element != null) {
			value = (LoginUser) element.getObjectValue();
		}
		cache.releaseWriteLockOnKey(KEY_TOKEN);
		return value;
	}

	@Override
	public boolean deleteToken(String token) {

		if (!JwtHelper.validate(token, jwtInfo))
			return false;
		Claims cliams = JwtHelper.parseJWT(token, jwtInfo.getBase64Secret());
		if (cliams == null)
			return false;
		if (cliams.getId() == null)
			return false;

		cache.acquireWriteLockOnKey(KEY_TOKEN);
		Element element = cache.get(KEY_TOKEN + cliams.getId());
		LoginUser value = null;
		if (element != null) {
			value = (LoginUser) element.getObjectValue();
			cache.remove(cliams.getId());
			cache.releaseWriteLockOnKey(KEY_TOKEN);
			logService.save(value.getId(), "退出", true, null);
			return true;
		} else
			return false;
	}
}
