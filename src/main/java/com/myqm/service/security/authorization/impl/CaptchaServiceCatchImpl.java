package com.myqm.service.security.authorization.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.myqm.config.data.sysfun.CaptchaCfg;
import com.myqm.service.security.authorization.CaptchaService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Primary
@Service
public class CaptchaServiceCatchImpl implements CaptchaService {

	// Create a cache manager
	final CacheManager cacheManager = CacheManager.create();

	// create the cache called
	final Cache cache = cacheManager.getCache("captcha");
	final static String KEY_CAPTCHA = "CAPTCHA";
	final static String KEY_VALUE = "captchaValue";
	@Autowired
	private CaptchaCfg captchaCfg;

	@Override
	public String saveCaptcha(String captcha) {
		// captcha = "123456";
		String uuid = UUID.randomUUID().toString();
		cache.acquireWriteLockOnKey(KEY_CAPTCHA);
		Element element = new Element(KEY_CAPTCHA + uuid, captcha);
		element.setTimeToIdle(captchaCfg.getCaptchaExpries());
		element.setTimeToLive(captchaCfg.getCaptchaExpries());
		cache.put(element);
		cache.releaseWriteLockOnKey(KEY_CAPTCHA);
		return uuid;
	}

	@Override
	public boolean checkCaptcha(String id, String captcha) {
		cache.acquireWriteLockOnKey(KEY_CAPTCHA);
		Element element = cache.get(KEY_CAPTCHA + id);
		String value = "";
		if (element != null) {
			value = (String) element.getObjectValue();
		}
		cache.releaseWriteLockOnKey(KEY_CAPTCHA);

		boolean result;
		result = captcha.equals(value);
		return result;
	}

}
