package com.myqm.service.security.authorization;

import com.myqm.pojo.security.authorization.LoginUser;
import com.myqm.pojo.security.authorization.Token;

/**
 * Token管理器<br>
 * 可存储到redis或者数据库<br>
 * 具体可看实现类<br>
 * 默认基于redis，实现类为 com.myqm.utils.service.impl.TokenServiceImpl<br>
 * 如要换成数据库存储，将TokenServiceImpl类上的注解@Primary挪到com.myqm.sys.service.impl.TokenServiceDbImpl
 * 
 */
public interface TokenService {

	Token saveToken(LoginUser loginUser);

	void refresh(LoginUser loginUser);

	LoginUser getLoginUser(String token);

	boolean deleteToken(String token);

}
