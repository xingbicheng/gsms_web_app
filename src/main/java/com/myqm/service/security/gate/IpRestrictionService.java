package com.myqm.service.security.gate;

import com.myqm.advice.RequstCheckException;
import com.myqm.pojo.security.gate.IpRestriction;

public interface IpRestrictionService {

	/**
	 * 根据IP来查找记录
	 * 
	 * @param ip
	 *            ip地址
	 * @return 名单
	 */
	public IpRestriction findByIp(String ip);

	public void addIpRestrictionInfo(IpRestriction ip);

	// 检查访问频率
	public void checkIpRestriction(IpRestriction ip) throws RequstCheckException;
}
