package com.myqm.service.security.gate;

public interface GatewayCacheService {

	public int addIpRequest(String ip, int expires);

	public int addApiRequest(String apiKey, int expires);

	public int getIpRequest(String ip);

	public int getApiRequest(String apiKey);
}
