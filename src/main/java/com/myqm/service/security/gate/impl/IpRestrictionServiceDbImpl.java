package com.myqm.service.security.gate.impl;

import java.sql.Timestamp;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.myqm.advice.RequstCheckException;
import com.myqm.config.data.security.ipfreqcheck.IpRestrictionCfg;
import com.myqm.dao.security.gate.IpRestrictionDao;
import com.myqm.pojo.security.gate.IpRestriction;
import com.myqm.service.security.gate.IpRestrictionService;

@Service
public class IpRestrictionServiceDbImpl implements IpRestrictionService {

	@Autowired
	private IpRestrictionCfg restrictionCfg;

	@Autowired
	private IpRestrictionDao ipDao;

	/**
	 * 根据IP来查找记录
	 * 
	 * @param ip
	 *            ip地址
	 * @return 名单
	 */
	@Override
	public IpRestriction findByIp(String ip) {
		return ipDao.findByIp(ip);
	}

	/**
	 * 添加记录
	 * 
	 * @param ip
	 *            ip地址
	 */
	@Override
	public void addIpRestrictionInfo(IpRestriction ip) {
		ipDao.save(ip.getIp());
	}

	public void delBlackWhiteIp(String ip) {
		ipDao.delIp(ip);
	}

	public enum EnumLockType {
		LEGAL_IP, BLACK_IP, LIMIT_IP, LOCK_IP;
	}

	// 检查访问频率
	@Override
	public void checkIpRestriction(IpRestriction ip) throws RequstCheckException {
		EnumLockType resultType;
		Long currenttimes = System.currentTimeMillis();
		Date lastdate = ip.getLastAccessTime();
		if (ip.isBlack()) {
			resultType = EnumLockType.BLACK_IP;
		} else if (ip.isLocked()) {// ip地址被锁定
			// 禁止被访问，检查锁定时间，如果锁定时间已到，解除锁定
			if (ip.getLastCounterTime().getTime() - currenttimes > restrictionCfg.getLimitLockTime()) {
				ip.resetLockCount();
				ip.setLocked(false);
				resultType = EnumLockType.LEGAL_IP;
			} else {
				resultType = EnumLockType.LOCK_IP;
			}
		} else if (!ip.isCounter()) {
			// 分两种情况，一种未开始计数，一种开始计数
			// 如果没有开始计数，根据访问控制，开始计数，或者放行
			if (lastdate.getTime() - currenttimes <= restrictionCfg.getLimteTime()) {
				ip.setCounter(true);
				ip.addLockCount();
				ip.setLastCounterTime(new Timestamp(System.currentTimeMillis()));
				resultType = EnumLockType.LIMIT_IP;
			} else {
				resultType = EnumLockType.LEGAL_IP;
			}
		} // 如果已经开始计数，根据访问时间开始计数，如果计数到达标准， 锁定IP
		else {
			// 访问时间过于频繁
			if (lastdate.getTime() - currenttimes >= restrictionCfg.getLimteTime()) {
				ip.addLockCount();
				if (ip.getLockCount() > restrictionCfg.getLockCount()) {
					ip.setLastCounterTime(new Timestamp(System.currentTimeMillis()));
					ip.setLocked(true);
					resultType = EnumLockType.LOCK_IP;
				} else {
					resultType = EnumLockType.LIMIT_IP;
				}

			} else // 开始计数下的 正常访问， 如果时间到解除锁定
			{
				if (ip.getLastCounterTime().getTime() - currenttimes >= restrictionCfg.getLimitLockTime()) {
					ip.resetLockCount();
					ip.setCounter(false);
				}
				resultType = EnumLockType.LEGAL_IP;
			}
		}

		// 更新设置
		ip.setLastAccessTime(new Timestamp(System.currentTimeMillis()));
		ipDao.update(ip);
		switch (resultType) {
		case BLACK_IP:
			throw new RequstCheckException(400,
					"IP address has been listed as a blacklist, is forbidden to access, please continuous system administrator!");
		case LIMIT_IP:
			throw new RequstCheckException(400, "IP access is too frequent, please try again later.");
		case LOCK_IP:
			throw new RequstCheckException(400,
					"IP access is too frequent and the IP address has been locked. Please try again later");
		default:
			break;
		}
	}
}