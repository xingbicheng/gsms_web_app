package com.myqm.service.security.gate.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.myqm.service.security.gate.GatewayCacheService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

//@Primary
@Service
public class GatewayEhcacheServiceImpl implements GatewayCacheService {
	// Create a cache manager
	final CacheManager cacheManager = CacheManager.create();

	// create the cache called
	final Cache cache = cacheManager.getCache("gateway");

	final static String KEY_IP = "IP:";
	final static String KEY_API = "API:";

	@Override
	public int addIpRequest(String ip, int expires) {
		cache.acquireWriteLockOnKey(KEY_IP);

		Element element = cache.get(KEY_IP + ip);
		Integer count = 1;
		if (element != null) {
			count = (Integer) element.getObjectValue();
			count++;
		}

		element = new Element(KEY_IP + ip, count);
		element.setTimeToIdle(expires);
		element.setTimeToLive(expires);
		cache.put(element);
		cache.releaseWriteLockOnKey(KEY_IP);

		return count;
	}

	@Override
	public int addApiRequest(String apiKey, int expires) {
		cache.acquireWriteLockOnKey(KEY_API);
		Element element = cache.get(KEY_API + apiKey);
		Integer count = 1;
		if (element != null) {
			count = (Integer) element.getObjectValue();
			count++;
		}

		element = new Element(KEY_API + apiKey, count);
		element.setTimeToIdle(expires);
		element.setTimeToLive(expires);
		cache.put(element);
		cache.releaseWriteLockOnKey(KEY_API);

		return count;
	}

	@Override
	public int getIpRequest(String ip) {
		cache.acquireReadLockOnKey(KEY_IP);
		Element element = cache.get(KEY_IP + ip);
		Integer count = 0;
		if (element != null) {
			count = (Integer) element.getObjectValue();
			count++;
		}
		cache.releaseReadLockOnKey(KEY_IP);

		return count;
	}

	@Override
	public int getApiRequest(String apiKey) {
		cache.acquireReadLockOnKey(KEY_API);
		Element element = cache.get(KEY_API + apiKey);
		Integer count = 0;
		if (element != null) {
			count = (Integer) element.getObjectValue();
			count++;
		}
		cache.releaseReadLockOnKey(KEY_API);

		return count;
	}

}
