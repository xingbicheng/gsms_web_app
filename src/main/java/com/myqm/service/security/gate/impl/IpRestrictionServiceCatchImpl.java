package com.myqm.service.security.gate.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.myqm.advice.RequstCheckException;
import com.myqm.config.data.security.ipfreqcheck.IpRestrictionCfg;
import com.myqm.pojo.security.gate.IpRestriction;
import com.myqm.service.security.gate.IpRestrictionService;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

@Primary
@Service
public class IpRestrictionServiceCatchImpl implements IpRestrictionService {

	// Create a cache manager
	final CacheManager cacheManager = CacheManager.create();
	// create the cache called
	final Cache cache = cacheManager.getCache("IpRestriction");
	final static String KEY_IP = "IP";
	@Autowired
	private IpRestrictionCfg restrictionCfg;

	/**
	 * 根据IP来查找记录
	 * 
	 * @param ip
	 *            ip地址
	 * @return 名单
	 */
	@Override
	public IpRestriction findByIp(String ip) {
		cache.acquireWriteLockOnKey(KEY_IP);
		Element element = cache.get(KEY_IP + ip);
		IpRestriction value = null;
		if (element != null) {
			value = (IpRestriction) element.getObjectValue();
		}
		cache.releaseWriteLockOnKey(KEY_IP);
		return value;
	}

	/**
	 * 添加记录
	 * 
	 * @param ip
	 *            ip地址
	 */
	@Override
	public void addIpRestrictionInfo(IpRestriction ip) {
		cache.acquireWriteLockOnKey(KEY_IP);
		Element element = new Element(KEY_IP + ip.getIp(), ip);
		element.setTimeToIdle(restrictionCfg.getLimteTime());
		element.setTimeToLive(restrictionCfg.getLimteTime());
		cache.put(element);
		cache.releaseWriteLockOnKey(KEY_IP);
	}

	public void delIpInfo(String ip) {
		cache.acquireWriteLockOnKey(KEY_IP);
		cache.remove(KEY_IP + ip);
		cache.releaseWriteLockOnKey(KEY_IP);
	}

	public enum EnumLockType {
		LEGAL_IP, BLACK_IP, LIMIT_IP, LOCK_IP; // 非法ip 黑ip 限制ip 锁定ip
	}

	// 检查访问频率
	@Override
	public void checkIpRestriction(IpRestriction ip) throws RequstCheckException {
		if (ip.getLockCount() > restrictionCfg.getMaxCount())
			throw new RequstCheckException(400, "IP access is too frequent, please try again later.");
		else {

			cache.acquireWriteLockOnKey(KEY_IP);
			ip.setLockCount(ip.getLockCount() + 1);
			Element element = new Element(KEY_IP + ip.getIp(), ip);
			cache.put(element);
			cache.releaseWriteLockOnKey(KEY_IP);

		}
	}
}