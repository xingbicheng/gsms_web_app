package com.myqm.service.base;

import java.util.List;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.vo.base.BaseQueryParam;

public interface IBaseRecordService<E extends BaseRecord, QP extends BaseQueryParam, DAO extends IBaseRecordDao<E, QP>> {
	/**
	 * 删除记录
	 * 
	 * @param id
	 *            主键id
	 * @return 删除记录数量
	 */
	String delById(String id);

	/**
	 * 插入记录
	 * 
	 * @param record
	 *            基于基本信息的记录
	 * @return 插入记录数量 如果为0 插入失败
	 */
	String save(E record);

	/**
	 * 根据主键查询记录
	 * 
	 * @param id
	 *            主键id
	 * @return 查询出的记录
	 */
	E getById(String id);

	/**
	 * 根据ID更新更新
	 * 
	 * @param record
	 *            记录
	 * @return 更新记录数量 如果为0 修改失败
	 */
	String edit(E record);

	/**
	 * 批量删除
	 * 
	 * @param l
	 *            记录列表ids，id数组
	 * @return 删除记录数量
	 */
	List<String> delAll(List<String> l);

	/**
	 * 分页查询较详细记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	PageTableData<E> list(int pagenum, int pagesize, QP param);



	/**
	 * 分页查询简要记录
	 * 
	 * @param page
	 *            查询条件及分页信息
	 * @return 记录
	 */
	PageTableData<E> listBrif(int pagenum, int pagesize, QP param);

	/**
	 * 查询所有简要记录
	 * 
	 * @param param
	 *            查询条件
	 * @return 记录
	 */
	List<E> listAllBrif(QP param);

}
