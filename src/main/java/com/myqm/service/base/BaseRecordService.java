package com.myqm.service.base;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.myqm.dao.base.IBaseRecordDao;
import com.myqm.pojo.PageTableData;
import com.myqm.pojo.base.BaseRecord;
import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class BaseRecordService<E extends BaseRecord, QP extends BaseQueryParam, DAO extends IBaseRecordDao<E, QP>> {
	protected static final Logger log = LoggerFactory.getLogger("adminLogger");

	protected DAO baseDaoMapper;

	@Autowired(required = true)
	public BaseRecordService(DAO mapper) {
		baseDaoMapper = mapper;
	}

	public String delById(String id) {
		if (baseDaoMapper.delById(id) > 0)
			return id;
		else
			return null;
	}

	public String save(E record) {
		if (baseDaoMapper.save(record) > 0)
			return record.getId();
		else
			return null;
	}

	public E getById(String id) {
		return (E) baseDaoMapper.getById(id);
	}

	public String edit(E record) {
		if (baseDaoMapper.editById(record) > 0)
			return record.getId();
		else
			return null;
	}

	@Transactional
	public List<String> delAll(List<String> list) {
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			if (baseDaoMapper.delById(list.get(i)) > 0)
				result.add(list.get(i));
		}
		return result;
	}

	public PageTableData<E> listBrif(int pagenum, int pagesize, QP param) {
		PageHelper.startPage(pagenum, pagesize);
		List<E> datalist = baseDaoMapper.listBrif(param);
		PageTableData<E> resultPage = new PageTableData<E>(datalist);
		PageInfo<E> p = new PageInfo<E>(datalist);
		resultPage.setPageNumber(pagenum);
		resultPage.setPageSize(pagesize);
		resultPage.setTotalPage(p.getPages());
		resultPage.setTotal(p.getTotal());
		return resultPage;
	}

	public List<E> listAllBrif(QP param) {
		return baseDaoMapper.listBrif(param);
	}

	public PageTableData<E> list(int pagenum, int pagesize, QP param) {
		PageHelper.startPage(pagenum, pagesize);
		List<E> datalist = baseDaoMapper.list(param);
		PageTableData<E> resultPage = new PageTableData<E>(datalist);
		PageInfo<E> p = new PageInfo<E>(datalist);
		BeanUtils.copyProperties(p, resultPage);
		return resultPage;
	}

}
