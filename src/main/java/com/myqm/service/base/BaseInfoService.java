package com.myqm.service.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.myqm.dao.base.IBaseInfoDao;
import com.myqm.dao.sysfun.SysUserDao;
import com.myqm.pojo.base.BaseInfo;
import com.myqm.pojo.sysfun.SysUser;
import com.myqm.utils.UserUtil;
import com.myqm.vo.base.BaseQueryParam;

@Component
public abstract class BaseInfoService<E extends BaseInfo, QP extends BaseQueryParam, DAO extends IBaseInfoDao<E, QP>>
		extends BaseRecordService<E, QP, DAO> {

	@Autowired
	protected SysUserDao userDao;
	DAO infoDao;

	@Autowired(required = true)
	public BaseInfoService(DAO mapper) {
		super(mapper);
		infoDao = mapper;
	}

	@Override
	public E getById(String id) {
		E findob = (E) baseDaoMapper.getById(id);
		if (findob != null) {
			String createrName = userDao.getUserNameById(findob.getCreaterId());
			String updaterName = userDao.getUserNameById(findob.getUpdaterId());

			findob.setCreaterName(createrName);
			findob.setUpdaterName(updaterName);
		}
		return findob;
	}

	@Override
	public String save(E record) {
		SysUser user = UserUtil.getLoginUser();
		record.setCreaterId(user.getId());
		return super.save(record);
	}

	public String edit(E record) {
		SysUser user = UserUtil.getLoginUser();
		record.setUpdaterId(user.getId());
		return super.edit(record);
	}

}
