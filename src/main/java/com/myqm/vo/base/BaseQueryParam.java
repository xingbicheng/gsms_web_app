package com.myqm.vo.base;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import io.swagger.annotations.ApiModelProperty;

public class BaseQueryParam {
	/**
	 * 当前页码
	 */
	@ApiModelProperty(value = "第几页")
	@Min(1)
	protected Integer currentPage = 1;
	/**
	 * 分页大小
	 */
	@ApiModelProperty(value = "分页大小")
	@Min(1)
	@Max(40)
	protected Integer pageSize = 20;
	/**
	 * 排序字段
	 */
	@ApiModelProperty(value = "排序字段")
	private String sort;
	/**
	 * 排序方式 asc或者desc
	 */
	@ApiModelProperty(value = "排序方式 asc或者desc")
	private String order;

	@ApiModelProperty(value = "编码和拼音查询")
	private String codePy;

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer page) {
		this.currentPage = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		String underlineToHump = this.UnderlineToHump(sort);
		this.sort = underlineToHump;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		String underlineToHump = this.UnderlineToHump(order);
		this.order = underlineToHump;
	}

	public String getCodePy() {
		return codePy;
	}

	public void setCodePy(String codePy) {
		this.codePy = codePy;
	}

    public  String UnderlineToHump(String para){
    	StringBuilder sb=new StringBuilder(para);
        int temp=0;//定位
        for(int i=0;i<para.length();i++){
            if(Character.isUpperCase(para.charAt(i))){
                sb.insert(i+temp, "_");
                temp+=1;
            }
        }
        return sb.toString().toLowerCase();
    } 
}
