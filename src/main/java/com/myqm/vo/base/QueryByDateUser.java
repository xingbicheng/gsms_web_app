package com.myqm.vo.base;
 
import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;

public class QueryByDateUser extends BaseQueryParam {
	/**
	 * 建立用户id
	 */
	@ApiModelProperty(value = "建立用户id")
	protected String updaterId;

	/**
	 * 修改用户id
	 */
	@ApiModelProperty(value = "修改用户id")
	protected String createrId;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value = "查询修改开始时间") 
	protected Date updateTimeStart;

	/**
	 * 建单时间
	 */ 
	@ApiModelProperty(value = "查询建单开始时间")
	protected Date createTimeStart;

	/**
	 * 修改时间
	 */ 
	@ApiModelProperty(value = "查询修改结束时间")
	protected Date updateTimeEnd;

	/**
	 * 建单时间
	 */
	@ApiModelProperty(value = "查询建单结束时间") 
	protected Date createTimeEnd;

	public String getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreateUserId(String createrId) {
		this.createrId = createrId;
	}

	public Date getUpdateTimeStart() {
		return updateTimeStart;
	}

	public void setUpdateTimeStart(Date updateTimeStart) {
		this.updateTimeStart = updateTimeStart;
	}

	public Date getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(Date createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public Date getChangeTimeEnd() {
		return updateTimeEnd;
	}

	public void setChangeTimeEnd(Date changeTimeEnd) {
		this.updateTimeEnd = changeTimeEnd;
	}

	public Date getCreateTimeEnd() {
		return createTimeEnd;
	}

	 

	public Date getUpdateTimeEnd() {
		return updateTimeEnd;
	}

	public void setUpdateTimeEnd(Date updateTimeEnd) {
		this.updateTimeEnd = updateTimeEnd;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

	public void setCreateTimeEnd(Date createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}

	 
}
