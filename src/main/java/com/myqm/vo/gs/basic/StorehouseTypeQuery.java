package com.myqm.vo.gs.basic;

import javax.validation.constraints.Size;

import com.myqm.vo.base.BaseQueryParam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

public class StorehouseTypeQuery extends BaseQueryParam {
	@ApiModelProperty(value = "内部商品分类(字符长度为1-20)")
	private String storeType;

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

}