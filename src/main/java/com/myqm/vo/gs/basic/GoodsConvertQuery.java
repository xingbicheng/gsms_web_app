package com.myqm.vo.gs.basic;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.myqm.vo.base.BaseQueryParam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

public class GoodsConvertQuery extends BaseQueryParam {


	@ApiModelProperty(value = "原始产品名称(字符长度为1-200)")
	@Size(min = 1, max = 200, message = "原始产品名称字符长度为1-200")
	private String originalName;

	@ApiModelProperty(value = "单位编号(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "单位编号字符长度为1-32")
	private String customerTypeId;
	
	
	@ApiModelProperty(value = "产品名称(字符长度为1-200)")
	@Size(min = 1, max = 200, message = "产品名称字符长度为1-200")
	private String goodsName;
	

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

}