package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class CustomerTypeQuery extends BaseQueryParam {
	@ApiModelProperty(value = "客户类型(字符长度为1-20)")
	private String customerType;

    @ApiModelProperty(value = "客户类型id(字符长度为1-20)")
    private String customerTypeId;

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

}