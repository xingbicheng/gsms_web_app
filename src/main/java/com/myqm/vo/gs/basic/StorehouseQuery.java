package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class StorehouseQuery extends BaseQueryParam {
	@ApiModelProperty(value = "库房名称(字符长度为1-255)")
	private String storeName;

	@ApiModelProperty(value = "库房类型id")
	private String storeTypeId;

	@ApiModelProperty(value = "1中央库，0非中央库")
	private String isCenter;

    public String getIsCenter() {
        return isCenter;
    }

    public void setIsCenter(String isCenter) {
        this.isCenter = isCenter;
    }

    public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreTypeId() {
		return storeTypeId;
	}

	public void setStoreTypeId(String storeTypeId) {
		this.storeTypeId = storeTypeId;
	}


}