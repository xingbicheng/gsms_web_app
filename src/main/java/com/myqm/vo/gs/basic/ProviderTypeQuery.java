package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class ProviderTypeQuery extends BaseQueryParam {
	@ApiModelProperty(value = "供货商类型名称(字符长度为1-20)")
	private String providerType;

    @ApiModelProperty(value = "供货商类型id(字符长度为1-20)")
    private String providerTypeId;

    public String getProviderTypeId() {
        return providerTypeId;
    }

    public void setProviderTypeId(String providerTypeId) {
        this.providerTypeId = providerTypeId;
    }

    public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

}