package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class GoodsQuery extends BaseQueryParam {
    @ApiModelProperty(value = "内部产品名称(字符长度为1-50)")
    private String goodsName;

    @ApiModelProperty(value = "计量单位(字符长度为1-10)")
    private String unitName;

    @ApiModelProperty(value = "0为内部 1为外部(字符长度为1-1)")
    private String internal;

    @ApiModelProperty(value = "备用名(字符长度为1-50)")
    private String alternateName;

    @ApiModelProperty(value = "修改人(字符长度为1-20)")
    private String updaterName;

    @ApiModelProperty(value = "商品类型id")
    private String goodsTypeId;

    @ApiModelProperty(value = "默认供货商id")
    private String providerId;
    
    public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
    public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public String getUpdaterName() {
        return updaterName;
    }

    public void setUpdaterName(String updaterName) {
        this.updaterName = updaterName;
    }
    //

    public String getGoodsTypeId() {
        return goodsTypeId;
    }

    public void setGoodsTypeId(String goodsTypeId) {
        this.goodsTypeId = goodsTypeId;
    }
}