package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class ProviderQuery extends BaseQueryParam {
	@ApiModelProperty(value = "供应商类型编号(字符长度为1-32)")
	private String providerTypeId;

	@ApiModelProperty(value = "供应商名称")
	private String providerName;

	public String getProviderTypeId() {
		return providerTypeId;
	}

	public void setProviderTypeId(String providerTypeId) {
		this.providerTypeId = providerTypeId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

}