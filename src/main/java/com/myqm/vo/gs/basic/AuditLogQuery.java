package com.myqm.vo.gs.basic;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Timestamp;


public class AuditLogQuery extends BaseQueryParam{
    @ApiModelProperty(value = "操作人的用户id(字符长度为1-32)")
    private String userName;

    @ApiModelProperty(value = "记录类型 0订单审核，1成本审核(字符长度为1-1)")
    private String type;

    @ApiModelProperty("审核记录开始时间")
    private Date startTime;


    @ApiModelProperty("审核记录截止时间")
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}