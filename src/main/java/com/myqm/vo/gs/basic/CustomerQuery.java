package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class CustomerQuery extends BaseQueryParam {
	@ApiModelProperty(value = "订货单位名称(字符长度为1-40)")
	private String customerName;

	@ApiModelProperty(value = "订货单位类型id")
	private String customerTypeId;
	//

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
}