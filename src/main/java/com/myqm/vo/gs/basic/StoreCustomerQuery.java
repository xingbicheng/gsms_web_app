package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class StoreCustomerQuery extends BaseQueryParam{
	@ApiModelProperty(value = "仓库编号id") 
	private String storeId;
	
	@ApiModelProperty(value = "订货单位类型id") 
	private String customerTypeId;
	
	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	} 
	
	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}



}
