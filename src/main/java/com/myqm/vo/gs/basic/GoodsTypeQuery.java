package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class GoodsTypeQuery extends BaseQueryParam {
	@ApiModelProperty(value = "内部商品分类(字符长度为1-20)")
	private String goodsType;
	private String memo;
	private String  updaterName;
	private String code;
	
	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

}