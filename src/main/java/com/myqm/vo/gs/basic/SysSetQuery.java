package com.myqm.vo.gs.basic;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class SysSetQuery extends BaseQueryParam{
	@ApiModelProperty(value = "id(字符长度为1-32)")
    private String id;
	
	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
