package com.myqm.vo.gs.in;
import com.myqm.vo.base.BaseQueryParam;


public class StoreLossTypeQuery extends BaseQueryParam{
	private String lossType;
	private String memo;
	private String  updaterName;
	private String code;
	public String getLossType() {
		return lossType;
	}
	public void setLossType(String lossType) {
		this.lossType = lossType;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getUpdaterName() {
		return updaterName;
	}
	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}