package com.myqm.vo.gs.in;
import java.sql.Date;
import java.sql.Timestamp;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class StoreLossQuery extends BaseQueryParam{
	
	@ApiModelProperty(value = "入库单编号(字符长度为1-32)")
	private String inId;
	
	@ApiModelProperty(value = "内部商品名id(字符长度为1-32)")
	private String goodsId;
	
	@ApiModelProperty(value = "内部商品名")
	private String goodsName;

	@ApiModelProperty(value = "损耗数量")
	private Double amount;

	@ApiModelProperty(value = "损耗时间开始")
	private Date lossTimeBegin;
	
	@ApiModelProperty(value = "损耗时间结束")
	private Date lossTimeEnd;

	@ApiModelProperty(value = "供货商id(字符长度为1-32)")
	private String providerId;

	@ApiModelProperty(value = "库房id(字符长度为1-32)")
	private String storehouseId;
	
	@ApiModelProperty(value = "0冲正1冲负(字符长度为1)")
	private String type;
	
	@ApiModelProperty(value = "损耗类型(字符长度为1)")
	private String lossType;

	public String getInId() {
		return inId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public void setInId(String inId) {
		this.inId = inId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	 

	public Date getLossTimeBegin() {
		return lossTimeBegin;
	}

	public void setLossTimeBegin(Date lossTimeBegin) {
		this.lossTimeBegin = lossTimeBegin;
	}

	public Date getLossTimeEnd() {
		return lossTimeEnd;
	}

	public void setLossTimeEnd(Date lossTimeEnd) {
		this.lossTimeEnd = lossTimeEnd;
	}

	public String getLossType() {
		return lossType;
	}

	public void setLossType(String lossType) {
		this.lossType = lossType;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getStorehouseId() {
		return storehouseId;
	}

	public void setStorehouseId(String storehouseId) {
		this.storehouseId = storehouseId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLoosType() {
		return lossType;
	}

	public void setLoosType(String lossType) {
		this.lossType = lossType;
	}
	
	
}