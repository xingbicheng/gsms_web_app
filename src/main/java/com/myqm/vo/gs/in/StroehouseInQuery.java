package com.myqm.vo.gs.in;


import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

import java.sql.Date;

public class StroehouseInQuery extends BaseQueryParam {
 
	@ApiModelProperty(value = "采购单明细ID")
	private String purchaseDetailId;
	@ApiModelProperty(value = "供应商id")
	private String providerId;
	@ApiModelProperty(value = "仓库id")
	private String storehouseId;
	@ApiModelProperty(value = "采购类型")
	private String purchaseType; 
	@ApiModelProperty(value = "入库时间")
	private Date inTime;
	@ApiModelProperty(value = "商品名")
	private String goodsName;
	@ApiModelProperty(value = "采购商名字")
	private String providerName;
	@ApiModelProperty(value = "仓库名称")
	private String storeName;
	@ApiModelProperty(value = "采购开始时间")
	private Date purchaseDateStart;
	@ApiModelProperty(value = "采购结束时间")
	private Date purchaseDateEnd;
	@ApiModelProperty(value = "中央库否")
	private boolean center = false;
	@ApiModelProperty(value = "库存为0否")
	private boolean zero = false;
	@ApiModelProperty(value = "状态")
	private String state; 
	@ApiModelProperty(value = "入库开始时间")
	private Date inTimeBegin;
	@ApiModelProperty(value = "入库结束时间")
	private Date inTimeEnd;

	@ApiModelProperty(value = "采购时间")
	private Date purchaseDate;
	 @ApiModelProperty(value = "是否可以退货的查询条件 0 可以退货，1可以退货")
	 private String canBack;
 
	 public String getCanBack() {
			return canBack;
		}

		public void setCanBack(String canBack) {
			this.canBack = canBack;
		}

	 

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public boolean isCenter() {
		return center;
	}

	public void setCenter(boolean center) {
		this.center = center;
	}

	public boolean isZero() {
		return zero;
	}

	public void setZero(boolean zero) {
		this.zero = zero;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

    public Date getPurchaseDateStart() {
        return purchaseDateStart;
    }

    public void setPurchaseDateStart(Date purchaseDateStart) {
        this.purchaseDateStart = purchaseDateStart;
    }

    public Date getPurchaseDateEnd() {
        return purchaseDateEnd;
    }

    public void setPurchaseDateEnd(Date purchaseDateEnd) {
        this.purchaseDateEnd = purchaseDateEnd;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getStorehouseId() {
		return storehouseId;
	}

	public void setStorehouseId(String storehouseId) {
		this.storehouseId = storehouseId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	private String goodsId;
	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}


	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Date getInTimeBegin() {
		return inTimeBegin;
	}

	public void setInTimeBegin(Date inTimeBegin) {
		this.inTimeBegin = inTimeBegin;
	}

	public Date getInTimeEnd() {
		return inTimeEnd;
	}

	public void setInTimeEnd(Date inTimeEnd) {
		this.inTimeEnd = inTimeEnd;
	}
	
	

}