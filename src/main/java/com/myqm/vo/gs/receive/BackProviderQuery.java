package com.myqm.vo.gs.receive;
import java.sql.Timestamp;

import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;

import com.myqm.dao.gs.basic.ProviderDao;
import com.myqm.utils.UserUtil;
import com.myqm.vo.base.BaseQueryParam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;


public class BackProviderQuery extends BaseQueryParam{
	
	@Autowired
	ProviderDao providerDao;
	
	@ApiModelProperty(value = "供货商名字(字符长度为1-32)")
	private String providerName;
	
	@ApiModelProperty(value = "货物名字(字符长度为1-32)")
	private String goodsName;
	
	@ApiModelProperty(value = "库房名字(字符长度为1-32)")
	private String  storeName;
	
	@ApiModelProperty(value = "入库单编号(字符长度为1-32)")
	@Size(min=1, max=32,message="入库单编号字符长度为1-32")
	@Excel(name="入库单编号",orderNum="1")
	private String inId;
	
	public String getInId() {
		return inId;
	}

	public void setInId(String inId) {
		this.inId = inId;
	}

	@ApiModelProperty(value = "退回时间开始")
	private Timestamp backTimeStart;
	@ApiModelProperty(value = "退回时间结束")
	private Timestamp backTimeEnd;
	
	public Timestamp getBackTimeStart() {
		return backTimeStart;
	}

	public void setBackTimeStart(Timestamp backTimeStart) {
		this.backTimeStart = backTimeStart;
	}

	public Timestamp getBackTimeEnd() {
		return backTimeEnd;
	}

	public void setBackTimeEnd(Timestamp backTimeEnd) {
		this.backTimeEnd = backTimeEnd;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	
	
}