package com.myqm.vo.gs.purchase;

import java.sql.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty; 

public class PerPurchaseQuery {

	@ApiModelProperty(value = "采购前统计结束时间")
	@NotNull
	Date perEnd;

	@ApiModelProperty(value = "用户类型组编号")
	@NotNull
	List<String> customerTypeIds;

	@ApiModelProperty(value = "采购前统计开始时间")
	@NotNull
	Date perStart;

	public List<String> getCustomerTypeIds() {
		return customerTypeIds;
	}

	public void setCustomerTypeIds(List<String> customerTypeIds) {
		this.customerTypeIds = customerTypeIds;
	}

	public Date getPerStart() {
		return perStart;
	}

	public void setPerStart(Date perStart) {
		this.perStart = perStart;
	}

	public Date getPerEnd() {
		return perEnd;
	}

	public void setPerEnd(Date perEnd) {
		this.perEnd = perEnd;
	}

}