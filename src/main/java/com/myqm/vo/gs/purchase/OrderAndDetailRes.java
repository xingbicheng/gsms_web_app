package com.myqm.vo.gs.purchase;

import java.math.BigDecimal;
import java.sql.Date;

public class OrderAndDetailRes {

	private String orderId;
	private String customerId;
	private Date distributionDate;
	private BigDecimal lumpSum;
	private String detailId;
	private String goodsOriginalName;
	private BigDecimal unitPrice;
	private Double amount;
	private String unitName;
	private BigDecimal sum;
	private Double selfAmount;
	private Double freeAmount;
	private BigDecimal selfSum;
	private BigDecimal freeSum;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getDistributionDate() {
		return distributionDate;
	}

	public void setDistributionDate(Date distributionDate) {
		this.distributionDate = distributionDate;
	}

	public BigDecimal getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(BigDecimal lumpSum) {
		this.lumpSum = lumpSum;
	}

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getGoodsOriginalName() {
		return goodsOriginalName;
	}

	public void setGoodsOriginalName(String goodsOriginalName) {
		this.goodsOriginalName = goodsOriginalName;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Double getSelfAmount() {
		return selfAmount;
	}

	public void setSelfAmount(Double selfAmount) {
		this.selfAmount = selfAmount;
	}

	public Double getFreeAmount() {
		return freeAmount;
	}

	public void setFreeAmount(Double freeAmount) {
		this.freeAmount = freeAmount;
	}

	public BigDecimal getSelfSum() {
		return selfSum;
	}

	public void setSelfSum(BigDecimal selfSum) {
		this.selfSum = selfSum;
	}

	public BigDecimal getFreeSum() {
		return freeSum;
	}

	public void setFreeSum(BigDecimal freeSum) {
		this.freeSum = freeSum;
	}

	@Override
	public String toString() {
		return "OrderAndDetailRes{" + "orderId='" + orderId + '\'' + ", customerId='" + customerId + '\''
				+ ", distributionDate=" + distributionDate + ", lumpSum=" + lumpSum + ", detailId='" + detailId + '\''
				+ ", goodsOriginalName='" + goodsOriginalName + '\'' + ", unitPrice=" + unitPrice + ", amount=" + amount
				+ ", unitName='" + unitName + '\'' + ", sum=" + sum + ", selfAmount=" + selfAmount + ", freeAmount="
				+ freeAmount + ", selfSum=" + selfSum + ", freeSum=" + freeSum + '}';
	}
}
