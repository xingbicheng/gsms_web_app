package com.myqm.vo.gs.purchase;

import javax.validation.constraints.Size;

import com.myqm.vo.base.BaseQueryParam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;

public class PurchasePriceQuery extends BaseQueryParam{
	
	@ApiModelProperty(value = "供应商id") 
	private String providerId;
	
	@ApiModelProperty(value = "询问货物的id") 
	private String goodsId;
	
	 
	
	@ApiModelProperty(value = "采购单明细(字符长度为1-32)")
	@Size(min = 1, max = 32, message = "采购单明细字符长度为1-32")
	@Excel(name = "采购单明细", orderNum = "3")
	private String purchaseDetailId;
	
	
	
	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	} 
	
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
}
