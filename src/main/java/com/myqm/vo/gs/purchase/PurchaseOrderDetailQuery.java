package com.myqm.vo.gs.purchase;

import java.sql.Date;
import io.swagger.annotations.ApiModelProperty;  
import com.myqm.vo.base.BaseQueryParam;

public class PurchaseOrderDetailQuery extends BaseQueryParam {

	@ApiModelProperty(value = "(字符长度为1-20)")
	private String orderId;

	@ApiModelProperty(value = "采购时间开始") 
	private Date purchaseDateStart;
	@ApiModelProperty(value = "采购时间结束") 
	private Date purchaseDateEnd;
	@ApiModelProperty(value = "商品类型id")
	private String goodsTypeId;
	@ApiModelProperty(value = "采购单明细状态 状态（d 删除 0下单、1审核 采购中，2入库中，3入库完成）")
	private String state;

	@ApiModelProperty(value = "商品id")
	private String goodsId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getpurchaseDateStart() {
		return purchaseDateStart;
	}

	public void setpurchaseDateStart(Date purchaseDateStart) {
		this.purchaseDateStart = purchaseDateStart;
	}

	public Date getPurchaseDateEnd() {
		return purchaseDateEnd;
	}

	public void setPurchaseDateEnd(Date purchaseDateEnd) {
		this.purchaseDateEnd = purchaseDateEnd;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}