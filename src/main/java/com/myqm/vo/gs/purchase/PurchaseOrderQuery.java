package com.myqm.vo.gs.purchase;
 
import java.sql.Date;  
import com.myqm.vo.base.QueryByDateUser; 
import io.swagger.annotations.ApiModelProperty; 

public class PurchaseOrderQuery extends QueryByDateUser {

	@ApiModelProperty(value = "(字符长度为1-20)")
	private String orderNo;

	@ApiModelProperty(value = "采购时间开始") 
	private Date purchaseDateStart;
	@ApiModelProperty(value = "采购时间结束") 
	private Date purchaseDateEnd;
	
	@ApiModelProperty(value = "采购单创建时间") 
	private Date createTime;
	
	@ApiModelProperty(value = "d 删除 0下单 1审核中 2审核通过，3采购中-入库中 4入库完成(字符长度为1-1)")
	private String state;

	@ApiModelProperty(value = "商品类型")
	private String goodsType;
	
	@ApiModelProperty(value = "采购单类型")
	private String purchaseType;
	 
    public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}



    public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getpurchaseDateStart() {
		return purchaseDateStart;
	}

	public void setpurchaseDateStart(Date purchaseDateStart) {
		this.purchaseDateStart = purchaseDateStart;
	}

	public Date getPurchaseDateEnd() {
		return purchaseDateEnd;
	}

	public void setPurchaseDateEnd(Date purchaseDateEnd) {
		this.purchaseDateEnd = purchaseDateEnd;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}