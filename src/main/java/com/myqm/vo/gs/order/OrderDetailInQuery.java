package com.myqm.vo.gs.order;
import com.myqm.vo.base.BaseQueryParam;


public class OrderDetailInQuery extends BaseQueryParam{
	private String detailId;
	private String inId;

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getInId() {
        return inId;
    }

    public void setInId(String inId) {
        this.inId = inId;
    }
}