package com.myqm.vo.gs.order;

import io.swagger.annotations.ApiModelProperty;

public class QueryAudit {
	@ApiModelProperty(value = "订单编号 ")
	private String id;
	
	@ApiModelProperty(value = "订单成功 ")
	private boolean audited;
	
	@ApiModelProperty(value = "订单意见 ")
	private String auditMemo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isAudited() {
		return audited;
	}

	public void setAudited(boolean audited) {
		this.audited = audited;
	}

	public String getAuditMemo() {
		return auditMemo;
	}

	public void setAuditMemo(String auditMemo) {
		this.auditMemo = auditMemo;
	}
	
}
