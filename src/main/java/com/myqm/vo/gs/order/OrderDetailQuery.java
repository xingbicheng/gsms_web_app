package com.myqm.vo.gs.order;

import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

public class OrderDetailQuery extends BaseQueryParam {
	@ApiModelProperty(value = "订单编号(字符长度为1-32)")
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}