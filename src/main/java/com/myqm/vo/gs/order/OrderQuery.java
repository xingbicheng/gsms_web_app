package com.myqm.vo.gs.order;

import java.sql.Date; 
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.myqm.vo.base.QueryByDateUser; 
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty; 

public class OrderQuery extends QueryByDateUser {
	@ApiModelProperty(value = "订单编号 ")
	private String orderNo;

	@ApiModelProperty(value = "订货单位类型id ")
	private String customerTypeId;

	@ApiModelProperty(value = "订货单位名称")
	@Excel(name = "订货单位名称", orderNum = "2")
	private String customerName;

	@ApiModelProperty(value = "订货单位id ")
	private String customerId;

	@ApiModelProperty(value = "数据异常 0 正常 1订单明细异常 2 订单重复异常(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "数据异常 0 正常 1订单明细异常 2 订单重复异常字符长度为1-1")
	private String flag;

	@ApiModelProperty(value = "状态（d删除 1下单，2审核通过，3预采购(不使用) 4采购(不使用)  5入库(不使用) 6出库 7收货8部分收货）(字符长度为1-1)")
	@Size(min = 1, max = 1, message = "状态（d删除 1下单，2审核通过，3预采购 4采购  5入库 6出库 7收货8部分收货）字符长度为1-1")
	private String state =null;

	@ApiModelProperty(value = "真实配送开始时间") 
	private Date realDistributionDateStart;

	@ApiModelProperty(value = "真实配送结束时间") 
	private Date realDistributionDateEnd;

	@ApiModelProperty(value = "0不通过， 1 通过")
	private Long verified;
	
	@JsonIgnore 
	@ApiModelProperty(value = "0出库查询， 1 结算查询") 
	private String sysState;
	
	@ApiModelProperty(value = "配送开始时间")
	private Date distributionDateStart;


	@ApiModelProperty(value = "配送结束时间")
	private Date distributionDateEnd;
	
	@ApiModelProperty(value = "建单开始时间")
	private Date createTimeStart;


	@ApiModelProperty(value = "建单结束时间")
	private Date createTimeEnd;
	
	public Date getDistributionDateStart() {
		return distributionDateStart;
	}

	public void setDistributionDateStart(Date distributionDateStart) {
		this.distributionDateStart = distributionDateStart;
	}

    @Override
    public Date getCreateTimeStart() {
        return createTimeStart;
    }

    @Override
    public void setCreateTimeStart(Date createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    @Override
    public Date getCreateTimeEnd() {
        return createTimeEnd;
    }

    @Override
    public void setCreateTimeEnd(Date createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public Date getDistributionDateEnd() {
		return distributionDateEnd;
	}

	public void setDistributionDateEnd(Date distributionDateEnd) {
		this.distributionDateEnd = distributionDateEnd;
	}
	
	
	public interface SysState{
		String out = "0";
		String receive = "1";
	}
	
	
	public String getSysState() {
		return sysState;
	}

	public void setSysState(String sysState) {
		this.sysState = sysState;
	}

	public Long getVerified() {
		return verified;
	}

	public void setVerified(Long verified) {
		this.verified = verified;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getRealDistributionDateStart() {
		return realDistributionDateStart;
	}

	public void setRealDistributionDateStart(Date realDistributionDateStart) {
		this.realDistributionDateStart = realDistributionDateStart;
	}

	public Date getRealDistributionDateEnd() {
		return realDistributionDateEnd;
	}

	public void setRealDistributionDateEnd(Date realDistributionDateEnd) {
		this.realDistributionDateEnd = realDistributionDateEnd;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


}