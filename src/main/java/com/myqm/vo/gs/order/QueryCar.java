package com.myqm.vo.gs.order;



import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class QueryCar extends BaseQueryParam{

	@ApiModelProperty(value = "客户Id")
	private String customerId;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	

	

	
}
