package com.myqm.vo.xxb.tableConfig;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;


public class TableConfigQuery extends BaseQueryParam{
    @ApiModelProperty(value = "(字符长度为1-20)")
    @Size(min=1, max=20,message="字符长度为1-20")
    @Excel(name="",orderNum="1")
    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}