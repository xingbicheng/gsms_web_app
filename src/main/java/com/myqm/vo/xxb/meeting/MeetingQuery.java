package com.myqm.vo.xxb.meeting;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.myqm.vo.base.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Size;


public class MeetingQuery extends BaseQueryParam{
    @ApiModelProperty(value = "(字符长度为1-20)")
    @Size(min=1, max=20,message="字符长度为1-20")
    @Excel(name="",orderNum="1")
    private String username;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "年龄")
    private Long age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @ApiModelProperty(value = "(字符长度为1-10)")
    @Size(min=1, max=10,message="字符长度为1-10")
    @Excel(name="",orderNum="3")
    private String gender;
}