package com.myqm.vo.sysfun;

import java.sql.Timestamp;

import com.myqm.vo.base.BaseQueryParam;

import io.swagger.annotations.ApiModelProperty;

public class QueryFileInfo extends BaseQueryParam {

	@ApiModelProperty(value = "文件原文件名")
	private String fileName;
	@ApiModelProperty(value = "上传人")
	private String createrId;
	@ApiModelProperty(value = "文件类型后缀名")
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 建单时间
	 */
	@ApiModelProperty(value = "查询建单开始时间")
	protected Timestamp createTimeStart;

	@ApiModelProperty(value = "查询建单开始时间")
	protected Timestamp createTimeEnd;

	public Timestamp getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(Timestamp createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}

	public Timestamp getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(Timestamp createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}
}
