package com.myqm.vo.sysfun;
 

import java.sql.Date;


import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

public class QuerySysUser extends QueryByDateUser {

	@ApiModelProperty(value = "登陆用户")
	private String username;

 

	@ApiModelProperty(value = "昵称")
	private String nickname;

	@ApiModelProperty(value = "电话")
	private String phone;
	@ApiModelProperty(value = "电话")
	private String telephone;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@ApiModelProperty(value = "用户生日")
	private Date birthday;

	@ApiModelProperty(value = "性别")
	@Pattern(regexp = "[FM]")
	@Size(min = 1, max = 1)
	private String sex;

	@ApiModelProperty(value = "状态:不可用 0;可用 1;锁定2;")
	private Long status;
	
	@ApiModelProperty(value = "部门id") 
	private String departmentId;




    public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	 

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
