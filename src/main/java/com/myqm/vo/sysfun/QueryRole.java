package com.myqm.vo.sysfun;

import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

public class QueryRole extends QueryByDateUser {

	@ApiModelProperty(value = "角色名字")
	private String name;

    @ApiModelProperty(value = "是否可用 1可用，0不可以")
    private String activation;


    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
