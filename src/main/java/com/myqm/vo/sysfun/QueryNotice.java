package com.myqm.vo.sysfun;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

public class QueryNotice extends QueryByDateUser {

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "状态")
	@Max(1)
	private Long status;

	@ApiModelProperty(value = "私信否")
	@Max(1)
	private String isPrivated;

	@ApiModelProperty(value = "用户Id")
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getIsPrivated() {
		return isPrivated;
	}

	public void setIsPrivated(String isPrivated) {
		this.isPrivated = isPrivated;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
