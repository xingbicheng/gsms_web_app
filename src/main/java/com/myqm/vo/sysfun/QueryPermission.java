package com.myqm.vo.sysfun;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

public class QueryPermission extends QueryByDateUser {

	@ApiModelProperty(value = "类型")
	private Integer type;

	@ApiModelProperty(value = "名称")
	@Size(min = 1, max = 50)
	private String name;
	
	@ApiModelProperty(value = "上级菜单id") 
	private String parentId;
	 

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
