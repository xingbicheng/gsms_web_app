package com.myqm.vo.sysfun;

import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

public class QueryDict extends QueryByDateUser {

	@ApiModelProperty(value = "字典类型名")
	private String type;
	@ApiModelProperty(value = "字典键值")
	private String k;
	@ApiModelProperty(value = "字典value")
	private String val;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = k;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

}
