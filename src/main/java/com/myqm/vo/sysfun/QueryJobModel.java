package com.myqm.vo.sysfun;

import javax.validation.constraints.Size;

import com.myqm.vo.base.QueryByDateUser;

import io.swagger.annotations.ApiModelProperty;

//无查询相关类
public class QueryJobModel extends QueryByDateUser {

	@ApiModelProperty(value = "任务名")
	@Size(min = 1, max = 16)
	private String jobName;

	@ApiModelProperty(value = "是否为系统Job")
	private Boolean isSysJob;

	@ApiModelProperty(value = "状态")
	private Long status;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public Boolean getIsSysJob() {
		return isSysJob;
	}

	public void setIsSysJob(Boolean isSysJob) {
		this.isSysJob = isSysJob;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
