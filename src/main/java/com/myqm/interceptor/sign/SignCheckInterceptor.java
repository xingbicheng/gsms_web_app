package com.myqm.interceptor.sign;
 
import java.sql.Timestamp;
import java.util.Date;
import java.util.TreeMap; 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.StringUtil;
import com.myqm.config.data.security.sign.SignCheckCfg;
import com.myqm.utils.StrUtil;
import com.myqm.utils.security.sign.SignUtil;

/**
 * 签名过滤器
 */
@Component
public class SignCheckInterceptor implements HandlerInterceptor {
	private static final String SIGN_KEY = "sign";
	private static final String TIMESTAMP_KEY = "timestamp";

	@Autowired
	private SignCheckCfg signCfg;

	/**
	 * 根据参数或者header获取token
	 * 
	 * @param request
	 * @return
	 */
	public static String getSign(HttpServletRequest request) {
		String sign = request.getHeader(SIGN_KEY);
		// if (StringUtils.isBlank(sign)) {
		// sign = request.getParameter(SIGN_KEY);
		// }
		return sign;
	}

	public static String getTimestamp(HttpServletRequest request) {
		String sign = request.getHeader(TIMESTAMP_KEY);
		// if (StringUtils.isBlank(sign)) {
		// sign = request.getParameter(SIGN_KEY);
		// }
		return sign;
	}

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse arg1, Object arg2) throws Exception {

		String sign = SignCheckInterceptor.getSign(request);
		String timestr = SignCheckInterceptor.getTimestamp(request);
		// 检查携带信息
		if (sign == null || StringUtil.isEmpty(sign)) {
			throw new ServletException("Request data does not carry effective data signatures");
		}
		// 检查携带时间戳
		if (timestr == null || StringUtil.isEmpty(timestr)) {
		}
		Timestamp ts1 = Timestamp.valueOf(timestr);
		Timestamp ts2 = new Timestamp((new Date()).getTime());
		if ((ts2.getTime() - ts1.getTime() > signCfg.getCheckTime() * 1000) || (ts2.getTime() - ts1.getTime() < 0)) {
			throw new ServletException("Request timestamp is error.");
		}

		String method = request.getMethod();
		String apiSecret = signCfg.getSecret();
		String calc_sign = "";
		switch (method) {
		case "GET":
			// 第一步：取出所有get参数
			TreeMap<String, Object> params = SignUtil.getMultipartParameter(request);
			calc_sign = SignUtil.sign((TreeMap<String, Object>) params, apiSecret, timestr);
			break;
		case "PUT":
		case "DELETE":
		case "POST":
			String data = StrUtil.getStrFromInputSteam(request.getInputStream());
			calc_sign = SignUtil.sign(data, apiSecret, timestr);
			break;
		default:
			throw new ServletException("Request carry error signatures");
		}
		if (!calc_sign.equals(sign)) {
			throw new ServletException("Request data signature verification error");
		}

		return true;
	}

}
