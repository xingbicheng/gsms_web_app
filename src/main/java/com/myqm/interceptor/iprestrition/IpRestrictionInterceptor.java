package com.myqm.interceptor.iprestrition;

import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.myqm.pojo.security.gate.IpRestriction;
import com.myqm.service.security.gate.IpRestrictionService;
import com.myqm.utils.IPAddressUtil;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class IpRestrictionInterceptor implements HandlerInterceptor {

	@Autowired(required = true)
	IpRestrictionService ipService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String ip = IPAddressUtil.getClientIpAddress(request);
		IpRestriction bwIp = ipService.findByIp(ip);
		if (bwIp != null) {
			ipService.checkIpRestriction(bwIp);
		} else {
			bwIp = new IpRestriction(ip, new Timestamp(System.currentTimeMillis()), false);
			ipService.addIpRestrictionInfo(bwIp);
		}
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}
}
